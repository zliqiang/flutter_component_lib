import 'package:flutter/material.dart';
import 'package:flutter_web_2021/base/BasePageP.dart';
import 'package:flutter_web_2021/utils/app_navigator.dart';
import 'package:flutter_web_2021/utils/toast/OkToastUtil.dart';
import 'package:flutter_web_2021/widgets/dialog/dio/base_dialog.dart';
import 'package:flutter_web_2021/widgets/dialog/dio/loading_dialog.dart';
import 'package:flutter_web_2021/widgets/dialog/dio/progress_dialog.dart';
import '../Include.dart';
import '../utils/dio/utils.dart';
import '../utils/dio/mvp/contract.dart';
import 'BasePermissionPageState.dart';

///回调函数定义
typedef ParamCallback = void Function(String data);

abstract class BaseMvpPageState<T extends BasePage, V extends BasePageP> extends BasePermissionPageState<T> implements IMvpView {

  V presenter;

  BaseMvpPageState() {
    presenter = createPresenter();
    presenter.view = this;
  }

  V createPresenter();

  @override
  void closeProgress() {
    if (mounted && _isShowDialog){
      _isShowDialog = false;
      AppNavigator.pop(context);
    }
  }

  bool _isShowDialog = false;

  void showDialogBox(String message,{VoidCallback cancelCallback,ParamCallback callback,String data,String title}){
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return BaseDialog(
            title: title??"",
            hiddenTitle: (null==title||title.isEmpty),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
              child: Text(message, style: TextStyles.textDark16),
            ),
            onCancel: (){
              if(null!=cancelCallback){
                cancelCallback();
              }
            },
            onPressed: (){
              AppNavigator.pop(context);
              if(null!=callback) {
                callback(data);
              }
            },
          );
        });
  }


  void showWidgetDialog(Widget widget,{VoidCallback cancelCallback,ParamCallback callback,String data,String title}){
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return BaseDialog(
            title: title??"",
            hiddenTitle: (null==title||title.isEmpty),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
              child: widget,
            ),
            onCancel: (){
              if(null!=cancelCallback){
                cancelCallback();
              }
            },
            onPressed: (){
              AppNavigator.pop(context);
              if(null!=callback) {
                callback(data);
              }
            },
          );
        });
  }

  void showSendProgress({double count:0,double total:1}){
    /// 避免重复弹出
    if (mounted && !_isShowDialog){
      _isShowDialog = true;
      try{
        showTransparentDialog(
            context: context,
            barrierDismissible: false,
            builder:(_) {
              return WillPopScope(
                onWillPop: () async {
                  // 拦截到返回键，证明dialog被手动关闭
                  _isShowDialog = false;
                  return Future.value(true);
                },
//                child: ProgressDialog(content: "正在加载..."),
              child: DioProgressDialog(view: Column(
                children: <Widget>[
                  LinearProgressIndicator(
                    value: count/total,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("$count/$total"),
                      Text("${count/total*100}%"),
                    ],
                  ),
                ],
              )),
              );
            }
        );
      }catch(e){
        /// 异常原因主要是页面没有build完成就调用Progress。
        print(e);
      }
    }
  }

  @override
  void showProgress({String content:"正在加载..."}) {
    /// 避免重复弹出
    if (mounted && !_isShowDialog){
      _isShowDialog = true;
      try{
        showTransparentDialog(
            context: context,
            barrierDismissible: false,
            builder:(_) {
              return WillPopScope(
                onWillPop: () async {
                  // 拦截到返回键，证明dialog被手动关闭
                  _isShowDialog = false;
                  return Future.value(true);
                },
                child: LoadingDialog(content: content),
              );
            }
        );
      }catch(e){
        /// 异常原因主要是页面没有build完成就调用Progress。
        print(e);
      }
    }
  }

  @override
  void showToast(String string) {
    ToastUtil.showToastBottom(string);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    presenter?.didChangeDependencies();
  }



  @override
  void deactivate() {
    super.deactivate();
    presenter?.deactivate();
  }

  @override
  void didUpdateWidget(T oldWidget) {
    super.didUpdateWidget(oldWidget);
    didUpdateWidgets<T>(oldWidget);
  }

  @override
  void initState() {
    print('[BasePageState]: 记录用户打开 ${widget.pageName} 页面');
    super.initState();
    presenter?.initState();
  }
  @override
  void dispose() {
    print('[BasePageState]: 记录用户离开 ${widget.pageName} 页面');
    super.dispose();
    presenter?.dispose();
  }
  void didUpdateWidgets<W>(W oldWidget) {
    presenter?.didUpdateWidgets<W>(oldWidget);
  }


  /// 封装的 pop。
  void pop<T extends Object>([T result]) {
    return Navigator.pop<T>(context, result);
  }

  getDartPath(frame){
    // 打印
    Log().info("所在文件：${frame.uri} 所在行 ${frame.line} 所在列 ${frame.column}");

    List<String> newString = frame.uri.toString().split("packages/");
    List<String> strs = frame.uri.toString().split("/");
    List<String> pagename = strs[strs.length-1].toString().split(".");
    print("pagename[0]="+pagename[0].toString());
    print("newString[1].replaceAll(\"packages\", "")="+newString[1].replaceAll("packages/", "").toString());

    // Log().info(newString[1].replaceAll("packages/", "").toString());
    // 打印结果
    // flutter: 所在文件：package:flutterlog/main.dart 所在行 55 所在列 23
    context.read<Counter>().put({pagename[0].toString():(newString[1].replaceAll("packages/", "")).toString()});
  }
}