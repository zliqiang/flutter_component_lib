
import 'package:flutter_web_2021/utils/dio/mvp/contract.dart';

abstract class BasePageP<V extends IMvpView> extends IPresenter {
  V view;
  int futureStatus =-999;
  Future<int> future() async{
    return Future.delayed(Duration(seconds: 2), () => futureStatus);
  }

  @override
  void deactivate() {}

  @override
  void didChangeDependencies() {}

  @override
  void didUpdateWidgets<W>(W oldWidget) {}

  @override
  void initState() {
  }

  @override
  void dispose() {
  }



}
