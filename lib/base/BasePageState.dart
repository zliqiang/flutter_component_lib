import 'package:flutter/material.dart';
import '../Include.dart';
import 'BasePage.dart';
import 'BasePermissionPageState.dart';

/// 持有 BasePage 实例的 State
abstract class BasePageState<T extends BasePage> extends BasePermissionPageState<T> {

  @mustCallSuper
  @override
  void initState() {
    print('[BasePageState]: 记录用户打开 ${widget.pageName} 页面');
    super.initState();
  }

  @mustCallSuper
  @override
  void dispose() {
    print('[BasePageState]: 记录用户离开 ${widget.pageName} 页面');
    super.dispose();
  }

  /// 封装的 pop。
  void pop<T extends Object>([T result]) {
    return Navigator.pop<T>(context, result);
  }

  getDartPath(frame){
    // 打印
    Log().info("所在文件：${frame.uri} 所在行 ${frame.line} 所在列 ${frame.column}");

    List<String> newString = frame.uri.toString().split("packages/");
    List<String> strs = frame.uri.toString().split("/");
    List<String> pagename = strs[strs.length-1].toString().split(".");
    print("pagename[0]="+pagename[0].toString());
    print("newString[1].replaceAll(\"packages\", "")="+newString[1].replaceAll("packages/", "").toString());

    // Log().info(newString[1].replaceAll("packages/", "").toString());
    // 打印结果
    // flutter: 所在文件：package:flutterlog/main.dart 所在行 55 所在列 23
    context.read<Counter>().put({pagename[0].toString():(newString[1].replaceAll("packages/", "")).toString()});
  }
}