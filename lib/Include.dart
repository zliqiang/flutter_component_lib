//包含基础类库
export 'package:flutter/material.dart';
export 'package:flutter/services.dart';
export 'dart:math';
//配置
export 'package:flutter_web_2021/config/config.dart';
export 'package:flutter_web_2021/config/NWApi.dart';
//dart路径
export 'package:stack_trace/stack_trace.dart';
//资源
export 'package:flutter_web_2021/res/styles.dart';
export 'package:flutter_web_2021/res/dimens.dart';
export 'package:flutter_web_2021/res/resources.dart';
export 'package:flutter_web_2021/res/styles.dart';
//公共页面
export 'package:flutter_web_2021/base/BasePage.dart';
export 'package:flutter_web_2021/base/BasePageState.dart';
//状态管理
export 'provider/Counter.dart';
export 'package:provider/provider.dart';
export 'package:redux/redux.dart';
export 'package:flutter_redux/flutter_redux.dart';
//提示
export 'package:oktoast/oktoast.dart';
//内测版本，目前提供了日志、method channel信息、路由信息、网络抓包、帧率、设备与内存信息查看、控件信息查看功能
export 'package:dokit/dokit.dart';
//工具类
export 'package:flutter_web_2021/utils/AppUtil.dart';
export 'package:flutter_web_2021/utils/PlatformUtils.dart';
export 'package:flutter_web_2021/utils/log/Log.dart';
export 'package:flutter_web_2021/utils/app_navigator.dart';
//bar
export 'package:flutter_web_2021/widgets/app_bar/AppBar.dart';
