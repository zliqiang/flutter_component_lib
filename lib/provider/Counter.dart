import 'package:flutter/foundation.dart';
import '../Include.dart';

class Counter with ChangeNotifier, DiagnosticableTreeMixin {
  Map<String,String> _allCasePage =Map();

  Map<String,String> get allCasePage => _allCasePage;

  void put(Map<String,String> map) {
    _allCasePage.addAll(map);
    notifyListeners();
  }

  // @override
  // void debugFillProperties(DiagnosticPropertiesBuilder properties) {
  //   super.debugFillProperties(properties);
  //   properties.add(ObjectFlagProperty('allCasePage', allCasePage));
  // }
}