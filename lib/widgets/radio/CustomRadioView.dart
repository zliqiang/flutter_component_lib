import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter_color/flutter_color.dart';
import 'package:flutter_web_2021/res/resources.dart';

/**
 * 自定义单选按钮
 */
class CustomRadioView extends StatefulWidget {
  bool defaults, isIco;
  final double size,height,width;
  final Function selectListen;
  final String assetName_n, assetName_y;
  final EdgeInsetsGeometry padding, margin;
  final Color iconColorY, iconColorN;

  CustomRadioView(
      {this.isIco = true,
      this.margin,
      this.height,
      this.width,
      this.padding,
      this.defaults = false,
      this.selectListen,
      this.size = 12,
      this.iconColorN = Colours.color_gray_CCCCCC,
      this.iconColorY = Colours.color_black_362603,
      this.assetName_n = "assets/images/app/acce_n.png",
      this.assetName_y = "assets/images/app/acce_y.png"});

  @override
  _CustomRadioViewState createState() => _CustomRadioViewState();
}

class _CustomRadioViewState extends State<CustomRadioView> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: widget.width,
      height: widget.height,
      margin: widget.margin,
      alignment: Alignment.center,
      child:  new Material(
        // 设置背景颜色 默认矩形
        // color: Colors.purple,
        child: new Ink(
          //用ink圆角矩形
          // color: Colors.red,
          decoration: new BoxDecoration(
            //不能同时”使用Ink的变量color属性以及decoration属性，两个只能存在一个
            // color: Colors.purple,
            //设置圆角
            borderRadius: new BorderRadius.all(new Radius.circular(widget.size/2)),
          ),
          child: InkWell(
            //圆角设置,给水波纹也设置同样的圆角
            //如果这里不设置就会出现矩形的水波纹效果
            borderRadius: new BorderRadius.circular(widget.size/2),
            onTap: () {
              if (widget.defaults) {
                widget.defaults = !widget.defaults;
                widget.selectListen(widget.defaults);
              } else {
                widget.defaults = !widget.defaults;
                widget.selectListen(widget.defaults);
              }
              setState(() {});
            },
            child: Container(
              alignment: Alignment.center,
              padding: widget.padding,
              child: widget.isIco
                  ? Icon(
                widget.defaults ? Icons.radio_button_checked : Icons.radio_button_unchecked,
                size: widget.size,
                color: widget.defaults ? widget.iconColorY : widget.iconColorN,
              )
                  : Image.asset(
                widget.defaults ? widget.assetName_n : widget.assetName_y,
                width: widget.size,
                height: widget.size,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
