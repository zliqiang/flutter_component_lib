import 'package:ant_icons/ant_icons.dart';
import 'package:flustars/flustars.dart';
import 'package:flutter_vant_kit/widgets/search.dart';
import 'package:flutter_web_2021/pages/mmx/home/creation/CreationCenterPage.dart';
import 'package:flutter_web_2021/widgets/app_bar/AddressSearchBar.dart';
import 'package:flutter_web_2021/widgets/app_bar/SearchBar.dart';
import 'package:getwidget/getwidget.dart';

import '../../Include.dart';

class MyAppBar {
  /// 标题栏 --标题
  static PreferredSizeWidget appBarAddressSearch(context, String hint, {Widget left ,FocusNode contentFocusNode,VoidCallback onPressed,Function(String val) onSubmitted,Function() onCancel}) {
    {
      return AppBar(
        toolbarHeight: 50,
        backgroundColor: Colours.color_black_252525,
        elevation: 0,
        title: AddressSearchBar(
          background: Colours.color_gray_585858,
          maxLength: 20,
          shape: "round",
          left: left,
          placeholder: hint,
          onSubmitted: onSubmitted,
          onCancel: onCancel,
            contentFocusNode:contentFocusNode,

        ),
        leadingWidth: 30,
        leading: Container(
          alignment: Alignment.center,
          margin: EdgeInsets.only(left: 15),
          padding: EdgeInsets.only(left: 0),
          child: IconButton(
              padding: EdgeInsets.only(left: 0),
              alignment: Alignment.center,
              splashRadius: 15,
              icon: Icon(
                Icons.arrow_back_ios,
                size: 15,
              ),
              color: Colors.grey,
              onPressed: onPressed != null
                  ? onPressed
                  : () {
                      Navigator.of(context).pop();
                    }),
        ),
      );
    }
  }
  /// 标题栏 --标题
  static PreferredSizeWidget appBarSearch(context, String hint, {FocusNode contentFocusNode,VoidCallback onPressed,Function(String val) onSubmitted,Function() onCancel}) {
    {
      return AppBar(
        toolbarHeight: 50,
        backgroundColor: Colours.color_black_252525,
        elevation: 0,
        title: SearchBar(
          background: Colours.color_gray_585858,
          maxLength: 20,
          shape: "round",
          placeholder: hint,
          onSubmitted: onSubmitted,
          onCancel: onCancel,
            contentFocusNode:contentFocusNode,

        ),
        leadingWidth: 30,
        leading: Container(
          alignment: Alignment.center,
          margin: EdgeInsets.only(left: 15),
          padding: EdgeInsets.only(left: 0),
          child: IconButton(
              padding: EdgeInsets.only(left: 0),
              alignment: Alignment.center,
              splashRadius: 15,
              icon: Icon(
                Icons.arrow_back_ios,
                size: 15,
              ),
              color: Colors.grey,
              onPressed: onPressed != null
                  ? onPressed
                  : () {
                      Navigator.of(context).pop();
                    }),
        ),
      );
    }
  }

  /// 标题栏 --标题
  static PreferredSizeWidget appBarCreationNoRight(context, String title, {VoidCallback onPressed}) {
    {
      return AppBar(
        toolbarHeight: 50,
        backgroundColor: Colours.color_black_252525,
        elevation: 0,
        title: Text(
          title,
          style: TextStyles.btnTextWhile16,
        ),
        centerTitle: true,
        leading: Container(
          alignment: Alignment.centerLeft,
          margin: EdgeInsets.only(left: 0),
          padding: EdgeInsets.only(left: 0),
          child: IconButton(
              padding: EdgeInsets.only(left: 0),
              alignment: Alignment.center,
              splashRadius: 15,
              icon: Icon(
                AntIcons.close,
                size: 15,
              ),
              color: Colors.grey,
              onPressed: onPressed != null
                  ? onPressed
                  : () {
                      Navigator.of(context).pop();
                    }),
        ),
      );
    }
  }
  /// 标题栏 --标题
  static PreferredSizeWidget appBarCreationNoRight_2(context, String title, {VoidCallback onPressed}) {
    {
      return AppBar(
        toolbarHeight: 50,
        backgroundColor: Colours.color_black_252525,
        elevation: 0,
        title: Text(
          title,
          style: TextStyles.btnTextWhile16,
        ),
        centerTitle: true,
        leading: Container(
          alignment: Alignment.centerLeft,
          margin: EdgeInsets.only(left: 0),
          padding: EdgeInsets.only(left: 0),
          child: IconButton(
              padding: EdgeInsets.only(left: 0),
              alignment: Alignment.center,
              splashRadius: 15,
              icon: Icon(
                Icons.arrow_back_ios,
                size: 15,
              ),
              color: Colors.grey,
              onPressed: () {
                Navigator.of(context).pop();
              }),
        ),
      );
    }
  }

  /// 标题栏 --标题
  static PreferredSizeWidget appBarCreation(context, String title, {VoidCallback onPressed,Widget right}) {
    {
      return AppBar(
        toolbarHeight: 50,
        backgroundColor: Colours.color_black_252525,
        elevation: 0,
        title: Text(
          title,
          style: TextStyles.btnTextWhile16,
        ),
        centerTitle: true,
        leading: Container(
          alignment: Alignment.center,
          margin: EdgeInsets.only(left: 0),
          padding: EdgeInsets.only(left: 0),
          child: IconButton(
              padding: EdgeInsets.only(left: 0),
              alignment: Alignment.center,
              splashRadius: 15,
              icon: Icon(
                Icons.arrow_back_ios,
                size: 15,
              ),
              color: Colors.grey,
              onPressed: () {
                Navigator.of(context).pop();
              }),
        ),
        actions: [
          UnconstrainedBox(
            child:right!=null?right: Container(
              width: 68,
              height: 25,
              child: GFButton(
                onPressed: onPressed,
                text: "参赛",
                color: Colors.black,
                textStyle: TextStyles.btnTextWhile11,
                icon: Image.asset(
                  "asset/image/home/creation/ico_video.png",
                  width: 15,
                  height: 15,
                ),

                shape: GFButtonShape.standard,
              ),
            ),
          ),
          Gaps.hGap12
        ],
      );
    }
  }

  /// 标题栏 --标题
  static PreferredSizeWidget appBar(context, text, {textColor = Colours.text_sub_title_gray, double elevation = 2}) {
    {
      return AppBar(
        title: Text(
          text,
          style: TextStyle(color: textColor, fontWeight: FontWeight.bold, fontSize: Dimens.dp16),
        ),
        backgroundColor: Colours.bg_while,
        brightness: Brightness.light,
        elevation: elevation,
        shadowColor: Colors.grey.withOpacity(0.1),
        centerTitle: true,
        leading: Container(
          margin: EdgeInsets.only(left: 8),
          child: IconButton(
              alignment: Alignment.center,
              splashRadius: 24,
              icon: Icon(
                Icons.arrow_back_ios,
                size: 15,
              ),
              color: Colors.grey,
              onPressed: () {
                Navigator.of(context).pop();
              }),
        ),
      );
    }
  }

  /// 标题栏 --标题
  static PreferredSizeWidget appBarNoBg(context, text, {toolbarHeight, textColor = Colours.bg_while, double elevation = 2}) {
    {
      return AppBar(
        title: Text(
          text,
          style: TextStyle(color: textColor, fontSize: 18),
        ),
        backgroundColor: Colors.transparent,
        brightness: Brightness.light,
        elevation: elevation,
        shadowColor: Colors.transparent,
        centerTitle: true,
        toolbarHeight: toolbarHeight,
        leading: Container(
          margin: EdgeInsets.only(left: 8),
          child: IconButton(
              alignment: Alignment.center,
              splashRadius: 24,
              icon: Icon(
                Icons.arrow_back_ios,
                size: 15,
                color: Colours.bg_while,
              ),
              color: Colours.bg_while,
              onPressed: () {
                Navigator.of(context).pop();
              }),
        ),
      );
    }
  }

  /// 标题栏--返回按钮
  static Widget appBarWidget(context, text) {
    return Container(
      color: Colors.transparent,
      padding: EdgeInsets.only(top: ScreenUtil.getStatusBarH(context), left: 8),
      child: Material(
        color: Colors.transparent,
        shape: CircleBorder(),
        child: IconButton(
            alignment: Alignment.center,
            splashRadius: 24,
            icon: Icon(
              Icons.arrow_back_ios,
              size: Dimens.sp11,
            ),
            color: Colors.grey,
            onPressed: () {
              Navigator.of(context).pop();
            }),
      ),
    );
  }

  /// 标题栏 --tab标题
  static PreferredSizeWidget tabAppBar(context, text, {bool showIcoLeft = true, showContent = true}) {
    {
      return AppBar(
        title: Text(
          text,
          style: TextStyle(color: Colours.text_title_black, fontWeight: FontWeight.bold, fontSize: 18),
        ),
        toolbarHeight: showContent ? null : 0,
        backgroundColor: Colours.bg_while,
        brightness: Brightness.light,
        elevation: 0,
        shadowColor: Colors.grey.withOpacity(0.1),
      );
    }
  }
}
