
import 'package:flutter_web_2021/Include.dart';
import 'package:flutter_web_2021/widgets/img/IconView.dart';

class BottomAppBarView extends BasePage {
  int index;
  TabController tabController;
  Color backgroundColor, colorSelected, colorNotSelected;
  final double selectedFontSize, unselectedFontSize, sizeIco;
  final Function selectedListen;

  BottomAppBarView(
      {this.index = 0,
        this.selectedListen,
        this.backgroundColor=Colors.transparent,
        this.tabController,
        this.selectedFontSize = 13,
        this.unselectedFontSize = 13,
        this.sizeIco = 30,
        this.colorSelected = Colors.blueAccent,
        this.colorNotSelected = Colors.grey});

  @override
  _BottomAppBarState createState() => _BottomAppBarState();

  @override
  String get pageName => '底部导航栏';
}

class _BottomAppBarState extends BasePageState<BottomAppBarView> {

  @override
  void initState() {
    super.initState();

    widget.tabController?.addListener(() {
      if (widget.tabController.index.toDouble() ==
          widget.tabController.animation.value) {
        Log().info(
          "tabController index=" + widget.tabController.index.toString(),
        );
        setState(() {
          widget.index =  widget.tabController.index;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return  BottomNavigationBar(
        backgroundColor: widget.backgroundColor,
        type: BottomNavigationBarType.fixed,
        elevation: 0.0,
        items: [
          BottomNavigationBarItem(
              icon: IconView(
                isAsset: true,
                url:  widget.index == 0?"asset/image/home/ico_home.png":"asset/image/home/ico_home.png",
                size: widget.sizeIco,
              ),
            title: Container()
             ),
          BottomNavigationBarItem(
              icon: IconView(
                isAsset: true,
                url:  widget.index == 1?"asset/image/home/ico_match.png":"asset/image/home/ico_match.png",
                size: widget.sizeIco,
              ),
              title: Container()),
          BottomNavigationBarItem(
              icon: IconView(
                isAsset: true,
                url:  widget.index == 2?"asset/image/home/ico_my.png":"asset/image/home/ico_my.png",
                size: widget.sizeIco,
              ),
              title: Container()),
        ],
        currentIndex: widget.index,
        onTap: (index) {
          widget.index = index;
          widget.selectedListen(index);
          setState(() {
            widget.tabController.animateTo(widget.index);
            Log().info("index=" + index.toString());
          });
        },
    );
  }
}