import 'package:common_utils/common_utils.dart';
import 'package:flutter_cupertino_date_picker/flutter_cupertino_date_picker.dart';
import 'package:flutter_web_2021/utils/date/MyDateUtil.dart';

import '../../Include.dart';

class MyDatePicker{


 //  ///日期选择器
 // static void showDatePickerDialog(context,String year,String month,Function function) {
 //    DateTime _dateTime = DateTime.now();
 //    DatePicker.showDatePicker(
 //      context,
 //      pickerTheme: DateTimePickerTheme(
 //        showTitle: true,
 //        confirm: Text('确定',
 //            style: TextStyle(
 //                color: Colors.blueAccent,
 //                fontSize: 15,
 //                fontWeight: FontWeight.bold)),
 //        cancel: Text('取消',
 //            style: TextStyle(
 //              color: Colors.grey,
 //              fontSize: 15,
 //            )),
 //      ),
 //      minDateTime: DateTime.parse('2018-11-00'),
 //      maxDateTime: _dateTime,
 //      initialDateTime: MyDateUti().getYearAndMonthDateTime(year,month),
 //      dateFormat: 'yyyy年-MM月',
 //      locale: DateTimePickerLocale.zh_cn,
 //      onConfirm: (dateTime, List<int> index) {
 //          year = DateUtil.formatDate(dateTime, format: "yyyy年");
 //          month = DateUtil.formatDate(dateTime, format: "MM月");
 //        function(year,month);
 //
 //        // setState(() {
 //
 //        //   eventBus.fire(new MonthSelectionEvent(month));
 //        // });
 //      },
 //    );
 //  }

 ///日期选择器
 static void showDatePickerDialog(context,String title,String str,Function function) {
   DateTime _dateTime = DateTime.now();
   DatePicker.showDatePicker(
     context,
     pickerTheme: DateTimePickerTheme(
       showTitle: true,
       confirm: Text('确定',
           style: TextStyle(
             color: Colors.blueAccent,
             fontSize: 12,
               fontWeight: FontWeight.bold
           )),
       cancel: Text('取消',
           style: TextStyle(
             color: Colors.grey,
             fontSize: 12,
           )),
     ),
     minDateTime: DateTime.parse('2018-11-00'),
     maxDateTime: _dateTime,
     initialDateTime: MyDateUtil().getStrDateTime(str),
     dateFormat: 'yyyy年-MM月-dd日',
     locale: DateTimePickerLocale.zh_cn,
     onConfirm: (dateTime, List<int> index) {
       // month = DateUtil.formatDate(dateTime, format: "MM月");
       // month = DateUtil.formatDate(dateTime, format: "dd日");
       function(DateUtil.formatDate(dateTime, format: "yyyy-mm-dd"));
     },
   );
 }
 ///日期选择器
 static void showTimePickerDialog(context,String title,String str,Function function) {
   DateTime _dateTime = DateTime.now();
   DatePicker.showDatePicker(
     context,
     pickerTheme: DateTimePickerTheme(
       showTitle: true,
       confirm: Text('确定',
           style: TextStyle(
               color: Colors.blueAccent,
               fontSize: 12,
               fontWeight: FontWeight.bold
             )),
       cancel: Text('取消',
           style: TextStyle(
             color: Colors.grey,
             fontSize: 12,

           )),
     ),
     minDateTime: DateTime.parse('2018-11-00'),
     maxDateTime: DateTime.parse( DateUtil.formatDate(_dateTime, format: "yyyy-MM-dd")+" 23:59"),
     initialDateTime: MyDateUtil().getStrDateTime(str),
     dateFormat: 'HH时:mm分',
     locale: DateTimePickerLocale.zh_cn,
     onConfirm: (dateTime, List<int> index) {
       // month = DateUtil.formatDate(dateTime, format: "MM月");
       // month = DateUtil.formatDate(dateTime, format: "dd日");
       function(DateUtil.formatDate(dateTime, format: "HH:mm"));
     },
   );
 }





}