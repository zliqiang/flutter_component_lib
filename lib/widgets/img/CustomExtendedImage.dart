import 'package:extended_image/extended_image.dart';
import 'package:loading_indicator_view/loading_indicator_view.dart';

import '../../Include.dart';

class CustomExtendedImage extends BasePage {
  double width, height, loadSize, minBallRadius, maxBallRadius;
  String url,assetUri;
  BoxFit fit;
  bool cache = true;
  BoxShape shape;
  BoxBorder border;
  BorderRadius borderRadius;

  CustomExtendedImage(this.url,
      {this.borderRadius,
      this.fit,
      this.cache,
      this.border,
      this.shape,
      this.width,
      this.height,
        this.assetUri = "asset/image/app/pic-default.jpg",
      this.loadSize = 20,
      this.minBallRadius = 0.5,
      this.maxBallRadius = 2});

  @override
  _CustomExtendedImageState createState() => _CustomExtendedImageState();

  @override
  String get pageName => '自定义图片组件（缓存）';
}

class _CustomExtendedImageState extends BasePageState<CustomExtendedImage> {
  Widget placeholderView(isAssets){
    if(!isAssets) {
      return Container(
          width: widget.loadSize,
          height: widget.loadSize,
          child:
          BallSpinFadeLoaderIndicator(ballColor: Colours.blue, minBallRadius: widget.minBallRadius, maxBallRadius: widget.maxBallRadius));
    }else{
        return   Container(
          width: widget.width,
          height: widget.height,
          decoration: BoxDecoration(
            borderRadius:  widget.borderRadius,
            border:widget.border,
            image: new DecorationImage(
              image: new ExactAssetImage(widget.assetUri),
              fit: BoxFit.cover,
            ),
          ),
        );
    }
  }

  @override
  Widget build(BuildContext context) {
    return

      ExtendedImage.network(
      // 'https://raw.githubusercontent.com/fluttercandies/flutter_candies/master/gif/extended_text/special_text.jpg',
      widget.url,
      excludeFromSemantics:true,
      fit: widget.fit,
      cache: widget.cache,
      shape: widget.shape,
      border: widget.border,
      borderRadius: widget.borderRadius,
      width: widget.width,
      height: widget.height,
      loadStateChanged: (ExtendedImageState state) {
        if (state.extendedImageLoadState == LoadState.loading) {
          return Center(
            child:placeholderView(false));
        }else if (state.extendedImageLoadState == LoadState.failed){
          return Center(
              child:placeholderView(true));
        }
        return null;
      },
    );
  }






}
