import 'package:flutter_web_2021/Include.dart';
import 'package:flutter_web_2021/widgets/img/CustomExtendedImage.dart';
import 'package:flutter_web_2021/widgets/str/StringUtils.dart';

class ImagesViews {
 static Widget circle(url, {double width=double.infinity, double height=double.infinity, double borderLine = 1.0, Color borderColor = Colors.transparent, double borderRadius = 5.0,BoxFit fit =BoxFit.cover}) =>
      CustomExtendedImage(
        StringUtils.emptyStr(url),
        fit: fit,
        width: width,
        height: height,
        cache: true,
        shape: BoxShape.circle,
        border: Border.all(color: borderColor, width: borderLine),
        borderRadius: BorderRadius.all(Radius.circular(borderRadius)),
      );
 static Widget rectangle(url, {double width=double.infinity, double height=double.infinity, double borderLine = 1.0, Color borderColor = Colors.transparent, double borderRadius = 5.0,BoxFit fit =BoxFit.cover}) =>
      CustomExtendedImage(
        StringUtils.emptyStr(url),
        fit: fit,
        width: width,
        height: height,
        cache: true,
        shape: BoxShape.rectangle,
        border: Border.all(color: borderColor, width: borderLine),
        borderRadius: BorderRadius.all(Radius.circular(borderRadius)),
      );
 static Widget customizeRectangle(url, {double width=double.infinity, double height=double.infinity, double borderLine = 1.0, Color borderColor = Colors.transparent, double borderRadius = 5.0,BoxFit fit =BoxFit.cover}) =>
      CustomExtendedImage(
        StringUtils.emptyStr(url),
        fit: fit,
        width: width,
        height: height,
        cache: true,
        shape: BoxShape.rectangle,
        border: Border.all(color: borderColor, width: borderLine),
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(borderRadius),
          topRight: Radius.circular(borderRadius),
        ),
      );
}
