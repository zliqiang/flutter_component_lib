import 'package:flutter_web_2021/Include.dart';

///图标控件
class IconView extends BasePage {
//是否是网络请求
  bool isAsset;

//图片url
  String url;

//图标
  IconData iconData;

//图标大小
  double size, width, height;

  Color color;

  IconView({this.isAsset = false, this.url, this.iconData, this.size = 30, this.width = 30, this.height = 30, this.color});

  @override
  _IconViewState createState() => _IconViewState();

  @override
  String get pageName => '封装图标组件';
}

class _IconViewState extends BasePageState<IconView> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(0),
      margin: EdgeInsets.all(0),
      child: widget.isAsset
          ? Image.asset(
              widget.url,
              width: widget.size,
              height: widget.size,
              color: widget.color,
            )
          : Icon(
              widget.iconData,
              size: widget.size,
              color: widget.color,
            ),
    );
  }
}
