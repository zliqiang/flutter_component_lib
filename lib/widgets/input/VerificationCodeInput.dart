import 'dart:async';

import 'package:flutter_web_2021/Include.dart';
import 'package:flutter_web_2021/eventbus/event.dart';
import 'package:flutter_web_2021/widgets/text/custom_text.dart';

class VerificationCodeInput extends BasePage {
  @override
  _PhoneNumberViewState createState() => _PhoneNumberViewState();

  @override
  String get pageName => '手机号输入';

  ///尺寸
  final double width, height, fontSize, left, right;

  ///字体颜色
  final Color hintTextColor, textColor;

  ///默认内容
  final String leftText, hintText, verificationCodeReminderText;

  ///左边布局-自定义布局后--无效
  final Widget leftWidget, rightWidget;
  final double leftWidth, leftPadding, rightPadding;

  //显示验证码
  final bool isShowVerificationCode;

  ///行
  int maxLines, minLines, maxlength;

  final TextEditingController controller;

  //键盘
  final TextInputType keyboardType;

  //焦点
  final FocusNode contentFocusNode;

  final Function isSendAction;

  GestureTapCallback onTap;

  VerificationCodeInput(
      {this.width,
      this.height,
      this.left = 0,
      this.right = 0,
      this.onTap,
      this.fontSize,
      this.hintTextColor,
      this.isSendAction,
      this.verificationCodeReminderText = "获取验证码",
      this.textColor,
      this.hintText,
      this.leftText = "",
      this.maxlength = 6,
      this.leftWidth = 100,
      this.leftPadding = 0,
      this.rightPadding = 0,
      this.keyboardType,
      this.controller,
      this.contentFocusNode,
      this.isShowVerificationCode = false,
      this.maxLines,
      this.minLines,
      this.leftWidget,
      this.rightWidget});
}

class _PhoneNumberViewState extends BasePageState<VerificationCodeInput> {
  bool isShowRight = false;

  //颜色
  Color btnColor = Colors.transparent;
  double radius = 30;
  Color color = Colours.text_title_black;

  //重新发送
  bool sendCodeBtn = true;

  //时间-毫秒
  int seconds = 120;

  //倒计时
  Timer timer;

  //计时文本
  String text;

  ///开始倒计时
  showTimer(void callback(Timer timer)) {
    timer = Timer.periodic(Duration(milliseconds: 1000), callback);
  }

  ///清除倒计时
  cancel() {
    if (timer != null) {
      timer.cancel(); //清除定时器
    }
  }

  void onupdate() {
    seconds--;
    text = "${seconds.toString()}" + "s";
    if (seconds > 0) {
      color = Colours.color_gray_CCCCCC;
      btnColor = Colors.transparent;
    } else {
      color = Colors.black;
      text = "重新获取";
      btnColor = Colors.transparent;
      widget.isSendAction(false);
    }
    Log().info("${seconds.toString()}");
    if (seconds <= 0) {
      cancel();
      sendCodeBtn = true;
      widget.isSendAction(false);
    }
  }

  var actionEventBus;




  @override
  void initState() {
    super.initState();
    text = widget.verificationCodeReminderText;
    actionEventBus = eventBus.on<LoginMSM>().listen((event) {
      if (sendCodeBtn) {
        widget.isSendAction(true);
        seconds = 60;
        sendCodeBtn = false;
        showTimer((timer) {
          setState(() {
            onupdate();
          });
        });
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    cancel();
    actionEventBus?.cancel();
  }


  ///右边布局--默认
  Widget suffix() => isShowRight
      ? InkWell(
          onTap: () {
            setState(() {
              widget.controller.text = "";
              isShowRight = false;
            });
          },
          child: Icon(
            Icons.cancel,
            color: Colors.grey,
            size: 20,
          ),
        )
      : null;

  ///装个
  ///https://blog.csdn.net/yuzhiqiang_1993/article/details/88204031
  InputDecoration decoration() => InputDecoration(
        hintStyle: TextStyle(color: widget.hintTextColor, textBaseline: TextBaseline.alphabetic),
        hintText: widget.hintText,
        contentPadding: EdgeInsets.only(
          top: 0,
          left: 30,
        ),
        suffixIcon: suffix(),
        fillColor: Colors.transparent,
        hoverColor: Colors.transparent,
        border: OutlineInputBorder(borderSide: BorderSide.none),
      );

  List<TextInputFormatter> inputFormatters() => [
        //只能输入数字
        WhitelistingTextInputFormatter.digitsOnly,
        //字数限制
        LengthLimitingTextInputFormatter(widget.maxlength),
      ];

  ///内容监听
  Function onChanged(value) {
    Log().info("value: =" + value);
    if (value.isEmpty) {
      isShowRight = false;
    } else {
      isShowRight = true;
    }
    setState(() {});
  }

  ///文字样式
  TextStyle textStyle() => TextStyle(fontSize: widget.fontSize, color: widget.textColor, textBaseline: TextBaseline.alphabetic);

  ///输入框组件
  Widget textField(int minLines, int maxLines) => TextField(
        style: textStyle(),
        maxLines: maxLines,
        minLines: minLines,
        controller: widget.controller,
        keyboardType: widget.keyboardType,
        focusNode: widget.contentFocusNode,
        decoration: decoration(),
        inputFormatters: inputFormatters(),
        onChanged: onChanged,
      );

  Widget rightWidget() => widget.isShowVerificationCode
      ? Container(
          child: InkWell(
            onTap: widget.onTap,
            child: Container(
              decoration: BoxDecoration(
                color: btnColor,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(radius),
                    topRight: Radius.circular(radius),
                    bottomLeft: Radius.circular(radius),
                    bottomRight: Radius.circular(radius)),
              ),
              child: Row(
                children: [
                  CustomText(
                    width: 115,
                    alignment: Alignment.center,
                    text: text,
                    color: color,
                    fontSize: Dimens.sp12,
                  ),
                ],
              ),
            ),
          ),
        )
      : Column();

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          Expanded(child: textField(widget.minLines, widget.maxLines)),
          Padding(padding: EdgeInsets.all(3), child: Dividers.dividerVertical(color: Colours.color_gray_CCCCCC)),
          rightWidget(),
        ],
      ),
    );
  }
}
