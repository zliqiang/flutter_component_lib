import 'package:flutter_web_2021/Include.dart';

class PhoneNumberInput extends BasePage {

  @override
  _PhoneNumberViewState createState() => _PhoneNumberViewState();

  @override
  String get pageName => '手机号输入';

  ///尺寸
  final double width, height, fontSize, left, right;

  ///字体颜色
  final Color hintTextColor, textColor;

  ///默认内容
  final String leftText, hintText, verificationCodeReminderText;

  ///左边布局-自定义布局后--无效
  final Widget leftWidget, rightWidget;
  final double leftWidth, leftPadding, rightPadding;

  //显示验证码
  final bool isShowVerificationCode;

  ///行
  int maxLines, minLines, maxlength;

  final TextEditingController controller;

  //键盘
  final TextInputType keyboardType;

  //焦点
  final FocusNode contentFocusNode;

  final Function isSendAction;

  GestureTapCallback onTap;


  PhoneNumberInput(
      {this.width,
        this.height,
        this.left = 0,
        this.right = 0,
        this.onTap,
        this.fontSize,
        this.hintTextColor,
        this.isSendAction,
        this.verificationCodeReminderText = "获取验证码",
        this.textColor,
        this.hintText,
        this.leftText = "",
        this.maxlength = 11,
        this.leftWidth = 100,
        this.leftPadding = 0,
        this.rightPadding = 0,
        this.keyboardType,
        this.controller,
        this.contentFocusNode,
        this.isShowVerificationCode = false,
        this.maxLines,
        this.minLines,
        this.leftWidget,
        this.rightWidget});


}

class _PhoneNumberViewState extends BasePageState<PhoneNumberInput> {
  bool isShowRight = false;

  ///右边布局--默认
  Widget suffix() => isShowRight
      ? InkWell(
    onTap: () {
      setState(() {
        widget.controller.text = "";
        isShowRight = false;
      });
    },
    child: Icon(
      Icons.cancel,
      color: Colors.grey,
      size: 20,
    ),
  )
      : null;

  ///装个
  ///https://blog.csdn.net/yuzhiqiang_1993/article/details/88204031
  InputDecoration decoration() => InputDecoration(
    hintStyle: TextStyle(
        color: widget.hintTextColor, textBaseline: TextBaseline.alphabetic),
    hintText: widget.hintText,
    contentPadding: EdgeInsets.only(top: 0,left:  30,),
    suffixIcon: suffix(),
    fillColor: Colors.transparent,
    hoverColor: Colors.transparent,
    border: OutlineInputBorder(borderSide: BorderSide.none),
  );
  List<TextInputFormatter> inputFormatters() => [
    //只能输入数字
    WhitelistingTextInputFormatter.digitsOnly,
    //字数限制
    LengthLimitingTextInputFormatter(widget.maxlength),
  ];
  ///内容监听
  Function onChanged(value) {
    Log().info("value: =" + value);
    if (value.isEmpty) {
      isShowRight = false;
    } else {
      isShowRight = true;
    }
    setState(() {});
  }
  ///文字样式
  TextStyle textStyle() => TextStyle(
      fontSize: widget.fontSize,
      color: widget.textColor,
      textBaseline: TextBaseline.alphabetic);
  ///输入框组件
  Widget textField(int minLines, int maxLines) =>  TextField(
      style: textStyle(),
      maxLines: maxLines,
      minLines: minLines,
      controller: widget.controller,
      keyboardType: widget.keyboardType,
      focusNode: widget.contentFocusNode,
      decoration: decoration(),
      inputFormatters: inputFormatters(),
      onChanged: onChanged,
  );

  @override
  Widget build(BuildContext context) {
    return Container(
      child:  textField(widget.minLines, widget.maxLines),
    );
  }

}