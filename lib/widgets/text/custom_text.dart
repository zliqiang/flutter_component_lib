import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomText extends StatelessWidget {
  //点击事件
  final Object onTab;
  final String text;
  final double fontSize,width,height;
  final FontWeight fontWeight;
  final Color color,bg;
  final Decoration decoration;
  final EdgeInsetsGeometry padding, margin;

  final AlignmentGeometry alignment;
  final int maxLines;

  CustomText({
    Key key,
    this.onTab,
    this.text = "",
    this.fontSize,
    this.fontWeight,
    this.decoration,
    this.maxLines=1,
    this.color,
    this.bg,
    this.width,
    this.height,
    this.padding,
    this.margin,
    this.alignment,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTab,
      child: Container(
        color: bg,
        width: width,
        height: height,
        padding: padding,
        margin: margin,
        decoration: decoration,
        alignment: alignment,
        child: Text(
          text!=null?text:"",
          maxLines:maxLines,
          overflow:TextOverflow.ellipsis,
          style: TextStyle(
              color: color, fontWeight: fontWeight, fontSize: fontSize),
        ),
      ),
    );
  }
}
