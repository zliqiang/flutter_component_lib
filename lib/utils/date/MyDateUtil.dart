import '../../Include.dart';

class MyDateUtil {
  static final _singleton = MyDateUtil._();
  factory MyDateUtil() => _singleton;
  MyDateUtil._();
  ///获取指定年、月，获取DateTime
  DateTime getYearAndMonthDateTime(String year, String month) {
    Log().info(DateTime.parse(
        "${year.replaceAll("年", "")}-${month.replaceAll("月", "").length > 1 ?
        month.replaceAll("月", "") : "0${month.replaceAll("月", "")}"}-20"));

    return DateTime.parse(
        "${year.replaceAll("年", "")}-${month.replaceAll("月", "").length > 1 ?
        month.replaceAll("月", "") : "0${month.replaceAll("月", "")}"}-20");
  }
  ///根据字符串获取DateTime 参数：例'2018-11-00'
  DateTime getStrDateTime(String str){
    return   DateTime.parse(str);
  }

}
