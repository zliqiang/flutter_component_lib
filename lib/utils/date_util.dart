class MyDateUtil {

  //时间格式化，根据总秒数转换为对应的 hh:mm:ss 格式
  static String constructTime(int seconds) {
    int day = seconds ~/3600 ~/24;
    int hour = seconds ~/ 3600;
    int minute = seconds % 3600 ~/ 60;
    int second = seconds % 60;
    if(day != 0){
      return '$day天$hour小时$minute分$second秒';
    }else if(hour != 0){
      return '$hour小时$minute分$second秒';
    }else if(minute !=0){
      return '$minute分$second秒';
    }else if(second!=0){
      return '$second秒';
    }else {
      return '0秒';
    }
  }
}