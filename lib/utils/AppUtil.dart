import '../Include.dart';
///单例
///用法：
///Manager manager = new Manager();
///Manager manager2 = Manager.instance;
class AppUtil {
  BuildContext context;
  set setContext(BuildContext context) {
    this.context = context;
  }

  BuildContext get getContext {
    return context;
  }
  // 工厂模式
  factory AppUtil() =>_getInstance();
  static AppUtil get instance => _getInstance();
  static AppUtil _instance;
  AppUtil._internal() {
    // 初始化
  }
  static AppUtil _getInstance() {
    if (_instance == null) {
      _instance = new AppUtil._internal();
    }
    return _instance;
  }
}