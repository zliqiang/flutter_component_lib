import 'dart:math';

class String110Util{

 static String getImgTitle(String index){
    switch(index){
      case "0":
        return "呼啦圈";
      case "1":
        return "蜡烛";
      case "2":
        return "鹅";
      case "3":
        return "耳朵";
      case "4":
        return "帆船";
      case "5":
        return "钩子";
      case "6":
        return "勺子";
      case "7":
        return "镰刀";
      case "8":
        return "墨镜";
      case "9":
        return "哨子";
      case "00":
        return "望远镜";
      case "01":
        return "小树";
      case "02":
        return "玲儿";
      case "03":
        return "凳子";
      case "04":
        return "轿车";
      case "05":
        return "手套";
      case "06":
        return "手枪";
      case "07":
        return "锄头";
      case "08":
        return "溜冰鞋";
      case "09":
        return "猫";
      case "10":
        return "棒球";
      case "11":
        return "梯子";
      case "12":
        return "椅子";
      case "13":
        return "医生";
      case "14":
        return "钥匙";
      case "15":
        return "鹦鹉";
      case "16":
        return "石榴";
      case "17":
        return "仪器";
      case "18":
        return "糖葫芦";
      case "19":
        return "衣钩";
      case "20":
        return "香烟";
      case "21":
        return "鳄鱼";
      case "22":
        return "双胞胎";
      case "23":
        return "和尚";
      case "24":
        return "闹钟";
      case "25":
        return "二胡";
      case "26":
        return "河流";
      case "27":
        return "耳机";
      case "28":
        return "恶霸";
      case "29":
        return "饿囚";
      case "30":
        return "三轮车";
      case "31":
        return "鲨鱼";
      case "32":
        return "扇儿";
      case "33":
        return "星星";
      case "34":
        return "三丝";
      case "35":
        return "山虎";
      case "36":
        return "山鹿";
      case "37":
        return "山鸡";
      case "38":
        return "妇女";
      case "39":
        return "山丘";
      case "40":
        return "司令";
      case "41":
        return "蜥蜴";
      case "42":
        return "柿儿";
      case "43":
        return "石山";
      case "44":
        return "蛇";
      case "45":
        return "师傅";
      case "46":
        return "饲料";
      case "47":
        return "司机";
      case "48":
        return "石板";
      case "49":
        return "湿狗";
      case "50":
        return "武林";
      case "51":
        return "工人";
      case "52":
        return "鼓儿";
      case "53":
        return "乌纱帽";
      case "54":
        return "青年";
      case "55":
        return "火车";
      case "56":
        return "蜗牛";
      case "57":
        return "武器";
      case "58":
        return "尾巴";
      case "59":
        return "蜈蚣";
      case "60":
        return "榴莲";
      case "61":
        return "儿童";
      case "62":
        return "牛儿";
      case "63":
        return "流沙";
      case "64":
        return "螺丝";
      case "65":
        return "绿壶";
      case "66":
        return "溜溜球";
      case "67":
        return "绿漆";
      case "68":
        return "喇叭";
      case "69":
        return "太极";
      case "70":
        return "麒麟";
      case "71":
        return "鸡翼";
      case "72":
        return "企鹅";
      case "73":
        return "花旗参";
      case "74":
        return "骑士";
      case "75":
        return "西服";
      case "76":
        return "汽油";
      case "77":
        return "机器";
      case "78":
        return "青蛙";
      case "79":
        return "气球";
      case "80":
        return "巴黎";
      case "81":
        return "白蚁";
      case "82":
        return "靶儿";
      case "83":
        return "芭蕉扇";
      case "84":
        return "巴士";
      case "85":
        return "保姆";
      case "86":
        return "八路";
      case "87":
        return "白旗";
      case "88":
        return "爸爸";
      case "89":
        return "芭蕉";
      case "90":
        return "酒瓶";
      case "91":
        return "球衣";
      case "92":
        return "球儿";
      case "93":
        return "旧伞";
      case "94":
        return "首饰";
      case "95":
        return "酒壶";
      case "96":
        return "蝴蝶";
      case "97":
        return "旧旗";
      case "98":
        return "酒杯";
      case "99":
        return "舅舅";
    }
  }
 static String getImgName(int index){

    int indexCL = index-10;

    switch(indexCL){
      case -10:
        return "0";
      case -9:
        return "1";
      case -8:
        return "2";
      case -7:
        return "3";
      case -6:
        return "4";
      case -5:
        return "5";
      case -4:
        return "6";
      case -3:
        return "7";
      case -2:
        return "8";
      case -1:
        return "9";
      case 0:
        return "-10";
      case 1:
        return "-1";
      case 2:
        return "-2";
      case 3:
        return "-3";
      case 4:
        return "-4";
      case 5:
        return "-5";
      case 6:
        return "-6";
      case 7:
        return "-7";
      case 8:
        return "-8";
      case 9:
        return "-9";
      default:
        return indexCL.toString();
    }
  }


 ///随机下标
 static List<int> random(int length,{bool sortBool}) {
   List<int> resultList = [];
   var rng = new Random();
   int count = 0;
   while (count < length) {
     //生成几个
     int index = rng.nextInt(length); // 0-(length-1)之间区间
     if (!resultList.contains(index)) {
       resultList.add(index);
       count++;
     }
   }
   if(sortBool) {
     resultList.sort();
   }
   // print(resultList);
   return resultList;
 }






}