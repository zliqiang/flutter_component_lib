import 'package:oktoast/oktoast.dart';

///信息提示
class ToastUtil{

  ///toast显示为中心，提示时间很短
  static showToastCenter(msg){
    showToast(msg);
  }
  ///toast显示为中心，提示时间长
  static showToastCenterLong(msg){
    showToast(msg,duration: Duration(
      milliseconds: 2300,
    ), position: ToastPosition.center);
  }
  ///toast显示为上方，提示时间很短
  static showToastTop(msg){
    showToast(msg, position: ToastPosition.top);
  }
  ///toast显示为上方，提示时间长
  static showToastTopLong(msg){
    showToast(msg,duration: Duration(
      milliseconds: 2300,
    ), position: ToastPosition.top);
  }
  ///toast显示为下方，提示时间很短
  static showToastBottom(msg){
    showToast(msg.toString()+"", position: ToastPosition.bottom);
  }
  ///toast显示为下方，提示时间长
  static showToastBottomLong(msg){
    showToast(msg,duration: Duration(
      milliseconds: 2300,
    ), position: ToastPosition.bottom);
  }
}