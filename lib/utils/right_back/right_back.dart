import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter_web_2021/Include.dart';
import 'package:flutter_web_2021/router/AppRouter.dart';
import 'package:flutter_web_2021/router/NavigatorRoutes.dart';
import 'package:flutter_web_2021/utils/animation/fade_route_animation.dart';
import 'package:flutter_web_2021/utils/animation/slide_route_animation.dart';
class RoutesUtils {
 static  Object routesPage(context, String path, dynamic arguments) {
    if (PlatformUtils.isIOS) {
      //ios相关代码
      return   CupertinoPageRoute(builder: (BuildContext context) {
        return CupertinoPageScaffold(
          child:  routes[path](path,arguments:arguments),
        );
      });
    } else {
     if(PlatformUtils.isWeb) {
       return FadeRoute(routes[path](path, arguments: arguments),);
     }else{
       return SlideRoute(routes[path](path, arguments: arguments),);
     }
    }
  }
 static  Object routesNavigatorPage(context, String path, dynamic arguments) {
    if (PlatformUtils.isIOS) {
      //ios相关代码
      return   CupertinoPageRoute(builder: (BuildContext context) {
        return CupertinoPageScaffold(
          child:  routesNavigator[path](path,arguments:arguments),
        );
      });
    } else {
      if(PlatformUtils.isWeb) {
        return FadeRoute( routesNavigator[path](path,arguments:arguments),);
      }else{
        return  SlideRoute(  routesNavigator[path](path,arguments:arguments),);
      }
    }
  }
}
