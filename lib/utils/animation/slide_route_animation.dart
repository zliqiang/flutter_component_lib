import 'package:flutter/material.dart';

///路由动画 - 左右滑动
class SlideRoute extends PageRouteBuilder {
  final Widget widget;
  final int milliseconds;

  SlideRoute(this.widget,{this.milliseconds:300})
      : super(
          transitionDuration: Duration(milliseconds:milliseconds),
          pageBuilder: (
            BuildContext context,
            Animation<double> animation1,
            Animation<double> animation2,
          ) {
            return widget;
          },
          transitionsBuilder: (BuildContext context, Animation<double> animation1, Animation<double> animation2, Widget child) {
            return SlideTransition(
              child: child,
              position: Tween<Offset>(begin: Offset(1.0, 0.0), end: Offset(0.0, 0.0))
                  .animate(CurvedAnimation(parent: animation1, curve: Curves.easeInOutCirc)),
            );
          },
        );
}
