import 'package:flutter/cupertino.dart';
///路由动画 - 旋转
class RotationRoute extends PageRouteBuilder {
  final Widget widget;
  final int milliseconds;

  RotationRoute(this.widget,{this.milliseconds:300})
      : super(
    transitionDuration: Duration(milliseconds: milliseconds),
    pageBuilder: (
        BuildContext context,
        Animation<double> animation1,
        Animation<double> animation2,
        ) {
      return widget;
    },
    transitionsBuilder: (BuildContext context,
        Animation<double> animation1,
        Animation<double> animation2,
        Widget child) {
      return RotationTransition(
        turns: Tween(begin: 0.0, end: 1.0).animate(CurvedAnimation(
            parent: animation1, curve: Curves.fastOutSlowIn)),
        child: child,
      );
    },
  );
}