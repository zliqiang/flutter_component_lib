import 'package:flutter_color/flutter_color.dart';

import '../Include.dart';

class TextStyles {

  //首页菜单目录按钮-text样式
  static final TextStyle btnTextBoldWhile_12 =  TextStyle(fontSize: Dimens.sp12,color: Colours.bg_while,fontWeight: FontWeight.bold);
  static final TextStyle btnTextBoldWhile_14 =  TextStyle(fontSize: Dimens.sp14,color: Colours.bg_while,fontWeight: FontWeight.bold);
  static final TextStyle btnTextWhile_12 =  TextStyle(fontSize: Dimens.sp12,color: Colours.bg_while);
  static final TextStyle btnTextWhile_10 =  TextStyle(fontSize: Dimens.sp10,color: Colours.bg_while);
  static final TextStyle btnTextRed_10 =  TextStyle(fontSize: Dimens.sp10,color: Colours.red);

  //=========================  mmx  ====================================
  ///注册页面
  //藍色
  static  TextStyle btnText({double fontSize,String color}) =>  TextStyle(fontSize:fontSize ,color: HexColor(color));
  static  TextStyle btnTextB({double fontSize,String color}) =>  TextStyle(fontSize:fontSize ,color: HexColor(color),fontWeight: FontWeight.bold);
  static final TextStyle btnText12_0B71C8 =  TextStyle(fontSize: Dimens.sp12,color: HexColor('#0B71C8'));
  static final TextStyle btnText12_112FCB =  TextStyle(fontSize: Dimens.sp12,color: HexColor('#112FCB'));
  //灰色
  static final TextStyle btnText12B_4A4A4A =  TextStyle(fontSize: Dimens.sp12,color: HexColor('#4A4A4A'),fontWeight: FontWeight.bold);
  static final TextStyle btnText14B_4A4A4A =  TextStyle(fontSize: Dimens.sp14,color: HexColor('#4A4A4A'),fontWeight: FontWeight.bold);
  static final TextStyle btnText10_4A4A4A =  TextStyle(fontSize: Dimens.sp10,color: HexColor('#4A4A4A'));
  static final TextStyle btnText8_4A4A4A =  TextStyle(fontSize: Dimens.sp8,color: HexColor('#4A4A4A'));
  static final TextStyle btnText11_999999 =  TextStyle(fontSize: Dimens.sp11,color: HexColor('#999999'));
  static final TextStyle btnText12_999999 =  TextStyle(fontSize: Dimens.sp12,color: HexColor('#999999'));
  static final TextStyle btnText11_666666 =  TextStyle(fontSize: Dimens.sp11,color: HexColor('#666666'));
  static final TextStyle btnText14_666666 =  TextStyle(fontSize: Dimens.sp14,color: HexColor('#666666'));
  static final TextStyle btnText10_666666 =  TextStyle(fontSize: Dimens.sp10,color: HexColor('#666666'));
  static final TextStyle btnText12_666666 =  TextStyle(fontSize: Dimens.sp12,color: HexColor('#666666'));
  static final TextStyle btnText13_333333 =  TextStyle(fontSize: Dimens.sp13,color: HexColor('#333333'));
  static final TextStyle btnText12_333333 =  TextStyle(fontSize: Dimens.sp12,color: HexColor('#333333'));
  static final TextStyle btnText11_333333 =  TextStyle(fontSize: Dimens.sp11,color: HexColor('#333333'));
  static final TextStyle btnText14_333333 =  TextStyle(fontSize: Dimens.sp14,color: HexColor('#333333'));
  static final TextStyle btnText14_8E8E8E =  TextStyle(fontSize: Dimens.sp14,color: HexColor('#8E8E8E'));
  static final TextStyle btnText13_8E8E8E =  TextStyle(fontSize: Dimens.sp13,color: HexColor('#8E8E8E'));
  static final TextStyle btnText15_333333 =  TextStyle(fontSize: Dimens.sp15,color: HexColor('#333333'));
  static final TextStyle btnText17_333333 =  TextStyle(fontSize: Dimens.sp17,color: HexColor('#333333'));
  static final TextStyle btnText17B_333333 =  TextStyle(fontSize: Dimens.sp17,color: HexColor('#333333'),fontWeight: FontWeight.bold);
  static final TextStyle btnText12B_333333 =  TextStyle(fontSize: Dimens.sp12,color: HexColor('#333333'),fontWeight: FontWeight.bold);
  static final TextStyle btnText11B_333333 =  TextStyle(fontSize: Dimens.sp11,color: HexColor('#333333'),fontWeight: FontWeight.bold);
  static final TextStyle btnText14B_333333 =  TextStyle(fontSize: Dimens.sp14,color: HexColor('#333333'),fontWeight: FontWeight.bold);
  static final TextStyle btnText15B_333333 =  TextStyle(fontSize: Dimens.sp15,color: HexColor('#333333'),fontWeight: FontWeight.bold);
  static final TextStyle btnText19_333333 =  TextStyle(fontSize: Dimens.sp19,color: HexColor('#333333'));
  static final TextStyle btnText20_333333 =  TextStyle(fontSize: Dimens.sp20,color: HexColor('#333333'),fontWeight: FontWeight.bold);
  static final TextStyle btnText23_333333 =  TextStyle(fontSize: Dimens.sp23,color: HexColor('#333333'),fontWeight: FontWeight.bold);
  static final TextStyle btnText11_CCCCCC =  TextStyle(fontSize: Dimens.sp11,color: HexColor('#CCCCCC'));
  static final TextStyle btnText12_CCCCCC =  TextStyle(fontSize: Dimens.sp12,color: HexColor('#CCCCCC'));
  static final TextStyle btnText10_CCCCCC =  TextStyle(fontSize: Dimens.sp10,color: HexColor('#CCCCCC'));
  static final TextStyle btnText9_CCCCCC =  TextStyle(fontSize: Dimens.sp9,color: HexColor('#CCCCCC'));
  static final TextStyle btnText14_CCCCCC =  TextStyle(fontSize: Dimens.sp14,color: HexColor('#CCCCCC'));
  static final TextStyle btnText14B_CCCCCC =  TextStyle(fontSize: Dimens.sp14,color: HexColor('#CCCCCC'),fontWeight: FontWeight.bold);
  //白色
  static final TextStyle btnTextWhile15 =  TextStyle(fontSize: Dimens.sp15,color:  Colours.bg_while,);
  static final TextStyle btnTextWhile16 =  TextStyle(fontSize: Dimens.sp16,color:  Colours.bg_while,);
  static final TextStyle btnTextWhile13 =  TextStyle(fontSize: Dimens.sp13,color:  Colours.bg_while,);
  static final TextStyle btnTextWhile12 =  TextStyle(fontSize: Dimens.sp12,color:  Colours.bg_while,);
  static final TextStyle btnTextWhile11 =  TextStyle(fontSize: Dimens.sp11,color:  Colours.bg_while,);
  static final TextStyle btnTextWhile10 =  TextStyle(fontSize: Dimens.sp10,color:  Colours.bg_while,);
  static final TextStyle btnTextWhile9 =  TextStyle(fontSize: Dimens.sp9,color:  Colours.bg_while,);
  static final TextStyle btnTextWhile8 =  TextStyle(fontSize: Dimens.sp8,color:  Colours.bg_while,);
  //黄色
  static final TextStyle btnText18_7E4F03 =  TextStyle(fontSize: Dimens.sp18,color: HexColor('#7E4F03'),fontWeight: FontWeight.bold);
  static final TextStyle btnText16_7E4F03 =  TextStyle(fontSize: Dimens.sp16,color: HexColor('#7E4F03'),fontWeight: FontWeight.bold);
  static final TextStyle btnText14_7E4F03 =  TextStyle(fontSize: Dimens.sp14,color: HexColor('#7E4F03'),fontWeight: FontWeight.bold);
  static final TextStyle btnText11_9B680E =  TextStyle(fontSize: Dimens.sp11,color: HexColor('#9B680E'),);
  static final TextStyle btnText10_9B680E =  TextStyle(fontSize: Dimens.sp10,color: HexColor('#9B680E'),);
  static final TextStyle btnText14_F06F2A =  TextStyle(fontSize: Dimens.sp14,color: HexColor('#F06F2A'),);
  static final TextStyle btnText14B_F06F2A =  TextStyle(fontSize: Dimens.sp14,color: HexColor('#F06F2A'),fontWeight: FontWeight.bold);
  static final TextStyle btnText12_F06F2A =  TextStyle(fontSize: Dimens.sp12,color: HexColor('#F06F2A'),);
  static final TextStyle btnText11_F06F2A =  TextStyle(fontSize: Dimens.sp11,color: HexColor('#F06F2A'),);
  static final TextStyle btnText10_F06F2A =  TextStyle(fontSize: Dimens.sp10,color: HexColor('#F06F2A'),);
  static final TextStyle btnText15_F06F2A =  TextStyle(fontSize: Dimens.sp15,color: HexColor('#F06F2A'),);
  static final TextStyle btnText13B_9C6A10 =  TextStyle(fontSize: Dimens.sp13,color: HexColor('#9C6A10'),fontWeight: FontWeight.bold);
  static  TextStyle tabText13B_EA8421(bool select) =>  TextStyle(fontSize: Dimens.sp13,color: HexColor(select?'#FDFDFD':'#EA8421',));
  //红色
  static final TextStyle btnTextRed11 =  TextStyle(fontSize: Dimens.sp11,color:  Colours.color_red_EA3866,);
  static final TextStyle btnTextRed12 =  TextStyle(fontSize: Dimens.sp12,color:  Colours.color_red_EA3866,);
  static final TextStyle btnTextRed14B =  TextStyle(fontSize: Dimens.sp14,color:  Colours.color_red_EA3866,fontWeight: FontWeight.bold);
  static final TextStyle btnTextRed15B =  TextStyle(fontSize: Dimens.sp15,color:  Colours.color_red_EA3866,fontWeight: FontWeight.bold);
  static final TextStyle btnTextRed12B =  TextStyle(fontSize: Dimens.sp12,color:  Colours.color_red_EA3866,fontWeight: FontWeight.bold);
  static final TextStyle btnTextRed11B =  TextStyle(fontSize: Dimens.sp11,color:  Colours.color_red_EA3866,fontWeight: FontWeight.bold);
  //黑色

  static final TextStyle btnText11B_000000 =  TextStyle(fontSize: Dimens.sp11,color: HexColor('#000000'),fontWeight: FontWeight.bold);

  //按钮-text样式
  static final TextStyle btn_text_bg_15 =  TextStyle(fontSize: Dimens.sp12,color: Colours.text_title_black,fontWeight: FontWeight.bold);
  //按钮-text样式
  static final TextStyle tab_text_bold_blue_15 =  TextStyle(fontSize: Dimens.sp12,color:Colours.text_title_black, fontWeight: FontWeight.bold);
  //按钮-text样式
  static final TextStyle tab_text_blue_15 =  TextStyle(fontSize: Dimens.sp12,color:Colours.blue,);


  static final TextStyle textBoldDark18 = TextStyle(fontSize: Dimens.sp16, color: Colours.text_title_black, fontWeight: FontWeight.bold);


  static final TextStyle textDark16 = TextStyle(fontSize: Dimens.sp16, color: Colours.text_title_black,);

}

/// 间隔
class Gaps {
  //=========================  mmx  ====================================
  static const Widget vGap42 = SizedBox(height:42);
  static const Widget vGap20 = SizedBox(height:20);
  static const Widget vGap18 = SizedBox(height:18);
  static const Widget vGap25 = SizedBox(height:25);
  static const Widget vGap10 = SizedBox(height:10);


  /// 水平间隔
  static const Widget wGap2 = SizedBox(width:2);
  static const Widget wGap5 = SizedBox(width:5);
  static const Widget wGap10 = SizedBox(width:10);
  static const Widget wGap19 = SizedBox(width:19);
  static const Widget wGap15 = SizedBox(width:15);
  static const Widget wGap25 = SizedBox(width:25);
  static const Widget wGap22 = SizedBox(width:22);




  /// 垂直间隔
  static const Widget vGap5 = SizedBox(height: 5);
  static const Widget vGap15 = SizedBox(height:15);




  static const Widget vGap2 = SizedBox(height: 2.0);
  static const Widget vGap4 = SizedBox(height: 4.0);
  static const Widget vGap8 = SizedBox(height: 8.0);
  static const Widget vGap12 = SizedBox(height: 12.0);

  static const Widget hGap6 = SizedBox(width: 6.0);
  static const Widget hGap4 = SizedBox(width: 4.0);
  static const Widget hGap8 = SizedBox(width: 8.0);
  static const Widget hGap12 = SizedBox(width: 12.0);
  static const Widget hGap11 = SizedBox(width: 11.0);

  static Widget line = Container(height: 0.6, color: Colours.text_sub_title_gray.withOpacity(0.3));
  static const Widget empty = SizedBox();
}

//阴影
class ShapeBorders{

  static const ShapeBorder bottomShapeBorder = BorderDirectional(
      bottom: BorderSide(
        color:  Colours.line,
      )
  );

}
class Dividers{
  static  Widget dividerHorizontal({double height=1,Color color}) =>  Divider(thickness:height, height: height,color:color==null? Colours.line:color,indent:0,endIndent:0);
  static  Widget dividerVertical({double width=1,Color color}) =>  VerticalDivider(thickness:width,width: width,color:color==null? Colours.line:color);
}
