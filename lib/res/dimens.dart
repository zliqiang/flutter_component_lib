import '../Include.dart';

class Dimens {
  ///组件 标准
  //最舒服 可触摸ui组件 例：【list默认高度】 【最小可点击区域】
  static const double dp48 = 48;
  //间距 【左右边距】
  static const double dp8 = 8;
  static const double dp6 = 6;
  //btn 默认高度40
  static const double dp40 = 40;
  //图像资源尺寸 均被8整除 16、24、32、48的序列
  static const double dp28 = 28;
  //删格化分45格,所有像素落在删格的边上  宽/45
  static  double dpCutOut = screenWidth()/45;
  //启动图标 48、72、96
  static const double dp72 = 72;
  static const double dp96 = 96;
  //底部导航栏图标 【图像资源大小32】  【图像区域为24】【导航栏高52】
  static const double dp24 = 24;
  static const double dp52 = 52;
  //【tab间距32】
  static const double dp32 = 32;
  //【tab标识线3】
  static const double dp3 = 3;
  //【分割线1】
  static const double dp1 = 1;
  //【item 图片与文字的间距】
  static const double dp10 = 10;
  //【item 文字上下的间距4】
  static const double dp4 = 4;
  //标题栏 图标 画笔最细不应小于2dp
  static const double dp2 =2;
  //小图标  【图像资源大小16】  【图像区域为12】
  static const double dp16 = 16;
  static const double dp12 = 12;

  ///字体  标准
  //列表分类标题
  static final double sp16 = setSp(16);
  //标题
  static final double sp14 = setSp(14);
  //子标题
  static final double sp12 = setSp(12);
  //时间
  static final double sp11 = setSp(11);
  //底部导航栏 文字
  static final double sp10 = setSp(10);
  ///水平间隔 标准
  //最小间距
  static const Widget hGap8 = SizedBox(width: Dimens.dp8);
  ///垂直间隔 标准
  //最小间距
  static const Widget vGap8 = SizedBox(height: Dimens.dp8);

  //=========================  mmx  ====================================
  static final double sp8 = setSp(8);
  static final double sp9 = setSp(9);
  static final double sp23 = setSp(23);
  static final double sp18 = setSp(18);
  static final double sp15 = setSp(15);
  static final double sp17 = setSp(17);
  static final double sp19 = setSp(19);
  static final double sp20 = setSp(20);
  static final double sp13 = setSp(13);




  ///外边距
  //页面左右
  static const EdgeInsetsGeometry edgeInsets_lr_dp8 = EdgeInsets.only(left: dp8, right: dp8);
  ///外边距 上下
  //底部导航栏内边距
  static const EdgeInsetsGeometry edgeInsets_tb_dp6 = EdgeInsets.only(top: dp6, bottom: dp6);
  //【item 文字上下的间距4】
  static const EdgeInsetsGeometry edgeInsets_tb_dp4 = EdgeInsets.only(top: dp4, bottom: dp4);
  //【列表分类标题和item的间距
  static const EdgeInsetsGeometry edgeInsets_tb_dp10 = EdgeInsets.only(top: dp10, bottom: dp10);

  //外边距 上下  左右
  static const EdgeInsetsGeometry edgeInsets_dp2 = EdgeInsets.only(top: 2, bottom: 2,left: 2,right: 2);

  //=========================  mmx  ====================================
  static const EdgeInsetsGeometry edgeInsets_lr_dp5 = EdgeInsets.only(left: 5, right: 5);
  static const EdgeInsetsGeometry edgeInsets_lr_dp35 = EdgeInsets.only(left: 35, right: 35);
  static const EdgeInsetsGeometry edgeInsets_lr_dp38 = EdgeInsets.only(left: 38, right: 38);
  static const EdgeInsetsGeometry edgeInsets_lr_dp20 = EdgeInsets.only(left: 20, right: 20);
  static const EdgeInsetsGeometry edgeInsets_lr_dp10 = EdgeInsets.only(left: 10, right: 10);
  static const EdgeInsetsGeometry edgeInsets_lr_dp14 = EdgeInsets.only(left: 14, right: 14);
  static const EdgeInsetsGeometry edgeInsets_lr_dp15 = EdgeInsets.only(left: 15, right: 15);

  static const EdgeInsetsGeometry edgeInsets_tb_dp15 = EdgeInsets.only(top: 15, bottom: 15);




  ///获取屏幕宽
  static double screenWidth() => MediaQuery.of(AppUtil.instance.context).size.width;
  static double screenHeight() => MediaQuery.of(AppUtil.instance.context).size.height;
  static double toolbarHeight() => kToolbarHeight;
  static double bottomNavigationBarHeight() => kBottomNavigationBarHeight;
  static double textTabBarHeight() => kTextTabBarHeight;
  static double devicePixelRatio() => MediaQuery.of(AppUtil.instance.context).devicePixelRatio;
  static double ppi() => sqrt((screenWidth()*screenWidth())+(screenHeight()*screenHeight()))/2;

  //字体  sp*ppi/160
  // static  double setSp(double fontSize) =>PlatformUtils.isAndroid||PlatformUtils.isIOS? fontSize*ppi()/screenWidth():fontSize;
  static  double setSp(double fontSize) =>PlatformUtils.isAndroid||PlatformUtils.isIOS? PlatformUtils.isAndroid?fontSize*ppi()/screenWidth():fontSize*ppi()/screenWidth()+1:fontSize;

}
