import 'package:flutter/material.dart';
import 'package:flutter_color/flutter_color.dart';

class Colours {
  //页面背景
  static const Color bg_while = Color(0xFFFDFDFD);

  //分割线颜色
  static const Color splitLine_gray = Color(0xFFCACBCC);

  //文字标题
  static const Color text_title_black = Color(0xFF333333);
  static const Color text_sub_title_gray = Color(0xFF808080);
  static const Color text_blue = blue;

  //按钮
  static const Color btn_blue = blue;

  ///线
  static const Color line = Color(0xFFEEEEEE);
  static const Color line_F1F1F1 = Color(0xFFF1F1F1);

  ///颜色
  static const Color blue = Color(0xff007BED);
  static const Color red = Color(0xffe40853);
  static final Color blue50 = Colors.blue.shade50;
  static final Color blue100 = Colors.blue.shade100;

  static Color colorRest_16 = HexColor('#ff0000');

//=========================  mmx  ====================================

  static const Color color_red_EA3866 = Color(0xffEA3866);
  static const Color color_red_E02222 = Color(0xffE02222);
  static const Color color_gray_F6F7FD = Color(0xffF6F7FD);
  static const Color color_gray_F1F1F1 = Color(0xffF1F1F1);
  static const Color color_gray_CCCCCC = Color(0xffCCCCCC);
  static const Color color_gray_333333 = Color(0xff333333);
  static const Color color_gray_666666 = Color(0xff666666);
  static const Color color_gray_999999 = Color(0xff999999);
  static const Color color_gray_585858 = Color(0xff585858);
  static const Color color_gray_8E8E8E = Color(0xff8E8E8E);
  static const Color color_black_252525 = Color(0xff252525);
  static const Color color_black_aa252525 = Color(0xaa252525);
  static Color colorRedFF0600 = HexColor('#FF0600');
  static const Color color_orange_FF5800 = Color(0xffFF5800);
  static const Color color_orange_EA8421 = Color(0xffEA8421);
  static const Color color_orange_FBF5EA = Color(0xffFBF5EA);
  static const Color color_orange_C59E66 = Color(0xffC59E66);
  static const Color color_orange_D9B17C = Color(0xffD9B17C);
  static const Color color_orange_E6BE78 = Color(0xffE6BE78);
  static const Color color_orange_FFB300 = Color(0xffFFB300);
  static const Color color_black_362603 = Color(0xff362603);
  static const Color color_purple_B196F8 = Color(0xffB196F8);
  static const Color color_purple_875FE1 = Color(0xff875FE1);
  static const Color color_blue_0B71C8 = Color(0xff0B71C8);

}

//颜色配置
class BaseColor {
  static const Color THEME_COLOR = Colors.green; //主题色
  static const Color THEME_UNSELECT_COLOR = Colors.grey; //主题色未选中颜色
}
