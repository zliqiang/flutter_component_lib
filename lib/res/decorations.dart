import 'package:flutter_color/flutter_color.dart';

import '../Include.dart';

///盒子装饰
class BoxDecorations {
  ///椭圆矩形--实心  5-1
  static Decoration rectCircleTabSelect5_left(bool select) => new BoxDecoration(
    border: new Border.all(color: Colours.color_orange_EA8421, width: 1), // 边色与边宽度
    color:select? Colours.color_orange_EA8421:Colors.transparent, // 底色
    borderRadius: BorderRadius.only(
      topLeft: Radius.circular(5.0),
      bottomLeft: Radius.circular(5.0),
    ), // 圆角度
  );
  static Decoration rectCircleTabSelect_center(bool select) => new BoxDecoration(
    border:Border(
        top: BorderSide(color:  Colours.color_orange_EA8421, width: 1),
        bottom: BorderSide(color:  Colours.color_orange_EA8421, width: 1)),
    color:select? Colours.color_orange_EA8421:Colors.transparent, // 底色
  );
  ///椭圆矩形--实心  5-1
  static Decoration rectCircleTabSelect5_right(bool select) => new BoxDecoration(
    border: new Border.all(color: Colours.color_orange_EA8421, width: 1), // 边色与边宽度
    color:select? Colours.color_orange_EA8421:Colors.transparent, // 底色
    borderRadius: BorderRadius.only(
      topRight: Radius.circular(5.0),
      bottomRight: Radius.circular(5.0),
    ), // 圆角度
  );
  //================================================================================
  ///椭圆矩形--圆环
  static Decoration rectCircleHollow({double radius=8.0,double border=0,String color='#F06F2A'}) => new BoxDecoration(
    border: new Border.all(color:  HexColor(color), width: border), // 边色与边宽度
    color: Colors.transparent, // 底色
    borderRadius: new BorderRadius.circular(radius), // 圆角度
  );
  ///椭圆矩形--圆环  10-0.5
  static Decoration rectCircleHollowGray_10 = new BoxDecoration(
    border: new Border.all(color: Colours.color_gray_CCCCCC, width: 0.5), // 边色与边宽度
    color: Colors.transparent, // 底色
    borderRadius: new BorderRadius.circular((15.0)), // 圆角度
  );
  ///椭圆矩形--圆环  20-0.5
  static Decoration rectCircleHollow20 = new BoxDecoration(
    border: new Border.all(color: Colours.color_gray_CCCCCC, width: 0.5), // 边色与边宽度
    color: Colors.transparent, // 底色
    borderRadius: new BorderRadius.circular((20.0)), // 圆角度
  );
  ///椭圆矩形--圆环  5.0-1
  static Decoration rectCircleHollow5 = new BoxDecoration(
    border: new Border.all(color: Colours.color_orange_FBF5EA, width: 1), // 边色与边宽度
    color: Colours.color_orange_FBF5EA, // 底色
    borderRadius: new BorderRadius.circular((5.0)), // 圆角度
  );
  ///椭圆矩形--圆环  5.0-1
  static Decoration rectCircleHollowGray5 = new BoxDecoration(
    border: new Border.all(color: Colours.color_gray_CCCCCC, width: 1), // 边色与边宽度
    color: Colors.transparent, // 底色
    borderRadius: new BorderRadius.circular((5.0)), // 圆角度
  );
  ///椭圆矩形--圆环  20-0.5
  static Decoration rectCircleHollow10 = new BoxDecoration(
    border: new Border.all(color: Colours.color_orange_C59E66, width: 1.5), // 边色与边宽度
    color: Colors.transparent, // 底色
    borderRadius: new BorderRadius.circular((10.0)), // 圆角度
  );
  ///椭圆矩形--圆环  20-0.5
  static Decoration rectCircleHollow10_gray = new BoxDecoration(
    border: new Border.all(color: Colours.color_gray_8E8E8E, width: 1.5), // 边色与边宽度
    color: Colors.transparent, // 底色
    borderRadius: new BorderRadius.circular(10.0), // 圆角度
  );
  static Decoration rectCircleGray5_8E8E8E = new BoxDecoration(
    border: new Border.all(color: Colours.color_gray_8E8E8E, width: 0), // 边色与边宽度
    color: Colours.color_gray_8E8E8E, // 底色
    borderRadius: BorderRadius.only(
        topRight: Radius.circular(5.0),
        bottomLeft: Radius.circular(5.0),
        ),
  );
  ///椭圆矩形--实心  5-1
  static Decoration rectCircleWhile5 = new BoxDecoration(
    border: new Border.all(color: Colours.bg_while, width: 1), // 边色与边宽度
    color: Colours.bg_while, // 底色
    borderRadius: new BorderRadius.circular((5.0)), // 圆角度
  );
  ///椭圆矩形--实心  5-1
  static Decoration rectCircle5_aaGray = new BoxDecoration(
    border: new Border.all(color: Colours.color_black_aa252525, width: 1), // 边色与边宽度
    color: Colours.color_black_aa252525, // 底色
    borderRadius: new BorderRadius.circular((5.0)), // 圆角度
  );
  ///椭圆矩形--实心  5-1
  static Decoration rectCircle5_gray = new BoxDecoration(
    border: new Border.all(color: Colours.color_gray_F6F7FD, width: 1), // 边色与边宽度
    color: Colours.color_gray_F6F7FD, // 底色
    borderRadius: new BorderRadius.circular((5.0)), // 圆角度
  );
  ///椭圆矩形--实心  10-3
  static Decoration rectCircleWhile10_3 = new BoxDecoration(
    border: new Border.all(color: Colours.color_orange_D9B17C, width: 3), // 边色与边宽度
    color: Colours.bg_while, // 底色
    borderRadius: new BorderRadius.circular((10.0)), // 圆角度
  );
  ///椭圆矩形--实心  10
  static Decoration rectCircleWhile10 = new BoxDecoration(
    border: new Border.all(color:  HexColor('#cc000000'), width: 0), // 边色与边宽度
    color:  HexColor('#bb000000'), // 底色
    borderRadius: new BorderRadius.circular((10.0)), // 圆角度
  );
  ///椭圆矩形--实心  10
  static Decoration rectCircleOrange10 = new BoxDecoration(
    border: new Border.all(color:  HexColor('#F06F2A'), width: 0), // 边色与边宽度
    color:  HexColor('#F06F2A'), // 底色
    borderRadius: new BorderRadius.circular((10.0)), // 圆角度
  );
  ///椭圆矩形--实心  5-1
  static Decoration rectCircle5_grayf1 = new BoxDecoration(
    border: new Border.all(color: Colours.color_gray_F1F1F1, width: 1), // 边色与边宽度
    color: Colours.color_gray_F1F1F1, // 底色
    borderRadius: new BorderRadius.circular((5.0)), // 圆角度
  );
  ///椭圆矩形--实心  5.1
  static Decoration rectCircle5_red = new BoxDecoration(
    border: new Border.all(color: Colours.color_red_E02222, width: 1), // 边色与边宽度
    color: Colours.color_red_E02222, // 底色
    borderRadius: new BorderRadius.circular((5.0)), // 圆角度
  );

  ///椭圆矩形--頂部圆角--实心  30-0
  static Decoration rectCircle30 = new BoxDecoration(
    border: new Border.all(color: HexColor('#f0f0f0'), width: 0), // 边色与边宽度
    color: HexColor('#f0f0f0'), // 底色
    borderRadius: BorderRadius.only(
      topLeft: Radius.circular(30.0),
      topRight: Radius.circular(30.0),
    ), // 圆角度
  );
  ///椭圆矩形--頂部圆角--实心  30-0
  static Decoration rectCircle15_black = new BoxDecoration(
    border: new Border.all(color: HexColor('#f0f0f0'), width: 0), // 边色与边宽度
    color: HexColor('#f0f0f0'), // 底色
    borderRadius: BorderRadius.only(
      topLeft: Radius.circular(15.0),
      topRight: Radius.circular(15.0),
    ), // 圆角度
  );
//==========================  tab ========================================
  ///椭圆矩形--頂部圆角--底部外圆角--实心  15-0
  static Decoration rectCircle15 = new BoxDecoration(
    border: new Border.all(color: HexColor('#E0C18B'), width: 0), // 边色与边宽度
    color: HexColor('#E0C18B'), // 底色
    borderRadius: BorderRadius.only(
      topLeft: Radius.circular(15.0),
      topRight: Radius.circular(10.0),
    ),
  );
  ///椭圆矩形--頂部圆角--底部外圆角--实心  15-0
  static Decoration rectCircle15_off = new BoxDecoration(
    border: new Border.all(color: HexColor('#E0C18B'), width: 0), // 边色与边宽度
    color: HexColor('#E0C18B'), // 底色
    borderRadius: BorderRadius.only(
      topLeft: Radius.circular(15.0),
      bottomRight: Radius.circular(10.0),
    ),
  );

  ///椭圆矩形--頂部圆角--底部外圆角--实心  15-0
  static Decoration rectCircle15tab2 = new BoxDecoration(
    border: new Border.all(color: HexColor('#F1AF2F'), width: 0), // 边色与边宽度
    color: HexColor('#F1AF2F'), // 底色
    borderRadius: BorderRadius.only(
      topLeft: Radius.circular(10.0),
      topRight: Radius.circular(15.0),
    ),
  );

  ///椭圆矩形--頂部圆角--底部外圆角--实心  15-0
  static Decoration rectCircle15tab2_off = new BoxDecoration(
    border: new Border.all(color: HexColor('#F1AF2F'), width: 0), // 边色与边宽度
    color: HexColor('#F1AF2F'), // 底色
    borderRadius: BorderRadius.only(
      bottomLeft: Radius.circular(15.0),
      topRight: Radius.circular(10.0),
    ),
  );

  ///椭圆矩形--頂部圆角--单个角--实心  15-0
  static Decoration rectCircleAngle15 = new BoxDecoration(
    border: new Border.all(color: HexColor('#292929'), width: 0), // 边色与边宽度
    color: HexColor('#292929'), // 底色
    borderRadius: BorderRadius.only(
      bottomRight: Radius.circular(15.0),
    ),
  );

  ///椭圆矩形--頂部圆角--单个角--实心  15-0
  static Decoration rectCircleAngle15tab2 = new BoxDecoration(
    border: new Border.all(color: HexColor('#292929'), width: 0), // 边色与边宽度
    color: HexColor('#292929'), // 底色
    borderRadius: BorderRadius.only(
      bottomLeft: Radius.circular(15.0),
    ),
  );

  ///椭圆矩形--頂部圆角--单个角--实心  15-0
  static Decoration rectBorder = new BoxDecoration(
    border: new Border.all(color: HexColor('#E0C18B'), width: 0), // 边色与边宽度
    color: HexColor('#E0C18B'), // 底色
  );

  ///椭圆矩形--頂部圆角--单个角--实心  15-0
  static Decoration rectBordertab2 = new BoxDecoration(
    border: new Border.all(color: HexColor('#F1AF2F'), width: 0), // 边色与边宽度
    color: HexColor('#F1AF2F'), // 底色
  );
  //==================================================================================
  ///椭圆矩形--頂部圆角--底部外圆角--实心  5-0
  static Decoration rectCircle5 = new BoxDecoration(
    border: new Border.all(color: HexColor('#F6F5F2'), width: 0), // 边色与边宽度
    color: HexColor('#F6F5F2'), // 底色
    borderRadius: BorderRadius.only(
      bottomRight: Radius.circular(5.0),
      bottomLeft: Radius.circular(5.0),
    ),
  );
  ///椭圆矩形--頂部圆角--底部外圆角--实心  8-0
  static Decoration rectCircle8 = new BoxDecoration(
    border: new Border.all(color: HexColor('#1B1D23'), width: 0), // 边色与边宽度
    color: HexColor('#1B1D23'), // 底色
    borderRadius: BorderRadius.all(Radius.circular(8.0)),
  );
  ///椭圆矩形--頂部圆角--底部外圆角--实心  8-0
  static Decoration rectCircle12 = new BoxDecoration(
    border: new Border.all(color: HexColor('#1B1D23'), width: 0), // 边色与边宽度
    color: HexColor('#1B1D23'), // 底色
    borderRadius: BorderRadius.all(Radius.circular(12.0)),
  );
  ///椭圆矩形--頂部圆角--底部外圆角--实心  8-0
  static Decoration rectCircle8_yellow = new BoxDecoration(
    border: new Border.all(color: HexColor('#F06F2A'), width: 0), // 边色与边宽度
    color: HexColor('#F06F2A'), // 底色
    borderRadius: BorderRadius.all(Radius.circular(8.0)),
  );
  ///椭圆矩形--頂部圆角--自定义
  static Decoration rectCircleColor({double radius=8.0,double border=0,String color='#F06F2A'}) => new BoxDecoration(
    border: new Border.all(color: HexColor(color), width: border), // 边色与边宽度
    color: HexColor(color), // 底色
    borderRadius: BorderRadius.all(Radius.circular(radius)),
  );
  ///椭圆矩形--頂部圆角--底部外圆角--实心  10-0
  static Decoration rectCircle10 = new BoxDecoration(
    border: new Border.all(color:  Colours.bg_while, width: 0), // 边色与边宽度
    color:  Colours.bg_while, // 底色
    borderRadius: BorderRadius.only(
      topLeft: Radius.circular(10.0),
      topRight: Radius.circular(10.0),
    ),
  );


  ///线
  static BoxDecoration line({double width: 1, color: Colors.black12}) {
    return BoxDecoration(border: Border(bottom: BorderSide(width: width, color: color)));
  }

  ///空
  static BoxDecoration none() {
    return null;
  }

  ///椭圆矩形--Line
  static BoxDecoration ovalRectangle({double width: 1, color: Colors.black12, double circular: 20.0}) {
    return BoxDecoration(
        border: Border.all(color: color, width: width), //边框
        borderRadius: BorderRadius.all(
          //圆角
          Radius.circular(circular),
        ));
  }

  ///四边矩形 - 设置4条变
  static BoxDecoration fourSides({double width: 1, color: Colors.black12, double circular: 20.0}) {
    return BoxDecoration(
      border: Border(
          left: BorderSide(color: color, width: width),
          right: BorderSide(color: color, width: width),
          top: BorderSide(color: color, width: width),
          bottom: BorderSide(color: color, width: width)),
    );
  }
  ///四边矩形 - 设置4条变
  static BoxDecoration fourSidesBottom({double width: 1, color: Colors.black12}) {
    return BoxDecoration(
      border: Border(
          bottom: BorderSide(color: color, width: width)),
    );
  }

  ///椭圆矩形 - 圆角设置
  static BoxDecoration ovalRectangleArc({double topLeft: 5, double topRight: 5, double bottomLeft: 5, double bottomRight: 5}) {
    return BoxDecoration(
      color: Colors.blue,
      borderRadius: BorderRadius.only(
          topLeft: Radius.circular(topLeft),
          topRight: Radius.circular(topRight),
          bottomLeft: Radius.circular(bottomLeft),
          bottomRight: Radius.circular(bottomRight)),
    );
  }

  ///椭圆矩形 - 圆角设置 - 阴影
  static BoxDecoration boxShadow({double circular: 8.0, color}) {
    return BoxDecoration(
      color: color == null ? Color(0XFFEEEEEE) : color,
      borderRadius: BorderRadius.circular(8.0),
      boxShadow: [
        BoxShadow(
            color: color == null ? Color(0XFFEEEEEE) : color,
            offset: Offset(0, 0), //阴影xy轴偏移量
            blurRadius: 2.0, //阴影模糊程度
            spreadRadius: 1.0 //阴影扩散程度
            )
      ],
    );
  }

  ///椭圆矩形 - 圆角设置
  static BoxDecoration boxBackground({double circular: 8.0, color}) {
    return BoxDecoration(
      color: color == null ? Color(0XFFEEEEEE) : color,
      borderRadius: BorderRadius.circular(circular),
      boxShadow: [
        BoxShadow(
          color: color == null ? Color(0XFFEEEEEE) : color,
        )
      ],
    );
  }
}
