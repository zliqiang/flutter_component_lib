import 'package:event_bus/event_bus.dart';
import 'package:flutter_web_2021/entity/bean/Node.dart';
import 'package:flutter_web_2021/pages/home/HomeAppBarTabPage.dart';

EventBus eventBus = EventBus();

class HomeAppBarTabEvent{
  bool isOpen;
  HomeAppBarTabEvent(bool isOpen){
    this.isOpen =isOpen;
  }
}
class TreeEvent{
  bool isOpen;
  String headerItemBean;
  TreeEvent(String headerItemBean, bool isOpen){
    this.isOpen =isOpen;
    this.headerItemBean =headerItemBean;
  }
}
class TabEvent{
  Node node;
  String headerItemBean;
  TabEvent(String headerItemBean,Node node){
    this.headerItemBean =headerItemBean;
    this.node =node;
  }
}


//登录验证码
class LoginMSM{
}