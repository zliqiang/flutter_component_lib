import 'package:flutter/material.dart';
import 'package:flutter_web_2021/pages/component/basis/XFDashedLinePage.dart';
import 'package:flutter_web_2021/pages/home/HomeWebPage.dart';
import 'package:flutter_web_2021/pages/mmx/home/TabHomePage.dart';
import 'package:flutter_web_2021/pages/mmx/login/ForgetPwdPage.dart';
import 'package:flutter_web_2021/pages/mmx/login/LoginPage.dart';
import 'package:flutter_web_2021/pages/mmx/login/RegisteredPage.dart';
import 'package:flutter_web_2021/pages/mmx/vdieo/UpRefreshPageViewPage.dart';

// //所有路由
class Routes {
  static const String HOME_WEB_ROUTE_NAME = '/'; //WEB首页
  // static const String DIO_MVP_ROUTE_NAME = '/dioMvp'; //引导页路由
  static const String DIO_MVP_ROUTE_NAME = '/'; //引导页路由
}

//配置路由
final routes = {
  // '/home': (context,{arguments}) => TabHomePage(),
  '/home': (context,{arguments}) => UpRefreshPageViewPage(),
  Routes.HOME_WEB_ROUTE_NAME: (context,{arguments}) => HomeWebPage(),
};

// ignore: missing_return
Route onGenerateRoute(RouteSettings settings) {
  final Function pageContentBuilder = routes[settings.name];
  if (pageContentBuilder != null) {
    if (settings.arguments != null) {
      return MaterialPageRoute(
          builder: (context) =>
              pageContentBuilder(context, arguments: settings.arguments));
    } else {
      return MaterialPageRoute(
          builder: (context) => pageContentBuilder(context));
    }
  }
}
