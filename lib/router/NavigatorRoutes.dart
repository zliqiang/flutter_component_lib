//配置路由
import 'package:flutter_web_2021/Include.dart';
import 'package:flutter_web_2021/pages/component/async_/StreamBuilderPage.dart';
import 'package:flutter_web_2021/pages/component/widget_Interactive/NavigatorPage.dart';

final routesNavigator={
  '/': (context,{arguments}) => PageC(),
  '/d':(context,{arguments}) =>PageD(),
  '/StreamBuilder':(context,{arguments}) => StreamBuilderPage(),

};



MaterialPageRoute onGenerateRouteNavigator(RouteSettings settings) {
  WidgetBuilder builder;
  builder = (context) => PageD();
  final Function pageContentBuilder = routesNavigator[settings.name];
  if (pageContentBuilder != null) {
    if (settings.arguments != null) {
      return MaterialPageRoute(
          builder: (context) =>
              pageContentBuilder(context, arguments: settings.arguments));
    } else {
      return MaterialPageRoute(
          builder: (context) => pageContentBuilder(context));
    }
  }
}