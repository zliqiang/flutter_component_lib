class UserEntity {
  String id;
  String account;
  String realname;
  String password;
  String phone;
  String pic;
  String cardNo;
  String isAdmin;
  String status;
  String lastLoginIp;
  String errorCount;
  String errorLoginTime;
  String lastLoginTime;
  String nickname;
  String sex;
  String content;
  String vipCardNo;
  String createTime;
  int isAnchor;

  UserEntity(
      {this.id,
        this.account,
        this.realname,
        this.password,
        this.phone,
        this.pic,
        this.cardNo,
        this.isAdmin,
        this.status,
        this.lastLoginIp,
        this.errorCount,
        this.errorLoginTime,
        this.lastLoginTime,
        this.nickname,
        this.sex,
        this.content,
        this.vipCardNo,
        this.createTime,
        this.isAnchor});

  UserEntity.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    account = json['account'];
    realname = json['realname'];
    password = json['password'];
    phone = json['phone'];
    pic = json['pic'];
    cardNo = json['card_no'];
    isAdmin = json['is_admin'];
    status = json['status'].toString();
    lastLoginIp = json['last_login_ip'];
    errorCount = json['error_count'];
    errorLoginTime = json['error_login_time'];
    lastLoginTime = json['last_login_time'];
    nickname = json['nickname'];
    sex = json['sex'];
    content = json['content'];
    vipCardNo = json['vip_card_no'];
    createTime = json['create_time'];
    isAnchor = json['is_anchor'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['account'] = this.account;
    data['realname'] = this.realname;
    data['password'] = this.password;
    data['phone'] = this.phone;
    data['pic'] = this.pic;
    data['card_no'] = this.cardNo;
    data['is_admin'] = this.isAdmin;
    data['status'] = this.status;
    data['last_login_ip'] = this.lastLoginIp;
    data['error_count'] = this.errorCount;
    data['error_login_time'] = this.errorLoginTime;
    data['last_login_time'] = this.lastLoginTime;
    data['nickname'] = this.nickname;
    data['sex'] = this.sex;
    data['content'] = this.content;
    data['vip_card_no'] = this.vipCardNo;
    data['create_time'] = this.createTime;
    data['is_anchor'] = this.isAnchor;
    return data;
  }
}
