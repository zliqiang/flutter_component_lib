import '../../Include.dart';

class Node {
  int id;

  Widget view ;

  //跟节点的pid=0
  int pId = 0;
  String name;
  String subName;
  String dartString;

  // 树的层级
  int level;

  // 是否是展开
  bool isExpand = false;

  //是否是目录
  bool isparent;

  //图标
  Icon icon;

  Node parent;

  List<Node> children = new List();


  Node({this.id,this.pId,this.name,this.isparent,this.subName,this.isExpand,this.view,this.dartString});


  ///属否是根节点
  bool isRoot(){
    return parent ==null;
  }
  ///判断当前父节点的收缩状态
  bool isParentExpand(){
    if (parent == null)
      return false;
    return parent.isExpand;
  }
  ///是否是叶子节点
  bool isLeaf(){
    return children.length == 0;
  }
  ///得到当前节点的层级
  int getLevel()
  {
    return parent == null ? 0 : parent.getLevel() + 1;
  }

  ///设置展开
  void setExpand(bool isExpand)
  {
    this.isExpand = isExpand;
    if(!isExpand)
    {
      for(int i=0;i<children.length;i++){
        children[i].setExpand(false);
      }
    }
  }
  void setLevel(int level)
  {
    this.level = level;
  }

  String toString()
  {
    return "Node [id= ${id}, pId=${pId}, name=${name}, level=${level}, isExpand=${isExpand}, parent=${parent}, icon=${icon}]";
  }
}
