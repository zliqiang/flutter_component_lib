import 'package:ant_icons/ant_icons.dart';

import '../../Include.dart';
import 'MenuEntity.dart';
import 'Node.dart';

class TreeHelper {
  ///将用户的数据转化为树形数据
  static List<Node> convertDatas2Nodes(List<MenuData> datas) {
    List<Node> nodes = [];
    datas.forEach((value) {
      int id = -1;
      int pid = -1;
      String label = null;
      bool isparent = false;
      bool open = false;

      if (value.id != null) {
        id = value.id!=null?value.id:id;
      }
      if (value.pId != null) {
        pid = value.pId!=null?value.pId:pid;
      }
      if (value.name != null) {
        label = value.name!=null?value.name:label;
      }
      if (value.isParent != null) {



        isparent = value.isParent!=null?value.isParent:isparent;

      }
      if (value.open != null) {
        open = value.open!=null?value.open:open;
      }
      nodes.add(Node(
          id: id, pId: pid, name: label, isparent: isparent, isExpand: open,view:value.view ,subName: value.subName,dartString: value.dartPath));
    });
    /**
     *   设置Node间的节点关联关系
     */
    for (int i = 0; i < nodes.length; i++) {
      Node n = nodes[i];

      for (int j = i + 1; j < nodes.length; j++) {
        Node m = nodes[j];
        if (m.pId == n.id) {
          n.children.add(m);
          m.parent = n;
        } else if (m.id == n.pId) {
          m.children.add(n);
          n.parent = m;
        }
      }
    }
    //设置图标
    nodes.forEach((value) {
      setNodeIcon(value);
    });
    return nodes;
  }

  ///所有节点
  static List<Node> getSortedNodes(
      List<MenuData> datas, int defaultExpandLevel) {
    List<Node> result = [];
    List<Node> nodes = convertDatas2Nodes(datas);
    Log().info("1.nodes=" + nodes.length.toString());
    // 获得树的根结点
    List<Node> rootNodes = getRootNodes(nodes);
    Log().info("2.rootNodes=" + rootNodes.length.toString());
    rootNodes.forEach((node) {
      addNode(result, node, defaultExpandLevel, 1);
    });
    Log().info(result.length.toString());
    return result;
  }

  /// 过滤出可见的节点
  static List<Node> filterVisibleNodes(List<Node> nodes) {
    List<Node> result = [];

    nodes.forEach((node) {
      if (node.isRoot() || node.isParentExpand()) {
        setNodeIcon(node);
        if(node.isparent) {
          result.add(node);
        }
      }
    });
    return result;
  }
  /// 过滤出可见的节点
  static List<Node> filterVisibleNodesOne(List<Node> nodes) {
    List<Node> result = [];

    nodes.forEach((node) {
      if (node.isRoot() || node.isParentExpand()) {
        setNodeIcon(node);
        if(node.isparent) {
          result.add(node);
        }
      }
    });
    return result;
  }

  /// 把一个节点的所有孩子节点都放入result
  static void addNode(
      List<Node> result, Node node, int defaultExpandLevel, int currentLevel) {
    result.add(node);

    if (node.isparent) {
      node.setExpand(node.isExpand);
    } else {
      if (defaultExpandLevel >= currentLevel) {
        node.setExpand(node.isExpand);
      }
    }
    if (node.isLeaf()) return;

    for (int i = 0; i < node.children.length; i++) {
      addNode(result, node.children[i], defaultExpandLevel, currentLevel + 1);
    }
  }

  ///从所有节点中过滤出根节点
  static List<Node> getRootNodes(List<Node> nodes) {
    List<Node> root = [];
    nodes.forEach((node) {
      if (node.isRoot()) {
        root.add(node);
      }
    });
    return root;
  }

  ///为Node设置图标
  static void setNodeIcon(Node n) {
    if (n.isparent&& n.isExpand) {
      n.icon = Icon(
        AntIcons.folder_open,
        color: Colours.blue,
        size: 16,
      );
    } else if (n.isparent && !n.isExpand) {
      n.icon = Icon(
        AntIcons.folder,
        color: Colours.blue,
        size: 16,
      );
    } else {
      n.icon = Icon(
        AntIcons.file,
        color: Colours.blue,
        size: 16,
      );
    }
  }
}
