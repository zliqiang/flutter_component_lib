
import 'package:flutter_web_2021/Include.dart';

class MenuEntity {
  List<MenuData> data;
  String message;
  String status;

  MenuEntity({this.data, this.message, this.status});

  MenuEntity.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<MenuData>();
      json['data'].forEach((v) {
        data.add(new MenuData.fromJson(v));
      });
    }
    message = json['message'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    data['message'] = this.message;
    data['status'] = this.status;
    return data;
  }
}

class MenuData {
  bool isParent;
  Widget view;
  String name;
  String subName;
  int pId;
  int id;
  bool open;
  String iconOpen;
  String icon;
  String dartPath;

  MenuData(
      {this.isParent,
        this.view,
        this.name,
        this.subName,
        this.dartPath,
        this.pId,
        this.id,
        this.open,
        this.iconOpen,
        this.icon});

  MenuData.fromJson(Map<String, dynamic> json) {
    isParent = json['isParent'];
    view = json['view'];
    name = json['name'];
    subName = json['subName'];
    pId = json['pId'];
    id = json['id'];
    open = json['open'];
    iconOpen = json['iconOpen'];
    icon = json['icon'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['isParent'] = this.isParent;
    data['view'] = this.view;
    data['name'] = this.name;
    data['subName'] = this.subName;
    data['pId'] = this.pId;
    data['id'] = this.id;
    data['open'] = this.open;
    data['iconOpen'] = this.iconOpen;
    data['icon'] = this.icon;
    return data;
  }
}
