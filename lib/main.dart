import 'package:dokit/dokit.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_web_2021/Include.dart';
import 'package:flutter_web_2021/base/BasePage.dart';
import 'package:flutter_web_2021/pages/basic/redux/language/fz_localizations/localization_delegate.dart';
import 'package:flutter_web_2021/redux_state/locale/locale_redux.dart';
import 'package:flutter_web_2021/redux_state/redux_state.dart';
import 'package:flutter_web_2021/redux_state/user/user.dart';
import 'package:flutter_web_2021/router/AppRouter.dart';
import 'package:responsive_framework/responsive_wrapper.dart';
import 'package:responsive_framework/utils/scroll_behavior.dart';



void main() {
  //设置Android头部的导航栏透明
  SystemUiOverlayStyle systemUiOverlayStyle = SystemUiOverlayStyle(
      statusBarColor: Colors.transparent, //全局设置透明
      statusBarIconBrightness: Brightness.dark
    //light:黑色图标 dark：白色图标
    //在此处设置statusBarIconBrightness为全局设置
  );
  SystemChrome.setSystemUIOverlayStyle(systemUiOverlayStyle);

  /// 创建全局Store
  final store = Store<ReduxState>(
      getReduce,
      initialState: ReduxState(
        locale: Locale("zh", 'CN'),
        user: UserRedux.initData(),
        themeData: ThemeData(primaryColor: Colors.white),
      )
  );
  //状态管理
  //内测版本，目前提供了日志、method channel信息、路由信息、网络抓包、帧率、设备与内存信息查看、控件信息查看功能
  // DoKit.runApp(
  //     app: DoKitApp(MyApp(store)),
  //     // 是否在release包内使用，默认release包会禁用
  //     useInRelease: false,
  //     releaseAction: () => {
  //       // release模式下执行该函数，一些用到runZone之类实现的可以放到这里，该值为空则会直接调用系统的runApp(MyApp())，
  //     });

  runApp(MyApp(store));
}

class MyApp extends StatefulWidget {
  final Store<ReduxState> store;
  MyApp(this.store);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return  StoreProvider(
        store: widget.store,
        /// StoreBuilder后要跟上我们定义的那个State类，要不会报错，
        child: StoreBuilder<ReduxState>(builder: (BuildContext context, Store<ReduxState> store){
          return  MultiProvider(
            providers: [
              ChangeNotifierProvider(create: (_) => Counter()),
            ],
            child: OKToast(
              //2. wrap your app with OKToast
              textStyle: TextStyle(fontSize: 15.0, color: Colors.white),
              backgroundColor: Colors.grey,
              radius: 10.0,
              child: MaterialApp(
                builder: (context, widget) => ResponsiveWrapper.builder(
                    BouncingScrollWrapper.builder(context, widget),
                    maxWidth: 2460,
                    minWidth: 375,
                    defaultScale: true,
                    breakpoints: [
                      ResponsiveBreakpoint.resize(375, name: MOBILE),
                      // ResponsiveBreakpoint.autoScale(450, name: MOBILE),
                      ResponsiveBreakpoint.autoScale(800, name: TABLET),
                      ResponsiveBreakpoint.autoScale(1000, name: TABLET),
                      ResponsiveBreakpoint.resize(1200, name: DESKTOP),
                      ResponsiveBreakpoint.autoScale(2460, name: "4K"),
                    ],
                    background: Container(color: Color(0xFFF5F5F5))),
                localizationsDelegates: [
                  /// 本地化的代理类
                  GlobalMaterialLocalizations.delegate,
                  GlobalWidgetsLocalizations.delegate,
                  /// 注册我们的Delegate
                  FZLocalizationDelegate.delegate
                ],
                /// 这边设置 locale 主要是为了在项目里手动更改语言，如果是不想手动变更，而是要随系统自动变更可以注释
                locale: store.state.locale,
                /// 这边的皮肤数据主要从store中去取
                theme: store.state.themeData,
                supportedLocales: [
                  const Locale('en', 'US'), // 美国英语
                  const Locale('zh', 'CN'), // 中文简体
                ],
                /// 监听系统语言切换
                // ignore: missing_return
                localeListResolutionCallback: (deviceLocale, supportedLocales){
                  print('deviceLocale: $deviceLocale');
                  // 系统语言是英语： deviceLocale: [en_CN, en_CN, zh_Hans_CN]
                  // 系统语言是中文： deviceLocale: [zh_CN, zh_Hans_CN, en_CN]
                  print('supportedLocales: $supportedLocales');
                  /// 系统自动检测到语言后修改Store里的语言
                  if (deviceLocale.length > 0) {
                    if (store.state.locale.languageCode != deviceLocale[0].languageCode) {
                      store.dispatch(LocaleRefreshAction(deviceLocale[0]));
                    }
                  }
                },
                navigatorKey: BasePage.navigatorKey,
                debugShowCheckedModeBanner: false,
                title: 'Flutter Demo',
                // initialRoute: PlatformUtils.isAndroid||PlatformUtils.isIOS?'/home':"/",
                initialRoute: "/",
                onGenerateRoute: onGenerateRoute,
                // home: PageOne('模板方法设计模式',),
              ),
            ),
          );     })
    );
  }
}
