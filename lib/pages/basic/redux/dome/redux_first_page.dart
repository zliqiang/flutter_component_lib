/**
 *  Create by fazi
 *  Date: 2019-06-10
 */

import "package:flutter/material.dart";
import 'package:flutter_web_2021/redux_state/redux_state.dart';
import 'package:redux/redux.dart';
import 'package:flutter_redux/flutter_redux.dart';

import '../../../../Include.dart';
import 'redux_next_page.dart';


class ReduxFirstPage extends BasePage {
  static String dartPath = "lib/pages/basic/redux/dome/redux_first_page.dart";

  @override
  State createState() => FirstPageState();
  @override
  String get pageName => 'PageOne';
}

class FirstPageState extends BasePageState<ReduxFirstPage> {

  @override
  Widget build(BuildContext context) {

    return StoreBuilder<ReduxState>(
        builder: (BuildContext context, Store<ReduxState> store){
          return Scaffold(
            appBar: AppBar(
              title: Text("ReduxDemo3"),
            ),
            body: Center(
                child: Column(
                  children: <Widget>[
                    SizedBox(height: 50,),
                    Text("姓名是：" + store.state.user.name),
                    SizedBox(height: 100,),
                    FlatButton(
                        onPressed: (){
                          Navigator.of(context).push(MaterialPageRoute(builder: (context){
                            return ReduxNextPage();
                          }));
                        },
                        child: Text("下一页")
                    )
                  ],
                )
            ),
          );
        }
    );
  }
}