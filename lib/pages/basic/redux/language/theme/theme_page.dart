/**
 *  Create by fazi
 *  Date: 2019-06-10
 */
import "package:flutter/material.dart";
import 'package:flutter_web_2021/Include.dart';
import 'package:flutter_web_2021/redux_state/redux_state.dart';
import 'package:flutter_web_2021/redux_state/theme/theme_redux.dart';
import 'package:redux/redux.dart';
import 'package:flutter_redux/flutter_redux.dart';



class ThemePage extends BasePage {
  static String dartPath = "lib/pages/basic/redux/language/theme/theme_page.dart";

  @override
  String get pageName => '全局主题';

  @override
  State createState() => FirstPageState();
}

class FirstPageState extends BasePageState<ThemePage> {
  @override
  Widget build(BuildContext context) {


    return StoreBuilder<ReduxState>(
        builder: (BuildContext context, Store<ReduxState> store){
          return Scaffold(
            appBar: AppBar(
              title: Text(widget.pageName),
            ),
            body: Center(
                child: Column(
                  children: _btnList(store),
                )
            ),
          );
        }
    );
  }

  _btnList(Store store) {
    List<Widget> list = List();
    for(int i = 0; i < getThemeListColors().length; i++) {
      list.add(SizedBox(height: 20,));
      list.add(_buildBtns(store, i));
    }
    return list;
  }

  /// 改变皮肤颜色的同时也修改文本的颜色
  Widget _buildBtns(Store store, int index) {
    return FlatButton(
        onPressed: (){
          store.dispatch(ThemeRefreshAction(ThemeData(
            primaryColor: getThemeListColors()[index]
          )));
        },
        child: Text(
         getThemeListNames()[index],
          style: TextStyle(
            /// 文本的颜色从store中获取
            color: store.state.themeData.primaryColor
          ),
        )
    );
  }

  /// 颜色值数组
  static List<Color> getThemeListColors() {
    return [
      Colors.black,
      Colors.red,
      Colors.orange,
      Colors.brown,
      Colors.blue,
    ];
  }

  /// 颜色名数组
  static List<String> getThemeListNames() {
    return [
      "black",
      "red",
      "orange",
      "brown",
      "blue",
    ];
  }
}


