/**
 *  Create by fazi
 *  Date: 2019-06-10
 */
import 'package:flutter_web_2021/Include.dart';
import 'package:flutter_web_2021/redux_state/locale/locale_redux.dart';
import 'package:flutter_web_2021/redux_state/redux_state.dart';
import 'more_localization.dart';

class FirstPage extends BasePage {
  static String dartPath = "lib/pages/basic/redux/language/fz_localizations/first_page.dart";
  @override
  String get pageName => '语言切换';
  @override
  State createState() => FirstPageState();
}

class FirstPageState extends BasePageState<FirstPage> {


  @override
  Widget build(BuildContext context) {

    return
      StoreBuilder<ReduxState>(
          builder: (BuildContext context, Store<ReduxState> store){
            return Scaffold(
              appBar: AppBar(
                title: Text("ReduxDemo3"),
              ),
              body: Center(
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: 50,),
                      FlatButton(
                          onPressed: (){
                            store.dispatch(LocaleRefreshAction(Locale('zh','CH')));
                          },
                          child: Text('手动设置APP语言为：中文')
                      ),
                      SizedBox(height: 50,),
                      FlatButton(
                          onPressed: (){
                            store.dispatch(LocaleRefreshAction(Locale('en','US')));
                          },
                          child: Text('手动设置APP语言为：英文')
                      ),
                      SizedBox(height: 50,),
                      Text("MoreLocalization 当前语言：${MoreLocalization.of(context).currentLocalized.name}"),
                      SizedBox(height: 50,),
                      Text("Localizations 当前语言：${Localizations.localeOf(context)}"),
                      SizedBox(height: 50,),
                      Text("Store 当前语言：${store.state.locale.languageCode}"),
                    ],
                  )
              ),
            );
          }
      );
  }
}


