
import 'package:flutter_web_2021/Include.dart';

import 'LoadView.dart';

class BaseLoadView extends BasePage {
  static String dartPath = "lib/pages/basic/load_view/LoadViewPage.dart";

  Widget child;
  Future<int> future;
  int initialData;
  BaseLoadView({this.child,this.future,this.initialData});

  @override
  _LoadViewPageState createState() => _LoadViewPageState();

  @override
  String get pageName => '加载页面';
}

class _LoadViewPageState extends BasePageState<BaseLoadView> {
    // 实现一个路由，当该路由打开时我们从网上获取数据，获取数据时弹一个加载框；获取结束时，如果成功则显示获取到的数据，如果失败则显示错误。
    // 不真正去网络请求数据，而是模拟一下这个过程，隔3秒后返回一个字符串
    //   Future<int> mockNetworkData() async{
    //     return Future.delayed(Duration(seconds: 2), () => 0);
    //   }
  bool  firstTime = true;

  @override
  void initState() {
    super.initState();
    if(firstTime){
      Future.delayed(Duration(milliseconds: 300), (){
        setState(() {
          firstTime = false;
        });
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return  FutureBuilder<int>(
          future: widget.future,
          initialData: widget.initialData,
          builder:(BuildContext context, AsyncSnapshot snapshot){
            // 请求已结束
            if(snapshot.connectionState == ConnectionState.done){
              if(snapshot.hasError){
                // 请求失败，显示错误
                return LoadView(false);
              }else{
                if(snapshot.data==0){
                  // 请求成功，显示数据
                  return widget.child;
                }else   if(snapshot.data==-999){
                  return Container(
                    alignment: Alignment.center,
                    child: LoadView(true,height: 90),
                  ) ;
                }else {
                  // 请求失败，显示错误
                  return LoadView(false);
                }
              }
            }else  if(snapshot.connectionState == ConnectionState.active){
              // 请求未结束，显示loading
              return Container(
                alignment: Alignment.center,
                child: LoadView(true,height: 90),
              ) ;
            }else{
              // 请求未结束，显示loading
              // 请求未结束，显示loading
              return widget.initialData==null||widget.initialData==-999?Container(
                alignment: Alignment.center,
                child: LoadView(true,height: 90),
              ) :firstTime?Container(
                alignment: Alignment.center,
                child: LoadView(true,height: 90),
              ) : widget.child;
            }
          },
    );
  }

}

