import 'package:flutter_web_2021/Include.dart';
import 'package:loading_indicator_view/loading_indicator_view.dart';


class LoadView extends BasePage {
  static String dartPath = "lib/pages/basic/load_view/LoadView.dart";
  bool isLoading;
  Color color;
  double width;
  double height;
  LoadView( this.isLoading,{this. color, this. width, this. height});
  @override
  _LoadViewState createState() => _LoadViewState();

  @override
  String get pageName => '加载动画';
}

class _LoadViewState extends BasePageState<LoadView> {


  @override
  Widget build(BuildContext context) {
    return Container(
      color: widget.color,
      width:widget. width,
      height: widget.height,
      child: _LoadingLayout(isLoading: widget.isLoading),
    );
  }

}


class _LoadingLayout extends CustomSingleChildLayout {


  _LoadingLayout({Key key, bool isLoading})
      : super(
    key: key,
    delegate: _LoadingLayoutDelegate(),
    child: isLoading
        ? BallSpinFadeLoaderIndicator(ballColor: Colours.blue,minBallRadius:1,maxBallRadius:4)
        :  FlutterLogo(size: 50),
  );
}
//   this.minBallRadius: 1.6,
//     this.maxBallRadius: 5,
class _LoadingLayoutDelegate extends SingleChildLayoutDelegate {
  @override
  Size getSize(BoxConstraints constraints) {
    //获取父容器约束条件确定CustomSingleChildLayout大小
    // print('getSize constraints = $constraints');
    return super.getSize(constraints);
  }

  @override
  bool shouldRelayout(covariant SingleChildLayoutDelegate oldDelegate) {
    //是否需要relayout
    return false;
  }

  @override
  BoxConstraints getConstraintsForChild(BoxConstraints constraints) {
    //确定child的约束，用于确定child的大小
    print('getConstraintsForChild constraints = $constraints ');
    var childWidth = min(constraints.maxWidth, constraints.maxHeight);
    var childBoxConstraints = BoxConstraints.tight(
      Size(childWidth / 2, childWidth / 2),
    );
    // print('getConstraintsForChild childBoxConstraints = $childBoxConstraints ');
    return childBoxConstraints;
  }

  @override
  Offset getPositionForChild(Size size, Size childSize) {
    // 确定child的位置，返回一个相对于parent的偏移值
    // size是layout的大小，由getSize确定
    // childSize由getConstraintsForChild得出的Constraints对child进行约束，得到child自身的size
    // print('size = $size childSize = $childSize');
    var dx = (size.width - childSize.width) / 2;
    var dy = (size.height - childSize.height) / 2;
    // print('dx = $dx dy = $dy');
    return Offset(dx, dy);
  }
}

