import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_web_2021/base/base_mvp_page_state.dart';
import 'package:flutter_web_2021/pages/basic/load_view/LoadViewPage.dart';
import 'package:flutter_web_2021/utils/toast/OkToastUtil.dart';

import '../../../Include.dart';
import 'presenter/dio_page_presenter.dart';



class DioPage extends BasePage<String> {
  static String dartPath = "lib/pages/basic/mvp/dio_page.dart";
  @override
  State<StatefulWidget> createState() {
    return DioPageState();
  }
  @override
  String get pageName => 'Mvp请求';
}
class DioPageState extends BaseMvpPageState<DioPage, DioPagePresenter> {
  String result;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.pageName),
      ),
      body:BaseLoadView(
        initialData: presenter.futureStatus,
        future: presenter.future(),
        child:  Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            FlatButton(
                onPressed: () {
                  setState(() {
                    presenter.futureStatus =-999;
                  });
                  presenter.requestUserInfo();
                },
                child: Text("request user info")),
          ],
        ),
      ),
    );
  }

  void updateResponseView(String entity) {
      setState(() {
        ToastUtil.showToastCenter(entity);
      });

  }

  @override
  DioPagePresenter createPresenter() {
    return DioPagePresenter();
  }


}



