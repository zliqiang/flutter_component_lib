import 'package:cached_network_image/cached_network_image.dart';

import '../../../Include.dart';

class UpRefreshPageViewPage extends BasePage {

  static String dartPath = "lib/pages/mmx/vdieo/UpRefreshPageViewPage.dart";

  @override
  _UpRefreshPageViewPageState createState() => _UpRefreshPageViewPageState();

  @override
  String get pageName => 'pageView 下拉刷新';
}

class _UpRefreshPageViewPageState extends BasePageState<UpRefreshPageViewPage> {
  //PageView当前显示页面索引
  int currentPage = 0;
  final PageController _controller = new PageController(
    //用来配置PageView中默认显示的页面 从0开始
    initialPage: 0,
    //为true是保持加载的每个页面的状态
    keepPage: true,
  );


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    buildInitState();
  }

  @override
  String buildInitState() {
    //设置pageView 滑动监听
    _controller.addListener(() {
      if (_controller.position.pixels == _controller.position.maxScrollExtent) {
        print('滑动到了最底部');
        _getMore();
      }
    });
    return null;
  }

  @override
  Widget build(BuildContext context) {

    return RefreshIndicator(
      //下拉刷新触发方法
      onRefresh: _onRefresh,
      //设置listView
      child: Container(
        child: PageView(

          children: buildPageItemView(),

        ),
      ),
    );
  }

  /**
   * 下拉刷新方法,为list重新赋值
   */
  Future<Null> _onRefresh() async {
    await Future.delayed(Duration(seconds: 1), () {
      print('refresh');
      setState(() {});
    });
  }

  /**
   * 上拉加载更多
   */
  Future<Null> _getMore() async {
    await Future.delayed(Duration(seconds: 1), () {
      print('_getMore');
      setState(() {});
    });
  }

  buildPageItemView() {
    List<Widget> _pages = new List();
    for (int i = 0; i < 10; i++) {
      Widget item = new ConstrainedBox(
        constraints: const BoxConstraints.expand(),
        child: new CachedNetworkImage(
          width: double.infinity,
          height: double.infinity,
          fit: BoxFit.fill,
          imageUrl:
          "http://b-ssl.duitang.com/uploads/item/201311/02/20131102150044_YGB5u.jpeg",
          placeholder: (context, url) => new SizedBox(
            width: 24.0,
            height: 24.0,
            child: new CircularProgressIndicator(
              strokeWidth: 2.0,
            ),
          ),
          errorWidget: (context, url, error) => new Icon(Icons.error),
        ),
      );
      _pages.add(item);
    }
    return _pages;
  }

}