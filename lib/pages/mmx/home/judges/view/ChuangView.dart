import 'package:ant_icons/ant_icons.dart';
import 'package:flutter_color/flutter_color.dart';
import 'package:flutter_vant_kit/main.dart';
import 'package:flutter_web_2021/Include.dart';
import 'package:flutter_web_2021/base/base_mvp_page_state.dart';
import 'package:flutter_web_2021/pages/basic/load_view/LoadViewPage.dart';
import 'package:flutter_web_2021/pages/mmx/home/creation/CreationCenterPage.dart';
import 'package:flutter_web_2021/pages/mmx/home/judges/view/ChuangP.dart';
import 'package:flutter_web_2021/pages/mmx/home/publish_works/MatchListPage.dart';
import 'package:flutter_web_2021/pages/mmx/home/publish_works/PublishWorksOkPage.dart';
import 'package:flutter_web_2021/widgets/img/ImagesViews.dart';
import 'package:getwidget/components/button/gf_button.dart';
import 'package:getwidget/shape/gf_button_shape.dart';

class ChuangView extends BasePage {
  @override
  State<StatefulWidget> createState() {
    return ChuangViewState();
  }
  @override
  String get pageName => 'PingView';
}

class ChuangViewState extends BaseMvpPageState<ChuangView,ChuangP> with AutomaticKeepAliveClientMixin{


  @protected
  bool get wantKeepAlive => true;
  @override
  ChuangP createPresenter() {
    return ChuangP();
  }

  List levelPrivileges = [
    {
      "index": 0,
      "showRank": true,
      "title": "最美女神评选",
      "imgUrl":
          "https://tse1-mm.cn.bing.net/th/id/R-C.d9522d356a538d12550bfc638abcba82?rik=5ctIvNjQV1q9bg&riu=http%3a%2f%2fimage2.sina.com.cn%2fent%2fy%2fp%2f2006-07-01%2fU1735P28T3D1143020F326DT20060701184010.jpg&ehk=vX%2fRQTI6nLFxuhrZxu0NUcLFYTpZpJZKKPgrn3bh38g%3d&risl=&pid=ImgRaw&r=0",
    },
    {
      "index": 1,
      "showRank": true,
      "title": "最具智慧明星评选",
      "imgUrl":
          "https://tse1-mm.cn.bing.net/th/id/R-C.544a3ba7aaad86e32e5ffa6250d9e900?rik=dMcOKOHaTmlfeQ&riu=http%3a%2f%2fpic3.16pic.com%2f00%2f54%2f83%2f16pic_5483144_b.jpg&ehk=sQIrwd4bdBVClyRCyntdnqbDlxzVHYd69RXsAlXYHSs%3d&risl=&pid=ImgRaw&r=0"
    },
    {
      "index": 2,
      "showRank": true,
      "title": "最具智慧明星评选",
      "imgUrl":
          "https://tse1-mm.cn.bing.net/th/id/R-C.076d29ef0839d9ca336d8004c82e93bb?rik=QoTeWP1a2UyHdw&riu=http%3a%2f%2fnews.cri.cn%2fmmsource%2fimages%2f2011%2f07%2f15%2f7414e2a06df840ba994055507c7c6393.jpg&ehk=maLqu%2bPtqGmQ9le1rvOwyJsi%2fDbYMPuKK5v1D9gh0FI%3d&risl=&pid=ImgRaw&r=0",
    },
    {
      "index": 3,
      "showRank": false,
      "title": "最美汉服评选",
      "imgUrl": "https://gss0.baidu.com/-fo3dSag_xI4khGko9WTAnF6hhy/zhidao/pic/item/2e2eb9389b504fc213303776e8dde71190ef6d52.jpg",
    },
    {
      "index": 4,
      "showRank": true,
      "title": "最美女神评选",
      "imgUrl":
          "https://tse1-mm.cn.bing.net/th/id/R-C.d9522d356a538d12550bfc638abcba82?rik=5ctIvNjQV1q9bg&riu=http%3a%2f%2fimage2.sina.com.cn%2fent%2fy%2fp%2f2006-07-01%2fU1735P28T3D1143020F326DT20060701184010.jpg&ehk=vX%2fRQTI6nLFxuhrZxu0NUcLFYTpZpJZKKPgrn3bh38g%3d&risl=&pid=ImgRaw&r=0",
    },
    {
      "index": 5,
      "showRank": true,
      "title": "最具智慧明星评选",
      "imgUrl":
          "https://tse1-mm.cn.bing.net/th/id/R-C.544a3ba7aaad86e32e5ffa6250d9e900?rik=dMcOKOHaTmlfeQ&riu=http%3a%2f%2fpic3.16pic.com%2f00%2f54%2f83%2f16pic_5483144_b.jpg&ehk=sQIrwd4bdBVClyRCyntdnqbDlxzVHYd69RXsAlXYHSs%3d&risl=&pid=ImgRaw&r=0"
    },
    {
      "index": 6,
      "showRank": true,
      "title": "最具智慧明星评选",
      "imgUrl":
          "https://tse1-mm.cn.bing.net/th/id/R-C.076d29ef0839d9ca336d8004c82e93bb?rik=QoTeWP1a2UyHdw&riu=http%3a%2f%2fnews.cri.cn%2fmmsource%2fimages%2f2011%2f07%2f15%2f7414e2a06df840ba994055507c7c6393.jpg&ehk=maLqu%2bPtqGmQ9le1rvOwyJsi%2fDbYMPuKK5v1D9gh0FI%3d&risl=&pid=ImgRaw&r=0",
    },
    {
      "index": 7,
      "showRank": false,
      "title": "最美汉服评选",
      "imgUrl": "https://gss0.baidu.com/-fo3dSag_xI4khGko9WTAnF6hhy/zhidao/pic/item/2e2eb9389b504fc213303776e8dde71190ef6d52.jpg",
    },
  ];
  List participateWorks = [
    {
      "votes": "6000",
      "status": "投稿中",
      "label": "中国赛区",
      "period": "2021年5月第一期",
      "content": "去过很多次沙漠,印象最深的是17年中秋那天,从阿尔及利亚首都向南,飞2小时时15分钟,深入撒利亚首都向南,飞2小时时15分钟,深入撒",
      "title": "最美女神",
      "imgUrl":
          "https://tse1-mm.cn.bing.net/th/id/R-C.544a3ba7aaad86e32e5ffa6250d9e900?rik=dMcOKOHaTmlfeQ&riu=http%3a%2f%2fpic3.16pic.com%2f00%2f54%2f83%2f16pic_5483144_b.jpg&ehk=sQIrwd4bdBVClyRCyntdnqbDlxzVHYd69RXsAlXYHSs%3d&risl=&pid=ImgRaw&r=0"
    },
    {
      "votes": "6000",
      "status": "投稿中",
      "label": "中国赛区",
      "period": "2021年5月第一期",
      "content": "去过很多次沙漠,印象最深的是17年中秋那天,从阿尔及利亚首都向南,飞2小时时15分钟,深入撒利亚首都向南,飞2小时时15分钟,深入撒",
      "title": "最美女神",
      "imgUrl":
          "https://tse1-mm.cn.bing.net/th/id/R-C.544a3ba7aaad86e32e5ffa6250d9e900?rik=dMcOKOHaTmlfeQ&riu=http%3a%2f%2fpic3.16pic.com%2f00%2f54%2f83%2f16pic_5483144_b.jpg&ehk=sQIrwd4bdBVClyRCyntdnqbDlxzVHYd69RXsAlXYHSs%3d&risl=&pid=ImgRaw&r=0"
    },
    {
      "votes": "6000",
      "status": "投稿中",
      "label": "中国赛区",
      "period": "2021年5月第一期",
      "content": "去过很多次沙漠,印象最深的是17年中秋那天,从阿尔及利亚首都向南,飞2小时时15分钟,深入撒利亚首都向南,飞2小时时15分钟,深入撒",
      "title": "最美女神",
      "imgUrl":
          "https://tse1-mm.cn.bing.net/th/id/R-C.544a3ba7aaad86e32e5ffa6250d9e900?rik=dMcOKOHaTmlfeQ&riu=http%3a%2f%2fpic3.16pic.com%2f00%2f54%2f83%2f16pic_5483144_b.jpg&ehk=sQIrwd4bdBVClyRCyntdnqbDlxzVHYd69RXsAlXYHSs%3d&risl=&pid=ImgRaw&r=0"
    },
    {
      "votes": "6000",
      "status": "投稿中",
      "label": "中国赛区",
      "period": "2021年5月第一期",
      "content": "去过很多次沙漠,印象最深的是17年中秋那天,从阿尔及利亚首都向南,飞2小时时15分钟,深入撒利亚首都向南,飞2小时时15分钟,深入撒",
      "title": "最美女神",
      "imgUrl":
          "https://tse1-mm.cn.bing.net/th/id/R-C.544a3ba7aaad86e32e5ffa6250d9e900?rik=dMcOKOHaTmlfeQ&riu=http%3a%2f%2fpic3.16pic.com%2f00%2f54%2f83%2f16pic_5483144_b.jpg&ehk=sQIrwd4bdBVClyRCyntdnqbDlxzVHYd69RXsAlXYHSs%3d&risl=&pid=ImgRaw&r=0"
    },
    {
      "votes": "6000",
      "status": "投稿中",
      "label": "中国赛区",
      "period": "2021年5月第一期",
      "content": "去过很多次沙漠,印象最深的是17年中秋那天,从阿尔及利亚首都向南,飞2小时时15分钟,深入撒利亚首都向南,飞2小时时15分钟,深入撒",
      "title": "最美女神",
      "imgUrl":
          "https://tse1-mm.cn.bing.net/th/id/R-C.544a3ba7aaad86e32e5ffa6250d9e900?rik=dMcOKOHaTmlfeQ&riu=http%3a%2f%2fpic3.16pic.com%2f00%2f54%2f83%2f16pic_5483144_b.jpg&ehk=sQIrwd4bdBVClyRCyntdnqbDlxzVHYd69RXsAlXYHSs%3d&risl=&pid=ImgRaw&r=0"
    },
    {
      "votes": "6000",
      "status": "投稿中",
      "label": "中国赛区",
      "period": "2021年5月第一期",
      "content": "去过很多次沙漠,印象最深的是17年中秋那天,从阿尔及利亚首都向南,飞2小时时15分钟,深入撒利亚首都向南,飞2小时时15分钟,深入撒",
      "title": "最美女神",
      "imgUrl":
          "https://tse1-mm.cn.bing.net/th/id/R-C.544a3ba7aaad86e32e5ffa6250d9e900?rik=dMcOKOHaTmlfeQ&riu=http%3a%2f%2fpic3.16pic.com%2f00%2f54%2f83%2f16pic_5483144_b.jpg&ehk=sQIrwd4bdBVClyRCyntdnqbDlxzVHYd69RXsAlXYHSs%3d&risl=&pid=ImgRaw&r=0"
    },
  ];

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(milliseconds:300), (){
      setState(() {
        presenter.futureStatus = 0;
      });
    });

  }


  ///按鈕
  Widget btns() => Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          UnconstrainedBox(
              child: Container(
            width: 140,
            height: 40,
            child: GFButton(
              onPressed: () {
                CreationCenterPage().active();
              },
              text: "创作中心",
              color: Colours.color_purple_B196F8,
              textStyle: TextStyles.btnTextWhile15,
              icon: Image.asset(
                "asset/image/home/ico_cczx.png",
                width: 30,
                height: 30,
              ),
              shape: GFButtonShape.standard,
            ),
          )),
          Gaps.wGap22,
          UnconstrainedBox(
            child: Container(
              width: 140,
              height: 40,
              child: GFButton(
                onPressed: () {
                  AppNavigator.push(context, MatchListPage());

                },
                text: "发表作品",
                icon: Image.asset(
                  "asset/image/home/ico_fbzp.png",
                  width: 30,
                  height: 30,
                ),
                textStyle: TextStyles.btnTextWhile15,
                color: Colours.color_orange_E6BE78,
                shape: GFButtonShape.standard,
              ),
            ),
          ),
        ],
      );

  //我的成绩 item
  Widget myScoreItem(String imgUrl, String title, String content, String unit,
          {bool showRedPoint = false, bool showSide = true, bool showSmall = false}) =>
      Container(
        height: 85,
        child: Stack(
          children: [
            Container(
              decoration: showSide ? BoxDecorations.rectCircleHollow10 : null,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Padding(
                    padding: EdgeInsets.only(bottom: 3),
                    child: Image.asset(
                      imgUrl,
                      width: 16,
                      height: 16,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(bottom: 4),
                    child: Text(
                      title,
                      style: TextStyles.btnText11_666666,
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    // crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Baseline(
                        baseline: showSmall ? 12 : 14,
                        baselineType: TextBaseline.alphabetic,
                        child: Text(
                          content,
                          style: showSmall ? TextStyles.btnText12B_4A4A4A : TextStyles.btnText14B_4A4A4A,
                        ),
                      ),
                      Baseline(
                        baseline: showSmall ? 11 : 13,
                        baselineType: TextBaseline.alphabetic,
                        child: Text(
                          unit,
                          style: showSmall ? TextStyles.btnText8_4A4A4A : TextStyles.btnText10_4A4A4A,
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
            Offstage(
              offstage: showRedPoint ? false : true,
              child: Align(
                child: Container(
                  decoration: BoxDecorations.rectCircle5_red,
                  width: 8,
                  height: 8,
                ),
                alignment: Alignment.topRight,
              ),
            ),
          ],
        ),
      );

  ///我的成绩
  Widget myScore() => Column(
        children: [
          Container(
            margin: Dimens.edgeInsets_lr_dp20,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image.asset(
                  "asset/image/home/ioc_crown_red.png",
                  width: 20,
                  height: 20,
                ),
                Gaps.hGap4,
                Text(
                  "我的等级",
                  style: TextStyles.btnText15B_333333,
                ),
                Gaps.hGap4,
                Expanded(
                  child: Gaps.hGap4,
                ),
                Baseline(
                  baseline: 11,
                  baselineType: TextBaseline.alphabetic,
                  child: Text(
                    "更多成绩",
                    style: TextStyles.btnText11_666666,
                  ),
                ),
                Baseline(
                  baseline: 6,
                  baselineType: TextBaseline.alphabetic,
                  child: Icon(
                    AntIcons.right_outline,
                    color: Colours.color_gray_666666,
                    size: 10,
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 20),
          Padding(
            padding: Dimens.edgeInsets_lr_dp15,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(child: myScoreItem("asset/image/home/ico_grade_01.png", "发表作品", "35", "票")),
                Gaps.hGap11,
                Expanded(child: myScoreItem("asset/image/home/ico_grade_02.png", "累计得票", "35", "万票")),
                Gaps.hGap11,
                Expanded(child: myScoreItem("asset/image/home/ico_grade_03.png", "参加赛事", "16", "个")),
                Gaps.hGap11,
                Expanded(child: myScoreItem("asset/image/home/ico_grade_04.png", "获奖次数", "35", "个")),
              ],
            ),
          ),
          SizedBox(height: 14),
          Padding(
            padding: Dimens.edgeInsets_lr_dp15,
            child: Container(
              decoration: BoxDecorations.rectCircleHollow10,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(child: myScoreItem("asset/image/home/ico_grade_05.png", "发出红包", "1千份", "/千元", showSide: false, showSmall: true)),
                  Gaps.hGap11,
                  Expanded(child: myScoreItem("asset/image/home/ico_grade_06.png", "被赞助红包", "1千份", "/千元", showSide: false, showSmall: true)),
                  Gaps.hGap11,
                  Expanded(child: myScoreItem("asset/image/home/ico_grade_07.png", "被领取红包", "500份", "/500元", showSide: false, showSmall: true)),
                  Gaps.hGap11,
                  Expanded(child: myScoreItem("asset/image/home/ico_grade_08.png", "剩余红包", "500份", "/500元", showSide: false, showSmall: true)),
                ],
              ),
            ),
          ),
          SizedBox(height: 14),
          Padding(
            padding: Dimens.edgeInsets_lr_dp15,
            child: Row(
              children: [
                Expanded(
                  child: Container(
                    decoration: BoxDecorations.rectCircleHollow10,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Expanded(child: myScoreItem("asset/image/home/ico_grade_09.png", "关注评委", "26500", "位", showSide: false)),
                        Gaps.hGap11,
                        Expanded(child: myScoreItem("asset/image/home/ico_grade_10.png", "粉丝评委", "12300", "票", showSide: false)),
                      ],
                    ),
                  ),
                ),
                Gaps.hGap11,
                Expanded(
                  child: Container(
                    decoration: BoxDecorations.rectCircleHollow10,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Expanded(child: myScoreItem("asset/image/home/ico_grade_11.png", "评贝余额", "1896.00", "", showSide: false)),
                        Gaps.hGap11,
                        Expanded(
                          child: Container(
                            height: 22,
                            margin: EdgeInsets.only(left: 15, right: 15),
                            child: GFButton(
                              padding: EdgeInsets.only(left: 10, right: 10),
                              color: HexColor('#F56924'),
                              borderShape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(12),
                                side: BorderSide(
                                  width: 0,
                                  color: HexColor('#F56924'),
                                  style: BorderStyle.solid,
                                ),
                              ),
                              textStyle: TextStyles.btnTextWhile10,
                              fullWidthButton: false,
                              size: 22,
                              text: "提现",
                              onPressed: () async {},
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 14),
          Padding(
            padding: Dimens.edgeInsets_lr_dp15,
            child: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(child: myScoreItem("asset/image/home/ico_grade_12.png", "评论", "28900", "条", showRedPoint: true)),
                  Gaps.hGap11,
                  Expanded(
                      child: myScoreItem(
                    "asset/image/home/ico_grade_13.png",
                    "指点",
                    "3548",
                    "条",
                  )),
                  Gaps.hGap11,
                  Expanded(child: myScoreItem("asset/image/home/ico_grade_14.png", "收到礼物", "5478", "币", showRedPoint: true)),
                  Gaps.hGap11,
                  Expanded(child: myScoreItem("asset/image/home/ico_grade_15.png", "被打赏", "6579", "币", showRedPoint: true)),
                ],
              ),
            ),
          ),
        ],
      );

  //热门赛事item
  Widget popularMatchItem({bool offstage = false, int index, String imgUrl, String title, double width}) => Container(
        height: 265,
        child: Container(
          width: width,
          height: 241,
          child: Stack(
            children: [
              Container(
                margin: EdgeInsets.only(top: 10.0),
                child: Column(
                  children: [
                    Container(
                      height: 205,
                      child: ImagesViews.customizeRectangle(
                        imgUrl,
                        borderRadius: 10.0,
                      ),
                    ),
                    Container(
                      padding: Dimens.edgeInsets_lr_dp14,
                      decoration: BoxDecorations.rectCircle5,
                      height: 40,
                      child: Row(
                        children: [
                          Text(
                            title,
                            style: TextStyles.btnText12B_333333,
                          ),
                          Expanded(
                            child: Gaps.vGap20,
                          ),
                          Icon(
                            AntIcons.right_outline,
                            color: Colours.color_gray_CCCCCC,
                            size: 11,
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Positioned(
                child: Offstage(
                  offstage: index > 2 ? true : !offstage,
                  child: index > 2
                      ? Container()
                      : Image.asset(
                          "asset/image/home/ico_top" + (index + 1).toString() + ".png",
                          width: 47,
                          height: 26,
                        ),
                ),
                top: 0,
                left: 5,
              ),
            ],
          ),
        ),
      );

  //热门赛事 list
  Widget popularMatchList() {
    double margin = 15;
    double spacing = 13;
    double runSpacing = 5;
    int row = 2;
    double itemWidth = (Dimens.screenWidth() - (margin * 2) - (spacing * row - 1)) / row;
    return Wrap(
      runSpacing: runSpacing,
      spacing: spacing,
      children: levelPrivileges.map((e) {
        //Map<String, dynamic> responseData = jsonDecode(e);
        // UserEntity userEntity = UserEntity.fromJson(responseData);
        return popularMatchItem(index: e["index"], offstage: e["showRank"], imgUrl: e["imgUrl"], title: e["title"], width: itemWidth);
      }).toList(),
    );
  }

  ///热门赛事
  Widget popularMatch() => Column(
        children: [
          Container(
            margin: Dimens.edgeInsets_lr_dp20,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image.asset(
                  "asset/image/home/ico_rmss.png",
                  width: 20,
                  height: 20,
                ),
                Gaps.hGap4,
                Text(
                  "热门赛事",
                  style: TextStyles.btnText15B_333333,
                ),
                Gaps.hGap4,
                Expanded(
                  child: Gaps.hGap4,
                ),
                Baseline(
                  baseline: 11,
                  baselineType: TextBaseline.alphabetic,
                  child: Text(
                    "更多赛事 ",
                    style: TextStyles.btnText11_666666,
                  ),
                ),
                Baseline(
                  baseline: 6,
                  baselineType: TextBaseline.alphabetic,
                  child: Icon(
                    AntIcons.right_outline,
                    color: Colours.color_gray_666666,
                    size: 10,
                  ),
                ),
              ],
            ),
          ),
          Gaps.vGap12,
          Container(
            margin: Dimens.edgeInsets_lr_dp15,
            alignment: Alignment.center,
            child: popularMatchList(),
          ),
        ],
      );

  //参赛作品 item
  Widget participateWorksItem(String imgUrl, String title, String content, {String period, String label, String status, String votes}) => Container(
      height: 218,
      child: Row(
        children: [
          Container(
            width: 157,
            height: 218,
            child: Stack(
              children: [
                Container(
                  width: 157,
                  height: 218,
                  child: ImagesViews.rectangle(
                    imgUrl,
                    borderRadius: 10.0,
                  ),
                ),
                Align(
                  child: Image.asset(
                    "asset/image/home/ico_bf.png",
                    width: 25,
                    height: 25,
                  ),
                ),
                Positioned(
                  child: Text(
                    "11:20",
                    style: TextStyles.btnTextWhile8,
                  ),
                  bottom: 10.0,
                  right: 10.0,
                ),
              ],
            ),
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(left: 17),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    title,
                    style: TextStyles.btnText15B_333333,
                  ),
                  Gaps.vGap8,
                  Text(
                    period,
                    style: TextStyles.btnText12_333333,
                  ),
                  Gaps.vGap8,
                  Row(
                    children: [
                      Container(
                        child: Baseline(
                          baseline: 12,
                          baselineType: TextBaseline.alphabetic,
                          // child: Image.asset("asset/image/home/vip.png", width: 10, height: 10),
                          child: Text(
                            label,
                            style: TextStyles.btnTextWhile11,
                          ),
                        ),
                        alignment: Alignment.center,
                        decoration: BoxDecorations.rectCircle8,
                        padding: EdgeInsets.only(left: 8, right: 8, top: 1, bottom: 1),
                      ),
                      Expanded(
                        child: Gaps.vGap8,
                      ),
                      Baseline(
                        baseline: 12,
                        baselineType: TextBaseline.alphabetic,
                        // child: Image.asset("asset/image/home/vip.png", width: 10, height: 10),
                        child: Text(
                          status,
                          style: TextStyles.btnTextRed11,
                        ),
                      ),
                    ],
                  ),
                  Gaps.vGap12,
                  Dividers.dividerHorizontal(height: 0.5, color: Colours.color_gray_CCCCCC),
                  Expanded(child: SizedBox(height: 27)),
                  Row(
                    children: [
                      Baseline(
                        baseline: 12,
                        baselineType: TextBaseline.alphabetic,
                        // child: Image.asset("asset/image/home/vip.png", width: 10, height: 10),
                        child: Text(
                          "当前得票：",
                          style: TextStyles.btnText11_333333,
                        ),
                      ),
                      Baseline(
                        baseline: 15,
                        baselineType: TextBaseline.alphabetic,
                        // child: Image.asset("asset/image/home/vip.png", width: 10, height: 10),
                        child: Text(
                          votes,
                          style: TextStyles.btnTextRed15B,
                        ),
                      ),
                      Baseline(
                        baseline: 12,
                        baselineType: TextBaseline.alphabetic,
                        // child: Image.asset("asset/image/home/vip.png", width: 10, height: 10),
                        child: Text(
                          "票",
                          style: TextStyles.btnText11_333333,
                        ),
                      ),
                    ],
                  ),
                  Expanded(child: SizedBox(height: 27)),
                  Dividers.dividerHorizontal(height: 0.5, color: Colours.color_gray_CCCCCC),
                  SizedBox(height: 15),
                  Text(
                    content,
                    style: TextStyles.btnText11_333333,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 3,
                  ),
                  SizedBox(height: 5),
                ],
              ),
            ),
          ),
        ],
      ));

  Widget participateWorksView() => ListView.separated(
      padding: EdgeInsets.all(0),
      shrinkWrap: true,
      physics: new NeverScrollableScrollPhysics(),
      itemCount: participateWorks.length,
      scrollDirection: Axis.vertical,
      separatorBuilder: (BuildContext context, int index) {
        return Dividers.dividerHorizontal(height: 10, color: Colors.transparent);
      },
      itemBuilder: (BuildContext context, int index) {
        return participateWorksItem(participateWorks[index]["imgUrl"], participateWorks[index]["title"], participateWorks[index]["content"],
            period: participateWorks[index]["period"],
            label: participateWorks[index]["label"],
            status: participateWorks[index]["status"],
            votes: participateWorks[index]["votes"]);
      });

  @override
  Widget build(BuildContext context) {
    // RepaintBoundary()提高paint效率

    return Scaffold(
      body: Container(
        color: Colours.bg_while,
        child: Stack(
            children: [
              Container(
                height: 150,
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        colors: [HexColor('#F1AF2F'), Colours.bg_while, Colours.bg_while],
                        begin: Alignment.topRight,
                        end: Alignment.bottomRight,
                        tileMode: TileMode.mirror)),
              ),
        BaseLoadView(
          initialData: presenter.futureStatus,
          future: presenter.future(),
          child:
          CustomScrollView(
                slivers: [
                  SliverList(
                    delegate: SliverChildBuilderDelegate((content, index) {
                      return Column(
                        children: [
                          SizedBox(height: 29),
                          btns(),
                          SizedBox(height: 35),
                          myScore(),
                          Gaps.vGap20,
                          Dividers.dividerHorizontal(height: 12, color: HexColor('#F1F1F1')),
                          Gaps.vGap20,
                          popularMatch(),
                          Gaps.vGap20,
                          Dividers.dividerHorizontal(height: 12, color: HexColor('#F1F1F1')),
                          Gaps.vGap20,
                          Container(
                            margin: Dimens.edgeInsets_lr_dp20,
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Image.asset(
                                  "asset/image/home/ico_cczp.png",
                                  width: 20,
                                  height: 20,
                                ),
                                Gaps.hGap4,
                                Text(
                                  "参赛作品",
                                  style: TextStyles.btnText15B_333333,
                                ),
                                Gaps.hGap4,
                                Expanded(
                                  child: Gaps.hGap4,
                                ),
                                Baseline(
                                  baseline: 11,
                                  baselineType: TextBaseline.alphabetic,
                                  child: Text(
                                    "更多赛事 ",
                                    style: TextStyles.btnText11_666666,
                                  ),
                                ),
                                Baseline(
                                  baseline: 6,
                                  baselineType: TextBaseline.alphabetic,
                                  child: Icon(
                                    AntIcons.right_outline,
                                    color: Colours.color_gray_666666,
                                    size: 10,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Gaps.vGap20,
                          Padding(
                            padding: Dimens.edgeInsets_lr_dp20,
                            child: participateWorksView(),
                          ),
                        ],
                      );
                    }, childCount: 1),
                  ),
                  SliverList(
                    delegate: SliverChildBuilderDelegate((content, index) {
                      return Padding(
                          padding: EdgeInsets.only(left: 20, right: 20, top: 5, bottom: 5),
                          child: participateWorksItem(
                              participateWorks[index]["imgUrl"], participateWorks[index]["title"], participateWorks[index]["content"],
                              period: participateWorks[index]["period"],
                              label: participateWorks[index]["label"],
                              status: participateWorks[index]["status"],
                              votes: participateWorks[index]["votes"]));
                    }, childCount: participateWorks.length),
                  ),
                  SliverList(
                    delegate: SliverChildBuilderDelegate((content, index) {
                      return Padding(
                          padding: EdgeInsets.only(left: 20, right: 20, top: 5, bottom: 5),
                          child: participateWorksItem(
                              participateWorks[index]["imgUrl"], participateWorks[index]["title"], participateWorks[index]["content"],
                              period: participateWorks[index]["period"],
                              label: participateWorks[index]["label"],
                              status: participateWorks[index]["status"],
                              votes: participateWorks[index]["votes"]));
                    }, childCount: participateWorks.length),
                  ),
                  SliverList(
                    delegate: SliverChildBuilderDelegate((content, index) {
                      return Padding(
                          padding: EdgeInsets.only(left: 20, right: 20, top: 5, bottom: 5),
                          child: participateWorksItem(
                              participateWorks[index]["imgUrl"], participateWorks[index]["title"], participateWorks[index]["content"],
                              period: participateWorks[index]["period"],
                              label: participateWorks[index]["label"],
                              status: participateWorks[index]["status"],
                              votes: participateWorks[index]["votes"]));
                    }, childCount: participateWorks.length),
                  ),
                  SliverList(
                    delegate: SliverChildBuilderDelegate((content, index) {
                      return Padding(
                          padding: EdgeInsets.only(left: 20, right: 20, top: 5, bottom: 5),
                          child: participateWorksItem(
                              participateWorks[index]["imgUrl"], participateWorks[index]["title"], participateWorks[index]["content"],
                              period: participateWorks[index]["period"],
                              label: participateWorks[index]["label"],
                              status: participateWorks[index]["status"],
                              votes: participateWorks[index]["votes"]));
                    }, childCount: participateWorks.length),
                  ),
                  SliverList(
                    delegate: SliverChildBuilderDelegate((content, index) {
                      return Padding(
                          padding: EdgeInsets.only(left: 20, right: 20, top: 5, bottom: 5),
                          child: participateWorksItem(
                              participateWorks[index]["imgUrl"], participateWorks[index]["title"], participateWorks[index]["content"],
                              period: participateWorks[index]["period"],
                              label: participateWorks[index]["label"],
                              status: participateWorks[index]["status"],
                              votes: participateWorks[index]["votes"]));
                    }, childCount: participateWorks.length),
                  ),
                  SliverList(
                    delegate: SliverChildBuilderDelegate((content, index) {
                      return Padding(
                          padding: EdgeInsets.only(left: 20, right: 20, top: 5, bottom: 5),
                          child: participateWorksItem(
                              participateWorks[index]["imgUrl"], participateWorks[index]["title"], participateWorks[index]["content"],
                              period: participateWorks[index]["period"],
                              label: participateWorks[index]["label"],
                              status: participateWorks[index]["status"],
                              votes: participateWorks[index]["votes"]));
                    }, childCount: participateWorks.length),
                  ),
                ],
              ),
              ),
            ],
          ),
        ),
    );
  }
}

