import 'dart:convert';

import 'package:ant_icons/ant_icons.dart';
import 'package:flutter_color/flutter_color.dart';
import 'package:flutter_vant_kit/widgets/steps.dart';
import 'package:flutter_web_2021/Include.dart';
import 'package:flutter_web_2021/widgets/steps/my_steps.dart';
import 'package:getwidget/components/button/gf_button.dart';
import 'package:getwidget/shape/gf_button_shape.dart';

class PingView extends BasePage {
  @override
  _PingViewState createState() => _PingViewState();

  @override
  String get pageName => 'PingView';
}

class _PingViewState extends BasePageState<PingView> with AutomaticKeepAliveClientMixin {

  @protected
  bool get wantKeepAlive => true;

  int _active = 1;

  List levelPrivileges = [
    {"isOpen": true, "title": "日票", "imgUrl": "asset/image/home/ico_dj_01.png",  "imgUrlOn": "asset/image/home/ico_dj_01.png",  "grade": "小学"},
    {"isOpen": true, "title": "金票", "imgUrl": "asset/image/home/ico_dj_02.png","imgUrlOn": "asset/image/home/ico_dj_02.png", "grade": "小学"},
    {"isOpen": true, "title": "指点", "imgUrl": "asset/image/home/ico_dj_03.png","imgUrlOn": "asset/image/home/ico_dj_03.png", "grade": "小学"},
  ];
  List levelPrivilegesAll = [
    {"isOpen": true, "title": "日票", "imgUrl": "asset/image/home/ico_dj_01.png",  "imgUrlOn": "asset/image/home/ico_dj_01.png",  "grade": "小学"},
    {"isOpen": true, "title": "金票", "imgUrl": "asset/image/home/ico_dj_02.png","imgUrlOn": "asset/image/home/ico_dj_02.png", "grade": "小学"},
    {"isOpen": true, "title": "指点", "imgUrl": "asset/image/home/ico_dj_03.png","imgUrlOn": "asset/image/home/ico_dj_03.png", "grade": "小学"},
    {"isOpen": false, "title": "引荐", "imgUrl": "asset/image/home/ico_dj_04.png", "imgUrlOn": "asset/image/home/ico_dj_on_04.png.png","grade": "小学"},
    {"isOpen": true, "title": "提名", "imgUrl": "asset/image/home/ico_dj_05.png", "imgUrlOn": "asset/image/home/ico_dj_on_05.png","grade": "小学"},
    {"isOpen": false, "title": "置顶评论", "imgUrl": "asset/image/home/ico_dj_06.png", "imgUrlOn": "asset/image/home/ico_dj_on_06.png","grade": "小学"},
    {"isOpen": false, "title": "捧星", "imgUrl": "asset/image/home/ico_dj_07.png","imgUrlOn": "asset/image/home/ico_dj_on_07.png", "grade": "小学"},
    {"isOpen": false, "title": "评论", "imgUrl": "asset/image/home/ico_dj_08.png", "imgUrlOn": "asset/image/home/ico_dj_on_08.png","grade": "小学"},
    {"isOpen": false, "title": "威望", "imgUrl": "asset/image/home/ico_dj_09.png", "imgUrlOn": "asset/image/home/ico_dj_on_09.png","grade": "小学"},
    {"isOpen": false, "title": "收红包", "imgUrl": "asset/image/home/ico_dj_10.png", "imgUrlOn": "asset/image/home/ico_dj_on_10.png","grade": "小学"},
    {"isOpen": false, "title": "赞助", "imgUrl": "asset/image/home/ico_dj_11.png","imgUrlOn": "asset/image/home/ico_dj_on_11.png", "grade": "小学"},
    {"isOpen": false, "title": "转发", "imgUrl": "asset/image/home/ico_dj_12.png", "imgUrlOn": "asset/image/home/ico_dj_on_12.png","grade": "小学"},
  ];

  ///我的等级
  Widget myGrade() => Column(
        children: [
          Container(
            margin: Dimens.edgeInsets_lr_dp20,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image.asset(
                  "asset/image/home/vip.png",
                  width: 20,
                  height: 20,
                ),
                Gaps.hGap4,
                Text(
                  "我的等级",
                  style: TextStyles.btnText15_333333,
                ),
                Gaps.hGap4,
                Container(
                  decoration: BoxDecorations.boxBackground(circular: 10, color: Colours.color_gray_CCCCCC),
                  padding: EdgeInsets.only(left: 7, right: 7, top: 0, bottom: 0),
                  alignment: Alignment.center,
                  child: Text(
                    "我的级别",
                    style: TextStyles.btnTextWhile11,
                  ),
                ),
                Expanded(
                  child: Gaps.hGap4,
                ),
                Container(
                  height: 30,
                  child: GFButton(
                    padding: EdgeInsets.only(left: 15, right: 15),
                    color: HexColor('#E8C37E'),
                    borderShape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5),
                      side: BorderSide(
                        width: 0,
                        color: HexColor('#E8C37E'),
                        style: BorderStyle.solid,
                      ),
                    ),
                    textStyle: TextStyles.btnText14_7E4F03,
                    fullWidthButton: false,
                    size: 31,
                    text: "去 评",
                    onPressed: () async {


                    },
                  ),
                ),
              ],
            ),
          ),
          Gaps.vGap10,
          Padding(
            padding: Dimens.edgeInsets_lr_dp10,
            child: MySteps(
              steps: [
                MyStepItem("本科"),
                MyStepItem("本科2级"),
                MyStepItem("本科3级"),
                MyStepItem("本科4级"),
                MyStepItem("硕士"),
              ],
              active: _active,
              // activeIcon: Icons.done,
              // inactiveIcon: Icons.chevron_right,
              activeColor: Colours.color_purple_875FE1,
            ),
          ),
        ],
      );
  //等级特权 item
  Widget levelPrivilegeItem({bool isOpen, String imgUrl,String imgUrlOn, String title, String grade, double width}) => Container(
    decoration: isOpen ? BoxDecorations.rectCircleHollow10 : BoxDecorations.rectCircleHollow10_gray,
    width: width,
    height: 45,
    child: Stack(
      children: [
        Container(
          alignment:isOpen ? Alignment.center:Alignment.bottomCenter,
          padding: EdgeInsets.only(bottom:isOpen ?0:3 ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Baseline(
                baseline: 13,
                baselineType: TextBaseline.alphabetic,
                child: Image.asset(isOpen?imgUrlOn:imgUrl, width: 19, height: 15),
              ),
              Gaps.hGap4,
              Baseline(
                baseline: 14,
                baselineType: TextBaseline.alphabetic,
                child: Text(
                  title,
                  style: isOpen ? TextStyles.btnText14_333333 : TextStyles.btnText13_8E8E8E,
                ),
              ),
            ],
          ),
        ),
        Offstage(
          offstage: isOpen,
          child: Align(
            alignment: Alignment.topRight,
            child: Row(
              children: [
                Expanded(child: Gaps.hGap8),
                Container(
                    decoration: isOpen ? null : BoxDecorations.rectCircleGray5_8E8E8E,
                    padding: Dimens.edgeInsets_lr_dp5,
                    height: 16,
                    child: Row(
                      children: [
                        Baseline(
                          baseline: 7,
                          baselineType: TextBaseline.alphabetic,
                          // child: Image.asset("asset/image/home/vip.png", width: 10, height: 10),
                          child: Icon(
                            AntIcons.unlock ,
                            color: Colours.bg_while,
                            size: 10,
                          ),
                        ),
                        SizedBox(width: 2.0),
                        Baseline(
                          baseline: 12,
                          baselineType: TextBaseline.alphabetic,
                          child: Text(
                            grade,
                            style: TextStyles.btnTextWhile10,
                          ),
                        ),
                      ],
                    )),
              ],
            ),
          ),
        ),



      ],
    ),
  );

  //等级特权 list
  Widget levelPrivilegeList() {
    double margin = 15;
    double spacing = 10;
    double runSpacing = 5;

    double itemWidth = (Dimens.screenWidth() - (margin * 2) - (spacing * 2)) / 3;
    return Wrap(
      runSpacing: runSpacing,
      spacing: spacing,
      children: levelPrivileges.map((e) {
        //Map<String, dynamic> responseData = jsonDecode(e);
        // UserEntity userEntity = UserEntity.fromJson(responseData);
        return levelPrivilegeItem(isOpen: e["isOpen"], imgUrl: e["imgUrl"],imgUrlOn:e["imgUrlOn"] , title: e["title"], grade: e["grade"], width: itemWidth);
      }).toList(),
    );
  }

  ///等级特权
  Widget levelPrivilege() => Column(
    children: [
      Container(
        margin: Dimens.edgeInsets_lr_dp20,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Image.asset(
              "asset/image/home/ioc_crown_red.png",
              width: 20,
              height: 20,
            ),
            Gaps.hGap4,
            Text(
              "等级特权",
              style: TextStyles.btnText15B_333333,
            ),
            Expanded(child: Gaps.hGap4,),
            Offstage(
                offstage: levelPrivilegesAll.length > 3?false:true,
                child: InkWell(
                  child:Padding(
                    padding: EdgeInsets.all(2),
                    child: Icon(
                      levelPrivileges.length == 3 ?AntIcons.right_circle :   AntIcons.down_circle ,
                      color: Colours.color_gray_666666,
                      size: 15,
                    ),
                    
                ) ,
                  onTap: () {
                    if (levelPrivilegesAll.length > 3) {
                      if (levelPrivileges.length == 3) {
                        levelPrivileges.clear();
                        levelPrivileges.addAll(levelPrivilegesAll);
                      } else {
                        levelPrivileges.clear();
                        levelPrivileges.add(levelPrivilegesAll[0]);
                        levelPrivileges.add(levelPrivilegesAll[1]);
                        levelPrivileges.add(levelPrivilegesAll[2]);
                      }
                      setState(() {});
                    }
                  },
                )),
          ],
        ),
      ),
      Gaps.vGap20,
      Padding(
        padding: Dimens.edgeInsets_lr_dp15,
        child: levelPrivilegeList(),
      ),
    ],
  );

  //成就 item
  Widget achievementItem(String title, String content, String unit) => Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding: EdgeInsets.only(bottom: 4),
            child: Text(
              title,
              style: TextStyles.btnText11_9B680E,
            ),
          ),
          Stack(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Baseline(
                    baseline: 13,
                    baselineType: TextBaseline.alphabetic,
                    child: Text(
                      content,
                      style: TextStyles.btnText13B_9C6A10,
                    ),
                  ),
                  Baseline(
                    baseline: 12,
                    baselineType: TextBaseline.alphabetic,
                    child: Text(
                      unit,
                      style: TextStyles.btnText10_9B680E,
                    ),
                  ),
                ],
              ),

              Offstage(
                offstage: title=="收到引荐"? false:true ,
                child: Align(
                  child: Container(
                    decoration: BoxDecorations.rectCircle5_red,
                    width: 5,
                    height: 5,
                  ),
                  alignment: Alignment.center,
                ),
              ),
            ],
          )

        ],
      );

  ///我的成就
  Widget myAchievement() => Column(
        children: [
          Container(
            margin: Dimens.edgeInsets_lr_dp20,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image.asset(
                  "asset/image/home/ico_rmss.png",
                  width: 20,
                  height: 20,
                ),
                Gaps.hGap4,
                Text(
                  "我的成就",
                  style: TextStyles.btnText15B_333333,
                ),
              ],
            ),
          ),
          Gaps.vGap15,
          Padding(
            padding: Dimens.edgeInsets_lr_dp15,
            child: Row(
              children: [
                Expanded(
                  child: Container(
                    alignment: Alignment.center,
                    decoration: BoxDecorations.rectCircleHollow5,
                    height: 62,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Expanded(child: achievementItem("投出日票", "0万", "票")),
                        Expanded(child: achievementItem("投出金票", "0万", "票")),
                        Expanded(child: achievementItem("投票作品", "0万", "票")),
                      ],
                    ),
                  ),
                  flex: 3,
                ),
                Dividers.dividerVertical(width: 8, color: Colors.transparent),
                Expanded(
                  child: Container(
                    decoration: BoxDecorations.rectCircle5_gray,
                    height: 62,
                    child: achievementItem("收藏作品", "0万", "票"),
                  ),
                  flex: 1,
                ),
              ],
            ),
          ),
          Gaps.vGap8,
          Padding(
            padding: Dimens.edgeInsets_lr_dp15,
            child: Row(
              children: [
                Expanded(
                  child: Container(
                    decoration: BoxDecorations.rectCircle5_gray,
                    height: 62,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Expanded(child: achievementItem("领取红包", "0", "份")),
                        Expanded(child: achievementItem("领取金额", "0", "元")),
                      ],
                    ),
                  ),
                  flex: 2,
                ),
                Dividers.dividerVertical(width: 8, color: Colors.transparent),
                Expanded(
                  child: Container(
                    decoration: BoxDecorations.rectCircleHollow5,
                    height: 62,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Expanded(child: achievementItem("送花作品", "100", "篇")),
                        Expanded(child: achievementItem("送花数量", "1000", "朵")),
                      ],
                    ),
                  ),
                  flex: 2,
                ),
              ],
            ),
          ),
          Gaps.vGap8,
          Padding(
            padding: Dimens.edgeInsets_lr_dp15,
            child: Row(
              children: [
                Expanded(
                  child: Container(
                    decoration: BoxDecorations.rectCircleHollow5,
                    height: 62,
                    child: achievementItem("关注作者", "0", "位"),
                  ),
                  flex: 1,
                ),
                Dividers.dividerVertical(width: 8, color: Colors.transparent),
                Expanded(
                  child: Container(
                    decoration: BoxDecorations.rectCircle5_gray,
                    height: 62,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Expanded(child: achievementItem("累计购花", "10000", "朵")),
                        Expanded(child: achievementItem("花朵余额", "100", "朵")),
                        Expanded(
                          child: Container(
                            height: 30,
                            margin: EdgeInsets.only(left: 15, right: 15),
                            child: GFButton(
                              padding: EdgeInsets.only(left: 10, right: 10),
                              color: HexColor('#F56924'),
                              borderShape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(5),
                                side: BorderSide(
                                  width: 0,
                                  color: HexColor('#F56924'),
                                  style: BorderStyle.solid,
                                ),
                              ),
                              textStyle: TextStyles.btnTextWhile13,
                              fullWidthButton: false,
                              size: 31,
                              text: "购买",
                              onPressed: () async {},
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  flex: 3,
                ),
              ],
            ),
          ),
          Gaps.vGap8,
          Padding(
            padding: Dimens.edgeInsets_lr_dp15,
            child: Container(
              decoration: BoxDecorations.rectCircle5_gray,
              height: 62,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(child: achievementItem("捧星", "0", "次")),
                  Image.asset("asset/image/home/line.png", width: 1, height: 50),
                  Expanded(child: achievementItem("捧星成功率", "0", "%")),
                  Image.asset("asset/image/home/line.png", width: 1, height: 50),
                  Expanded(child: achievementItem("捧星累积经验", "0万", "经验")),
                  Image.asset("asset/image/home/line.png", width: 1, height: 50),
                  Expanded(child: achievementItem("最好成绩", "--", "")),
                ],
              ),
            ),
          ),
          Gaps.vGap8,
          Padding(
            padding: Dimens.edgeInsets_lr_dp15,
            child: Container(
              decoration: BoxDecorations.rectCircleHollow5,
              height: 62,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(child: achievementItem("指点", "0", "次")),
                  Image.asset("asset/image/home/line.png", width: 1, height: 50),
                  Expanded(child: achievementItem("被接纳", "0", "次")),
                  Image.asset("asset/image/home/line.png", width: 1, height: 50),
                  Expanded(child: achievementItem("指点成功率", "0", "")),
                  Image.asset("asset/image/home/line.png", width: 1, height: 50),
                  Expanded(child: achievementItem("指点累计经验", "0", "经验")),
                ],
              ),
            ),
          ),
          Gaps.vGap8,
          Padding(
            padding: Dimens.edgeInsets_lr_dp15,
            child: Container(
              decoration: BoxDecorations.rectCircle5_gray,
              height: 62,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(child: achievementItem("引荐作品", "0", "个")),
                  Image.asset("asset/image/home/line.png", width: 1, height: 50),
                  Expanded(child: achievementItem("引荐评委", "0", "人")),
                  Image.asset("asset/image/home/line.png", width: 1, height: 50),
                  Expanded(child: achievementItem("引荐成功", "0", "人")),
                  Image.asset("asset/image/home/line.png", width: 1, height: 50),
                  Expanded(child: achievementItem("收到引荐", "0", "次")),
                ],
              ),
            ),
          ),
          Gaps.vGap8,
          Padding(
            padding: Dimens.edgeInsets_lr_dp15,
            child: Container(
              decoration: BoxDecorations.rectCircleHollow5,
              height: 62,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(child: achievementItem("赞助作品", "0", "个")),
                  Image.asset("asset/image/home/line.png", width: 1, height: 50),
                  Expanded(child: achievementItem("赞助红包", "0", "次")),
                  Image.asset("asset/image/home/line.png", width: 1, height: 50),
                  Expanded(child: achievementItem("赞助金额", "0", "元")),
                ],
              ),
            ),
          ),
          Gaps.vGap8,
          Padding(
            padding: Dimens.edgeInsets_lr_dp15,
            child: Container(
              decoration: BoxDecorations.rectCircle5_gray,
              height: 62,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(child: achievementItem("提名作品", "0", "个")),
                  Image.asset("asset/image/home/line.png", width: 1, height: 50),
                  Expanded(child: achievementItem("威望", "0", "")),
                  Image.asset("asset/image/home/line.png", width: 1, height: 50),
                  Expanded(child: achievementItem("获奖作品", "0", "个")),
                  Image.asset("asset/image/home/line.png", width: 1, height: 50),
                  Expanded(child: achievementItem("提名获得经验", "0", "个")),
                ],
              ),
            ),
          ),
          Gaps.vGap8,
          Padding(
            padding: Dimens.edgeInsets_lr_dp15,
            child: Container(
              decoration: BoxDecorations.rectCircleHollow5,
              height: 62,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(child: achievementItem("评论作品", "0", "个")),
                  Image.asset("asset/image/home/line.png", width: 1, height: 50),
                  Expanded(child: achievementItem("置顶评论", "0", "次")),
                  Image.asset("asset/image/home/line.png", width: 1, height: 50),
                  Expanded(child: achievementItem("精华评论", "0", "个")),
                  Image.asset("asset/image/home/line.png", width: 1, height: 50),
                  Expanded(child: achievementItem("评论获得经验", "0", "个")),
                ],
              ),
            ),
          ),
          Gaps.vGap8,
          Gaps.vGap8,
        ],
      );


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colours.bg_while,
        child: Stack(
          children: [
            Container(
              height: 150,
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      colors: [HexColor('#E0C18B'), Colours.bg_while, Colours.bg_while],
                      begin: Alignment.topRight,
                      end: Alignment.bottomRight,
                      tileMode: TileMode.mirror)),
            ),
            ListView(
              children: [
                Gaps.vGap20,
                Gaps.vGap10,
                myGrade(),
                levelPrivilege(),
                Gaps.vGap20,
                Dividers.dividerHorizontal(height: 12, color: HexColor('#F1F1F1')),
                Gaps.vGap20,
                myAchievement(),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
