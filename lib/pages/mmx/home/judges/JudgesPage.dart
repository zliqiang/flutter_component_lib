import 'package:flutter_color/flutter_color.dart';
import 'package:flutter_web_2021/Include.dart';
import 'package:flutter_web_2021/pages/component/painting_and_effects/ClipPathPage.dart';
import 'package:flutter_web_2021/pages/mmx/home/judges/view/ChuangView.dart';
import 'package:flutter_web_2021/pages/mmx/home/judges/view/PingView.dart';
import 'package:flutter_web_2021/pages/mmx/login/LoginPage.dart';
import 'package:flutter_web_2021/widgets/card/flip_card.dart';
import 'package:getwidget/components/button/gf_button.dart';

class JudgesPage extends BasePage {
  static String dartPath = "lib/pages/mmx/home/judges/JudgesPage.dart";

  @override
  _JudgesPageState createState() => _JudgesPageState();

  @override
  String get pageName => 'JudgesPage';
}

class _JudgesPageState extends BasePageState<JudgesPage> with TickerProviderStateMixin {
  TabController _tabController;

  int index = 0;

  @override
  void initState() {
    super.initState();

    _tabController = null;
    _tabController = TabController(initialIndex: 0, length: 2, vsync: this); // 直接传this

    _tabController.addListener(() {
      if (_tabController.index.toDouble() == _tabController.animation.value) {
        setState(() {
          index = _tabController.index;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        toolbarHeight: 0.0,
        backgroundColor: HexColor('#292929'),
      ),
      body: Column(
        children: [
          Container(
            width: Dimens.screenWidth(),
            height: 145,
            color: HexColor('#292929'),
            child: Column(
              children: [
                Expanded(
                  child: new UnconstrainedBox(
                    child: Container(
                      height: 30,
                      child: GFButton(
                        padding: EdgeInsets.only(left: 15, right: 15),
                        color: HexColor('#121212'),
                        borderShape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5),
                          side: BorderSide(
                            width: 0,
                            color: HexColor('#121212'),
                            style: BorderStyle.solid,
                          ),
                        ),
                        textStyle: TextStyles.btnTextWhile13,
                        fullWidthButton: false,
                        size: 31,
                        text: "登录/注册",
                        onPressed: () async {
                          AppNavigator.push(context, LoginPage());
                        },
                      ),
                    ),
                  ),
                ),
                Container(
                  width: Dimens.screenWidth(),
                  padding: EdgeInsets.only(left: index == 0 ? 15 : 0, right: index == 1 ? 15 : 0),
                  height: 58,
                  alignment: Alignment.bottomCenter,
                  child: TabBar(
                    controller: _tabController,
                    labelPadding: EdgeInsets.all(0),
                    indicatorWeight: 0.0001,
                    tabs: <Widget>[
                      index == 0
                          ? tabOn1(
                              child: tabChild(height: 58, imgPath: "asset/image/home/ping.png", text: "我是评委", style: TextStyles.btnText18_7E4F03))
                          : tabOff1(
                              child: tabChild(height: 48, imgPath: "asset/image/home/ping.png", text: "我是评委", style: TextStyles.btnText16_7E4F03)),
                      index == 1
                          ? tabOn2(
                              child: tabChild(height: 58, imgPath: "asset/image/home/chuang.png", text: "我是创作者", style: TextStyles.btnText18_7E4F03))
                          : tabOff2(
                              child: tabChild(height: 48, imgPath: "asset/image/home/chuang.png", text: "我是创作者", style: TextStyles.btnText16_7E4F03)),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: TabBarView(
              controller: _tabController,
              children: <Widget>[
                PingView(),
                ChuangView(),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

Widget tabChild({double height, String imgPath, String text, TextStyle style}) {
  return Container(
    height: height,
    child: Row(
      children: [
        Expanded(
          child: Container(
            alignment: Alignment.center,
            child: Text(
              text,
              style: style,
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(right: 5),
          child: Image.asset(
            imgPath,
          ),
        ),
      ],
    ),
  );
}

Widget tabOn1({Widget child}) {
  return Container(
    height: 58,
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Container(
            width: 15,
            height: 15,
            child: Stack(
              children: [
                Align(
                  child: Container(
                    width: 14,
                    height: 14,
                    decoration: BoxDecorations.rectBorder,
                  ),
                  alignment: Alignment.bottomRight,
                ),
                Align(
                  child: Container(
                    width: 15,
                    height: 15,
                    decoration: BoxDecorations.rectCircleAngle15,
                  ),
                  alignment: Alignment.bottomRight,
                ),
              ],
            )),
        Expanded(
          child: Container(
            padding: EdgeInsets.all(0),
            decoration: BoxDecorations.rectCircle15,
            child: child,
          ),
        ),
      ],
    ),
  );
}

Widget tabOn2({Widget child}) {
  return Container(
    height: 58,
    child: Stack(
      children: [

        Align(
          child: Container(
              width: 15,
              height: 15,
              child: Stack(
                children: [
                  Align(
                    child: Container(
                      width: 14,
                      height: 14,
                      decoration: BoxDecorations.rectBordertab2,
                    ),
                    alignment: Alignment.bottomLeft,
                  ),
                  Align(
                    child: Container(
                      width: 15,
                      height: 15,
                      decoration: BoxDecorations.rectCircleAngle15tab2,
                    ),
                    alignment: Alignment.bottomLeft,
                  ),
                ],
              )),
          alignment: Alignment.bottomRight,
        ),
        Align(
          child: Container(
            margin: EdgeInsets.only(right: 15),
            decoration: BoxDecorations.rectCircle15tab2,
            child: child,
          ),
        ),
      ],
    ),
  );
}

Widget tabOff2({Widget child}) {
  return Container(
    height: 58,
    alignment: Alignment.bottomLeft,
    padding: EdgeInsets.only(right: 20),
    child: Container(
      height: 48,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Expanded(
            child: Stack(
              children: [
                Align(
                  child: Container(
                    width: 14,
                    height: 14,
                    color: HexColor('#E0C18B'),
                  ),
                  alignment: Alignment.bottomLeft,
                ),
                Container(
                  padding: EdgeInsets.all(0),
                  decoration: BoxDecorations.rectCircle15tab2_off,
                  child: child,
                ),
              ],
            ),
          ),
        ],
      ),
    ),
  );
}

Widget tabOff1({Widget child}) {
  return Container(
    height: 58,
    alignment: Alignment.bottomLeft,
    padding: EdgeInsets.only(left: 20),
    child: Container(
      height: 48,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Expanded(
            child: Stack(
              children: [
                Align(
                  child: Container(
                    width: 14,
                    height: 14,
                    color: HexColor('#F1AF2F'),
                  ),
                  alignment: Alignment.bottomRight,
                ),
                Container(
                  padding: EdgeInsets.all(0),
                  decoration: BoxDecorations.rectCircle15_off,
                  child: child,
                ),
              ],
            ),
          ),
        ],
      ),
    ),
  );
}
