import 'package:ant_icons/ant_icons.dart';
import 'package:flutter_color/flutter_color.dart';
import 'package:flutter_web_2021/Include.dart';
import 'package:flutter_web_2021/utils/toast/OkToastUtil.dart';
import 'package:flutter_web_2021/widgets/app_bar/BottomAppBarView.dart';
import 'package:getwidget/components/button/gf_button.dart';

import 'judges/JudgesPage.dart';

class TabHomePage extends BasePage {
  static String dartPath = "lib/pages/mmx/home/TabHomePage.dart";

  @override
  _TabHomePageState createState() => _TabHomePageState();

  @override
  String get pageName => 'Tab首頁';
}

class _TabHomePageState extends BasePageState<TabHomePage> with SingleTickerProviderStateMixin {
  //当前页面下标
  int currentIndex = 1;

  TabController tabController;

  @override
  void initState() {
    super.initState();
    this.tabController = TabController(length: 3, vsync: this);
    Future.delayed(Duration.zero, () {
      _showDialog();
    });

  }

  Future<void> _showDialog(){
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return SimpleDialog(
          backgroundColor:Colors.transparent,
          children: [
            Container(
              width: 258,
              decoration: BoxDecorations.rectCircleWhile10_3,
              padding: EdgeInsets.all(5),
              child: Column(
                children: [
                  Gaps.vGap4,
                  Row(
                    children: [
                      Expanded(child: Gaps.hGap4),
                      Container(
                        width: 48,
                        height: 20,
                        margin: EdgeInsets.only(left: 5, right: 5,top: 0,bottom: 0),
                        child: GFButton(
                          padding: EdgeInsets.all(0),
                          color: HexColor('#ECECEC'),
                          borderShape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10),
                            side: BorderSide(
                              width: 0,
                              color: HexColor('#CCCCCC'),
                              style: BorderStyle.solid,
                            ),
                          ),
                          textStyle: TextStyles.btnText12_333333,
                          fullWidthButton: false,
                          size: 31,
                          text: "跳过",
                          onPressed: () async {
                            AppNavigator.pop(context);

                          },
                        ),
                      ),
                    ],
                  ),

                  Image.asset(
                    "asset/image/login/ico_zl.png",
                    width: 175,
                    height: 142,
                  ),

                  Text(
                    "您的个人资料待完善",
                    style: TextStyles.btnText14_333333,
                  ),
                  Gaps.vGap10,
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "去编辑",
                        style: TextStyles.btnText12_0B71C8,
                      ),
                      Gaps.wGap2,
                      Baseline(
                        baseline: 10,
                        baselineType: TextBaseline.alphabetic,
                        child: Icon(
                          AntIcons.right_outline,
                          color: Colours.color_blue_0B71C8,
                          size: 10,
                        ),
                      ),
                    ],
                  ),
                  Gaps.vGap10,
                ],
              ),
            ),
          ],
        );
      },
    );
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Column(
          children: [
            Expanded(
              child: TabBarView(
                controller: tabController,
                children: [
                  JudgesPage(),
                  Container(color: Colours.blue,),
                  Container(color: Colours.color_orange_FF5800,),
                ],
              ),
            ),
            Container(
              decoration: BoxDecorations.rectCircle30,
              height:PlatformUtils.isIOS?null: 50,
              child: BottomAppBarView(
                tabController: tabController,
                selectedListen: (int index) {
                  ToastUtil.showToastBottom(index);
                },
              ),
            ),
          ],
        ),
      ),
      // bottomNavigationBar: BottomAppBarView(
      //   tabController: tabController,
      //   selectedListen: (int index) {
      //     ToastUtil.showToastBottom(index);
      //   },
      // ),
    );
  }
}


