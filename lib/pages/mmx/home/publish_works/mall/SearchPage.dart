import 'package:ant_icons/ant_icons.dart';
import 'package:flutter_color/flutter_color.dart';
import 'package:flutter_web_2021/Include.dart';
import 'package:flutter_web_2021/pages/mmx/home/publish_works/ProductDetailsPage.dart';
import 'package:flutter_web_2021/widgets/img/ImagesViews.dart';
import 'package:getwidget/components/button/gf_button.dart';

class SearchPage extends BasePage {
  String title="";
  SearchPage(this.title);
  @override
  _SearchPageState createState() => _SearchPageState();

  @override
  String get pageName => '搜索页';
}

class _SearchPageState extends BasePageState<SearchPage> {
  List draftBoxs = [
    {
    "title": "最美女神",
    "imgUrl":
    "https://tse1-mm.cn.bing.net/th/id/R-C.544a3ba7aaad86e32e5ffa6250d9e900?rik=dMcOKOHaTmlfeQ&riu=http%3a%2f%2fpic3.16pic.com%2f00%2f54%2f83%2f16pic_5483144_b.jpg&ehk=sQIrwd4bdBVClyRCyntdnqbDlxzVHYd69RXsAlXYHSs%3d&risl=&pid=ImgRaw&r=0"
  },
    {
    "title": "最美女神",
    "imgUrl":
    "https://tse1-mm.cn.bing.net/th/id/R-C.544a3ba7aaad86e32e5ffa6250d9e900?rik=dMcOKOHaTmlfeQ&riu=http%3a%2f%2fpic3.16pic.com%2f00%2f54%2f83%2f16pic_5483144_b.jpg&ehk=sQIrwd4bdBVClyRCyntdnqbDlxzVHYd69RXsAlXYHSs%3d&risl=&pid=ImgRaw&r=0"
  },
    {
    "title": "最美女神",
    "imgUrl":
    "https://tse1-mm.cn.bing.net/th/id/R-C.544a3ba7aaad86e32e5ffa6250d9e900?rik=dMcOKOHaTmlfeQ&riu=http%3a%2f%2fpic3.16pic.com%2f00%2f54%2f83%2f16pic_5483144_b.jpg&ehk=sQIrwd4bdBVClyRCyntdnqbDlxzVHYd69RXsAlXYHSs%3d&risl=&pid=ImgRaw&r=0"
  },
    {
    "title": "最美女神",
    "imgUrl":
    "https://tse1-mm.cn.bing.net/th/id/R-C.544a3ba7aaad86e32e5ffa6250d9e900?rik=dMcOKOHaTmlfeQ&riu=http%3a%2f%2fpic3.16pic.com%2f00%2f54%2f83%2f16pic_5483144_b.jpg&ehk=sQIrwd4bdBVClyRCyntdnqbDlxzVHYd69RXsAlXYHSs%3d&risl=&pid=ImgRaw&r=0"
  },
    {
    "title": "最美女神",
    "imgUrl":
    "https://tse1-mm.cn.bing.net/th/id/R-C.544a3ba7aaad86e32e5ffa6250d9e900?rik=dMcOKOHaTmlfeQ&riu=http%3a%2f%2fpic3.16pic.com%2f00%2f54%2f83%2f16pic_5483144_b.jpg&ehk=sQIrwd4bdBVClyRCyntdnqbDlxzVHYd69RXsAlXYHSs%3d&risl=&pid=ImgRaw&r=0"
  },
    {
    "title": "最美女神",
    "imgUrl":
    "https://tse1-mm.cn.bing.net/th/id/R-C.544a3ba7aaad86e32e5ffa6250d9e900?rik=dMcOKOHaTmlfeQ&riu=http%3a%2f%2fpic3.16pic.com%2f00%2f54%2f83%2f16pic_5483144_b.jpg&ehk=sQIrwd4bdBVClyRCyntdnqbDlxzVHYd69RXsAlXYHSs%3d&risl=&pid=ImgRaw&r=0"
  },
  ];

  Widget draftBoxItem(bean, int index) => Container(
    padding: EdgeInsets.all(15),
    child: Row(
      children: [
        Container(
          width: 100,
          height: 110,
          child: ImagesViews.rectangle(
            "https://tse1-mm.cn.bing.net/th/id/R-C.d9522d356a538d12550bfc638abcba82?rik=5ctIvNjQV1q9bg&riu=http%3a%2f%2fimage2.sina.com.cn%2fent%2fy%2fp%2f2006-07-01%2fU1735P28T3D1143020F326DT20060701184010.jpg&ehk=vX%2fRQTI6nLFxuhrZxu0NUcLFYTpZpJZKKPgrn3bh38g%3d&risl=&pid=ImgRaw&r=0",
            borderRadius: 10.0,
          ),
        ),
        Gaps.wGap10,
        Expanded(
          child:Container(
            height: 110,
            child:     Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "全国最美美女",
                  style: TextStyles.btnText14B_333333,
                ),
                Gaps.vGap12,
                Row(
                  children: [
                    Baseline(
                      baseline: 12,
                      baselineType: TextBaseline.alphabetic,
                      // child: Image.asset("asset/image/home/vip.png", width: 10, height: 10),
                      child: Text(
                      "价格:",
                      style: TextStyles.btnText12_333333,
                    ),
                    ),
                    Baseline(
                      baseline: 13,
                      baselineType: TextBaseline.alphabetic,
                      // child: Image.asset("asset/image/home/vip.png", width: 10, height: 10),
                      child: Text(
                      "￥1,588.00",
                      style: TextStyles.btnTextRed14B,
                    ),
                    ),
                  ],
                ),

                Expanded(child: Gaps.vGap12),
                Row(
                  children: [
                    Baseline(
                      baseline: 12,
                      baselineType: TextBaseline.alphabetic,
                      // child: Image.asset("asset/image/home/vip.png", width: 10, height: 10),
                      child: Text(
                        "商品价格:",
                        style: TextStyles.btnText12_333333,
                      ),
                    ),
                    Baseline(
                      baseline: 13,
                      baselineType: TextBaseline.alphabetic,
                      // child: Image.asset("asset/image/home/vip.png", width: 10, height: 10),
                      child: Text(
                        "999朵花",
                        style: TextStyles.btnText14B_F06F2A,
                      ),
                    ),

                    Expanded(child: Gaps.wGap15),

                    InkWell(
                      onTap: (){
                        AppNavigator.push(context, ProductDetailsPage());
                      },
                      child: Row(
                        children: [
                          Baseline(
                            baseline: 12,
                            baselineType: TextBaseline.alphabetic,
                            // child: Image.asset("asset/image/home/vip.png", width: 10, height: 10),
                            child: Text(
                              "查看详情",
                              style: TextStyles.btnText12_999999,
                            ),
                          ),
                          Baseline(
                            baseline: 10,
                            baselineType: TextBaseline.alphabetic,
                            child: Icon(
                              AntIcons.right_outline,
                              color: Colours.color_gray_999999,
                              size: 14,
                            ),
                          ),
                        ],
                      ),
                    )


                  ],
                ),

              ],
            ),
          ),
        ),





      ],
    ),




  );

  Widget draftBoxList() => ListView.separated(
    // shrinkWrap: true,
    // physics: new NeverScrollableScrollPhysics(),
      itemCount: draftBoxs.length,
      scrollDirection: Axis.vertical,
      separatorBuilder: (BuildContext context, int index) {
        return Container(
          padding: Dimens.edgeInsets_tb_dp15,
          child: Dividers.dividerHorizontal(height: 1, color: Colours.color_gray_F1F1F1),
        );
      },
      itemBuilder: (BuildContext context, int index) {
        return InkWell(
          child: draftBoxItem(draftBoxs[index], index),
          onTap: () {
            // AppNavigator.push(context, EditWorksPage());
          },
        );
      });


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: MyAppBar.appBarCreationNoRight(context, widget.title),
        backgroundColor: Colours.color_black_252525,
        body:
        Container(
          decoration: BoxDecorations.rectCircle10,
          child: Stack(
            children: [

              Container(
                child: draftBoxList(),
              ),
              Offstage(
                offstage: draftBoxs.length!=0,
                child:  Container(
                  color: Colours.bg_while,
                  height: Dimens.screenHeight(),
                  padding: EdgeInsets.only(top: 65),
                  alignment: Alignment.topCenter,
                  child: Column(
                    children: [
                      Image.asset(
                        "asset/image/home/works/no_data.png",
                        width: 139,
                        height: 149,
                      ),
                      Gaps.vGap20,
                      Gaps.vGap10,
                      Text(
                        "很抱歉，没有找到相关产品",
                        style: TextStyles.btnText12_333333,
                      ),
                      Expanded(child:Gaps.vGap10 ),
                      Container(
                        height: 40,
                        margin: EdgeInsets.only(left: 15, right: 15),
                        child: GFButton(
                          padding: EdgeInsets.only(left: 10, right: 10),
                          color: false ? HexColor('#1B1D23') : HexColor('#CCCCCC'),
                          borderShape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5),
                            side: BorderSide(
                              width: 0,
                              color: false ? HexColor('#1B1D23') : HexColor('#CCCCCC'),
                              style: BorderStyle.solid,
                            ),
                          ),
                          textStyle: TextStyles.btnTextWhile13,
                          fullWidthButton: true,
                          size: 31,
                          text: "返回上一页",
                          onPressed: () async {},
                        ),
                      ),
                      Gaps.vGap20,
                      Gaps.vGap20,
                    ],
                  ),
                ) ,
              )


            ],

          ) ,
        ));
  }
}
