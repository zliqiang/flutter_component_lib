import 'package:ant_icons/ant_icons.dart';
import 'package:flutter_web_2021/Include.dart';
import 'package:flutter_web_2021/pages/component/material_design/input_and_selection/RadioPage.dart';
import 'package:flutter_web_2021/pages/mmx/home/publish_works/ProductDetailsPage.dart';
import 'package:flutter_web_2021/widgets/img/ImagesViews.dart';
import 'package:flutter_web_2021/widgets/radio/CustomRadioView.dart';

class ProductListView extends BasePage {
  @override
  _ProductListViewState createState() => _ProductListViewState();

  @override
  String get pageName => '商品列表';
}

class _ProductListViewState extends BasePageState<ProductListView> {
  List products = [
    {
      "title": "最美女神评选",
      "bloom": "30",
      "price": "价值:￥159.00",
      "note": "最低送花:100朵    等级要求:幼儿园",
      "imgUrl":
          "https://tse1-mm.cn.bing.net/th/id/R-C.d9522d356a538d12550bfc638abcba82?rik=5ctIvNjQV1q9bg&riu=http%3a%2f%2fimage2.sina.com.cn%2fent%2fy%2fp%2f2006-07-01%2fU1735P28T3D1143020F326DT20060701184010.jpg&ehk=vX%2fRQTI6nLFxuhrZxu0NUcLFYTpZpJZKKPgrn3bh38g%3d&risl=&pid=ImgRaw&r=0",
    },
    {
      "title": "最美女神评选",
      "bloom": "30",
      "price": "价值:￥159.00",
      "note": "最低送花:100朵    等级要求:幼儿园",
      "imgUrl":
          "https://tse1-mm.cn.bing.net/th/id/R-C.d9522d356a538d12550bfc638abcba82?rik=5ctIvNjQV1q9bg&riu=http%3a%2f%2fimage2.sina.com.cn%2fent%2fy%2fp%2f2006-07-01%2fU1735P28T3D1143020F326DT20060701184010.jpg&ehk=vX%2fRQTI6nLFxuhrZxu0NUcLFYTpZpJZKKPgrn3bh38g%3d&risl=&pid=ImgRaw&r=0",
    },
    {
      "title": "最美女神评选",
      "bloom": "30",
      "price": "价值:￥159.00",
      "note": "最低送花:100朵    等级要求:幼儿园",
      "imgUrl":
          "https://tse1-mm.cn.bing.net/th/id/R-C.d9522d356a538d12550bfc638abcba82?rik=5ctIvNjQV1q9bg&riu=http%3a%2f%2fimage2.sina.com.cn%2fent%2fy%2fp%2f2006-07-01%2fU1735P28T3D1143020F326DT20060701184010.jpg&ehk=vX%2fRQTI6nLFxuhrZxu0NUcLFYTpZpJZKKPgrn3bh38g%3d&risl=&pid=ImgRaw&r=0",
    },
    {
      "title": "最美女神评选",
      "bloom": "30",
      "price": "价值:￥159.00",
      "note": "最低送花:100朵    等级要求:幼儿园",
      "imgUrl":
          "https://tse1-mm.cn.bing.net/th/id/R-C.d9522d356a538d12550bfc638abcba82?rik=5ctIvNjQV1q9bg&riu=http%3a%2f%2fimage2.sina.com.cn%2fent%2fy%2fp%2f2006-07-01%2fU1735P28T3D1143020F326DT20060701184010.jpg&ehk=vX%2fRQTI6nLFxuhrZxu0NUcLFYTpZpJZKKPgrn3bh38g%3d&risl=&pid=ImgRaw&r=0",
    },
  ];

  int  currentSelection =-1;

  Widget productItem(ietm,int index) => Container(
        height: 90,
        padding: EdgeInsets.only(left: 15, right: 15, top: 0, bottom: 0),
        child:InkWell(
          child:    Row(
            children: [
              CustomRadioView(
                isIco: true,
                padding: EdgeInsets.all(2),
                width: 18,
                height: 18,
                size: 16,
                defaults: currentSelection==index?true:false,
              ),
              Gaps.wGap5,
              Gaps.wGap5,
              Container(
                height: 90,
                width: 100,
                child: ImagesViews.rectangle(
                  ietm["imgUrl"],
                  borderRadius: 5.0,
                ),
              ),
              Gaps.hGap8,
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(
                      ietm["title"],
                      style: TextStyles.btnText14B_333333,
                    ),
                    Gaps.vGap5,
                    Row(
                      children: [
                        Text(
                          "实收:",
                          style: TextStyles.btnTextRed11,
                        ),
                        Text(
                          ietm["bloom"],
                          style: TextStyles.btnTextRed12B,
                        ),
                        Text(
                          "朵花",
                          style: TextStyles.btnTextRed11,
                        ),
                      ],
                    ),
                    Text(
                      ietm["price"],
                      style: TextStyles.btnText11B_333333,
                    ),
                    Expanded(child: Gaps.vGap2),
                    Text(
                      ietm["note"],
                      style: TextStyles.btnText10_CCCCCC,
                    ),
                  ],
                ),
              ),
              Gaps.vGap5,
              InkWell(
                child:Padding(
                  padding: EdgeInsets.all(5),
                  child:Icon(
                    AntIcons.right_outline,
                    color: Colours.color_gray_666666,
                    size: 15,
                  ) ,

                ) ,
                onTap: (){
                  AppNavigator.push(context, ProductDetailsPage());
                },
              )
              ,
            ],
          ),
          onTap: (){
            setState(() {
              currentSelection=index;

            });

          },
        ) ,

      );

  Widget participateWorksView() => ListView.separated(
      padding: EdgeInsets.all(0),
      // shrinkWrap: true,
      // physics: new NeverScrollableScrollPhysics(),
      itemCount: products.length,
      scrollDirection: Axis.vertical,
      separatorBuilder: (BuildContext context, int index) {
        return Padding(
          padding: EdgeInsets.only(left: 15, right: 15, top: 15, bottom: 15),
          child: Dividers.dividerHorizontal(height: 1, color: Colours.line_F1F1F1),
        );
      },
      itemBuilder: (BuildContext context, int index) {
        return productItem(products[index],index);
      });

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Gaps.vGap20,
          Padding(
            padding: EdgeInsets.only(left: 15),
            child: Text(
              "商品列表",
              style: TextStyles.btnText15B_333333,
            ),
          ),
          Gaps.vGap15,
          Expanded(
            child: participateWorksView(),
          ),
        ],
      ),
    );
  }
}
