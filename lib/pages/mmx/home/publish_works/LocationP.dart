import 'package:flutter_web_2021/Include.dart';
import 'package:flutter_web_2021/base/BasePageP.dart';
import 'package:flutter_web_2021/utils/http/DioManagerNoBaseUri.dart';
import 'package:flutter_web_2021/utils/http/NWMethod.dart';
import 'package:flutter_web_2021/utils/toast/OkToastUtil.dart';

import 'LocationPage.dart';

class LocationP extends BasePageP<LocationPageState> {
  String titleLeft = "北京";
  String address = "";
  String location = "";
  List<MapEntity> pois = [];

  @override
  void initState() {
    super.initState();
    // Future.delayed(Duration.zero, () {
    // });
  }

  //地理编码
  aMapV3Geo() {
    DioManagerNoBaseUri().request(NWMethod.GET, NWApi.aMapV3Geo, params: {
      "address": address,
      "output": "JSON",
      "city": titleLeft,
      "key": NWApi.mapKey,
    }, success: (data) {
      try {
        if (data["status"].toString() == "1") {
          location = data["geocodes"][0]["location"];
          view.aMapV3Geo(location);
        } else {
          ToastUtil.showToastTop("请求失败，请稍后再试！");
        }
      } catch (e) {
        Log().info("地理编码请求成功，解析失败,无内容");
      }
    }, error: (error) {
      ToastUtil.showToastTop("${error.message}");
    });
  }

  //poi
  aMapV3Regeo() {
    DioManagerNoBaseUri().request(NWMethod.GET, NWApi.aMapV3Regeo, params: {
      "location": location,
      "extensions": "all",
      "output": "JSON",
      "key": NWApi.mapKey,
    }, success: (data) {
      try {
        List aois = data["regeocode"]["pois"];
        if (data["status"].toString() == "1") {
          Log().info("-------" + aois.length.toString());
          pois.clear();
          for (int i = 0; i < aois.length; i++) {
          String  addres ="";
            try{
              addres = aois[i]["address"] == null ? "" : aois[i]["address"];
            }catch(e){

            }
            pois.add(
              MapEntity(
                title: aois[i]["name"] == null ? "" : aois[i]["name"],
                address:addres,
                distance: aois[i]["distance"] == null ? "" : aois[i]["distance"],
                location: aois[i]["location"] == null ? "" : aois[i]["location"],
                // type: aois[i]["type"] == null ? "" : aois[i]["type"],
                // direction: aois[i]["direction"] == null ? "" : aois[i]["direction"],
              ),
            );
          }
          view.aMapV3Regeo(pois);
        } else {
          ToastUtil.showToastTop("请求失败，请稍后再试！");
        }
      } catch (e) {
        Log().info("poi请求成功，解析失败,无内容");
      }
    }, error: (error) {
      ToastUtil.showToastTop("${error.message}");
    });
  }

  @override
  void dispose() {
    super.dispose();
  }
}

class MapEntity {
  String title = "";
  String address = "";

  //距离
  String distance = "";
  String location = "";

  // "购物服务;商场;购物中心"
  String type = "";

  //方向
  String direction = "";

  MapEntity({this.title, this.address, this.distance, this.location, this.type, this.direction});
}
