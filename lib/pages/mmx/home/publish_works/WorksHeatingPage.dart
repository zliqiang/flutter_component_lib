import 'package:ant_icons/ant_icons.dart';
import 'package:flutter_color/flutter_color.dart';
import 'package:flutter_web_2021/Include.dart';
import 'package:flutter_web_2021/utils/toast/OkToastUtil.dart';
import 'package:flutter_web_2021/widgets/input/DecimalInput.dart';
import 'package:flutter_web_2021/widgets/input/NumberInput.dart';
import 'package:getwidget/components/button/gf_button.dart';

class WorksHeatingPage extends BasePage {
  static String dartPath = "lib/pages/mmx/home/publish_works/WorksHeatingPage.dart";

  @override
  _WorksHeatingPageState createState() => _WorksHeatingPageState();

  @override
  String get pageName => '作品加热--发红包';
}

class _WorksHeatingPageState extends BasePageState<WorksHeatingPage> with WidgetsBindingObserver{
  //红包数量
  FocusNode numberFocusNode = FocusNode();
  TextEditingController numberController = TextEditingController();

  //红包金额
  FocusNode decimalFocusNode = FocusNode();
  TextEditingController decimalController = TextEditingController();

bool keyboardPopUp = false;

  @override
  void didChangeMetrics() {
    super.didChangeDependencies();
    WidgetsBinding.instance.addPostFrameCallback((_) {
        if(MediaQuery.of(context).viewInsets.bottom==0){
          //关闭键盘
          keyboardPopUp=false;
        }else{
          //显示键盘
          keyboardPopUp=true;
        }
    });
  }

@override
  void initState() {
    super.initState();
    //初始化
    WidgetsBinding.instance.addObserver(this);


  }

  @override
  void dispose() {
    //销毁
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }
  // 监听返回
  Future<bool> _requestPop() {
    if(keyboardPopUp){
      numberFocusNode.unfocus();
      decimalFocusNode.unfocus();
    }else{
      Navigator.of(context).pop();
    }
    return new Future.value(false);
  }
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child:   Scaffold(
        resizeToAvoidBottomInset:false,
        appBar: MyAppBar.appBarCreationNoRight(context, "作品加热",onPressed: (){
          _requestPop();
        }),
        backgroundColor: Colours.color_black_252525,
        body: Container(
          decoration: BoxDecorations.rectCircle10,
          width: Dimens.screenWidth(),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(left: 15, top: 10, right: 15),
                child: Text(
                  "发红包能吸引更多的评委为您投票~",
                  style: TextStyles.btnText11_666666,
                ),
              ),
              Gaps.vGap20,
              Padding(
                padding: EdgeInsets.only(left: 15, right: 15),
                child: Container(
                  height: 50,
                  decoration: BoxDecorations.rectCircleHollowGray5,
                  child: Row(
                    children: [
                      Expanded(
                        child: NumberInput(
                          keyboardType: TextInputType.numberWithOptions(decimal: true),
                          controller: numberController,
                          hintText: "不少于100个",
                          textColor: Colors.black,
                          hintTextColor: Colors.grey,
                          contentFocusNode: numberFocusNode,
                          fontSize: Dimens.sp14,
                          height: 40,
                        ),
                      ),
                      Baseline(
                        baseline: 14,
                        baselineType: TextBaseline.alphabetic,
                        child: Text(
                          "个",
                          style: TextStyles.btnText14B_333333,
                        ),
                      ),
                      Gaps.hGap8,
                    ],
                  ),
                ),
              ),
              Gaps.vGap15,
              Padding(
                padding: EdgeInsets.only(left: 15, right: 15),
                child: Container(
                  height: 50,
                  decoration: BoxDecorations.rectCircleHollowGray5,
                  child: Row(
                    children: [
                      Expanded(
                        child: DecimalInput(
                          keyboardType: TextInputType.numberWithOptions(decimal: true),
                          controller: decimalController,
                          hintText: "设置单个红包金额",
                          textColor: Colors.black,
                          hintTextColor: Colors.grey,
                          contentFocusNode: decimalFocusNode,
                          fontSize: Dimens.sp14,
                          height: 40,
                        ),
                      ),
                      Baseline(
                        baseline: 14,
                        baselineType: TextBaseline.alphabetic,
                        child: Text(
                          "元",
                          style: TextStyles.btnText14B_333333,
                        ),
                      ),
                      Gaps.hGap8,
                    ],
                  ),
                ),
              ),
              Gaps.vGap10,
              Padding(
                padding: EdgeInsets.only(left: 15, right: 15),
                child: Row(
                  children: [
                    Baseline(
                      baseline: 14,
                      baselineType: TextBaseline.alphabetic,
                      child: Icon(
                        AntIcons.exclamation_circle,
                        color: Colours.color_orange_FFB300,
                        size: 13,
                      ),
                    ),
                    Gaps.hGap4,
                    Text(
                      "单个红包",
                      style: TextStyles.btnText14_666666,
                    ),
                    Text(
                      "不得低于0.2元",
                      style: TextStyles.btnText14_F06F2A,
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 15, right: 15, top: 30, bottom: 30),
                child: Dividers.dividerHorizontal(height: 1, color: HexColor('#F1F1F1')),
              ),
              Padding(
                padding: EdgeInsets.only(left: 15, top: 10),
                child: Text(
                  "预计为您带来",
                  style: TextStyles.btnText17B_333333,
                ),
              ),
              Gaps.vGap20,
              Padding(
                padding: EdgeInsets.only(
                  left: 15,
                  right: 15,
                ),
                child: InkWell(
                  child: Container(
                    height: 50,
                    decoration: BoxDecorations.rectCircleHollowGray5,
                    padding: EdgeInsets.only(
                      left: 10,
                      right: 10,
                    ),
                    child: Row(
                      children: [
                        Text(
                          "浏览人次",
                          style: TextStyles.btnText14B_CCCCCC,
                        ),
                        Expanded(child: Gaps.vGap2),
                        Text(
                          "1587 流量",
                          style: TextStyles.btnText14B_333333,
                        ),
                      ],
                    ),
                  ),
                  onTap: () {
                    itemOnClick(0);
                  },
                ),
              ),
              Gaps.vGap20,
              Padding(
                padding: EdgeInsets.only(left: 15, right: 15),
                child: InkWell(
                  child: Container(
                    height: 50,
                    decoration: BoxDecorations.rectCircleHollowGray5,
                    padding: EdgeInsets.only(
                      left: 10,
                      right: 10,
                    ),
                    child: Row(
                      children: [
                        Text(
                          "投票数量",
                          style: TextStyles.btnText14B_CCCCCC,
                        ),
                        Expanded(child: Gaps.vGap2),
                        Text(
                          "18743  票",
                          style: TextStyles.btnText14B_333333,
                        ),
                      ],
                    ),
                  ),
                  onTap: () {
                    itemOnClick(1);
                  },
                ),
              ),
              Expanded(
                child: Gaps.vGap20,
              ),
              Container(
                height: 40,
                margin: EdgeInsets.only(left: 15, right: 15),
                child: GFButton(
                  padding: EdgeInsets.only(left: 10, right: 10),
                  color: true ? HexColor('#1B1D23') : HexColor('#CCCCCC'),
                  borderShape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5),
                    side: BorderSide(
                      width: 0,
                      color: true ? HexColor('#1B1D23') : HexColor('#CCCCCC'),
                      style: BorderStyle.solid,
                    ),
                  ),
                  textStyle: TextStyles.btnTextWhile13,
                  fullWidthButton: true,
                  size: 31,
                  text: "确认发红包",
                  onPressed: () async {},
                ),
              ),
              Gaps.vGap10,
              Padding(
                padding: EdgeInsets.only(left: 15, right: 15),
                child: Row(
                  children: [
                    Text(
                      "评委",
                      style: TextStyles.btnText10_666666,
                    ),
                    Text(
                      "投5票方可领取您得红包",
                      style: TextStyles.btnText10_F06F2A,
                    ),
                    Text(
                      "，同时也可赞助您得作品。",
                      style: TextStyles.btnText10_666666,
                    ),
                  ],
                ),
              ),
              Gaps.vGap20,
            ],
          ),
        ),
      ),
      onWillPop: _requestPop,
    );
  }

  itemOnClick(int index) {
    switch (index) {
      case 0: //浏览人次
        break;
      case 1: //投票数量
        break;
    }
  }
}
