import 'package:ant_icons/ant_icons.dart';
import 'package:flutter_color/flutter_color.dart';
import 'package:flutter_web_2021/Include.dart';
import 'package:flutter_web_2021/pages/mmx/home/publish_works/GiveBackProductsPage.dart';
import 'package:flutter_web_2021/pages/mmx/home/publish_works/WorksHeatingPage.dart';
import 'package:flutter_web_2021/pages/mmx/home/publish_works/mall/MallPage.dart';
import 'package:flutter_web_2021/widgets/img/ImagesViews.dart';
import 'package:getwidget/components/button/gf_button.dart';

class PublishWorksOkPage extends BasePage {
  static String dartPath = "lib/pages/mmx/home/publish_works/PublishWorksPage.dart";

  @override
  _PublishWorksPageState createState() => _PublishWorksPageState();

  @override
  String get pageName => '发布作品成功';
}

class _PublishWorksPageState extends BasePageState<PublishWorksOkPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBarCreationNoRight(context, widget.pageName),
      body: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: 195,
              color: Colours.color_black_252525,
              child: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecorations.rectCircle10,
                child: Row(
                  children: [
                    Container(
                      width: 130,
                      height: 260,
                      child: Stack(
                        children: [
                          ImagesViews.rectangle(
                            "https://tse1-mm.cn.bing.net/th/id/R-C.d9522d356a538d12550bfc638abcba82?rik=5ctIvNjQV1q9bg&riu=http%3a%2f%2fimage2.sina.com.cn%2fent%2fy%2fp%2f2006-07-01%2fU1735P28T3D1143020F326DT20060701184010.jpg&ehk=vX%2fRQTI6nLFxuhrZxu0NUcLFYTpZpJZKKPgrn3bh38g%3d&risl=&pid=ImgRaw&r=0",
                            borderRadius: 10.0,
                          ),
                          Align(
                            child: Image.asset(
                              "asset/image/home/ico_bf.png",
                              width: 25,
                              height: 25,
                            ),
                          ),
                          Positioned(
                            child: Text(
                              "11:20",
                              style: TextStyles.btnTextWhile8,
                            ),
                            bottom: 10.0,
                            right: 10.0,
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      child: Container(
                        margin: Dimens.edgeInsets_lr_dp20,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Gaps.hGap12,
                            Baseline(
                              baseline: 17,
                              baselineType: TextBaseline.alphabetic,
                              child: Text(
                                "恭喜您~",
                                style: TextStyles.btnText17_333333,
                              ),
                            ),
                            Gaps.vGap20,
                            Gaps.vGap10,
                            Baseline(
                              baseline: 17,
                              baselineType: TextBaseline.alphabetic,
                              child: Text(
                                "作品已发布成功！",
                                style: TextStyles.btnText17B_333333,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Dividers.dividerHorizontal(height: 12, color: HexColor('#F1F1F1')),
            Padding(
              padding: EdgeInsets.only(left: 15, top: 10),
              child: Text(
                "作品加热",
                style: TextStyles.btnText17B_333333,
              ),
            ),
            Gaps.vGap20,
            Padding(
              padding: EdgeInsets.only(
                left: 15,
                right: 15,
              ),
              child: InkWell(
                child: Container(
                  height: 50,
                  decoration: BoxDecorations.rectCircleHollowGray5,
                  padding: EdgeInsets.only(
                    left: 10,
                    right: 10,
                  ),
                  child: Row(
                    children: [
                      Text(
                        "发红包",
                        style: TextStyles.btnText14B_333333,
                      ),
                      Expanded(child: Gaps.vGap2),
                      Text(
                        "未选择",
                        style: TextStyles.btnText14_CCCCCC,
                      ),
                      Baseline(
                        baseline: 15,
                        baselineType: TextBaseline.alphabetic,
                        child: Icon(
                          AntIcons.right_outline,
                          color: Colours.color_gray_CCCCCC,
                          size: 14,
                        ),
                      ),
                    ],
                  ),
                ),
                onTap: () {
                  itemOnClick(0);
                },
              ),
            ),
            Gaps.vGap20,
            Padding(
              padding: EdgeInsets.only(left: 15, right: 15),
              child: InkWell(
                child: Container(
                  height: 50,
                  decoration: BoxDecorations.rectCircleHollowGray5,
                  padding: EdgeInsets.only(
                    left: 10,
                    right: 10,
                  ),
                  child: Row(
                    children: [
                      Text(
                        "送花回馈",
                        style: TextStyles.btnText14B_333333,
                      ),
                      Expanded(child: Gaps.vGap2),
                      Text(
                        "未选择",
                        style: TextStyles.btnText14_CCCCCC,
                      ),
                      Baseline(
                        baseline: 15,
                        baselineType: TextBaseline.alphabetic,
                        child: Icon(
                          AntIcons.right_outline,
                          color: Colours.color_gray_CCCCCC,
                          size: 14,
                        ),
                      ),
                    ],
                  ),
                ),
                onTap: () {
                  itemOnClick(1);
                },
              ),
            ),
            Expanded(
              child: Gaps.vGap20,
            ),
            Container(
              height: 40,
              margin: EdgeInsets.only(left: 15, right: 15),
              child: GFButton(
                padding: EdgeInsets.only(left: 10, right: 10),
                color: true ? HexColor('#1B1D23') : HexColor('#CCCCCC'),
                borderShape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5),
                  side: BorderSide(
                    width: 0,
                    color: true ? HexColor('#1B1D23') : HexColor('#CCCCCC'),
                    style: BorderStyle.solid,
                  ),
                ),
                textStyle: TextStyles.btnTextWhile13,
                fullWidthButton: true,
                size: 31,
                text: "完成",
                onPressed: () async {},
              ),
            ),
            Gaps.vGap20,
            Gaps.vGap20,
          ],
        ),
      ),
    );
  }

  itemOnClick(int index) {
    switch (index) {
      case 0: //发红包
        AppNavigator.push(context, WorksHeatingPage());
        break;
      case 1://送花回馈
        AppNavigator.push(context, MallPage());

        break;
    }
  }
}
