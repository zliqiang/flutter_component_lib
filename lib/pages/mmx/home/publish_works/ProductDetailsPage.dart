import 'package:flutter_color/flutter_color.dart';
import 'package:flutter_web_2021/Include.dart';
import 'package:flutter_web_2021/widgets/img/ImagesViews.dart';
import 'package:getwidget/components/button/gf_button.dart';

class ProductDetailsPage extends BasePage {
  static String dartPath = "lib/pages/mmx/home/publish_works/ProductDetailsPage.dart";
  @override
  _ProductDetailsPageState createState() => _ProductDetailsPageState();

  @override
  String get pageName => '商品詳情';
}

class _ProductDetailsPageState extends BasePageState<ProductDetailsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: MyAppBar.appBarCreationNoRight(context, "选择回馈商品"),
      backgroundColor: Colours.color_black_252525,
      body: Container(
        decoration: BoxDecorations.rectCircle10,
        width: Dimens.screenWidth(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Expanded(child:  ListView(
              children: [
                Container(
                  padding: EdgeInsets.all(15),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        height: 170,
                        child: ImagesViews.rectangle(
                          "https://tse1-mm.cn.bing.net/th/id/R-C.d9522d356a538d12550bfc638abcba82?rik=5ctIvNjQV1q9bg&riu=http%3a%2f%2fimage2.sina.com.cn%2fent%2fy%2fp%2f2006-07-01%2fU1735P28T3D1143020F326DT20060701184010.jpg&ehk=vX%2fRQTI6nLFxuhrZxu0NUcLFYTpZpJZKKPgrn3bh38g%3d&risl=&pid=ImgRaw&r=0",
                          borderRadius: 5.0,
                        ),
                      ),
                      Gaps.vGap10,
                      Text(
                        "PECHOIN百雀羚",
                        style: TextStyles.btnText14B_333333,
                      ),
                      Gaps.vGap12,
                      Row(
                        children: [
                          Expanded(child: Text("价值:￥399.00", style: TextStyles.btnText11B_000000)),
                          Row(
                            children: [
                              Text("实收:", style: TextStyles.btnTextRed11),
                              Text("50", style: TextStyles.btnTextRed11B),
                              Text("朵花", style: TextStyles.btnTextRed11),
                            ],
                          ),
                        ],
                      ),
                      Gaps.vGap2,
                      Row(
                        children: [
                          Expanded(child: Text("最低送花:100朵 ", style: TextStyles.btnText11_666666)),
                          Text("等级要求:幼儿园", style: TextStyles.btnText11_666666)
                        ],
                      ),
                    ],
                  ),
                ),
                Dividers.dividerHorizontal(height: 12, color: HexColor('#F1F1F1')),
                Container(
                  padding: EdgeInsets.all(15),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        "产品简介：",
                        style: TextStyles.btnText14B_333333,
                      ),
                      Gaps.vGap10,
                      Gaps.vGap4,
                      Container(
                        child: Row(
                          children: [
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Container(
                                    width: 60,
                                    height: 60,
                                    child: ImagesViews.circle(
                                      "https://tse1-mm.cn.bing.net/th/id/R-C.d9522d356a538d12550bfc638abcba82?rik=5ctIvNjQV1q9bg&riu=http%3a%2f%2fimage2.sina.com.cn%2fent%2fy%2fp%2f2006-07-01%2fU1735P28T3D1143020F326DT20060701184010.jpg&ehk=vX%2fRQTI6nLFxuhrZxu0NUcLFYTpZpJZKKPgrn3bh38g%3d&risl=&pid=ImgRaw&r=0",
                                      borderRadius: 5.0,
                                    ),
                                  ),
                                  Gaps.vGap10,
                                  Gaps.vGap4,
                                  Text(
                                    "淡斑耀白",
                                    style: TextStyles.btnText12_333333,
                                  ),
                                ],
                              ),
                            ),
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Container(
                                    width: 60,
                                    height: 60,
                                    child: ImagesViews.circle(
                                      "https://tse1-mm.cn.bing.net/th/id/R-C.d9522d356a538d12550bfc638abcba82?rik=5ctIvNjQV1q9bg&riu=http%3a%2f%2fimage2.sina.com.cn%2fent%2fy%2fp%2f2006-07-01%2fU1735P28T3D1143020F326DT20060701184010.jpg&ehk=vX%2fRQTI6nLFxuhrZxu0NUcLFYTpZpJZKKPgrn3bh38g%3d&risl=&pid=ImgRaw&r=0",
                                      borderRadius: 5.0,
                                    ),
                                  ),
                                  Gaps.vGap10,
                                  Gaps.vGap4,
                                  Text(
                                    "淡斑耀白",
                                    style: TextStyles.btnText12_333333,
                                  ),
                                ],
                              ),
                            ),
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Container(
                                    width: 60,
                                    height: 60,
                                    child: ImagesViews.circle(
                                      "https://tse1-mm.cn.bing.net/th/id/R-C.d9522d356a538d12550bfc638abcba82?rik=5ctIvNjQV1q9bg&riu=http%3a%2f%2fimage2.sina.com.cn%2fent%2fy%2fp%2f2006-07-01%2fU1735P28T3D1143020F326DT20060701184010.jpg&ehk=vX%2fRQTI6nLFxuhrZxu0NUcLFYTpZpJZKKPgrn3bh38g%3d&risl=&pid=ImgRaw&r=0",
                                      borderRadius: 5.0,
                                    ),
                                  ),
                                  Gaps.vGap10,
                                  Gaps.vGap4,
                                  Text(
                                    "淡斑耀白",
                                    style: TextStyles.btnText12_333333,
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Dividers.dividerHorizontal(height: 12, color: HexColor('#F1F1F1')),
                Container(
                    padding: EdgeInsets.all(15),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          "产品简介：",
                          style: TextStyles.btnText14B_333333,
                        ),
                        Gaps.vGap10,
                        Gaps.vGap4,

                        Text(
                          "1、作者收到用户打赏的花朵，平台收取50%的手续费。",
                          style: TextStyles.btnText12_666666,
                        ),
                        Gaps.vGap8,
                        Text(
                          "2、作者选择商品后，根据商品的不同，收取最低打赏花朵的10%~30%不等的手续费。",
                          style: TextStyles.btnText12_666666,
                        ),
                        Gaps.vGap8,
                        Text(
                          "3、根据以上原则，商品的“实收”为作者选择商品后实际收到的花朵数量。",
                          style: TextStyles.btnText12_666666,
                        ),


                      ],
                    )),

              ],
            ),)
           ,
            Gaps.vGap10,
            Container(
              height: 40,
              margin: EdgeInsets.only(left: 15, right: 15),
              child: GFButton(
                padding: EdgeInsets.only(left: 10, right: 10),
                color: true ? HexColor('#1B1D23') : HexColor('#CCCCCC'),
                borderShape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5),
                  side: BorderSide(
                    width: 0,
                    color: true ? HexColor('#1B1D23') : HexColor('#CCCCCC'),
                    style: BorderStyle.solid,
                  ),
                ),
                textStyle: TextStyles.btnTextWhile13,
                fullWidthButton: true,
                size: 31,
                text: "确认选择",
                onPressed: () async {},
              ),
            ),
            Gaps.vGap10,
            Gaps.vGap10,
          ],
        ),
      ),
    );
  }
}
