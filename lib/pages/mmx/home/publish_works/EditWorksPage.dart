import 'package:ant_icons/ant_icons.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter_color/flutter_color.dart';
import 'package:flutter_vant_kit/main.dart';
import 'package:flutter_vant_kit/widgets/cellGroup.dart';
import 'package:flutter_vant_kit/widgets/field.dart';
import 'package:flutter_web_2021/Include.dart';
import 'package:flutter_web_2021/pages/mmx/home/publish_works/PublishWorksOkPage.dart';
import 'package:flutter_web_2021/widgets/field/my_field.dart';
import 'package:flutter_web_2021/widgets/img/ImagesViews.dart';
import 'package:flutter_web_2021/widgets/input/TextInput.dart';
import 'package:getwidget/components/button/gf_button.dart';

import 'LocationPage.dart';

class EditWorksPage extends BasePage {
  @override
  _EditWorksPageState createState() => _EditWorksPageState();

  @override
  String get pageName => '发布作品编辑';
}

class _EditWorksPageState extends BasePageState<EditWorksPage> with WidgetsBindingObserver {
  bool keyboardPopUp = false;
  FocusNode textFocusNode = FocusNode();
  FocusNode introduceFocusNode = FocusNode();
  TextEditingController textController = TextEditingController();
  TextEditingController introduceController = TextEditingController();
  List tabData = [
    {"id": "11", "title": '大兴区亦庄'},
    {"id": "12", "title": '城乡世纪广场'},
    {"id": "13", "title": '经海一路38街道'},
  ];
  dynamic options = [
    PickerItem("2021年全国最美美女评比"),
    PickerItem("2022年全国最美美女评比"),
    PickerItem("2023年全国最美美女评比"),
  ];

  @override
  void didChangeMetrics() {
    super.didChangeDependencies();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      if (MediaQuery.of(context).viewInsets.bottom == 0) {
        //关闭键盘
        keyboardPopUp = false;
      } else {
        //显示键盘
        keyboardPopUp = true;
      }
    });
  }

  @override
  void initState() {
    super.initState();
    //初始化
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    //销毁
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

// 监听返回
  Future<bool> _requestPop() {
    if (keyboardPopUp) {
      textFocusNode.unfocus();
      introduceFocusNode.unfocus();
    } else {
      Navigator.of(context).pop();
    }
    return new Future.value(false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBarCreationNoRight_2(context, "编辑", onPressed: () {
        _requestPop();
      }),
      backgroundColor: Colours.color_black_252525,
      body: Container(
        decoration: BoxDecorations.rectCircle10,
        width: Dimens.screenWidth(),
        child: ListView(
          children: [
            Gaps.vGap15,
            Padding(
              padding: EdgeInsets.only(left: 15, right: 15),
              child: InkWell(
                child: Container(
                  height: 50,
                  decoration: BoxDecorations.rectCircleHollowGray5,
                  padding: EdgeInsets.only(
                    left: 10,
                    right: 10,
                  ),
                  child: Row(
                    children: [
                      Text(
                        "赛事名称",
                        style: TextStyles.btnText14B_333333,
                      ),
                      Gaps.wGap10,
                      Text(
                        "2021年全国最美美女评比",
                        style: TextStyles.btnText14_333333,
                      ),
                      Expanded(child: Gaps.vGap2),
                      Baseline(
                        baseline: 15,
                        baselineType: TextBaseline.alphabetic,
                        child: Icon(
                          AntIcons.caret_down,
                          color: Colours.color_gray_333333,
                          size: 14,
                        ),
                      ),
                    ],
                  ),
                ),
                onTap: () {
                  textFocusNode.unfocus();
                  introduceFocusNode.unfocus();
                  showModalBottomSheet(
                      context: context,
                      builder: (BuildContext context) {
                        return Picker(
                          colums: options,
                          defaultIndex: 0,
                          showToolbar: true,
                          onConfirm: (values, index) {
                            Log().info(values.toString() + ";" + index.toString());
                            AppNavigator.pop(context);
                          },
                        );
                      });
                },
              ),
            ),
            Gaps.vGap15,
            Padding(
              padding: EdgeInsets.only(left: 15, right: 15),
              child: Container(
                height: 50,
                decoration: BoxDecorations.rectCircleHollowGray5,
                child: Row(
                  children: [
                    Gaps.wGap10,
                    Baseline(
                      baseline: 14,
                      baselineType: TextBaseline.alphabetic,
                      child: Text(
                        "作品名称",
                        style: TextStyles.btnText14B_333333,
                      ),
                    ),
                    Gaps.wGap10,
                    Expanded(
                      child: TextInputView(
                        keyboardType: TextInputType.text,
                        controller: textController,
                        hintText: "请输入参赛作品名称",
                        textColor: Colors.black,
                        hintTextColor: Colors.grey,
                        contentFocusNode: textFocusNode,
                        fontSize: Dimens.sp14,
                        height: 40,
                      ),
                    ),
                    Gaps.wGap10,
                  ],
                ),
              ),
            ),
            Row(
              children: [
                Container(
                  width: 130,
                  height: 165,
                  margin: EdgeInsets.only(top: 15, left: 15, right: 10, bottom: 15),
                  child: Stack(
                    children: [
                      ImagesViews.rectangle(
                        "https://tse1-mm.cn.bing.net/th/id/R-C.d9522d356a538d12550bfc638abcba82?rik=5ctIvNjQV1q9bg&riu=http%3a%2f%2fimage2.sina.com.cn%2fent%2fy%2fp%2f2006-07-01%2fU1735P28T3D1143020F326DT20060701184010.jpg&ehk=vX%2fRQTI6nLFxuhrZxu0NUcLFYTpZpJZKKPgrn3bh38g%3d&risl=&pid=ImgRaw&r=0",
                        borderRadius: 10.0,
                      ),
                      Align(
                        child: Image.asset(
                          "asset/image/home/ico_bf.png",
                          width: 25,
                          height: 25,
                        ),
                      ),
                      Positioned(
                        child: Container(
                          padding: EdgeInsets.only(left: 4, right: 4, top: 1, bottom: 1),
                          decoration: BoxDecorations.rectCircleWhile10,
                          child: Text(
                            "11:20",
                            style: TextStyles.btnTextWhile8,
                          ),
                        ),
                        bottom: 10.0,
                        right: 10.0,
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Container(
                    height: 165,
                    decoration: BoxDecorations.rectCircleHollowGray5,
                    padding: Dimens.edgeInsets_lr_dp10,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Gaps.vGap10,
                        Text(
                          "作品名称",
                          style: TextStyles.btnText12B_333333,
                        ),
                        Expanded(
                          child: MyField(
                              label: null,
                              placeholder: "添加作品描述，获得更多推荐...",
                              controller: introduceController,
                              focusNode: introduceFocusNode,
                              type: "textarea",
                              rows: 6,
                              maxLength: 100,
                              showWordLimit: true),
                        ),
                      ],
                    ),
                  ),
                ),
                Gaps.wGap15
              ],
            ),
            Container(
              margin: Dimens.edgeInsets_lr_dp15,
              padding: EdgeInsets.all(10),
              decoration: BoxDecorations.rectCircleHollowGray5,
              child: Column(
                children: [
                  Row(
                    children: [
                      Baseline(
                        baseline: 15,
                        baselineType: TextBaseline.alphabetic,
                        child: Image.asset(
                          "asset/image/home/works/ico_location.png",
                          width: 12,
                          height: 14,
                        ),
                      ),
                      Gaps.wGap10,
                      Expanded(
                        child: Text(
                          "选择赛区",
                          style: TextStyles.btnText12B_333333,
                        ),
                      ),
                      Baseline(
                        baseline: 15,
                        baselineType: TextBaseline.alphabetic,
                        child: InkWell(
                          onTap: () {
                            AppNavigator.push(context, LocationPage());
                          },
                          child: Icon(
                            AntIcons.right_outline,
                            color: Colours.color_gray_CCCCCC,
                            size: 14,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Gaps.vGap15,
                  Container(
                    width: Dimens.screenWidth(),
                    child: Wrap(
                      spacing: 10,
                      runSpacing: 10,
                      children: tabData
                          .map((e) => Container(
                              padding: EdgeInsets.only(left: 5, right: 5, top: 2, bottom: 2),
                              decoration: BoxDecorations.rectCircleOrange10,
                              child: Baseline(
                                baseline: 12,
                                baselineType: TextBaseline.alphabetic,
                                child: AutoSizeText(
                                  e["title"],
                                  style: TextStyles.btnTextWhile11,
                                ),
                              )))
                          .toList(),
                    ),
                  ),
                  Gaps.vGap5,
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 54, bottom: 30),
              alignment: Alignment.center,
              child: Text(
                "作品分享至",
                style: TextStyles.btnText15B_333333,
              ),
            ),
            Row(
              children: [
                Expanded(
                  child: Container(
                    alignment: Alignment.center,
                    child: Image.asset(
                      "asset/image/home/works/ico_wx.png",
                      width: 22,
                      height: 22,
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    alignment: Alignment.center,
                    child: Image.asset(
                      "asset/image/home/works/ico_pyq.png",
                      width: 22,
                      height: 22,
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    alignment: Alignment.center,
                    child: Image.asset(
                      "asset/image/home/works/ico_qq.png",
                      width: 22,
                      height: 22,
                    ),
                  ),
                ),
              ],
            ),
            Gaps.vGap10,
            Gaps.vGap10,
            Gaps.vGap10,
            Container(
              width: Dimens.screenWidth(),
              height: 40,
              margin: EdgeInsets.only(left: 15, right: 15),
              child: GFButton(
                padding: EdgeInsets.only(left: 10, right: 10),
                color: true ? HexColor('#1B1D23') : HexColor('#CCCCCC'),
                borderShape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5),
                  side: BorderSide(
                    width: 0,
                    color: true ? HexColor('#1B1D23') : HexColor('#CCCCCC'),
                    style: BorderStyle.solid,
                  ),
                ),
                textStyle: TextStyles.btnTextWhile13,
                fullWidthButton: true,
                size: 31,
                text: "确认选择",
                onPressed: () async {
                  textFocusNode.unfocus();
                  introduceFocusNode.unfocus();
                  AppNavigator.push(context, PublishWorksOkPage());
                },
              ),
            ),
            Gaps.vGap10,
            Gaps.vGap10,
          ],
        ),
      ),
    );
  }
}
