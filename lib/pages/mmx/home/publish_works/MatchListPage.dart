import 'package:flutter_web_2021/Include.dart';
import 'package:flutter_web_2021/pages/mmx/home/publish_works/EditWorksPage.dart';
import 'package:flutter_web_2021/pages/mmx/home/publish_works/PublishWorksOkPage.dart';
import 'package:flutter_web_2021/widgets/img/ImagesViews.dart';

class MatchListPage extends BasePage {
  static String dartPath = "lib/pages/mmx/home/publish_works/MatchListPage.dart";

  @override
  _MatchListPageState createState() => _MatchListPageState();

  @override
  String get pageName => '赛事列表';
}

class _MatchListPageState extends BasePageState<MatchListPage> {
  List participateWorks = [
    {
      "numberPeople": "1967",
      "numberCopies": "3458",
      "status": "投稿中",
      "label": "赛事结束时间：2021-5-15  24:00",
      "period": "2021年5月第一期",
      "title": "最美女神",
      "imgUrl":
          "https://tse1-mm.cn.bing.net/th/id/R-C.544a3ba7aaad86e32e5ffa6250d9e900?rik=dMcOKOHaTmlfeQ&riu=http%3a%2f%2fpic3.16pic.com%2f00%2f54%2f83%2f16pic_5483144_b.jpg&ehk=sQIrwd4bdBVClyRCyntdnqbDlxzVHYd69RXsAlXYHSs%3d&risl=&pid=ImgRaw&r=0"
    },
    {
      "numberPeople": "1967",
      "numberCopies": "3458",
      "status": "投稿中",
      "label": "赛事结束时间：2021-5-15  24:00",
      "period": "2021年5月第一期",
      "title": "最美女神",
      "imgUrl":
          "https://tse1-mm.cn.bing.net/th/id/R-C.544a3ba7aaad86e32e5ffa6250d9e900?rik=dMcOKOHaTmlfeQ&riu=http%3a%2f%2fpic3.16pic.com%2f00%2f54%2f83%2f16pic_5483144_b.jpg&ehk=sQIrwd4bdBVClyRCyntdnqbDlxzVHYd69RXsAlXYHSs%3d&risl=&pid=ImgRaw&r=0"
    },
    {
      "numberPeople": "1967",
      "numberCopies": "3458",
      "status": "投稿中",
      "label": "赛事结束时间：2021-5-15  24:00",
      "period": "2021年5月第一期",
      "title": "最美女神",
      "imgUrl":
          "https://tse1-mm.cn.bing.net/th/id/R-C.544a3ba7aaad86e32e5ffa6250d9e900?rik=dMcOKOHaTmlfeQ&riu=http%3a%2f%2fpic3.16pic.com%2f00%2f54%2f83%2f16pic_5483144_b.jpg&ehk=sQIrwd4bdBVClyRCyntdnqbDlxzVHYd69RXsAlXYHSs%3d&risl=&pid=ImgRaw&r=0"
    },
    {
      "numberPeople": "1967",
      "numberCopies": "3458",
      "status": "投稿中",
      "label": "赛事结束时间：2021-5-15  24:00",
      "period": "2021年5月第一期",
      "title": "最美女神",
      "imgUrl":
          "https://tse1-mm.cn.bing.net/th/id/R-C.544a3ba7aaad86e32e5ffa6250d9e900?rik=dMcOKOHaTmlfeQ&riu=http%3a%2f%2fpic3.16pic.com%2f00%2f54%2f83%2f16pic_5483144_b.jpg&ehk=sQIrwd4bdBVClyRCyntdnqbDlxzVHYd69RXsAlXYHSs%3d&risl=&pid=ImgRaw&r=0"
    },
    {
      "numberPeople": "1967",
      "numberCopies": "3458",
      "status": "投稿中",
      "label": "赛事结束时间：2021-5-15  24:00",
      "period": "2021年5月第一期",
      "title": "最美女神",
      "imgUrl":
          "https://tse1-mm.cn.bing.net/th/id/R-C.544a3ba7aaad86e32e5ffa6250d9e900?rik=dMcOKOHaTmlfeQ&riu=http%3a%2f%2fpic3.16pic.com%2f00%2f54%2f83%2f16pic_5483144_b.jpg&ehk=sQIrwd4bdBVClyRCyntdnqbDlxzVHYd69RXsAlXYHSs%3d&risl=&pid=ImgRaw&r=0"
    },
    {
      "numberPeople": "1967",
      "numberCopies": "3458",
      "status": "投稿中",
      "label": "赛事结束时间：2021-5-15  24:00",
      "period": "2021年5月第一期",
      "title": "最美女神",
      "imgUrl":
          "https://tse1-mm.cn.bing.net/th/id/R-C.544a3ba7aaad86e32e5ffa6250d9e900?rik=dMcOKOHaTmlfeQ&riu=http%3a%2f%2fpic3.16pic.com%2f00%2f54%2f83%2f16pic_5483144_b.jpg&ehk=sQIrwd4bdBVClyRCyntdnqbDlxzVHYd69RXsAlXYHSs%3d&risl=&pid=ImgRaw&r=0"
    },
    {
      "numberPeople": "1967",
      "numberCopies": "3458",
      "status": "投稿中",
      "label": "赛事结束时间：2021-5-15  24:00",
      "period": "2021年5月第一期",
      "title": "最美女神",
      "imgUrl":
          "https://tse1-mm.cn.bing.net/th/id/R-C.544a3ba7aaad86e32e5ffa6250d9e900?rik=dMcOKOHaTmlfeQ&riu=http%3a%2f%2fpic3.16pic.com%2f00%2f54%2f83%2f16pic_5483144_b.jpg&ehk=sQIrwd4bdBVClyRCyntdnqbDlxzVHYd69RXsAlXYHSs%3d&risl=&pid=ImgRaw&r=0"
    },
  ];

//参赛作品 item
  Widget participateWorksItem(String imgUrl, String title, String content,
          {String period, String label, String status, String numberPeople, String numberCopies}) =>
      Container(
          height: 118,
          child: Row(
            children: [
              Container(
                width: 98,
                height: 118,
                child: Stack(
                  children: [
                    Container(
                      child: ImagesViews.rectangle(
                        imgUrl,
                        borderRadius: 10.0,
                      ),
                    ),
                    Align(
                      child: Image.asset(
                        "asset/image/home/ico_bf.png",
                        width: 25,
                        height: 25,
                      ),
                    ),
                    Positioned(
                      child: Text(
                        "11:20",
                        style: TextStyles.btnTextWhile8,
                      ),
                      bottom: 10.0,
                      right: 10.0,
                    ),
                  ],
                ),
              ),
              Expanded(
                child: Padding(
                  padding: EdgeInsets.only(left: 17),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        title,
                        style: TextStyles.btnText14_333333,
                      ),
                      Gaps.vGap4,
                      Text(
                        period,
                        style: TextStyles.btnText11_CCCCCC,
                      ),
                      Gaps.vGap8,
                      Row(
                        children: [
                          Container(
                            child: Baseline(
                              baseline: 12,
                              baselineType: TextBaseline.alphabetic,
                              // child: Image.asset("asset/image/home/vip.png", width: 10, height: 10),
                              child: Text(
                                label,
                                style: TextStyles.btnTextWhile9,
                              ),
                            ),
                            alignment: Alignment.center,
                            decoration: BoxDecorations.rectCircle12,
                            padding: EdgeInsets.only(left: 8, right: 8, top: 2, bottom: 2),
                          ),
                          Expanded(
                            child: Gaps.wGap2,
                          ),
                          Baseline(
                            baseline: 12,
                            baselineType: TextBaseline.alphabetic,
                            // child: Image.asset("asset/image/home/vip.png", width: 10, height: 10),
                            child: Text(
                              status,
                              style: TextStyles.btnTextRed11,
                            ),
                          ),

                        ],
                      ),
                      Expanded(child: SizedBox(height: 27)),
                      Row(
                        children: [
                          Baseline(
                            baseline: 15,
                            baselineType: TextBaseline.alphabetic,
                            // child: Image.asset("asset/image/home/vip.png", width: 10, height: 10),
                            child: Text(
                              numberPeople,
                              style: TextStyles.btnText15B_333333,
                            ),
                          ),
                          Baseline(
                            baseline: 14,
                            baselineType: TextBaseline.alphabetic,
                            // child: Image.asset("asset/image/home/vip.png", width: 10, height: 10),
                            child: Text(
                              "人参加",
                              style: TextStyles.btnText12_666666,
                            ),
                          ),
                          Expanded(
                            child: Gaps.wGap2,
                          ),
                          Baseline(
                            baseline: 14,
                            baselineType: TextBaseline.alphabetic,
                            // child: Image.asset("asset/image/home/vip.png", width: 10, height: 10),
                            child: Text(
                              "已投",
                              style: TextStyles.btnText12_666666,
                            ),
                          ),
                          Baseline(
                            baseline: 15,
                            baselineType: TextBaseline.alphabetic,
                            // child: Image.asset("asset/image/home/vip.png", width: 10, height: 10),
                            child: Text(
                              numberCopies,
                              style: TextStyles.btnText15B_333333,
                            ),
                          ),
                          Baseline(
                            baseline: 14,
                            baselineType: TextBaseline.alphabetic,
                            // child: Image.asset("asset/image/home/vip.png", width: 10, height: 10),
                            child: Text(
                              "份",
                              style: TextStyles.btnText12_666666,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ));

  Widget participateWorksView() => ListView.separated(
      padding: EdgeInsets.all(0),
      // shrinkWrap: true,
      // physics: new NeverScrollableScrollPhysics(),
      itemCount: participateWorks.length,
      scrollDirection: Axis.vertical,
      separatorBuilder: (BuildContext context, int index) {
        return Container(
          padding: Dimens.edgeInsets_tb_dp15,
          child: Dividers.dividerHorizontal(height: 1, color: Colours.color_gray_F1F1F1),
        );
      },
      itemBuilder: (BuildContext context, int index) {
        return InkWell(
          child: participateWorksItem(participateWorks[index]["imgUrl"], participateWorks[index]["title"], participateWorks[index]["content"],
              period: participateWorks[index]["period"],
              label: participateWorks[index]["label"],
              status: participateWorks[index]["status"],
              numberPeople: participateWorks[index]["numberPeople"],
              numberCopies: participateWorks[index]["numberCopies"]),
          onTap: (){
            AppNavigator.push(context, EditWorksPage());

          },
        ) ;
      });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBarCreationNoRight_2(context, widget.pageName),
      body: Container(
        padding: EdgeInsets.all(15),
        child: participateWorksView(),
      ),
    );
  }
}
