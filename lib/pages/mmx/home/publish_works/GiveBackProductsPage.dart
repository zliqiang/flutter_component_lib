import 'package:flutter_color/flutter_color.dart';
import 'package:flutter_web_2021/Include.dart';
import 'package:flutter_web_2021/pages/mmx/home/publish_works/view/ProductListView.dart';
import 'package:getwidget/components/button/gf_button.dart';

class GiveBackProductsPage extends BasePage {

  static String dartPath = "lib/pages/mmx/home/publish_works/GiveBackProductsPage.dart";

  @override
  _GiveBackProductsPageState createState() => _GiveBackProductsPageState();

  @override
  String get pageName => '选择回馈商品页面';
}

class _GiveBackProductsPageState extends BasePageState<GiveBackProductsPage> with TickerProviderStateMixin {
  TabController _tabController;

  //当前页面下标
  int currentIndex = 0;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(initialIndex: 0, length: 3, vsync: this); // 直接传this

    _tabController?.addListener(() {
      if (_tabController.index.toDouble() == _tabController.animation.value) {
        setState(() {
          currentIndex = _tabController.index;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: MyAppBar.appBarCreationNoRight(context, widget.pageName),
      backgroundColor: Colours.color_black_252525,
      body: Container(
        decoration: BoxDecorations.rectCircle10,
        width: Dimens.screenWidth(),
        child: Column(
          children: [
            Gaps.vGap15,
            Container(
              width: Dimens.screenWidth(),
              padding: Dimens.edgeInsets_lr_dp15,
              height: 40,
              child: TabBar(
                indicatorColor: Colors.transparent,
                indicatorWeight: 0.01,
                labelPadding: EdgeInsets.all(0),
                controller: _tabController,
                tabs: <Widget>[
                  Tab(
                    child: Container(
                      width: double.infinity,
                      height: double.infinity,
                      alignment: Alignment.center,
                      decoration: BoxDecorations.rectCircleTabSelect5_left(currentIndex == 0 ? true : false),
                      child: Text(
                        "100朵花",
                        style: TextStyles.tabText13B_EA8421(currentIndex == 0 ? true : false),
                      ),
                    ),
                  ),
                  Tab(
                    icon: Container(
                      width: double.infinity,
                      height: double.infinity,
                      alignment: Alignment.center,
                      decoration: BoxDecorations.rectCircleTabSelect_center(currentIndex == 1 ? true : false),
                      child: Text(
                        "500朵花",
                        style: TextStyles.tabText13B_EA8421(currentIndex == 1 ? true : false),
                      ),
                    ),
                  ),
                  Tab(
                    icon: Container(
                      width: double.infinity,
                      height: double.infinity,
                      alignment: Alignment.center,
                      decoration: BoxDecorations.rectCircleTabSelect5_right(currentIndex == 2 ? true : false),
                      child: Text(
                        "1000朵花",
                        style: TextStyles.tabText13B_EA8421(currentIndex == 2 ? true : false),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: TabBarView(
                controller: _tabController,
                children: <Widget>[
                  Center(
                    child:ProductListView(),
                  ),
                  Center(
                    child:ProductListView(),
                  ),
                  Center(
                    child:ProductListView(),
                  ),
                ],
              ),
            ),
            Container(
              decoration: BoxDecorations.rectCircle15_black,
              height: 55,
              child: Row(
                children: [
                  Gaps.wGap15,
                  Text(
                    "已选择：PECHOIN百雀羚",
                    style: TextStyles.btnText12_666666,
                  ),
                  Expanded(child: Gaps.hGap4),
                  Container(
                    height: 30,
                    margin: EdgeInsets.only(left: 15, right: 15),
                    child: GFButton(
                      padding: EdgeInsets.only(left: 20, right: 20),
                      color: true ? HexColor('#1B1D23') : HexColor('#CCCCCC'),
                      borderShape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5),
                        side: BorderSide(
                          width: 0,
                          color: true ? HexColor('#1B1D23') : HexColor('#CCCCCC'),
                          style: BorderStyle.solid,
                        ),
                      ),
                      textStyle: TextStyles.btnTextWhile13,
                      fullWidthButton: false,
                      size: 30,
                      text: "确认选择",
                      onPressed: () async {},
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
