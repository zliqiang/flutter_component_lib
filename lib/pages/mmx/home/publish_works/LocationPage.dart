import 'package:ant_icons/ant_icons.dart';
import 'package:city_pickers/city_pickers.dart';
import 'package:flutter_web_2021/Include.dart';
import 'package:flutter_web_2021/utils/http/DioManagerNoBaseUri.dart';
import 'package:flutter_web_2021/utils/http/NWMethod.dart';
import 'package:flutter_web_2021/utils/toast/OkToastUtil.dart';
import 'package:flutter_web_2021/base/base_mvp_page_state.dart';
import 'LocationP.dart';

class LocationPage extends BasePage {

  static String dartPath = "lib/pages/mmx/home/publish_works/LocationPage.dart";

  @override
  State<StatefulWidget> createState() {
    return LocationPageState();
  }

  @override
  String get pageName => '定位';
}

class LocationPageState extends BaseMvpPageState<LocationPage, LocationP> with WidgetsBindingObserver {
  @override
  LocationP createPresenter() {
    return LocationP();
  }

  FocusNode focusNode = FocusNode();
  bool keyboardPopUp = false;

  @override
  void didChangeMetrics() {
    super.didChangeDependencies();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      if (MediaQuery.of(context).viewInsets.bottom == 0) {
        //关闭键盘
        keyboardPopUp = false;
      } else {
        //显示键盘
        keyboardPopUp = true;
      }
    });
  }

  @override
  void dispose() {
    //销毁
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

// 监听返回
  Future<bool> _requestPop() {
    if (keyboardPopUp) {
      focusNode.unfocus();
    } else {
      Navigator.of(context).pop();
    }
    return new Future.value(false);
  }

  ///poi
  aMapV3Regeo(List<MapEntity> list) {
    setState(() {});
  }

  ///地理编码
  aMapV3Geo(String location) {
    presenter.aMapV3Regeo();
  }

  @override
  void initState() {
    super.initState();

    //初始化
    WidgetsBinding.instance.addObserver(this);
  }

  Widget poiList() => ListView.separated(
      padding: EdgeInsets.all(0),
      // shrinkWrap: true,
      // physics: new NeverScrollableScrollPhysics(),
      itemCount: presenter.pois.length,
      scrollDirection: Axis.vertical,
      separatorBuilder: (BuildContext context, int index) {
        return Padding(
          padding: Dimens.edgeInsets_lr_dp15,
          child: Dividers.dividerHorizontal(height: 1, color: Colours.color_gray_F1F1F1),
        );
      },
      itemBuilder: (BuildContext context, int index) {
        return Row(
          children: [
            Container(
              width: 37,
              alignment: Alignment.center,
              child: Image.asset(
                "asset/image/home/works/ico_dw.png",
                height: 17,
              ),
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Gaps.vGap10,
                  Text(
                    presenter.pois[index].title,
                    style: TextStyles.btnText14B_333333,
                  ),
                  Row(
                    children: [
                      Expanded(
                          child: Text(
                        presenter.pois[index].address,
                        style: TextStyles.btnText12_CCCCCC,
                      )),
                      Text(
                        presenter.pois[index].distance + "m",
                        style: TextStyles.btnText12_CCCCCC,
                      ),
                    ],
                  ),
                  Gaps.vGap10,
                ],
              ),
            ),
            Gaps.wGap15
          ],
        );
      });

  show(BuildContext context) async {
    Result result = await CityPickers.showCityPicker(
        context: context,
        height: 300,
        locationCode: "110000",
        cancelWidget: Text("取消"),
        confirmWidget: Text("确认"),
        itemExtent: 40.0,
        showType: ShowType.pc);
    if (result != null) {
      setState(() {
        presenter.pois.clear();
        presenter.titleLeft =
            result.cityName
                .replaceAll("市", "")
                .replaceAll("省", "")
                .replaceAll("自治州", "")
                .replaceAll("回族", "")
                .replaceAll("城区", "")
                .replaceAll("维吾尔", "")
                .replaceAll("藏族", "")
                .replaceAll("蒙古族", "")
                .replaceAll("特别行政区", "");
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: MyAppBar.appBarAddressSearch(context, "所在位置",
          contentFocusNode: focusNode,
          left: Container(
            child: InkWell(
              onTap: () {
                show(context);
              },
              child: Row(
                children: [
                  Baseline(
                    baseline: 7,
                    baselineType: TextBaseline.alphabetic,
                    // child: Image.asset("asset/image/home/vip.png", width: 10, height: 10),
                    child: Container(
                      width: 4,
                      height: 4,
                      decoration: BoxDecorations.rectCircleWhile5,
                    ),
                  ),
                  Gaps.wGap5,
                  Baseline(
                    baseline: 16,
                    baselineType: TextBaseline.alphabetic,
                    // child: Image.asset("asset/image/home/vip.png", width: 10, height: 10),
                    child: Text(
                      presenter.titleLeft,
                      style: TextStyles.btnTextWhile12,
                    ),
                  ),
                  Gaps.wGap2,
                  Baseline(
                    baseline: 11,
                    baselineType: TextBaseline.alphabetic,
                    child: Icon(
                      AntIcons.caret_down,
                      color: Colours.color_gray_CCCCCC,
                      size: 10,
                    ),
                  ),
                  Gaps.wGap5,
                  Baseline(
                    baseline: 31,
                    baselineType: TextBaseline.alphabetic,
                    child: Padding(
                      padding: EdgeInsets.only(
                        top: 8,
                        bottom: 8,
                      ),
                      child: Dividers.dividerVertical(width: 1, color: Colours.color_gray_8E8E8E),
                    ),
                  ),
                ],
              ),
            ),
          ), onPressed: () {
        _requestPop();
      }, onSubmitted: (String text) {
        presenter.address = text;
        if (presenter.address != "") {
          presenter.aMapV3Geo();
        } else {
          presenter.pois.clear();
        }
      }),
      backgroundColor: Colours.color_black_252525,
      body: Container(
        decoration: BoxDecorations.rectCircle10,
        width: Dimens.screenWidth(),
        child: poiList(),
      ),
    );
  }
}
