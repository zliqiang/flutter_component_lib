import 'package:flutter_web_2021/Include.dart';
import 'package:flutter_web_2021/utils/toast/OkToastUtil.dart';
import 'package:flutter_web_2021/widgets/img/ImagesViews.dart';
import 'package:getwidget/components/button/gf_button.dart';
import 'package:getwidget/shape/gf_button_shape.dart';

class DraftBoxPage extends BasePage {

  static String dartPath = "lib/pages/mmx/home/creation/DraftBoxPage.dart";

  @override
  _DraftBoxPageState createState() => _DraftBoxPageState();

  @override
  String get pageName => '草稿箱';
}

class _DraftBoxPageState extends BasePageState<DraftBoxPage> {
  List draftBoxs = [
    {
      "votes": "0",
      "rank": "暂无排名",
      "data": "上传时间:2021-05-02  13:52",
      "subTitle": "2021年5月第一期",
      "title": "最美女神",
      "imgUrl":
          "https://tse1-mm.cn.bing.net/th/id/R-C.544a3ba7aaad86e32e5ffa6250d9e900?rik=dMcOKOHaTmlfeQ&riu=http%3a%2f%2fpic3.16pic.com%2f00%2f54%2f83%2f16pic_5483144_b.jpg&ehk=sQIrwd4bdBVClyRCyntdnqbDlxzVHYd69RXsAlXYHSs%3d&risl=&pid=ImgRaw&r=0"
    },
    {
      "votes": "0",
      "rank": "暂无排名",
      "data": "上传时间:2021-05-02  13:52",
      "subTitle": "2021年5月第一期",
      "title": "最美女神",
      "imgUrl":
          "https://tse1-mm.cn.bing.net/th/id/R-C.544a3ba7aaad86e32e5ffa6250d9e900?rik=dMcOKOHaTmlfeQ&riu=http%3a%2f%2fpic3.16pic.com%2f00%2f54%2f83%2f16pic_5483144_b.jpg&ehk=sQIrwd4bdBVClyRCyntdnqbDlxzVHYd69RXsAlXYHSs%3d&risl=&pid=ImgRaw&r=0"
    },
    {
      "votes": "0",
      "rank": "暂无排名",
      "data": "上传时间:2021-05-02  13:52",
      "subTitle": "2021年5月第一期",
      "title": "最美女神",
      "imgUrl":
          "https://tse1-mm.cn.bing.net/th/id/R-C.544a3ba7aaad86e32e5ffa6250d9e900?rik=dMcOKOHaTmlfeQ&riu=http%3a%2f%2fpic3.16pic.com%2f00%2f54%2f83%2f16pic_5483144_b.jpg&ehk=sQIrwd4bdBVClyRCyntdnqbDlxzVHYd69RXsAlXYHSs%3d&risl=&pid=ImgRaw&r=0"
    },
    {
      "votes": "0",
      "rank": "暂无排名",
      "data": "上传时间:2021-05-02  13:52",
      "subTitle": "2021年5月第一期",
      "title": "最美女神",
      "imgUrl":
          "https://tse1-mm.cn.bing.net/th/id/R-C.544a3ba7aaad86e32e5ffa6250d9e900?rik=dMcOKOHaTmlfeQ&riu=http%3a%2f%2fpic3.16pic.com%2f00%2f54%2f83%2f16pic_5483144_b.jpg&ehk=sQIrwd4bdBVClyRCyntdnqbDlxzVHYd69RXsAlXYHSs%3d&risl=&pid=ImgRaw&r=0"
    },
    {
      "votes": "0",
      "rank": "暂无排名",
      "data": "上传时间:2021-05-02  13:52",
      "subTitle": "2021年5月第一期",
      "title": "最美女神",
      "imgUrl":
          "https://tse1-mm.cn.bing.net/th/id/R-C.544a3ba7aaad86e32e5ffa6250d9e900?rik=dMcOKOHaTmlfeQ&riu=http%3a%2f%2fpic3.16pic.com%2f00%2f54%2f83%2f16pic_5483144_b.jpg&ehk=sQIrwd4bdBVClyRCyntdnqbDlxzVHYd69RXsAlXYHSs%3d&risl=&pid=ImgRaw&r=0"
    },
    {
      "votes": "0",
      "rank": "暂无排名",
      "data": "上传时间:2021-05-02  13:52",
      "subTitle": "2021年5月第一期",
      "title": "最美女神",
      "imgUrl":
          "https://tse1-mm.cn.bing.net/th/id/R-C.544a3ba7aaad86e32e5ffa6250d9e900?rik=dMcOKOHaTmlfeQ&riu=http%3a%2f%2fpic3.16pic.com%2f00%2f54%2f83%2f16pic_5483144_b.jpg&ehk=sQIrwd4bdBVClyRCyntdnqbDlxzVHYd69RXsAlXYHSs%3d&risl=&pid=ImgRaw&r=0"
    },
  ];

  Widget draftBoxItem(bean, int index) => Container(
        child: Row(
          children: [
            Container(
              width: 140,
              height: 174,
              child: Stack(
                children: [
                  ImagesViews.rectangle(
                    bean["imgUrl"],
                    borderRadius: 10.0,
                  ),
                  Align(
                    child: Image.asset(
                      "asset/image/home/ico_bf.png",
                      width: 25,
                      height: 25,
                    ),
                  ),
                  Positioned(
                    child: Text(
                      "11:20",
                      style: TextStyles.btnTextWhile8,
                    ),
                    bottom: 10.0,
                    right: 10.0,
                  ),
                ],
              ),
            ),
            Gaps.wGap10,
            Expanded(
              child:Container(
                height: 174,
                child:     Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      bean["title"],
                      style: TextStyles.btnText14B_333333,
                    ),
                    Text(
                      bean["subTitle"],
                      style: TextStyles.btnText10_CCCCCC,
                    ),
                    Gaps.vGap5,
                    Dividers.dividerHorizontal(color: Colours.color_gray_F1F1F1),
                    Gaps.vGap10,
                    Row(
                      children: [
                        Baseline(
                          baseline: 12,
                          baselineType: TextBaseline.alphabetic,
                          // child: Image.asset("asset/image/home/vip.png", width: 10, height: 10),
                          child: Text(
                            "累计得票：",
                            style: TextStyles.btnText11_333333,
                          ),
                        ),
                        Baseline(
                          baseline: 15,
                          baselineType: TextBaseline.alphabetic,
                          // child: Image.asset("asset/image/home/vip.png", width: 10, height: 10),
                          child: Text(
                              bean["votes"],
                            style: TextStyles.btnTextRed15B,
                          ),
                        ),
                        Baseline(
                          baseline: 12,
                          baselineType: TextBaseline.alphabetic,
                          // child: Image.asset("asset/image/home/vip.png", width: 10, height: 10),
                          child: Text(
                            "票",
                            style: TextStyles.btnText11_333333,
                          ),
                        ),
                      ],
                    ),
                    Dividers.dividerHorizontal(color: Colours.color_gray_F1F1F1),
                    Expanded(child:  Container(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        bean["rank"],
                        style: TextStyles.btnText15B_333333,
                      ),
                    ) ),
                    Container(
                      alignment: Alignment.center,
                      child:  Image.asset(
                        "asset/image/home/creation/btn_fb.png",
                        width: 189,
                        height: 30,
                      ),
                    ),
                    Gaps.vGap10,
                    Row(
                      children: [
                        Expanded(
                            child: Text(
                              bean["data"],
                              style: TextStyles.btnText9_CCCCCC,
                            )),


                        Container(
                          height: 15,
                          margin: EdgeInsets.all(0),
                          padding: EdgeInsets.all(0),
                          child:PopupMenuButton<String>(
                            padding: EdgeInsets.all(0),
                            icon:Image.asset(
                              "asset/image/home/creation/btn_gd.png",
                              width: 15,
                            ),
                            onSelected: (value) {
                              ToastUtil.showToastBottom(value);
                            },

                            itemBuilder: (context) => <PopupMenuItem<String>>[
                              PopupMenuItem<String>(
                                value: "删除",
                                child: Text("删除"),
                              ),
                              PopupMenuItem<String>(
                                value: "更多",
                                child: Text("更多"),
                              ),
                            ],
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),

            ),
          ],
        ),
      );

  Widget draftBoxList() => ListView.separated(
      // shrinkWrap: true,
      // physics: new NeverScrollableScrollPhysics(),
      itemCount: draftBoxs.length,
      scrollDirection: Axis.vertical,
      separatorBuilder: (BuildContext context, int index) {
        return Container(
          padding: Dimens.edgeInsets_tb_dp15,
          child: Dividers.dividerHorizontal(height: 1, color: Colours.color_gray_F1F1F1),
        );
      },
      itemBuilder: (BuildContext context, int index) {
        return InkWell(
          child: draftBoxItem(draftBoxs[index], index),
          onTap: () {
            // AppNavigator.push(context, EditWorksPage());
          },
        );
      });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBarCreation(
        context,
        widget.pageName,
        right: Container(
          width: 68,
          height: 25,
          child: GFButton(
            // onPressed: onPressed,
            text: "全部  18",
            color: Colors.black,
            textStyle: TextStyles.btnTextWhile11,
            shape: GFButtonShape.standard,
          ),
        ),
      ),
      backgroundColor: Colours.color_black_252525,
      body: Container(
        decoration: BoxDecorations.rectCircle10,
        width: Dimens.screenWidth(),
        padding: EdgeInsets.all(15),
        child: draftBoxList(),
      ),
    );
  }
}
