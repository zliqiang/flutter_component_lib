import 'package:ant_icons/ant_icons.dart';
import 'package:flutter_web_2021/Include.dart';
import 'package:flutter_web_2021/utils/toast/OkToastUtil.dart';
import 'package:flutter_web_2021/widgets/img/ImagesViews.dart';

class PublishedWorksPage extends BasePage {
  static String dartPath = "lib/pages/mmx/home/creation/PublishedWorksPage.dart";
  @override
  _PublishedWorksPageState createState() => _PublishedWorksPageState();

  @override
  String get pageName => '已发表作品';
}

class _PublishedWorksPageState extends BasePageState<PublishedWorksPage> {
  List data = [
    {
      "votes": "11096",
      "data": "上传时间:2021-05-02  13:52",
      "subTitle": "5月第一期赛事",
      "TitleTwo": "全国最美美女",
      "titleOne": "2021年全国最美美女评比",
      "label": "2021/05/01 ~ 2021/05/07   5月第一期赛事",
      "imgUrl":
          "https://tse1-mm.cn.bing.net/th/id/R-C.544a3ba7aaad86e32e5ffa6250d9e900?rik=dMcOKOHaTmlfeQ&riu=http%3a%2f%2fpic3.16pic.com%2f00%2f54%2f83%2f16pic_5483144_b.jpg&ehk=sQIrwd4bdBVClyRCyntdnqbDlxzVHYd69RXsAlXYHSs%3d&risl=&pid=ImgRaw&r=0"
    },
    {
      "votes": "11096",
      "data": "上传时间:2021-05-02  13:52",
      "subTitle": "5月第一期赛事",
      "TitleTwo": "全国最美美女",
      "titleOne": "2021年全国最美美女评比",
      "label": "2021/05/01 ~ 2021/05/07   5月第一期赛事",
      "imgUrl":
          "https://tse1-mm.cn.bing.net/th/id/R-C.544a3ba7aaad86e32e5ffa6250d9e900?rik=dMcOKOHaTmlfeQ&riu=http%3a%2f%2fpic3.16pic.com%2f00%2f54%2f83%2f16pic_5483144_b.jpg&ehk=sQIrwd4bdBVClyRCyntdnqbDlxzVHYd69RXsAlXYHSs%3d&risl=&pid=ImgRaw&r=0"
    },
    {
      "votes": "11096",
      "data": "上传时间:2021-05-02  13:52",
      "subTitle": "5月第一期赛事",
      "TitleTwo": "全国最美美女",
      "titleOne": "2021年全国最美美女评比",
      "label": "2021/05/01 ~ 2021/05/07   5月第一期赛事",
      "imgUrl":
          "https://tse1-mm.cn.bing.net/th/id/R-C.544a3ba7aaad86e32e5ffa6250d9e900?rik=dMcOKOHaTmlfeQ&riu=http%3a%2f%2fpic3.16pic.com%2f00%2f54%2f83%2f16pic_5483144_b.jpg&ehk=sQIrwd4bdBVClyRCyntdnqbDlxzVHYd69RXsAlXYHSs%3d&risl=&pid=ImgRaw&r=0"
    },
    {
      "votes": "11096",
      "data": "上传时间:2021-05-02  13:52",
      "subTitle": "5月第一期赛事",
      "TitleTwo": "全国最美美女",
      "titleOne": "2021年全国最美美女评比",
      "label": "2021/05/01 ~ 2021/05/07   5月第一期赛事",
      "imgUrl":
          "https://tse1-mm.cn.bing.net/th/id/R-C.544a3ba7aaad86e32e5ffa6250d9e900?rik=dMcOKOHaTmlfeQ&riu=http%3a%2f%2fpic3.16pic.com%2f00%2f54%2f83%2f16pic_5483144_b.jpg&ehk=sQIrwd4bdBVClyRCyntdnqbDlxzVHYd69RXsAlXYHSs%3d&risl=&pid=ImgRaw&r=0"
    },
    {
      "votes": "11096",
      "data": "上传时间:2021-05-02  13:52",
      "subTitle": "5月第一期赛事",
      "TitleTwo": "全国最美美女",
      "titleOne": "2021年全国最美美女评比",
      "label": "2021/05/01 ~ 2021/05/07   5月第一期赛事",
      "imgUrl":
          "https://tse1-mm.cn.bing.net/th/id/R-C.544a3ba7aaad86e32e5ffa6250d9e900?rik=dMcOKOHaTmlfeQ&riu=http%3a%2f%2fpic3.16pic.com%2f00%2f54%2f83%2f16pic_5483144_b.jpg&ehk=sQIrwd4bdBVClyRCyntdnqbDlxzVHYd69RXsAlXYHSs%3d&risl=&pid=ImgRaw&r=0"
    },
  ];

  //参赛作品 item
  Widget participateWorksItem(
    String imgUrl,
    String title,
    String content,
    String unit, {
    bool showRedPoint = false,
    bool showSide = true,
  }) =>
      Container(
        height: 48,
        child: Stack(
          children: [
            Container(
              decoration: showSide ? BoxDecorations.rectCircleHollow10 : null,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Baseline(
                        baseline: 16,
                        baselineType: TextBaseline.alphabetic,
                        child: Image.asset(
                          imgUrl,
                          width: 16,
                          height: 16,
                        ),
                      ),
                      Gaps.hGap4,
                      Baseline(
                        baseline: 12,
                        baselineType: TextBaseline.alphabetic,
                        child: Text(
                          title,
                          style: TextStyles.btnText11_666666,
                        ),
                      ),
                    ],
                  ),
                  Gaps.vGap4,
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    // crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Baseline(
                        baseline: 12,
                        baselineType: TextBaseline.alphabetic,
                        child: Text(
                          content,
                          style: TextStyles.btnText12B_4A4A4A,
                        ),
                      ),
                      Baseline(
                        baseline: 9,
                        baselineType: TextBaseline.alphabetic,
                        child: Text(
                          unit,
                          style: TextStyles.btnText8_4A4A4A,
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
            Offstage(
              offstage: showRedPoint ? false : true,
              child: Align(
                child: Container(
                  decoration: BoxDecorations.rectCircle5_red,
                  width: 8,
                  height: 8,
                ),
                alignment: Alignment.topRight,
              ),
            ),
          ],
        ),
      );

  Widget worksItem(bean, int index) => Container(
        padding: EdgeInsets.only(left: 15, right: 15, top: 10, bottom: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              bean["titleOne"],
              style: TextStyles.btnText15B_333333,
            ),
            Gaps.vGap4,
            Container(
              child: Baseline(
                baseline: 12,
                baselineType: TextBaseline.alphabetic,
                // child: Image.asset("asset/image/home/vip.png", width: 10, height: 10),
                child: Text(
                  bean["label"],
                  style: TextStyles.btnTextWhile10,
                ),
              ),
              decoration: BoxDecorations.rectCircleColor(color: "#C69F66", radius: 10),
              padding: EdgeInsets.only(left: 10, right: 10, top: 1, bottom: 1),
            ),
            SizedBox(height: 14),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  width: 140,
                  height: 217,
                  child: Stack(
                    children: [
                      Container(
                        child: ImagesViews.rectangle(
                          bean["imgUrl"],
                          borderRadius: 10.0,
                        ),
                      ),
                      Align(
                        child: Image.asset(
                          "asset/image/home/ico_bf.png",
                          width: 25,
                          height: 25,
                        ),
                      ),
                      Positioned(
                        child: Text(
                          "11:20",
                          style: TextStyles.btnTextWhile8,
                        ),
                        bottom: 10.0,
                        right: 10.0,
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Container(
                    width: 140,
                    height: 217,
                    margin: EdgeInsets.only(left:15 ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Baseline(
                              baseline: 14,
                              baselineType: TextBaseline.alphabetic,
                              // child: Image.asset("asset/image/home/vip.png", width: 10, height: 10),
                              child: Text(
                                bean["TitleTwo"],
                                style: TextStyles.btnText14_333333,
                              ),
                            ),
                            Baseline(
                              baseline: 16,
                              baselineType: TextBaseline.alphabetic,
                              // child: Image.asset("asset/image/home/vip.png", width: 10, height: 10),
                              child: Text(
                                bean["subTitle"],
                                style: TextStyles.btnText10_CCCCCC,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 10),
                        Dividers.dividerHorizontal(height: 0.5, color: Colours.color_gray_CCCCCC),
                        Expanded(child: SizedBox(height: 10)),
                        Row(
                          children: [
                            Baseline(
                              baseline: 12,
                              baselineType: TextBaseline.alphabetic,
                              // child: Image.asset("asset/image/home/vip.png", width: 10, height: 10),
                              child: Text(
                                "已投作品：",
                                style: TextStyles.btnText11_999999,
                              ),
                            ),
                            Baseline(
                              baseline: 14,
                              baselineType: TextBaseline.alphabetic,
                              // child: Image.asset("asset/image/home/vip.png", width: 10, height: 10),
                              child: Text(
                                bean["votes"],
                                style: TextStyles.btnTextRed12B,
                              ),
                            ),
                            Baseline(
                              baseline: 12,
                              baselineType: TextBaseline.alphabetic,
                              // child: Image.asset("asset/image/home/vip.png", width: 10, height: 10),
                              child: Text(
                                "个",
                                style: TextStyles.btnText11_999999,
                              ),
                            ),
                          ],
                        ),
                        Expanded(child: SizedBox(height: 10)),
                        Dividers.dividerHorizontal(height: 0.5, color: Colours.color_gray_CCCCCC),
                        SizedBox(height: 12),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Expanded(child: participateWorksItem("asset/image/home/ico_grade_15.png", "打赏", "35", "平币")),
                            Gaps.hGap6,
                            Expanded(child: participateWorksItem("asset/image/home/creation/ico_zz.png", "赞助", "35", "")),
                          ],
                        ),
                        SizedBox(height: 5),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Expanded(child: participateWorksItem("asset/image/home/creation/ico_tm.png", "赞助", "1039", "")),
                            Gaps.hGap6,
                            Expanded(child: participateWorksItem("asset/image/home/creation/ico_px.png", "捧星", "1571", "")),
                          ],
                        ),
                        SizedBox(height: 8),
                        Row(
                          children: [
                            Text(
                              bean["data"],
                              style: TextStyles.btnText12_CCCCCC,
                              overflow: TextOverflow.ellipsis,
                              maxLines: 1,
                            ),
                            Expanded(child: Gaps.vGap10),
                            InkWell(
                              child: Icon(
                                AntIcons.ellipsis,
                                color: Colours.color_black_252525,
                                size: 15,
                              ),
                              onTap: () {
                                ToastUtil.showToastBottom("...");
                              },
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            Gaps.vGap15,
            Row(
              children: [
                Expanded(child: Container(
                  height: 66,
                  decoration:  BoxDecorations.rectCircleColor(color: "#F1F1F1", radius: 5),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "全国",
                        style: TextStyles.btnText12_666666,
                      ),
                      Gaps.vGap2,
                      Text(
                        "第 10 名",
                        style: TextStyles.btnText15_F06F2A,
                      ),
                    ],
                  ),
                )),
                Gaps.wGap10,
                Expanded(child: Container(
                  height: 66,
                  decoration:  BoxDecorations.rectCircleColor(color: "#F1F1F1", radius: 5),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "河北省",
                        style: TextStyles.btnText12_666666,
                      ),
                      Gaps.vGap2,
                      Text(
                        "第 6 名",
                        style: TextStyles.btnText15_F06F2A,
                      ),
                    ],
                  ),
                )),
                Gaps.wGap10,
                Expanded(child: Container(
                  height: 66,
                  decoration:  BoxDecorations.rectCircleColor(color: "#F1F1F1", radius: 5),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "石家庄",
                        style: TextStyles.btnText12_666666,
                      ),
                      Gaps.vGap2,
                      Text(
                        "第 3 名",
                        style: TextStyles.btnText15_F06F2A,
                      ),
                    ],
                  ),
                )),
                Gaps.wGap10,
                Expanded(child: Container(
                  height: 66,
                  decoration:  BoxDecorations.rectCircleColor(color: "#F1F1F1", radius: 5),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "长安区",
                        style: TextStyles.btnText12_666666,
                      ),
                      Gaps.vGap2,
                      Text(
                        "第 1 名",
                        style: TextStyles.btnText15_F06F2A,
                      ),
                    ],
                  ),
                )),
              ],
            ),
            Gaps.vGap4,
          ],
        ),
      );

  Widget worksList() => ListView.separated(
      // shrinkWrap: true,
      // physics: new NeverScrollableScrollPhysics(),
      itemCount: data.length,
      scrollDirection: Axis.vertical,
      separatorBuilder: (BuildContext context, int index) {
        return Container(
          child: Dividers.dividerHorizontal(height: 10, color: Colours.color_gray_F1F1F1),
        );
      },
      itemBuilder: (BuildContext context, int index) {
        return InkWell(
          child: worksItem(data[index], index),
          onTap: () {
            // AppNavigator.push(context, EditWorksPage());
          },
        );
      });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBarCreationNoRight_2(
        context,
        widget.pageName,
      ),
      backgroundColor: Colours.color_black_252525,
      body: Container(
        decoration: BoxDecorations.rectCircle10,
        width: Dimens.screenWidth(),
        child: worksList(),
      ),
    );
  }
}
