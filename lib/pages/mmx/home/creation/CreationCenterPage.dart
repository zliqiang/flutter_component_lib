import 'package:ant_icons/ant_icons.dart';
import 'package:flutter_color/flutter_color.dart';
import 'package:flutter_web_2021/Include.dart';
import 'package:flutter_web_2021/pages/mmx/home/creation/DraftBoxPage.dart';
import 'package:flutter_web_2021/pages/mmx/home/creation/PublishedWorksPage.dart';
import 'package:flutter_web_2021/pages/mmx/home/creation/WorkIncomePage.dart';
import 'package:flutter_web_2021/utils/toast/OkToastUtil.dart';
import 'package:flutter_web_2021/widgets/img/ImagesViews.dart';
import 'package:flutter_web_2021/widgets/load_view/LoadInitializeView.dart';
import 'package:getwidget/components/button/gf_button.dart';

class CreationCenterPage extends BasePage {
  static String dartPath = "lib/pages/mmx/home/creation/CreationCenterPage.dart";

  @override
  _CreationCenterPageState createState() => _CreationCenterPageState();

  @override
  String get pageName => '创作中心';
}

class _CreationCenterPageState extends BasePageState<CreationCenterPage> {
  List popularMatchs = [
    {
      "title": "全国最美美女",
      "period": "5月第一期赛事",
      "label": "赛事时间：5月1日-5月7日",
      "votes": "6000",
      "endData": "报名截止时间：2021-05-07",
      "publishData": "结果公布时间：2021-05-15",
      "imgUrl":
          "https://tse1-mm.cn.bing.net/th/id/R-C.d9522d356a538d12550bfc638abcba82?rik=5ctIvNjQV1q9bg&riu=http%3a%2f%2fimage2.sina.com.cn%2fent%2fy%2fp%2f2006-07-01%2fU1735P28T3D1143020F326DT20060701184010.jpg&ehk=vX%2fRQTI6nLFxuhrZxu0NUcLFYTpZpJZKKPgrn3bh38g%3d&risl=&pid=ImgRaw&r=0",
    },
    {
      "title": "全国最美美女",
      "period": "5月第一期赛事",
      "label": "赛事时间：5月1日-5月7日",
      "votes": "6000",
      "endData": "报名截止时间：2021-05-07",
      "publishData": "结果公布时间：2021-05-15",
      "imgUrl":
          "https://tse1-mm.cn.bing.net/th/id/R-C.d9522d356a538d12550bfc638abcba82?rik=5ctIvNjQV1q9bg&riu=http%3a%2f%2fimage2.sina.com.cn%2fent%2fy%2fp%2f2006-07-01%2fU1735P28T3D1143020F326DT20060701184010.jpg&ehk=vX%2fRQTI6nLFxuhrZxu0NUcLFYTpZpJZKKPgrn3bh38g%3d&risl=&pid=ImgRaw&r=0",
    },
    {
      "title": "全国最美美女",
      "period": "5月第一期赛事",
      "label": "赛事时间：5月1日-5月7日",
      "votes": "6000",
      "endData": "报名截止时间：2021-05-07",
      "publishData": "结果公布时间：2021-05-15",
      "imgUrl":
          "https://tse1-mm.cn.bing.net/th/id/R-C.d9522d356a538d12550bfc638abcba82?rik=5ctIvNjQV1q9bg&riu=http%3a%2f%2fimage2.sina.com.cn%2fent%2fy%2fp%2f2006-07-01%2fU1735P28T3D1143020F326DT20060701184010.jpg&ehk=vX%2fRQTI6nLFxuhrZxu0NUcLFYTpZpJZKKPgrn3bh38g%3d&risl=&pid=ImgRaw&r=0",
    },
    {
      "title": "全国最美美女",
      "period": "5月第一期赛事",
      "label": "赛事时间：5月1日-5月7日",
      "votes": "6000",
      "endData": "报名截止时间：2021-05-07",
      "publishData": "结果公布时间：2021-05-15",
      "imgUrl":
          "https://tse1-mm.cn.bing.net/th/id/R-C.d9522d356a538d12550bfc638abcba82?rik=5ctIvNjQV1q9bg&riu=http%3a%2f%2fimage2.sina.com.cn%2fent%2fy%2fp%2f2006-07-01%2fU1735P28T3D1143020F326DT20060701184010.jpg&ehk=vX%2fRQTI6nLFxuhrZxu0NUcLFYTpZpJZKKPgrn3bh38g%3d&risl=&pid=ImgRaw&r=0",
    },
    {
      "title": "全国最美美女",
      "period": "5月第一期赛事",
      "label": "赛事时间：5月1日-5月7日",
      "votes": "6000",
      "endData": "报名截止时间：2021-05-07",
      "publishData": "结果公布时间：2021-05-15",
      "imgUrl":
          "https://tse1-mm.cn.bing.net/th/id/R-C.d9522d356a538d12550bfc638abcba82?rik=5ctIvNjQV1q9bg&riu=http%3a%2f%2fimage2.sina.com.cn%2fent%2fy%2fp%2f2006-07-01%2fU1735P28T3D1143020F326DT20060701184010.jpg&ehk=vX%2fRQTI6nLFxuhrZxu0NUcLFYTpZpJZKKPgrn3bh38g%3d&risl=&pid=ImgRaw&r=0",
    },
    {
      "title": "全国最美美女",
      "period": "5月第一期赛事",
      "label": "赛事时间：5月1日-5月7日",
      "votes": "6000",
      "endData": "报名截止时间：2021-05-07",
      "publishData": "结果公布时间：2021-05-15",
      "imgUrl":
          "https://tse1-mm.cn.bing.net/th/id/R-C.d9522d356a538d12550bfc638abcba82?rik=5ctIvNjQV1q9bg&riu=http%3a%2f%2fimage2.sina.com.cn%2fent%2fy%2fp%2f2006-07-01%2fU1735P28T3D1143020F326DT20060701184010.jpg&ehk=vX%2fRQTI6nLFxuhrZxu0NUcLFYTpZpJZKKPgrn3bh38g%3d&risl=&pid=ImgRaw&r=0",
    },
    {
      "title": "全国最美美女",
      "period": "5月第一期赛事",
      "label": "赛事时间：5月1日-5月7日",
      "votes": "6000",
      "endData": "报名截止时间：2021-05-07",
      "publishData": "结果公布时间：2021-05-15",
      "imgUrl":
          "https://tse1-mm.cn.bing.net/th/id/R-C.d9522d356a538d12550bfc638abcba82?rik=5ctIvNjQV1q9bg&riu=http%3a%2f%2fimage2.sina.com.cn%2fent%2fy%2fp%2f2006-07-01%2fU1735P28T3D1143020F326DT20060701184010.jpg&ehk=vX%2fRQTI6nLFxuhrZxu0NUcLFYTpZpJZKKPgrn3bh38g%3d&risl=&pid=ImgRaw&r=0",
    },
    {
      "title": "全国最美美女",
      "period": "5月第一期赛事",
      "label": "赛事时间：5月1日-5月7日",
      "votes": "6000",
      "endData": "报名截止时间：2021-05-07",
      "publishData": "结果公布时间：2021-05-15",
      "imgUrl":
          "https://tse1-mm.cn.bing.net/th/id/R-C.d9522d356a538d12550bfc638abcba82?rik=5ctIvNjQV1q9bg&riu=http%3a%2f%2fimage2.sina.com.cn%2fent%2fy%2fp%2f2006-07-01%2fU1735P28T3D1143020F326DT20060701184010.jpg&ehk=vX%2fRQTI6nLFxuhrZxu0NUcLFYTpZpJZKKPgrn3bh38g%3d&risl=&pid=ImgRaw&r=0",
    },
    {
      "title": "全国最美美女",
      "period": "5月第一期赛事",
      "label": "赛事时间：5月1日-5月7日",
      "votes": "6000",
      "endData": "报名截止时间：2021-05-07",
      "publishData": "结果公布时间：2021-05-15",
      "imgUrl":
          "https://tse1-mm.cn.bing.net/th/id/R-C.d9522d356a538d12550bfc638abcba82?rik=5ctIvNjQV1q9bg&riu=http%3a%2f%2fimage2.sina.com.cn%2fent%2fy%2fp%2f2006-07-01%2fU1735P28T3D1143020F326DT20060701184010.jpg&ehk=vX%2fRQTI6nLFxuhrZxu0NUcLFYTpZpJZKKPgrn3bh38g%3d&risl=&pid=ImgRaw&r=0",
    },
    {
      "title": "全国最美美女",
      "period": "5月第一期赛事",
      "label": "赛事时间：5月1日-5月7日",
      "votes": "6000",
      "endData": "报名截止时间：2021-05-07",
      "publishData": "结果公布时间：2021-05-15",
      "imgUrl":
          "https://tse1-mm.cn.bing.net/th/id/R-C.d9522d356a538d12550bfc638abcba82?rik=5ctIvNjQV1q9bg&riu=http%3a%2f%2fimage2.sina.com.cn%2fent%2fy%2fp%2f2006-07-01%2fU1735P28T3D1143020F326DT20060701184010.jpg&ehk=vX%2fRQTI6nLFxuhrZxu0NUcLFYTpZpJZKKPgrn3bh38g%3d&risl=&pid=ImgRaw&r=0",
    },
    {
      "title": "全国最美美女",
      "period": "5月第一期赛事",
      "label": "赛事时间：5月1日-5月7日",
      "votes": "6000",
      "endData": "报名截止时间：2021-05-07",
      "publishData": "结果公布时间：2021-05-15",
      "imgUrl":
          "https://tse1-mm.cn.bing.net/th/id/R-C.d9522d356a538d12550bfc638abcba82?rik=5ctIvNjQV1q9bg&riu=http%3a%2f%2fimage2.sina.com.cn%2fent%2fy%2fp%2f2006-07-01%2fU1735P28T3D1143020F326DT20060701184010.jpg&ehk=vX%2fRQTI6nLFxuhrZxu0NUcLFYTpZpJZKKPgrn3bh38g%3d&risl=&pid=ImgRaw&r=0",
    },
    {
      "title": "全国最美美女",
      "period": "5月第一期赛事",
      "label": "赛事时间：5月1日-5月7日",
      "votes": "6000",
      "endData": "报名截止时间：2021-05-07",
      "publishData": "结果公布时间：2021-05-15",
      "imgUrl":
          "https://tse1-mm.cn.bing.net/th/id/R-C.d9522d356a538d12550bfc638abcba82?rik=5ctIvNjQV1q9bg&riu=http%3a%2f%2fimage2.sina.com.cn%2fent%2fy%2fp%2f2006-07-01%2fU1735P28T3D1143020F326DT20060701184010.jpg&ehk=vX%2fRQTI6nLFxuhrZxu0NUcLFYTpZpJZKKPgrn3bh38g%3d&risl=&pid=ImgRaw&r=0",
    },
  ];

  Widget menuItem(String imgUri, String title, {GestureTapCallback onTap}) => Container(
        height: 90,
        child: InkWell(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset(
                imgUri,
                width: 40,
                height: 40,
              ),
              Gaps.vGap8,
              Text(
                title,
                style: TextStyles.btnText12_333333,
              ),
            ],
          ),
          onTap: onTap,
        ),
      );

//参赛作品 item
  Widget participateWorksItem(
    String imgUrl,
    String title,
    String content,
    String unit, {
    bool showRedPoint = false,
    bool showSide = true,
  }) =>
      Container(
        height: 48,
        child: Stack(
          children: [
            Container(
              decoration: showSide ? BoxDecorations.rectCircleHollow10 : null,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Baseline(
                        baseline: 16,
                        baselineType: TextBaseline.alphabetic,
                        child: Image.asset(
                          imgUrl,
                          width: 16,
                          height: 16,
                        ),
                      ),
                      Gaps.hGap4,
                      Baseline(
                        baseline: 12,
                        baselineType: TextBaseline.alphabetic,
                        child: Text(
                          title,
                          style: TextStyles.btnText11_666666,
                        ),
                      ),
                    ],
                  ),
                  Gaps.vGap4,
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    // crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Baseline(
                        baseline: 12,
                        baselineType: TextBaseline.alphabetic,
                        child: Text(
                          content,
                          style: TextStyles.btnText12B_4A4A4A,
                        ),
                      ),
                      Baseline(
                        baseline: 9,
                        baselineType: TextBaseline.alphabetic,
                        child: Text(
                          unit,
                          style: TextStyles.btnText8_4A4A4A,
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
            Offstage(
              offstage: showRedPoint ? false : true,
              child: Align(
                child: Container(
                  decoration: BoxDecorations.rectCircle5_red,
                  width: 8,
                  height: 8,
                ),
                alignment: Alignment.topRight,
              ),
            ),
          ],
        ),
      );

  ///参赛作品
  Widget participateWorks({String data, String title, String imgUrl, String period, String label, String status, String votes}) => Container(
      height: 260,
      child: Row(
        children: [
          Container(
            width: 145,
            height: 260,
            child: Stack(
              children: [
                Container(
                  width: 145,
                  height: 260,
                  child: ImagesViews.rectangle(
                    imgUrl,
                    borderRadius: 10.0,
                  ),
                ),
                Align(
                  child: Image.asset(
                    "asset/image/home/ico_bf.png",
                    width: 25,
                    height: 25,
                  ),
                ),
                Positioned(
                  child: Text(
                    "11:20",
                    style: TextStyles.btnTextWhile8,
                  ),
                  bottom: 10.0,
                  right: 10.0,
                ),
              ],
            ),
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(left: 17, top: 0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    title,
                    style: TextStyles.btnText14B_333333,
                  ),
                  Text(
                    period,
                    style: TextStyles.btnText12_CCCCCC,
                  ),
                  Gaps.vGap8,
                  Row(
                    children: [
                      Container(
                        child: Baseline(
                          baseline: 12,
                          baselineType: TextBaseline.alphabetic,
                          // child: Image.asset("asset/image/home/vip.png", width: 10, height: 10),
                          child: Text(
                            label,
                            style: TextStyles.btnTextWhile10,
                          ),
                        ),
                        alignment: Alignment.center,
                        decoration: BoxDecorations.rectCircle8,
                        padding: EdgeInsets.only(left: 10, right: 10, top: 1, bottom: 1),
                      ),
                      Expanded(
                        child: Gaps.vGap8,
                      ),
                    ],
                  ),
                  Gaps.vGap12,
                  Dividers.dividerHorizontal(height: 0.5, color: Colours.color_gray_CCCCCC),
                  Expanded(child: SizedBox(height: 27)),
                  Row(
                    children: [
                      Baseline(
                        baseline: 12,
                        baselineType: TextBaseline.alphabetic,
                        // child: Image.asset("asset/image/home/vip.png", width: 10, height: 10),
                        child: Text(
                          "当前得票：",
                          style: TextStyles.btnText11_333333,
                        ),
                      ),
                      Baseline(
                        baseline: 15,
                        baselineType: TextBaseline.alphabetic,
                        // child: Image.asset("asset/image/home/vip.png", width: 10, height: 10),
                        child: Text(
                          votes,
                          style: TextStyles.btnTextRed15B,
                        ),
                      ),
                      Baseline(
                        baseline: 12,
                        baselineType: TextBaseline.alphabetic,
                        // child: Image.asset("asset/image/home/vip.png", width: 10, height: 10),
                        child: Text(
                          "票",
                          style: TextStyles.btnText11_333333,
                        ),
                      ),
                    ],
                  ),
                  Expanded(child: SizedBox(height: 27)),
                  Dividers.dividerHorizontal(height: 0.5, color: Colours.color_gray_CCCCCC),
                  SizedBox(height: 10),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Expanded(child: participateWorksItem("asset/image/home/ico_grade_15.png", "打赏", "35", "平币")),
                      Gaps.hGap6,
                      Expanded(child: participateWorksItem("asset/image/home/creation/ico_zz.png", "赞助", "35", "")),
                    ],
                  ),
                  SizedBox(height: 5),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Expanded(child: participateWorksItem("asset/image/home/creation/ico_tm.png", "赞助", "1039", "")),
                      Gaps.hGap6,
                      Expanded(child: participateWorksItem("asset/image/home/creation/ico_px.png", "捧星", "1571", "")),
                    ],
                  ),
                  SizedBox(height: 5),
                  Row(
                    children: [
                      Text(
                        data,
                        style: TextStyles.btnText12_CCCCCC,
                        overflow: TextOverflow.ellipsis,
                        maxLines: 1,
                      ),
                      Expanded(child: Gaps.vGap10),
                      InkWell(
                        child: Icon(
                          AntIcons.ellipsis,
                          color: Colours.color_black_252525,
                          size: 15,
                        ),
                        onTap: () {
                          ToastUtil.showToastBottom("...");
                        },
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
        ],
      ));

//热门赛事 item
  Widget popularMatchItem(int index, {String endData, String publishData, String title, String imgUrl, String period, String label, String votes}) =>
      Container(
          height: 225,
          child: Row(
            children: [
              Container(
                width: 155,
                height: 260,
                child: Stack(
                  children: [
                    Container(
                      width: 155,
                      height: 252,
                      child: ImagesViews.rectangle(
                        imgUrl,
                        borderRadius: 10.0,
                      ),
                      margin: EdgeInsets.only(top: 8),
                    ),
                    Align(
                      child: Image.asset(
                        "asset/image/home/ico_bf.png",
                        width: 25,
                        height: 25,
                      ),
                    ),
                    Positioned(
                      child: Text(
                        "11:20",
                        style: TextStyles.btnTextWhile8,
                      ),
                      bottom: 10.0,
                      right: 10.0,
                    ),
                    Positioned(
                      child: Offstage(
                        offstage: index > 2 ? true : false,
                        child: index > 2
                            ? Container()
                            : Image.asset(
                                "asset/image/home/ico_top" + (index + 1).toString() + ".png",
                                width: 47,
                                height: 26,
                              ),
                      ),
                      top: 0,
                      left: 5,
                    ),
                  ],
                ),
              ),
              Expanded(
                child: Padding(
                  padding: EdgeInsets.only(left: 17, top: 0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        title,
                        style: TextStyles.btnText14B_333333,
                      ),
                      Text(
                        period,
                        style: TextStyles.btnText12_CCCCCC,
                      ),
                      Gaps.vGap8,
                      Row(
                        children: [
                          Container(
                            child: Baseline(
                              baseline: 12,
                              baselineType: TextBaseline.alphabetic,
                              // child: Image.asset("asset/image/home/vip.png", width: 10, height: 10),
                              child: Text(
                                label,
                                style: TextStyles.btnTextWhile10,
                              ),
                            ),
                            alignment: Alignment.center,
                            decoration: BoxDecorations.rectCircle8_yellow,
                            padding: EdgeInsets.only(left: 10, right: 10, top: 1, bottom: 1),
                          ),
                          Expanded(
                            child: Gaps.vGap8,
                          ),
                        ],
                      ),
                      Expanded(
                        child: Container(
                          alignment: Alignment.center,
                          child: Container(
                            height: 100,
                            padding: EdgeInsets.only(left: 8),
                            decoration: BoxDecorations.rectCircle5_grayf1,
                            child: Column(
                              children: [
                                Row(
                                  children: [
                                    Baseline(
                                      baseline: 12,
                                      baselineType: TextBaseline.alphabetic,
                                      // child: Image.asset("asset/image/home/vip.png", width: 10, height: 10),
                                      child: Text(
                                        "已投作品：",
                                        style: TextStyles.btnText11_999999,
                                      ),
                                    ),
                                    Baseline(
                                      baseline: 14,
                                      baselineType: TextBaseline.alphabetic,
                                      // child: Image.asset("asset/image/home/vip.png", width: 10, height: 10),
                                      child: Text(
                                        votes,
                                        style: TextStyles.btnTextRed12B,
                                      ),
                                    ),
                                    Baseline(
                                      baseline: 12,
                                      baselineType: TextBaseline.alphabetic,
                                      // child: Image.asset("asset/image/home/vip.png", width: 10, height: 10),
                                      child: Text(
                                        "个",
                                        style: TextStyles.btnText11_999999,
                                      ),
                                    ),
                                  ],
                                ),
                                Gaps.vGap4,
                                Text(
                                  endData,
                                  style: TextStyles.btnText10_666666,
                                ),
                                Gaps.vGap2,
                                Text(
                                  publishData,
                                  style: TextStyles.btnText10_666666,
                                ),
                                Gaps.vGap8,
                                Row(
                                  children: [
                                    Expanded(child: Gaps.hGap4),
                                    Image.asset(
                                      "asset/image/home/creation/btn_ss_xq.png",
                                      width: 125,
                                      height: 15,
                                    ),
                                    Gaps.hGap8
                                  ],
                                ),
                              ],
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.center,
                            ),
                          ),
                        ),
                      ),
                      Row(
                        children: [
                          Expanded(child: Gaps.hGap4),
                          Container(
                            width: 100,
                            height: 26,
                            margin: EdgeInsets.only(left: 15, right: 0),
                            child: GFButton(
                              padding: EdgeInsets.only(left: 10, right: 10),
                              color: HexColor('#1B1D23'),
                              borderShape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(5),
                                side: BorderSide(
                                  width: 0,
                                  color: HexColor('#1B1D23'),
                                  style: BorderStyle.solid,
                                ),
                              ),
                              textStyle: TextStyles.btnTextWhile13,
                              fullWidthButton: false,
                              size: 31,
                              text: "我要参赛",
                              onPressed: () async {},
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ));

  Widget participateWorksView() => ListView.separated(
      padding: EdgeInsets.all(0),
      shrinkWrap: true,
      physics: new NeverScrollableScrollPhysics(),
      itemCount: popularMatchs.length,
      scrollDirection: Axis.vertical,
      separatorBuilder: (BuildContext context, int index) {
        return Dividers.dividerHorizontal(height: 10, color: Colors.transparent);
      },
      itemBuilder: (BuildContext context, int index) {
        return popularMatchItem(
          index,
          title: popularMatchs[index]["title"],
          imgUrl: popularMatchs[index]["imgUrl"],
          period: popularMatchs[index]["period"],
          label: popularMatchs[index]["label"],
          votes: popularMatchs[index]["votes"],
          endData: popularMatchs[index]["endData"],
          publishData: popularMatchs[index]["publishData"],
        );
      });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBarCreation(context, widget.pageName, onPressed: () {}),
      body: LoadInitializeView(
        child: Container(
          child: Column(
            children: [
              Container(
                height: 90,
                color: Colours.color_black_252525,
                child: Container(
                  decoration: BoxDecorations.rectCircle10,
                  padding: Dimens.edgeInsets_lr_dp10,
                  child: Row(
                    children: [
                      Expanded(child: menuItem("asset/image/home/creation/menu_01.png", "作品管理", onTap: () {
                        AppNavigator.push(context, PublishedWorksPage());
                      })),
                      Expanded(child: menuItem("asset/image/home/creation/menu_02.png", "作者回馈")),
                      Expanded(child:InkWell(
                        onTap: (){
                           AppNavigator.push(context, WorkIncomePage());
                        },
                        child:  menuItem("asset/image/home/creation/menu_03.png", "作品收入"),
                      )),
                      Expanded(child:InkWell(
                        child: menuItem("asset/image/home/creation/menu_04.png", "草稿箱"),
                        onTap: (){
                          AppNavigator.push(context, DraftBoxPage());
                        },
                      ), ),
                      Expanded(child: menuItem("asset/image/home/creation/menu_05.png", "作品审核")),
                    ],
                  ),
                ),
              ),
              Dividers.dividerHorizontal(height: 12, color: HexColor('#F1F1F1')),
              Expanded(
                child: ListView(
                  children: [
                    Gaps.vGap15,
                    Padding(
                      padding: Dimens.edgeInsets_lr_dp15,
                      child: participateWorks(
                          data: "上传时间:2021-05-02",
                          title: "国潮风之街拍评比",
                          imgUrl:
                              "https://tse1-mm.cn.bing.net/th/id/R-C.544a3ba7aaad86e32e5ffa6250d9e900?rik=dMcOKOHaTmlfeQ&riu=http%3a%2f%2fpic3.16pic.com%2f00%2f54%2f83%2f16pic_5483144_b.jpg&ehk=sQIrwd4bdBVClyRCyntdnqbDlxzVHYd69RXsAlXYHSs%3d&risl=&pid=ImgRaw&r=0",
                          period: "5月第一期赛事",
                          label: "朝阳赛区第 3 名",
                          status: "投稿中",
                          votes: "6000"),
                    ),
                    Gaps.vGap15,
                    Dividers.dividerHorizontal(height: 12, color: HexColor('#F1F1F1')),
                    Gaps.vGap12,
                    Container(
                      margin: Dimens.edgeInsets_lr_dp20,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Baseline(
                            baseline: 20,
                            baselineType: TextBaseline.alphabetic,
                            child: Image.asset(
                              "asset/image/home/ico_rmss.png",
                              width: 20,
                              height: 20,
                            ),
                          ),
                          Gaps.hGap12,
                          Baseline(
                            baseline: 17,
                            baselineType: TextBaseline.alphabetic,
                            child: Text(
                              "热门赛事",
                              style: TextStyles.btnText15B_333333,
                            ),
                          ),
                          Gaps.hGap4,
                          Expanded(
                            child: Gaps.hGap4,
                          ),
                        ],
                      ),
                    ),
                    Gaps.vGap12,
                    Padding(
                      padding: Dimens.edgeInsets_lr_dp15,
                      child: participateWorksView(),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
