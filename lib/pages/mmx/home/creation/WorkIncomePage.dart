import 'package:ant_icons/ant_icons.dart';
import 'package:flutter_vant_kit/widgets/picker.dart';
import 'package:flutter_web_2021/Include.dart';
import 'package:flutter_web_2021/widgets/img/ImagesViews.dart';

class WorkIncomePage extends BasePage {
  static String dartPath = "lib/pages/mmx/home/creation/WorkIncomePage.dart";
  @override
  _WorkIncomePageState createState() => _WorkIncomePageState();

  @override
  String get pageName => '作品收益';
}

class _WorkIncomePageState extends BasePageState<WorkIncomePage> {
  List data = [
    {
      "votes": "11096",
      "data": "上传时间:2021-05-02  13:52",
      "subTitle": "5月第一期赛事",
      "TitleTwo": "全国最美美女",
      "titleOne": "2021年全国最美美女评比",
      "label": "2021/05/01 ~ 2021/05/07   5月第一期赛事",
      "imgUrl":
          "https://tse1-mm.cn.bing.net/th/id/R-C.544a3ba7aaad86e32e5ffa6250d9e900?rik=dMcOKOHaTmlfeQ&riu=http%3a%2f%2fpic3.16pic.com%2f00%2f54%2f83%2f16pic_5483144_b.jpg&ehk=sQIrwd4bdBVClyRCyntdnqbDlxzVHYd69RXsAlXYHSs%3d&risl=&pid=ImgRaw&r=0"
    },
    {
      "votes": "11096",
      "data": "上传时间:2021-05-02  13:52",
      "subTitle": "5月第一期赛事",
      "TitleTwo": "全国最美美女",
      "titleOne": "2021年全国最美美女评比",
      "label": "2021/05/01 ~ 2021/05/07   5月第一期赛事",
      "imgUrl":
          "https://tse1-mm.cn.bing.net/th/id/R-C.544a3ba7aaad86e32e5ffa6250d9e900?rik=dMcOKOHaTmlfeQ&riu=http%3a%2f%2fpic3.16pic.com%2f00%2f54%2f83%2f16pic_5483144_b.jpg&ehk=sQIrwd4bdBVClyRCyntdnqbDlxzVHYd69RXsAlXYHSs%3d&risl=&pid=ImgRaw&r=0"
    },
    {
      "votes": "11096",
      "data": "上传时间:2021-05-02  13:52",
      "subTitle": "5月第一期赛事",
      "TitleTwo": "全国最美美女",
      "titleOne": "2021年全国最美美女评比",
      "label": "2021/05/01 ~ 2021/05/07   5月第一期赛事",
      "imgUrl":
          "https://tse1-mm.cn.bing.net/th/id/R-C.544a3ba7aaad86e32e5ffa6250d9e900?rik=dMcOKOHaTmlfeQ&riu=http%3a%2f%2fpic3.16pic.com%2f00%2f54%2f83%2f16pic_5483144_b.jpg&ehk=sQIrwd4bdBVClyRCyntdnqbDlxzVHYd69RXsAlXYHSs%3d&risl=&pid=ImgRaw&r=0"
    },
    {
      "votes": "11096",
      "data": "上传时间:2021-05-02  13:52",
      "subTitle": "5月第一期赛事",
      "TitleTwo": "全国最美美女",
      "titleOne": "2021年全国最美美女评比",
      "label": "2021/05/01 ~ 2021/05/07   5月第一期赛事",
      "imgUrl":
          "https://tse1-mm.cn.bing.net/th/id/R-C.544a3ba7aaad86e32e5ffa6250d9e900?rik=dMcOKOHaTmlfeQ&riu=http%3a%2f%2fpic3.16pic.com%2f00%2f54%2f83%2f16pic_5483144_b.jpg&ehk=sQIrwd4bdBVClyRCyntdnqbDlxzVHYd69RXsAlXYHSs%3d&risl=&pid=ImgRaw&r=0"
    },
    {
      "votes": "11096",
      "data": "上传时间:2021-05-02  13:52",
      "subTitle": "5月第一期赛事",
      "TitleTwo": "全国最美美女",
      "titleOne": "2021年全国最美美女评比",
      "label": "2021/05/01 ~ 2021/05/07   5月第一期赛事",
      "imgUrl":
          "https://tse1-mm.cn.bing.net/th/id/R-C.544a3ba7aaad86e32e5ffa6250d9e900?rik=dMcOKOHaTmlfeQ&riu=http%3a%2f%2fpic3.16pic.com%2f00%2f54%2f83%2f16pic_5483144_b.jpg&ehk=sQIrwd4bdBVClyRCyntdnqbDlxzVHYd69RXsAlXYHSs%3d&risl=&pid=ImgRaw&r=0"
    },
    {
      "votes": "11096",
      "data": "上传时间:2021-05-02  13:52",
      "subTitle": "5月第一期赛事",
      "TitleTwo": "全国最美美女",
      "titleOne": "2021年全国最美美女评比",
      "label": "2021/05/01 ~ 2021/05/07   5月第一期赛事",
      "imgUrl":
          "https://tse1-mm.cn.bing.net/th/id/R-C.544a3ba7aaad86e32e5ffa6250d9e900?rik=dMcOKOHaTmlfeQ&riu=http%3a%2f%2fpic3.16pic.com%2f00%2f54%2f83%2f16pic_5483144_b.jpg&ehk=sQIrwd4bdBVClyRCyntdnqbDlxzVHYd69RXsAlXYHSs%3d&risl=&pid=ImgRaw&r=0"
    },
  ];

  Widget rewardItem(bean, int index) => Container(
    height: 110,
        alignment: Alignment.center,
        child: Row(
          children: [
            Gaps.wGap15,
            Image.asset(
              "asset/image/home/creation/ico_bei.png",
              width: 40,
              height: 40,
            ),
            Gaps.wGap10,
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "饮雪煮茶",
                    style: TextStyles.btnTextB(fontSize: Dimens.sp15, color: "#333333"),
                  ),
                  Text(
                    "2021-05-11 09:33",
                    style: TextStyles.btnTextB(fontSize: Dimens.sp11, color: "#CCCCCC"),
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text(
                        "打赏",
                        style: TextStyles.btnText(fontSize: Dimens.sp15, color: "#666666"),
                      ),
                      Text(
                        "300",
                        style: TextStyles.btnTextB(fontSize: Dimens.sp15, color: "#666666"),
                      ),
                      Text(
                        "朵花，",
                        style: TextStyles.btnText(fontSize: Dimens.sp15, color: "#666666"),
                      ),
                      Text(
                        "+30",
                        style: TextStyles.btnTextB(fontSize: Dimens.sp15, color: "#666666"),
                      ),
                      Text(
                        "元",
                        style: TextStyles.btnText(fontSize: Dimens.sp15, color: "#666666"),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Container(
              width: 70,
              height: 75,
              child: ImagesViews.rectangle(
                "https://tse1-mm.cn.bing.net/th/id/R-C.544a3ba7aaad86e32e5ffa6250d9e900?rik=dMcOKOHaTmlfeQ&riu=http%3a%2f%2fpic3.16pic.com%2f00%2f54%2f83%2f16pic_5483144_b.jpg&ehk=sQIrwd4bdBVClyRCyntdnqbDlxzVHYd69RXsAlXYHSs%3d&risl=&pid=ImgRaw&r=0",
                borderRadius: 10.0,
                borderColor: Colours.color_orange_FF5800,
                borderLine: 0.5
              ),
            ),  Gaps.wGap10,
            Icon(
              AntIcons.right_outline,
              color: Colours.color_gray_CCCCCC,
              size: 14,
            ),
            Gaps.wGap15,
          ],
        ),
      );

  Widget rewardList() => ListView.separated(
      // shrinkWrap: true,
      // physics: new NeverScrollableScrollPhysics(),
      itemCount: data.length,
      scrollDirection: Axis.vertical,
      separatorBuilder: (BuildContext context, int index) {
        return Container(
          padding: Dimens.edgeInsets_lr_dp15,
          child: Dividers.dividerHorizontal(height: 1, color: Colours.color_gray_F1F1F1),
        );
      },
      itemBuilder: (BuildContext context, int index) {
        return InkWell(
          child: rewardItem(data[index], index),
          onTap: () {
            // AppNavigator.push(context, EditWorksPage());
          },
        );
      });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBarCreationNoRight_2(
        context,
        widget.pageName,
      ),
      backgroundColor: Colours.color_black_252525,
      body: Container(
        decoration: BoxDecorations.rectCircle10,
        width: Dimens.screenWidth(),
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.all(15),
              padding: Dimens.edgeInsets_lr_dp20,
              height: 82,
              decoration: BoxDecorations.rectCircleColor(color: "#1B1D23", radius: 10),
              child: Row(
                children: [
                  Expanded(
                    child: Text(
                      "作品收益",
                      style: TextStyles.btnText(fontSize: Dimens.sp11, color: "#7B7C7F"),
                    ),
                  ),
                  Text(
                    "1,973.00 ",
                    style: TextStyles.btnText(fontSize: Dimens.sp17, color: "#f1f1f1"),
                  ),
                ],
              ),
            ),
            Dividers.dividerHorizontal(height: 10, color: Colours.color_gray_F1F1F1),
            Container(
              color: Colours.bg_while,
              width: Dimens.screenWidth(),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Column(
                    children: [
                      Container(
                        decoration: BoxDecorations.fourSidesBottom(width: 1.5, color: Colours.color_orange_FF5800),
                        margin: EdgeInsets.only(left: 15),
                        padding: EdgeInsets.only(bottom: 8, top: 12),
                        child: Text(
                          "打赏收入",
                          style: TextStyles.btnTextB(fontSize: Dimens.sp12, color: "#F15930"),
                        ),
                      ),
                    ],
                  ),
                  Expanded(child: Gaps.wGap10),
                ],
              ),
            ),
            Container(
              color: Colours.line_F1F1F1,
              width: Dimens.screenWidth(),
              height: 50,
              child: Row(
                children: [
                  Gaps.wGap15,
                  Container(
                    height: 22,
                    padding: EdgeInsets.only(left: 11, right: 8),
                    decoration: BoxDecorations.rectCircleHollow(radius: 11),
                    child: InkWell(
                      child: Row(
                        children: [
                          Text(
                            "本月",
                            style: TextStyles.btnText12_666666,
                          ),
                          Gaps.wGap2,
                          Baseline(
                            baseline: 15,
                            baselineType: TextBaseline.alphabetic,
                            child: Icon(
                              AntIcons.caret_down,
                              color: Colours.color_gray_666666,
                              size: 14,
                            ),
                          ),
                        ],
                      ),
                      onTap: () {},
                    ),
                  ),
                  Gaps.wGap15,
                  Container(
                    height: 22,
                    padding: EdgeInsets.only(left: 11, right: 5),
                    decoration: BoxDecorations.rectCircleHollow(radius: 11),
                    child: InkWell(
                      child: Row(
                        children: [
                          Text(
                            "2021-05",
                            style: TextStyles.btnText12_666666,
                          ),
                          Gaps.wGap2,
                          Gaps.wGap2,
                          Baseline(
                            baseline: 15,
                            baselineType: TextBaseline.alphabetic,
                            child: Icon(
                              AntIcons.caret_down,
                              color: Colours.color_gray_666666,
                              size: 14,
                            ),
                          ),
                        ],
                      ),
                      onTap: () {},
                    ),
                  ),
                  Expanded(child: Gaps.wGap15),
                  Text(
                    "收入：￥1,257.23",
                    style: TextStyles.btnText(fontSize: Dimens.sp13, color: "#999999"),
                  ),
                  Gaps.wGap15
                ],
              ),
            ),
            Expanded(
              child: rewardList(),
            ),
          ],
        ),
      ),
    );
  }
}
