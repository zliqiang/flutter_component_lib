import 'package:flustars/flustars.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter_web_2021/eventbus/event.dart';
import 'package:flutter_web_2021/pages/mmx/login/LoginOkBindPhoneNumberPage.dart';
import 'package:flutter_web_2021/utils/toast/OkToastUtil.dart';
import 'package:flutter_web_2021/widgets/input/PhoneNumberInput.dart';
import 'package:flutter_web_2021/widgets/input/PwdInput.dart';
import 'package:flutter_web_2021/widgets/input/VerificationCodeInput.dart';
import 'package:flutter_web_2021/widgets/radio/CustomRadioView.dart';
import 'package:getwidget/getwidget.dart';

import '../../../Include.dart';

class ForgetPwdPage extends BasePage {
  static String dartPath = "lib/pages/mmx/login/ForgetPwdPage.dart";

  @override
  _ForgetPwdPageState createState() => _ForgetPwdPageState();

  @override
  String get pageName => '忘记密码页面';
}

class _ForgetPwdPageState extends BasePageState<ForgetPwdPage> {
  FocusNode userFocusNode = FocusNode();
  FocusNode smsFocusNode = FocusNode();
  FocusNode pwdFocusNode = FocusNode();

  TextEditingController userController = TextEditingController();
  TextEditingController smsController = TextEditingController();
  TextEditingController pwdController = TextEditingController();

  bool sendStatus = false;

  ///发送验证码
  Future<void> sendMsM() async {
    await SpUtil.getInstance();
    if (!sendStatus) {
      String user = userController.text;
      if (user.toString().isEmpty) {
        ToastUtil.showToastTop("手机号不能为空！");
        return;
      }
      if (!RegexUtil.isMobileExact(user)) {
        ToastUtil.showToastTop("手机号错误！");
        return;
      }
      // presenter.onSendVerificationCode(user); //发送验证码
      eventBus.fire(new LoginMSM());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          Padding(
            padding: EdgeInsets.only(left: Dimens.dp24, top: 16),
            child:InkWell(
              child: Text(
                "返回登录",
                style: TextStyles.btnText12_333333,
              ),
              onTap: (){
                AppNavigator.pop(context);
              },
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 47, top: 30),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image.asset(
                  "asset/image/login/mima.png",
                  width: 25,
                  height: 26,
                ),
                Gaps.hGap8,
                Text(
                  "忘记密码",
                  style: TextStyles.btnText23_333333,
                ),
              ],
            ),
          ),
          Gaps.vGap42,
          Container(
            width: Dimens.screenWidth(),
            height: 40,
            margin: Dimens.edgeInsets_lr_dp35,
            decoration: BoxDecorations.rectCircleHollow20,
            child: PhoneNumberInput(
              keyboardType: TextInputType.number,
              controller: userController,
              hintText: "请输入您的账号",
              textColor: Colors.black,
              hintTextColor: Colors.grey,
              contentFocusNode: userFocusNode,
              fontSize: Dimens.sp12,
              height: 40,
            ),
          ),
          Gaps.vGap20,
          Container(
            width: Dimens.screenWidth(),
            height: 40,
            margin: Dimens.edgeInsets_lr_dp35,
            decoration: BoxDecorations.rectCircleHollow20,
            child: VerificationCodeInput(
              keyboardType: TextInputType.number,
              isShowVerificationCode: true,
              controller: smsController,
              hintText: "请输入验证码",
              textColor: Colors.black,
              hintTextColor: Colors.grey,
              contentFocusNode: smsFocusNode,
              fontSize: Dimens.sp12,
              height: 40,
              isSendAction: (sendStatus) {
                this.sendStatus = sendStatus;
              },
              onTap: () {
                sendMsM();
              },
            ),
          ),
          Gaps.vGap20,
          Container(
            width: Dimens.screenWidth(),
            height: 40,
            margin: Dimens.edgeInsets_lr_dp35,
            decoration: BoxDecorations.rectCircleHollow20,
            child: PwdInput(
              keyboardType: TextInputType.visiblePassword,
              controller: pwdController,
              hintText: "请输入密码",
              textColor: Colors.black,
              hintTextColor: Colors.grey,
              contentFocusNode: pwdFocusNode,
              fontSize: Dimens.sp12,
              height: 40,
            ),
          ),
          Gaps.vGap18,
          Offstage(
            offstage: false,
            child:  Container(
              margin: Dimens.edgeInsets_lr_dp38,
              child: RichText(
                text: TextSpan(text: '密码强度较弱', style: TextStyle(fontSize: Dimens.sp12, color: Colours.colorRedFF0600), children: [
                  TextSpan(text: '（区分大小写，增加特殊字符可增强密码强度）', style: TextStyle(fontSize: Dimens.sp12, color: Colours.text_title_black)),
                ]),
              ),
            ),
          ),

          Gaps.vGap18,
          Container(
            margin: Dimens.edgeInsets_lr_dp38,
            height: 41,
            child: GFButton(
              onPressed: () {
              },
              text: "完成",
              textStyle: TextStyles.btnTextWhile16,
              color: Colours.color_orange_FF5800,
              fullWidthButton: true,
              shape: GFButtonShape.pills,
            ),
          ),
          Gaps.vGap25,
        ],
      ),
    );
  }
}
