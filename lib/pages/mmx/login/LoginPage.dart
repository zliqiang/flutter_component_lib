import 'package:flutter/gestures.dart';
import 'package:flutter_web_2021/Include.dart';
import 'package:flutter_web_2021/pages/mmx/login/ForgetPwdPage.dart';
import 'package:flutter_web_2021/pages/mmx/login/RegisteredPage.dart';
import 'package:flutter_web_2021/utils/toast/OkToastUtil.dart';
import 'package:flutter_web_2021/widgets/input/PhoneNumberInput.dart';
import 'package:flutter_web_2021/widgets/input/PwdInput.dart';
import 'package:getwidget/components/button/gf_button.dart';
import 'package:getwidget/shape/gf_button_shape.dart';

import 'LoginOkBindPhoneNumberPage.dart';

class LoginPage extends BasePage {
  static String dartPath = "lib/pages/mmx/login/LoginPage.dart";

  @override
  _LoginPageState createState() => _LoginPageState();

  @override
  String get pageName => '登录页面';
}

class _LoginPageState extends BasePageState<LoginPage> {
  FocusNode userFocusNode = FocusNode();
  FocusNode pwdFocusNode = FocusNode();

  TextEditingController userController = TextEditingController();
  TextEditingController pwdController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          Container(
            height: Dimens.screenHeight()-Dimens.textTabBarHeight(),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.only(left: Dimens.dp24, top: 16),
                  child: InkWell(
                    child: Text(
                      "关闭",
                      style: TextStyles.btnText12_333333,
                    ),
                    onTap: () {
                      AppNavigator.pop(context);
                    },
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(left: 37, top: 27),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "手机登录",
                        style: TextStyles.btnText20_333333,
                      ),
                      Gaps.vGap18,
                      RichText(
                        text: TextSpan(text: '登录后即表明同意 ', style: TextStyle(fontSize: Dimens.sp12, color: Colours.text_title_black), children: [
                          TextSpan(
                            text: '《用户协议和隐私政策》',
                            style: TextStyle(fontSize: Dimens.sp12, color: Colours.color_orange_FF5800),
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                // 手势触发
                                ToastUtil.showToastBottomLong("手势触发");
                              },
                          ),
                        ]),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 28),
                Container(
                  width: Dimens.screenWidth(),
                  height: 40,
                  margin: Dimens.edgeInsets_lr_dp35,
                  decoration: BoxDecorations.rectCircleHollow20,
                  child: PhoneNumberInput(
                    keyboardType: TextInputType.number,
                    controller: userController,
                    hintText: "请输入您的账号",
                    textColor: Colors.black,
                    hintTextColor: Colours.color_gray_666666,
                    contentFocusNode: userFocusNode,
                    fontSize: Dimens.sp14,
                    height: 40,
                  ),
                ),
                Gaps.vGap20,
                Container(
                  width: Dimens.screenWidth(),
                  height: 40,
                  margin: Dimens.edgeInsets_lr_dp35,
                  decoration: BoxDecorations.rectCircleHollow20,
                  child: PwdInput(
                    keyboardType: TextInputType.visiblePassword,
                    controller: pwdController,
                    hintText: "请输入密码",
                    textColor: Colors.black,
                    hintTextColor: Colours.color_gray_666666,
                    contentFocusNode: pwdFocusNode,
                    fontSize: Dimens.sp14,
                    height: 40,
                  ),
                ),
                SizedBox(height: 48),
                Container(
                  margin: Dimens.edgeInsets_lr_dp35,
                  height: 41,
                  child: GFButton(
                    onPressed: () {
                      AppNavigator.push(context, LoginOkBindPhoneNumberPage());
                    },
                    text: "登  录",
                    textStyle: TextStyles.btnTextWhile16,
                    color: Colours.color_orange_FF5800,
                    fullWidthButton: true,
                    shape: GFButtonShape.pills,
                  ),
                ),
                SizedBox(height: 25),
                Container(
                  margin: Dimens.edgeInsets_lr_dp38,
                  child: Row(
                    children: [
                      InkWell(
                        child: Text(
                          "忘记密码？",
                          style: TextStyles.btnText12_112FCB,
                        ),
                        onTap: () {
                          AppNavigator.push(context, ForgetPwdPage());
                        },
                      ),
                      Expanded(
                        child: Gaps.wGap5,
                      ),
                      InkWell(
                        child: Text(
                          "立即注册",
                          style: TextStyles.btnText12_333333,
                        ),
                        onTap: () {
                          AppNavigator.push(context, RegisteredPage());
                        },
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Gaps.hGap8,
                ),
                Container(
                  margin: Dimens.edgeInsets_lr_dp38,
                  child: Row(
                    children: [
                      Expanded(child: Container(height: 1, margin:EdgeInsets.only(left: 10,right: 12),color: Colours.color_gray_CCCCCC)),
                      Text(
                        "其他方式登录",
                        style: TextStyles.btnText12_333333,
                      ),
                      Expanded(child: Container(height: 1, margin:EdgeInsets.only(left: 12,right: 10),color: Colours.color_gray_CCCCCC)),
                    ],
                  ),
                ),
                SizedBox(height: 18),
                Container(
                  margin: Dimens.edgeInsets_lr_dp38,
                  alignment: Alignment.center,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image.asset(
                        "asset/image/login/ico_weixin.png",
                        width: 35,
                        height: 35,
                      ),
                       Gaps.wGap25,
                      Image.asset(
                        "asset/image/login/ico_qq.png",
                        width: 35,
                        height: 35,
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 30),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
