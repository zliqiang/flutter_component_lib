import 'package:ant_icons/ant_icons.dart';
import 'package:flutter_web_2021/Include.dart';
import 'package:flutter_web_2021/widgets/img/ImagesViews.dart';
import 'package:getwidget/components/button/gf_button.dart';
import 'package:getwidget/shape/gf_button_shape.dart';

class InterestPage extends BasePage {
  static String dartPath = "lib/pages/mmx/login/InterestPage.dart";
  @override
  _InterestPageState createState() => _InterestPageState();

  @override
  String get pageName => '选择兴趣';
}

class _InterestPageState extends BasePageState<InterestPage> {
  List tabData = [];
  List listData = [
    {
      "title": '生活',
      "subtitle": 'Life',
      "select": false,
      "imageUrl":
          "https://tse1-mm.cn.bing.net/th/id/R-C.544a3ba7aaad86e32e5ffa6250d9e900?rik=dMcOKOHaTmlfeQ&riu=http%3a%2f%2fpic3.16pic.com%2f00%2f54%2f83%2f16pic_5483144_b.jpg&ehk=sQIrwd4bdBVClyRCyntdnqbDlxzVHYd69RXsAlXYHSs%3d&risl=&pid=ImgRaw&r=0",
    },
    {
      "title": '母婴',
      "subtitle": 'Mother and baby',
      "select": false,
      "imageUrl":
          "https://tse1-mm.cn.bing.net/th/id/R-C.544a3ba7aaad86e32e5ffa6250d9e900?rik=dMcOKOHaTmlfeQ&riu=http%3a%2f%2fpic3.16pic.com%2f00%2f54%2f83%2f16pic_5483144_b.jpg&ehk=sQIrwd4bdBVClyRCyntdnqbDlxzVHYd69RXsAlXYHSs%3d&risl=&pid=ImgRaw&r=0",
    },
    {
      "title": '娱乐',
      "subtitle": 'Entertainment',
      "select": false,
      "imageUrl":
          "https://tse1-mm.cn.bing.net/th/id/R-C.544a3ba7aaad86e32e5ffa6250d9e900?rik=dMcOKOHaTmlfeQ&riu=http%3a%2f%2fpic3.16pic.com%2f00%2f54%2f83%2f16pic_5483144_b.jpg&ehk=sQIrwd4bdBVClyRCyntdnqbDlxzVHYd69RXsAlXYHSs%3d&risl=&pid=ImgRaw&r=0",
    },
    {
      "title": '舞蹈',
      "subtitle": 'Dance',
      "select": false,
      "imageUrl":
          "https://tse1-mm.cn.bing.net/th/id/R-C.544a3ba7aaad86e32e5ffa6250d9e900?rik=dMcOKOHaTmlfeQ&riu=http%3a%2f%2fpic3.16pic.com%2f00%2f54%2f83%2f16pic_5483144_b.jpg&ehk=sQIrwd4bdBVClyRCyntdnqbDlxzVHYd69RXsAlXYHSs%3d&risl=&pid=ImgRaw&r=0",
    },
    {
      "title": '美食',
      "subtitle": 'Food',
      "select": false,
      "imageUrl":
          "https://tse1-mm.cn.bing.net/th/id/R-C.544a3ba7aaad86e32e5ffa6250d9e900?rik=dMcOKOHaTmlfeQ&riu=http%3a%2f%2fpic3.16pic.com%2f00%2f54%2f83%2f16pic_5483144_b.jpg&ehk=sQIrwd4bdBVClyRCyntdnqbDlxzVHYd69RXsAlXYHSs%3d&risl=&pid=ImgRaw&r=0",
    },
    {
      "title": '摄影',
      "subtitle": 'photograhhy',
      "select": false,
      "imageUrl":
          "https://tse1-mm.cn.bing.net/th/id/R-C.544a3ba7aaad86e32e5ffa6250d9e900?rik=dMcOKOHaTmlfeQ&riu=http%3a%2f%2fpic3.16pic.com%2f00%2f54%2f83%2f16pic_5483144_b.jpg&ehk=sQIrwd4bdBVClyRCyntdnqbDlxzVHYd69RXsAlXYHSs%3d&risl=&pid=ImgRaw&r=0",
    },
    {
      "title": '城市',
      "subtitle": 'City',
      "select": false,
      "imageUrl":
          "https://tse1-mm.cn.bing.net/th/id/R-C.544a3ba7aaad86e32e5ffa6250d9e900?rik=dMcOKOHaTmlfeQ&riu=http%3a%2f%2fpic3.16pic.com%2f00%2f54%2f83%2f16pic_5483144_b.jpg&ehk=sQIrwd4bdBVClyRCyntdnqbDlxzVHYd69RXsAlXYHSs%3d&risl=&pid=ImgRaw&r=0",
    },
    {
      "title": '艺术',
      "subtitle": 'Art',
      "select": false,
      "imageUrl":
          "https://tse1-mm.cn.bing.net/th/id/R-C.544a3ba7aaad86e32e5ffa6250d9e900?rik=dMcOKOHaTmlfeQ&riu=http%3a%2f%2fpic3.16pic.com%2f00%2f54%2f83%2f16pic_5483144_b.jpg&ehk=sQIrwd4bdBVClyRCyntdnqbDlxzVHYd69RXsAlXYHSs%3d&risl=&pid=ImgRaw&r=0",
    },
    {
      "title": '家居',
      "subtitle": 'Decoration',
      "select": false,
      "imageUrl":
          "https://tse1-mm.cn.bing.net/th/id/R-C.544a3ba7aaad86e32e5ffa6250d9e900?rik=dMcOKOHaTmlfeQ&riu=http%3a%2f%2fpic3.16pic.com%2f00%2f54%2f83%2f16pic_5483144_b.jpg&ehk=sQIrwd4bdBVClyRCyntdnqbDlxzVHYd69RXsAlXYHSs%3d&risl=&pid=ImgRaw&r=0",
    },
  ];

  Widget _getListData(context, index) {
    return AspectRatio(
      aspectRatio: 1 / 1,
      child: InkWell(
        child: Container(
          child: Stack(
            children: [
              Container(
                padding: EdgeInsets.all(4),
                child: ImagesViews.rectangle(
                  listData[index]['imageUrl'],
                  borderRadius: 5.0,
                ),
              ),

              Positioned(
                child: Container(
                  margin: EdgeInsets.all(4),
                  decoration: BoxDecorations.rectCircle5_aaGray,
                ),
              ),
              Container(
                alignment: Alignment.center,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      listData[index]['title'],
                      style: TextStyles.btnTextWhile13,
                    ),
                    Text(
                      listData[index]['subtitle'],
                      style: TextStyles.btnTextWhile8,
                    ),
                  ],
                ),
              ),
              Align(
                alignment: Alignment.topRight,
                child: Offstage(
                  offstage: listData[index]['select'] ? false : true,
                  child: Padding(
                    padding: EdgeInsets.all(0),
                    child: Icon(
                      AntIcons.check_circle,
                      color: Colors.greenAccent,
                      size: 16,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        onTap: () {
          setState(() {
            listData[index]['select'] = !listData[index]['select'];
            tabView();
          });
        },
      ),
    );
  }

  List<Widget> tabView() {
    tabData.clear();
    for (int i = 0; i < listData.length; i++) {
      if (listData[i]["select"]) {
        tabData.add(listData[i]);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colours.bg_while,
      body: Container(
        padding: Dimens.edgeInsets_lr_dp15,
        width: Dimens.screenWidth(),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              "选择你感兴趣的标签",
              style: TextStyles.btnText20_333333,
            ),
            SizedBox(height: 17),
            Text(
              "选择1-3个标签",
              style: TextStyles.btnText12_333333,
            ),
            GridView.builder(
              shrinkWrap: true,
              physics: new NeverScrollableScrollPhysics(),
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisSpacing: 4.0, //水平子 Widget 之间间距
                mainAxisSpacing: 4.0, //垂直子 Widget 之间间距
                crossAxisCount: 3, //一行的 Widget 数量
              ),
              itemCount: listData.length,
              itemBuilder: this._getListData,
            ),
            SizedBox(height: 28),
            Container(
              height: 90,
              width: Dimens.screenWidth(),
              child: Wrap(
                spacing: 10,
                runSpacing: 10,
                children: tabData.map((e) => MyButton(e["title"])).toList(),
              ),
            ),
            SizedBox(height: 60),
            Container(
              margin: Dimens.edgeInsets_lr_dp38,
              height: 41,
              child: GFButton(
                onPressed: () {},
                text: "前往",
                textStyle: TextStyles.btnTextWhile16,
                color: Colours.color_orange_FF5800,
                fullWidthButton: true,
                shape: GFButtonShape.pills,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class MyButton extends StatelessWidget {
  final String text;

  const MyButton(this.text, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: (Dimens.screenWidth() - (16 * 3) - 15) / 4,
      child: Baseline(
        baseline: 14,
        baselineType: TextBaseline.alphabetic,
        child: Text(
          text,
          style: TextStyles.btnTextWhile12,
        ),
      ),
      alignment: Alignment.center,
      decoration: BoxDecorations.rectCircle12,
      padding: EdgeInsets.only(left: 8, right: 8, top: 3, bottom: 3),
    );
  }
}
