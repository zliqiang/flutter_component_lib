import 'package:flutter_web_2021/Include.dart';
import 'package:flutter_web_2021/pages/mmx/login/InterestPage.dart';
import 'package:getwidget/components/button/gf_button.dart';
import 'package:getwidget/shape/gf_button_shape.dart';

class LoginOkBindPhoneNumberPage extends BasePage {
  static String dartPath = "lib/pages/mmx/login/LoginOkBindPhoneNumberPage.dart";

  @override
  _LoginOkBindPhoneNumberPageState createState() => _LoginOkBindPhoneNumberPageState();

  @override
  String get pageName => '微信登录绑定手机号';
}

class _LoginOkBindPhoneNumberPageState extends BasePageState<LoginOkBindPhoneNumberPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        alignment: Alignment.center,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: EdgeInsets.only(top: 136),
              child: Text(
                "登录成功，请绑定手机号",
                style: TextStyles.btnText20_333333,
              ),
            ),
            Gaps.vGap10,
            Text(
              "绑定后下次可直接使用手机号登录",
              style: TextStyles.btnText12_333333,
            ),
            SizedBox(height: 47),
            Text(
              "15122650973",
              style: TextStyles.btnText19_333333,
            ),
            SizedBox(height: 17),
            Text(
              "手机号认证服务由中国移动提供",
              style: TextStyles.btnText12_333333,
            ),
            SizedBox(height: 46),
            Container(
              margin: Dimens.edgeInsets_lr_dp38,
              height: 41,
              child: GFButton(
                onPressed: () {
                  AppNavigator.push(context, InterestPage());
                },
                text: "本机号码一键绑定",
                textStyle: TextStyles.btnTextWhile16,
                color: Colours.color_orange_FF5800,
                fullWidthButton: true,
                shape: GFButtonShape.pills,
              ),
            ),
            Padding(padding: EdgeInsets.all(18),child:InkWell(
              onTap: (){
                AppNavigator.pop(context);
              },
              child: Text(
                "切换手机号",
                style: TextStyles.btnText12_333333,
              ),
            ) ,)
            ,
          ],
        ),
      ),
    );
  }
}
