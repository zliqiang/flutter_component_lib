import 'package:flutter_vant_kit/main.dart';
import 'package:flutter_vant_kit/widgets/steps.dart';
import 'package:flutter_web_2021/Include.dart';

class DemoSteps extends BasePage {

  static String dartPath = "lib/pages/vant_kit/DemoSteps.dart";

  @override
  _DemoStepsState createState() => _DemoStepsState();

  @override
  String get pageName => '步骤条';
}

class _DemoStepsState extends BasePageState<DemoSteps> {

  int _active = 1;

  Widget title(String title) {
    return Padding(
      padding: EdgeInsets.fromLTRB(20, 30, 0, 10),
      child: Text(title, style: TextStyle(fontSize: 14, color: Colors.grey)),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body:  SingleChildScrollView(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                title("基础用法"),
                Steps(steps: [
                  StepItem("买家下单1"),
                  StepItem("买家下单2"),
                  StepItem("买家下单3"),
                  StepItem("买家下单4"),
                ], active: _active),
                Padding(
                    padding: EdgeInsets.all(20),
                    child: Wrap(
                      runSpacing: 10,
                      children: <Widget>[
                        NButton(
                          text: "上一步",
                          type: "primary",
                          plain: true,
                          onClick: () {
                            setState(() {
                              _active -= 1;
                              if (_active < 0) _active = 3;
                            });
                          },
                        ),
                        SizedBox(width: 6),
                        NButton(
                          text: "下一步",
                          type: "primary",
                          plain: true,
                          onClick: () {
                            setState(() {
                              _active += 1;
                              if (_active > 3) _active = 0;
                            });
                          },
                        ),
                      ],
                    )),
                title("自定义样式"),
                Steps(
                  steps: [
                    StepItem("买家下单1"),
                    StepItem("买家下单2"),
                    StepItem("买家下单3"),
                    StepItem("买家下单4"),
                  ],
                  active: _active,
                  activeIcon: Icons.done,
                  inactiveIcon: Icons.chevron_right,
                  activeColor: Colors.green,
                ),
                title("竖向步骤样式"),
                Steps(
                  steps: [
                    StepItem("【城市】 物流状态1", "2016-07-12 12:40"),
                    StepItem("【城市】 物流状态2", "2016-07-12 12:40"),
                    StepItem("【城市】 物流状态3", "2016-07-12 12:40"),
                    StepItem("【城市】 物流状态4", "2016-07-12 12:40")
                  ],
                  active: _active,
                  direction: 'vertical',
                ),
              ])),
    );
  }

}