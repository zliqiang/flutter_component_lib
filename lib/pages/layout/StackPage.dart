import '../../Include.dart';

class StackPage extends BasePage {

  static String dartPath = "lib/pages/layout/StackPage.dart";

  @override
  _StackPageState createState() => _StackPageState();

  @override
  String get pageName => 'Stack堆叠布局';
}

class _StackPageState extends BasePageState<StackPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: ListView(
        children: [
          Container(
            height: 400,
            width: 300,
            color: Colors.red,
            child: Stack(
              // alignment: Alignment.center,
              children: <Widget>[
                Align(
                  alignment: Alignment(1,-0.2),
                  child: Icon(Icons.home,size: 40,color: Colors.white),
                ),
                Align(
                  alignment: Alignment.center,
                  child: Icon(Icons.search,size: 30,color: Colors.white),
                ),
                Align(
                  alignment: Alignment.bottomRight,
                  child: Icon(Icons.settings_applications,size: 30,color: Colors.white),
                )
              ],
            ),
          ),
          Stack(
            alignment: Alignment(1,0.3),
            children: <Widget>[
              Container(
                height: 400,
                color: Colors.blue,
              ),
              Text('我是一个文本',style: TextStyle(
                  fontSize: 20,
                  color: Colors.white
              ))
            ],
          ),

        ],
      ),
    );
  }

}