import 'package:flutter_color/flutter_color.dart';
import 'package:flutter_web_2021/Include.dart';
import 'package:getwidget/components/button/gf_button.dart';

class UnconstrainedBoxPage extends BasePage {
  static String dartPath = "lib/pages/layout/UnconstrainedBoxPage.dart";

  @override
  _UnconstrainedBoxPageState createState() => _UnconstrainedBoxPageState();

  @override
  String get pageName => 'UnconstrainedBox【解决限制布局】';
}

class _UnconstrainedBoxPageState extends BasePageState<UnconstrainedBoxPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Column(
        children: [
          Container(
            width: Dimens.screenWidth(),
            height: 145,
            color: HexColor('#292929'),
            child: Column(
              children: [
                Expanded(
                  child: new UnconstrainedBox(
                    child: Container(
                      height: 30,
                      child: GFButton(
                        padding: EdgeInsets.only(left: 15, right: 15),
                        color: HexColor('#121212'),
                        borderShape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5),
                          side: BorderSide(
                            width: 0,
                            color: HexColor('#121212'),
                            style: BorderStyle.solid,
                          ),
                        ),
                        textStyle: TextStyles.btnTextWhile13,
                        fullWidthButton: false,
                        size: 31,
                        text: "登录/注册",
                        onPressed: () async {},
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
