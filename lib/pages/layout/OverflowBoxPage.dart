import '../../Include.dart';

//https://api.flutter.dev/flutter/widgets/OverflowBox-class.html
class OverflowBoxPage extends BasePage {

  static String dartPath = "lib/pages/layout/OverflowBoxPage.dart";

  @override
  _OverflowBoxPageState createState() => _OverflowBoxPageState();

  @override
  String get pageName => 'OverflowBox溢出布局';
}

class _OverflowBoxPageState extends BasePageState<OverflowBoxPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: new Container(  //1 注意：父容器的宽高是200 减去pading后是180
        padding: EdgeInsets.all(10),
        color: Colors.green,
        width: 200,
        height: 200,
        child: new OverflowBox(
          maxHeight: 400,//2 不能小于父容器的高度180
          child: new Container(
            color: Colors.deepOrange,
            width: 200,
            height:600,
          ),
        ),
      ),
    );
  }

}