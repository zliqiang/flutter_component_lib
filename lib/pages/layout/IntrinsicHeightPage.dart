import '../../Include.dart';
//https://api.flutter.dev/flutter/widgets/IntrinsicHeight-class.html
class IntrinsicHeightPage extends BasePage {

  static String dartPath = "lib/pages/layout/IntrinsicHeightPage.dart";

  @override
  _IntrinsicHeightPageState createState() => _IntrinsicHeightPageState();

  @override
  String get pageName => 'IntrinsicHeight内部高度';
}

class _IntrinsicHeightPageState extends BasePageState<IntrinsicHeightPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        child: new IntrinsicHeight(
          child: new Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              new Container(color: Colors.blue, width: 100.0),
              new Container(color: Colors.red, width: 50.0,height: 50.0,),
              new Container(color: Colors.yellow, width: 150.0),
            ],
          ),
        ),
      ),
    );
  }

}