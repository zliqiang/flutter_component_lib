import '../../Include.dart';
//https://api.flutter.dev/flutter/widgets/IntrinsicWidth-class.html
class IntrinsicWidthPage extends BasePage {

  static String dartPath = "lib/pages/layout/IntrinsicWidthPage.dart";

  @override
  _IntrinsicWidthPageState createState() => _IntrinsicWidthPageState();

  @override
  String get pageName => 'IntrinsicWidth内部宽度';
}

class _IntrinsicWidthPageState extends BasePageState<IntrinsicWidthPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        child: new Container(
          color: Colors.green,
          padding: const EdgeInsets.all(5.0),
          child: new IntrinsicWidth(
            stepHeight: 450.0,
            stepWidth: 300.0,
            child: new Column(
              children: <Widget>[
                new Container(color: Colors.blue, height: 100.0),
                new Container(color: Colors.red, width: 150.0, height: 100.0),
                new Container(color: Colors.yellow, height: 150.0,),
              ],
            ),
          ),
        )
        ,
      ),
    );
  }

}