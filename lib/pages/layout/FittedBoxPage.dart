import '../../Include.dart';
//https://api.flutter.dev/flutter/widgets/FittedBox-class.html
class FittedBoxPage extends BasePage {

  static String dartPath = "lib/pages/layout/FittedBoxPage.dart";

  @override
  _FittedBoxPageState createState() => _FittedBoxPageState();

  @override
  String get pageName => 'FittedBox缩放布局';
}

class _FittedBoxPageState extends BasePageState<FittedBoxPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        child: MyStatelessWidget(),
      ),
    );
  }

}


/// This is the stateless widget that the main application instantiates.
class MyStatelessWidget extends StatelessWidget {
  const MyStatelessWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 400,
      width: 300,
      color: Colors.red,
      child: FittedBox(
        child: Image.network(
            'https://flutter.github.io/assets-for-api-docs/assets/widgets/owl-2.jpg'),
        fit: BoxFit.fill,
      ),
    );
  }
}