import '../../Include.dart';

//https://api.flutter.dev/flutter/widgets/Expanded-class.html
class ExpandedPage extends BasePage {

  static String dartPath = "lib/pages/layout/ExpandedPage.dart";

  @override
  _ExpandedPageState createState() => _ExpandedPageState();

  @override
  String get pageName => 'ExpandedPage';
}

class _ExpandedPageState extends BasePageState<ExpandedPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Center(
        child: Column(
          children: <Widget>[
            Container(
              color: Colors.blue,
              height: 100,
              width: 100,
            ),
            Expanded(
              child: Container(
                color: Colors.amber,
                width: 100,
              ),
            ),
            Container(
              color: Colors.blue,
              height: 100,
              width: 100,
            ),
          ],
        ),
      ),
    );
  }

}