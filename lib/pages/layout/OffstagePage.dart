import '../../Include.dart';

//https://api.flutter.dev/flutter/widgets/OverflowBox-class.html
class OffstagePage extends BasePage {

  static String dartPath = "lib/pages/layout/OffstagePage.dart";

  @override
  _OffstagePageState createState() => _OffstagePageState();

  @override
  String get pageName => 'Offstage组件显示/隐藏';
}

class _OffstagePageState extends BasePageState<OffstagePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Column(
        children: [
          Container(
            width: 200,
            height: 200,
            color: Colors.amber,
            child: Offstage(
              offstage: true,
              child: Image.asset('asset/memory/110/10.png'),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 10),
            width: 200,
            height: 200,
            color: Colors.amber,
            child: Offstage(
              offstage: false,
              child: Image.asset('asset/memory/110/10.png'),
            ),
          ),
        ],
      ),
    );
  }

}