import '../../Include.dart';

//https://api.flutter.dev/flutter/widgets/Padding-class.html
class PaddingPage extends BasePage {

  static String dartPath = "lib/pages/layout/PaddingPage.dart";

  @override
  _PaddingPageState createState() => _PaddingPageState();

  @override
  String get pageName => 'Padding内边距';
}

class _PaddingPageState extends BasePageState<PaddingPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        child: const Card(
          child: Padding(
            padding: EdgeInsets.all(16.0),
            child: Text('Hello World!'),
          ),
        ),
      ),
    );
  }

}