import 'package:flutter_web_2021/utils/app_navigator.dart';
import 'package:getwidget/components/button/gf_button.dart';
import '../../Include.dart';
import 'MdPage.dart';

class CaseStudy extends BasePage {
  List<Widget> pages;

  CaseStudy(this.pages);

  @override
  _CaseStudyState createState() => _CaseStudyState();

  @override
  String get pageName => 'CaseStudy';
}

class _CaseStudyState extends BasePageState<CaseStudy>
    with AutomaticKeepAliveClientMixin {
  PageController pageController;

  @override
  bool get wantKeepAlive => true;
  Widget selectedPage;

  @override
  void initState() {
    super.initState();
    pageController = new PageController();
  }

  List<Widget> children() {
    return widget.pages.map((value) {
      Widget view = InkWell(
        onTap: () {
          String widChi = value.toString();

          if (widChi.contains("\$")) {
            Log().info(widChi.split("\$")[0]);
            // this.code = allCasePage[widChi.split("\$")[0]];
          } else {
            Log().info("#############abbbbbbbbb###" + widChi);
          }
          pageController.jumpToPage(1);
        },
        child:
        Container(
          padding: EdgeInsets.all(15),
          width: 350,
          child: Column(
            children: [
              new AspectRatio(
                aspectRatio: 8 / 16, //横纵比 长宽比
                child:InkWell(
                  onTap: (){
                    if( PlatformUtils.isAndroid || PlatformUtils.isIOS) {
                      AppNavigator.push(context, value);
                    }
                  },
                  child: value,
                ) , //项目资源文件
              ),
              PlatformUtils.isAndroid || PlatformUtils.isIOS
                  ? Container()
                  : Row(
                      children: [
                        Expanded(
                          child: GFButton(
                              onPressed: () {
                                // MdPage(child:  value).active();
                                this.selectedPage = value;
                                setState(() {});
                              },
                              text: "代码"),
                        ),
                        SizedBox(
                          width: 1,
                        ),
                        // Expanded(child: GFButton(onPressed: () {}, text: "网页")),
                      ],
                    ),
            ],
          ),
        ),
      );
      return view;
    }).toList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colours.text_title_black.withOpacity(0.7),
      body: Container(
        child: PageView(
          controller: pageController,
          scrollDirection: Axis.horizontal,
          children: PlatformUtils.isAndroid || PlatformUtils.isIOS
              ? [
                  new SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    reverse: false,
                    padding: EdgeInsets.all(5.0),
                    primary: true,
                    physics: BouncingScrollPhysics(),
                    child: Wrap(
                      direction: Axis.horizontal,
                      alignment: WrapAlignment.start,
                      children: children(),
                    ),
                  ),
                ]
              : [
                  new SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    reverse: false,
                    padding: EdgeInsets.all(5.0),
                    primary: true,
                    physics: BouncingScrollPhysics(),
                    child: Wrap(
                      direction: Axis.horizontal,
                      alignment: WrapAlignment.start,
                      children: children(),
                    ),
                  ),
                  MdPage(child: this.selectedPage),
                ],
        ),
      ),
    );
  }
}
