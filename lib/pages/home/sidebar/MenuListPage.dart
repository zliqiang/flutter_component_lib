import 'package:ant_icons/ant_icons.dart';
import 'package:flutter_color/flutter_color.dart';
import 'package:flutter_web_2021/Include.dart';
import 'package:flutter_web_2021/db/TreeDB1.dart';
import 'package:flutter_web_2021/entity/bean/MenuEntity.dart';
import 'package:flutter_web_2021/entity/bean/Node.dart';
import 'package:flutter_web_2021/entity/bean/TreeHelper.dart';
import 'package:flutter_web_2021/eventbus/event.dart';
import 'package:flutter_web_2021/pages/home/HomeAppBarTabPage.dart';

class MenuListPage extends BasePage {
  final HeaderItemBean headerItemBean;
  final List<MenuData> db;

  MenuListPage(this.headerItemBean, this.db);

  @override
  _MenuListPageState createState() => _MenuListPageState();

  @override
  String get pageName => '分组列表';
}

class _MenuListPageState extends BasePageState<MenuListPage> {
  List nodes = [];
  int id = -1;

  var actionEventBus;
  bool isOpen = true;

  @override
  void initState() {
    super.initState();
    nodes = TreeHelper.getSortedNodes(widget.db, 1);
    // nodes = TreeHelper.getSortedNodes(TreeDB1.getNodes(), 1);
    nodes = TreeHelper.filterVisibleNodes(nodes);

    actionEventBus = eventBus.on<TreeEvent>().listen((event) {
      this.isOpen = event.isOpen;
      setState(() {});
    });
  }

  List<Widget> traverseList() {
    return nodes.map((node) {
      Node nodeAs = node as Node;
      List<Widget> listTiles = [];
      widget.db.forEach((element) {
        if (nodeAs.id == element.pId) {
          listTiles.add(
            ListTile(
              minVerticalPadding: 0,
              contentPadding: EdgeInsets.all(0),
              title: Container(
                margin: EdgeInsets.all(0),
                padding: EdgeInsets.only(top: 10, bottom: 10),
                color: element.id != id ? Colours.text_sub_title_gray : HexColor('#4E6B9E'),
                child: Row(
                  children: [
                    Container(
                      padding: EdgeInsets.only(
                          left: !isOpen ? Dimens.dp16 : Dimens.dp16 * (nodeAs.level == null ? 2 : nodeAs.level + 1), right: Dimens.dp16),
                      child: setNodeIcon(nodeAs, false, false),
                    ),
                    !isOpen
                        ? Container()
                        : Expanded(
                            child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                element.name,
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: element.id != id ? TextStyles.btnTextBoldWhile_12 : TextStyles.btnTextBoldWhile_14,
                              ),
                              SizedBox(
                                height: Dimens.dp2,
                              ),
                              Text(element.subName, style: element.dartPath == null ? TextStyles.btnTextRed_10 : TextStyles.btnTextWhile_10)
                            ],
                          )),
                  ],
                ),
              ),
              // subtitle: Text(element.subName),
              onTap: () {
                setState(() {
                  id = element.id;
                  if (PlatformUtils.isAndroid || PlatformUtils.isIOS) {
                    AppNavigator.push(context, nodeAs.view);
                  } else {
                    Log().info("目录" + widget.headerItemBean.labelTitle);
                    // this.id,this.pId,this.name,this.isparent,this.subName,this.isExpand,this.view
                    Node node = Node(
                        id: element.id,
                        pId: element.pId,
                        name: element.name,
                        isparent: element.isParent,
                        subName: element.subName,
                        dartString: element.dartPath,
                        isExpand: false,
                        view: element.view);
                    eventBus.fire(new TabEvent(widget.headerItemBean.labelTitle, node));
                  }
                });
                print('camel+i${id.toString()}');
              },
              enabled: true,
            ),
          );
        }
      });
      return ExpansionTile(
        initiallyExpanded: nodeAs.isExpand,
        backgroundColor: Colours.text_sub_title_gray,
        collapsedBackgroundColor: Colours.text_title_black,
        collapsedIconColor: Colours.text_sub_title_gray,
        childrenPadding: EdgeInsets.all(0),
        tilePadding: !isOpen ? EdgeInsets.only(left: Dimens.dp16, right: 0, top: 0, bottom: 0) : null,
        trailing: isOpen
            ? null
            : Container(
                width: 0,
              ),
        textColor: Colours.bg_while,
        collapsedTextColor: Colours.bg_while,
        title: Row(
          children: [
            Container(
              margin: EdgeInsets.all(0),
              padding: EdgeInsets.only(right: isOpen ? Dimens.dp16 : 0),
              child: setNodeIcon(nodeAs, true, nodeAs.isExpand),
            ),
            !isOpen
                ? Container()
                : Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(nodeAs.name,
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: nodeAs.isExpand ? TextStyles.btnTextBoldWhile_14 : TextStyles.btnTextBoldWhile_12),
                        SizedBox(
                          height: Dimens.dp2,
                        ),
                        Text(nodeAs.subName, style: TextStyles.btnTextWhile_10)
                      ],
                    ),
                  ),
            !isOpen
                ? Container()
                : InkWell(
                    onTap: () {
                      eventBus.fire(new TabEvent(widget.headerItemBean.labelTitle, nodeAs));
                    },
                    child: Text(
                      "目录",
                      style: TextStyles.btnTextBoldWhile_12,
                    ),
                  ),
          ],
        ),
        onExpansionChanged: (bool value) {
          setState(() {
            nodeAs.isExpand = value;
          });
        },
        children: listTiles,
      );
    }).toList();
  }

  ///为Node设置图标
  Widget setNodeIcon(Node n, bool isparent, bool isExpand) {
    if (isparent && isExpand) {
      return n.icon = Icon(
        AntIcons.folder_open,
        color: Colours.blue,
        size: 16,
      );
    } else if (isparent && !isExpand) {
      return n.icon = Icon(
        AntIcons.folder,
        color: Colours.blue,
        size: 16,
      );
    } else {
      return n.icon = Icon(
        AntIcons.file,
        color: Colours.blue,
        size: 16,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 250,
      color: Colours.text_title_black,
      alignment: Alignment.topCenter,
      child: SingleChildScrollView(
        child: Column(
          // children: _buildExpansionTilelList(),
          children: traverseList(),
        ),
      ),
    );
  }
}
