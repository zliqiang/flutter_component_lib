import 'package:flutter_web_2021/eventbus/event.dart';
import 'package:flutter_web_2021/pages/home/HomeAppBarTabPage.dart';
import 'package:flutter_web_2021/pages/home/display/HomeNavigtorContentPage.dart';
import 'package:flutter_web_2021/pages/home/MdPage.dart';
import 'package:intl/intl.dart';

import '../../../Include.dart';
import 'view/pageVO.dart';

class TabPage2 extends BasePage {
  @override
  _TabPageState createState() => _TabPageState();
  final HeaderItemBean headerItemBean;
  TabPage2(this.headerItemBean);
  @override
  String get pageName => 'TabPage';
}

class _TabPageState extends BasePageState<TabPage2>
    with TickerProviderStateMixin {
  List<PageVO> pageVoAll;
  List<PageVO> pageVoOpened;
  TabController tabController;
  Container content;
  int length;
  bool expandMenu = true;
  bool isEventBus = false;
  var actionEventBus;
  static
  // Widget lista(){
  //   return ListView(
  //     children: [
  //       MdPage(child:  SliverGridPage(),),
  //       MdPage(link: "flutter_component/page/component/sliver/SliverToBoxAdapterPage.dart",),
  //       MdPage(child: SliverChildBuilderDelegatePage()),
  //       MdPage(child: SliverChildListDelegatePage()),
  //       MdPage(child: SliverFixedExtentListPage()),
  //     ],
  //   );
  // }


  List<PageVO> testPageVOAll_en = <PageVO>[
    // PageVO(id: "1", icon: Icons.dashboard, title: '代码阅读', widget: Container(child: lista(),)),
    // PageVO(id: "2", icon: Icons.dashboard, title: '样例列表', widget: Container(child: CaseStudy(),)),
    // PageVO(id: "6", icon: Icons.dashboard, title: '富文本', widget: Container(child: HomeBadyPage(),)),
    // PageVO(id: "4", icon: Icons.people, title: 'Personnel List', widget:  Container(child: Text("3ddd82"),)),
    // PageVO(id: "9", icon: Icons.menu, title: 'Menu List', widget: Container(child: Text("sas"),)),
    PageVO(id: "2", icon: Icons.grade, title: '首页', widget:  Container(child: Text("首页"),)),
  ];
  @override
  void initState() {
    super.initState();

    pageVoOpened = testPageVOAll_en;
    length = pageVoOpened.length;
    tabController = TabController(vsync: this, length: length);
    if (pageVoOpened.length > 0) {
      loadPage(pageVoOpened[0]);
    }

    actionEventBus = eventBus.on<TabEvent>().listen((event) {
      if(event.headerItemBean!=widget.headerItemBean.labelTitle){
        return;
      }
      int index =0;
      bool isExist =false;

      for(int i=0;i<testPageVOAll_en.length;i++){
        if(event.node.id.toString()==testPageVOAll_en[i].id.toString()){
          index =i;
          tabController.index=index;
          isExist =true;
          loadPage(pageVoOpened[index]);
          return;
        }
      }
      if(!isExist){
        testPageVOAll_en.add(PageVO(
            id: event.node.id.toString(),
            icon: Icons.dashboard,
            title: event.node.subName,
            widget: HomeNavigtorContentPage(node: event.node, widget: event.node.view,headerItemBean:widget.headerItemBean)));
      }
      pageVoOpened = testPageVOAll_en;
      length = pageVoOpened.length;
      tabController = TabController(vsync: this, length: length);
      if (pageVoOpened.length > 0) {
        loadPage(pageVoOpened[pageVoOpened.length-1]);
      }
      tabController.index =testPageVOAll_en.length-1;
    });
  }


  loadPage(page) {
    if (page == null) {
      content = Container();
      return;
    }
    content = Container(
      child: Expanded(
        child: page.widget != null ? page.widget : Center(child: Text('404')),
      ),
    );

    int index = pageVoOpened.indexWhere((note) => note.id == page.id);
    if (index > -1) {
        tabController.index = index;
      Log().info(56789);
    } else {
      pageVoOpened.add(page);
      tabController = TabController(vsync: this, length: ++length);
      tabController.index = length - 1;

      Log().info(567890);
    }
    setState(() {});
  }
  closePage(page) {
    Log().info(pageVoOpened.length.toString());
    pageVoOpened.remove(page);
    Log().info(pageVoOpened.length.toString());
    --length;
    tabController = TabController(vsync: this, length: length);
    var openPage;
    if (length > 0) {
      tabController.index = length - 1;
      openPage = pageVoOpened[0];
    }
    loadPage(openPage);
    setState(() {});
  }
  getIndicator() {
    return const UnderlineTabIndicator();
  }

  List<Widget> genListTile(data) {
    List<Widget> listTileList = data.map<Widget>((PageVO page) {
      Text title = Text(expandMenu ? page.title : '');
      if (page.children != null && page.children.length > 0) {
        return ExpansionTile(
          leading: Icon(expandMenu ? page.icon : null),
          backgroundColor: Theme.of(context).accentColor.withOpacity(0.025),
          children: genListTile(page.children),
          title: title,
        );
      } else {
        return ListTile(
          leading: Icon(page.icon),
          title: title,
          onTap: () => loadPage(page),
        );
      }
    }).toList();
    return listTileList;
  }


  @override
  Widget build(BuildContext context) {
    TabBar tabBar = TabBar(
      onTap: (index) => loadPage(pageVoOpened[index]),
      controller: tabController,
      isScrollable: true,
      indicator: getIndicator(),
      tabs: pageVoOpened.map<Tab>((PageVO page) {
        return Tab(
          child: Row(
            children: <Widget>[
              Text(page.title,style: TextStyle(fontSize: Dimens.dp12),),
              SizedBox(width: 5),
              InkWell(
                child: Icon(Icons.close, size: 12,color: Colours.bg_while,),
                onTap: () => closePage(page),
              ),
            ],
          ),
        );
      }).toList(),
    );
    Row body = Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Expanded(
                    child: Container(
                      height: 35,
                      child: tabBar,
                      decoration: BoxDecoration(
                        color: Colours.text_sub_title_gray,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black54,
                            offset: Offset(2.0, 2.0),
                            blurRadius: 4.0,
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              content,
            ],
          ),
        ),
      ],
    );
    AppUtil.instance.context = context;
    return Scaffold(
      body: body,
    );
  }
}
