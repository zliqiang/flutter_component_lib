import 'package:flutter_web_2021/entity/bean/Node.dart';
import 'package:flutter_web_2021/pages/home/HomeAppBarTabPage.dart';
import 'package:flutter_web_2021/pages/home/display/MdTabViewPage.dart';

import '../../../Include.dart';

class DisplayTabContentPage extends BasePage {
  final HeaderItemBean headerItemBean;
  final Node nodeAs;
  double indicatorWeight = 1.5;

  DisplayTabContentPage(this.headerItemBean, {this.nodeAs});

  @override
  _DisplayMdPageState createState() => _DisplayMdPageState();

  @override
  String get pageName => 'DisplayMdPage';
}

class _DisplayMdPageState extends BasePageState<DisplayTabContentPage> with TickerProviderStateMixin {
  List _spList = ["页面", "代码", "网页"];
  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = null;
    _tabController = TabController(initialIndex: 0, length: _spList.length, vsync: this); // 直接传this
    print("---->${_tabController.previousIndex}");
    if (_tabController.indexIsChanging) {
      print("---->indexch");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Expanded(
            child: TabBarView(
                physics: NeverScrollableScrollPhysics(), //禁止滑动
                controller: _tabController,
                children: _spList.isEmpty
                    ? []
                    : _spList.map((f) {
                        switch (f) {
                          case "页面":
                            return widget.nodeAs.view;
                          case "代码":
                            return MdTabViewPage(widget.headerItemBean,nodeAs: widget.nodeAs);
                          case "网页":
                            return Center(
                              child: new Text("第$f页"),
                            );
                        }
                      }).toList()),
          ),
          Container(
            width: Dimens.screenWidth(),
            height: Dimens.dp32,
            child: Stack(
              children: [
                Container(
                  height: widget.indicatorWeight,
                  child: TabBar(
                    labelColor: Colors.transparent,
                    unselectedLabelColor: Colors.transparent,
                    indicatorColor: Colours.blue,
                    isScrollable: true,
                    indicatorWeight: widget.indicatorWeight,
                    indicatorPadding: EdgeInsets.zero,
                    tabs: _spList.map((f) {
                      return Center(
                        child: new Text(
                          f,
                          style: TextStyle(color: Colors.black, fontSize: Dimens.sp12),
                        ),
                      );
                    }).toList(),
                    controller: _tabController,
                  ),
                ),
                Container(
                  child: TabBar(
                    labelColor: Colours.blue,
                    unselectedLabelColor: Colours.splitLine_gray,
                    indicatorWeight: widget.indicatorWeight,
                    isScrollable: true,
                    indicatorColor: Colors.transparent,
                    tabs: _spList.map((f) {
                      return Center(
                        child: new Text(
                          f,
                          style: TextStyle(color: Colors.black, fontSize: Dimens.sp12),
                        ),
                      );
                    }).toList(),
                    controller: _tabController,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
