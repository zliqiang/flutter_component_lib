import 'package:flutter_web_2021/entity/bean/Node.dart';
import 'package:flutter_web_2021/pages/home/HomeAppBarTabPage.dart';
import 'package:flutter_web_2021/pages/home/display/HomeNavigtorContentPage.dart';
import 'package:flutter_web_2021/utils/http/DioManager.dart';
import 'package:flutter_web_2021/utils/http/NWMethod.dart';
import 'package:markdown_widget/markdown_widget.dart';
import '../../../Include.dart';

class MdTabViewPage extends BasePage {
  final HeaderItemBean headerItemBean;
  final Node nodeAs;

  MdTabViewPage(this.headerItemBean, {this.nodeAs});

  @override
  _MdTabViewPageState createState() => _MdTabViewPageState();

  @override
  String get pageName => 'MdTabViewPage';
}

class _MdTabViewPageState extends BasePageState<MdTabViewPage> {
  final TocController controller = TocController();
  Map<String, String> allCasePage;
  String code ;
  String pageName;

  bool isOk = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    this.pageName=widget.nodeAs.dartString;
    // Log().info("------------dddddddddddddddd----------->"+  widget.nodeAs.view.toString());
    // // Log().info("------------ddddddddddddddddll----------->"+  widget.nodeAs.dartString==null?"":widget.nodeAs.dartString);
    // new Future.delayed(Duration(seconds: 1), () {
    //   if (code == null) {
    //     getData(pageName);
    //   }
    // });
  }

  void getData(String pageName) {
    DioManager().request(NWMethod.POST, "cat/setCode", params: {"code": pageName==null?"":pageName}, success: (data) {
      isOk = true;
      this.code = data;
      setState(() {});
    }, error: (error) {
      isOk = false;
      setState(() {});
      // ToastUtil.showToastTop("${error.message}");
    });
  }

  // ///获得页面名称
  // String getPageName(BuildContext context) {
  //   Log().info("#######didChangeDependencies#########");
  //   if (pageName == null) {
  //     allCasePage = context.select((Counter p) => p.allCasePage);
  //     Log().info("################" + allCasePage.length.toString());
  //     String widChi = widget.nodeAs.view.t;
  //     Log().info("################" + widChi.toString());
  //
  //     if (widChi.contains("\$")) {
  //       Log().info("############# if ###");
  //       this.pageName = allCasePage[widChi.split("\$")[0]];
  //     } else {
  //       Log().info("############# else " + widChi);
  //       this.pageName = allCasePage[widChi.toString()];
  //
  //     }
  //     Log().info("----------------------->"+ this.pageName.toString());
  //     return pageName;
  //   }
  //
  //   return pageName;
  // }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    // if (code == null) {
    //   getData(getPageName());
    // }
  }

  @override
  Widget build(BuildContext context) {
    if (code == null) {
      getData(pageName);
    }
    return Container(
      padding: EdgeInsets.only(left: 15),
      child: Row(
              children: [
                Expanded(
                    child: isOk
                        ? MarkdownWidget(
                  data: code == null ? "" : code,
                  controller: controller,
                  styleConfig: StyleConfig(markdownTheme: MarkdownTheme.lightTheme),
                ): Center(
                      child: Text("无内容"),
                    )),
                Container(
                  margin: EdgeInsets.only(left: 15,right: 15),
                  color: Colours.bg_while,
                  width: 420,
                  child: Card(
                    color: Colours.bg_while,
                    child: new AspectRatio(
                      aspectRatio: 8 / 16, //横纵比 长宽比
                      child: HomeNavigtorContentPage(
                        node: null,//widget.nodeAs 这里现实view  写null
                        headerItemBean: widget.headerItemBean,
                        widget: widget.nodeAs.view,
                      ),
                    ),
                  ),
                ),
              ],
            )
          ,
    );
  }
}
