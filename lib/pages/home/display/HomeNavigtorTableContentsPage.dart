import 'package:flutter_web_2021/entity/bean/Node.dart';
import 'package:flutter_web_2021/eventbus/event.dart';
import 'package:flutter_web_2021/pages/home/display/DisplayComponentListPage.dart';
import 'package:flutter_web_2021/router/NavigatorRoutes.dart';

import '../../../Include.dart';
import '../HomeAppBarTabPage.dart';
import 'DisplayTabContentPage.dart';

class HomeNavigtorTableContentsPage extends BasePage {
  final Widget widget;
  final Node node;
  final HeaderItemBean headerItemBean;

  HomeNavigtorTableContentsPage({this.node, this.headerItemBean, this.widget});

  @override
  _HomeNavigtorContentPageState createState() => _HomeNavigtorContentPageState();

  @override
  String get pageName => 'HomeNavigtorContentPage';
}

class _HomeNavigtorContentPageState extends BasePageState<HomeNavigtorTableContentsPage> with AutomaticKeepAliveClientMixin {
  var actionEventBus;

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();
    actionEventBus = eventBus.on<TreeEvent>().listen((event) {});
  }

  ///目录
  Widget catalog_page() {
    return Center(
        child: Column(
      children: [
        Text(widget.node.name),
        Text(widget.node.subName),
        Text("目录"),
      ],
    ));
  }

  ///空页面
  Widget emptyPage() {
    return Center(
        child: Column(
      children: [
        Text(widget.node.name),
        Text(widget.node.subName),
        Text("没有查询到"),
      ],
    ));
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          color: Colours.bg_while,
          padding: EdgeInsets.only(top: 30),
          child: Navigator(
            key: widget.node == null
                ? Key("00${widget.headerItemBean.labelTitle}")
                : Key("组件" + widget.node.id.toString() + widget.node.pId.toString()),
            // initialRoute: '/${widget.node.name}',
            onGenerateRoute: onGenerateRouteNavigator,

            onGenerateInitialRoutes: (a, b) {
              // 首页
              if (widget.node == null) {
                return [MaterialPageRoute(builder: (context) => widget.widget)];
              }

              if (widget.node.isparent != null && widget.node.isparent) {
                ///目录
                return [MaterialPageRoute(builder: (context) => DisplayComponentListPage(widget.headerItemBean, nodeAs: widget.node))];
              } else {
                return [
                  MaterialPageRoute(
                      builder: (context) => DisplayTabContentPage(
                            widget.headerItemBean,
                            nodeAs: widget.node,
                          ))
                ];
              }
            },
          ),
        ),
        Positioned(
          child: InkWell(
            child: Text(
              "返回目录",
              style: TextStyles.tab_text_blue_15,
            ),
            onTap: () {
              AppNavigator.pop(context);
            },
          ),
          top: 10,
          left: 20,
        ),
      ],
    );
  }
}
