import 'package:flutter/rendering.dart';
import 'package:flutter_web_2021/db/TreeDB1.dart';
import 'package:flutter_web_2021/db/TreeDB2.dart';
import 'package:flutter_web_2021/db/TreeDB3.dart';
import 'package:flutter_web_2021/db/TreeDB4.dart';
import 'package:flutter_web_2021/entity/bean/MenuEntity.dart';
import 'package:flutter_web_2021/entity/bean/Node.dart';
import 'package:flutter_web_2021/entity/bean/TreeHelper.dart';
import 'package:flutter_web_2021/pages/home/HomeAppBarTabPage.dart';
import 'package:flutter_web_2021/pages/home/display/HomeNavigtorContentPage.dart';
import 'package:flutter_web_2021/pages/home/display/HomeNavigtorTableContentsPage.dart';
import 'package:flutter_web_2021/utils/animation/fade_route_animation.dart';
import 'package:getwidget/components/button/gf_button.dart';

import '../../../Include.dart';

class DisplayComponentListPage extends BasePage {
  final HeaderItemBean headerItemBean;
  final Node nodeAs;

  DisplayComponentListPage(this.headerItemBean, {this.nodeAs});

  @override
  _DisplayComponentListPageState createState() => _DisplayComponentListPageState();

  @override
  String get pageName => '首页';
}

class _DisplayComponentListPageState extends BasePageState<DisplayComponentListPage> {
  List nodes = [];
  List<Widget> views = [];

  ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    switch (widget.headerItemBean.labelTitle) {
      case "组件":
        nodes = TreeHelper.getSortedNodes(TreeDB1.getNodes(), 1);
        break;
      case "页面":
        nodes = TreeHelper.getSortedNodes(TreeDB2.getNodes(), 1);
        break;
      case "案例":
        nodes = TreeHelper.getSortedNodes(TreeDB3.getNodes(), 1);
        break;
      case "插件":
        nodes = TreeHelper.getSortedNodes(TreeDB4.getNodes(), 1);
        break;
      default:
        nodes = TreeHelper.getSortedNodes(TreeDB1.getNodes(), 1);
        break;
    }
    nodes = TreeHelper.filterVisibleNodes(nodes);

    //监听滚动位置
    _scrollController.addListener(() {
      print('${_scrollController.position}');
    });

    if (widget.nodeAs == null) {
      traverseList();
    } else {
      getSubView(widget.nodeAs);
    }
  }

  getSubView(Node nodeAs) {
    List subviews = [];
    views.add(Container(
      padding: EdgeInsets.only(left: Dimens.dp16),
      height: Dimens.dp48,
      alignment: Alignment.centerLeft,
      // color: Colors.grey,
      child: Text(
        nodeAs.name + nodeAs.subName,
        style: TextStyle(color: Colors.white, fontSize: 20),
      ),
    ));
    switch (widget.headerItemBean.labelTitle) {
      case "组件":
        TreeDB1.getNodes().forEach((element) {
          if (nodeAs.id == element.pId) {
            // element.name
            subviews.add(element);
          }
        });
        break;
      case "页面":
        TreeDB2.getNodes().forEach((element) {
          if (nodeAs.id == element.pId) {
            // element.name
            subviews.add(element);
          }
        });
        break;
      case "案例":
        TreeDB3.getNodes().forEach((element) {
          if (nodeAs.id == element.pId) {
            // element.name
            subviews.add(element);
          }
        });
        break;
      case "插件":
        TreeDB4.getNodes().forEach((element) {
          if (nodeAs.id == element.pId) {
            // element.name
            subviews.add(element);
          }
        });
        break;
    }

    views.add(Wrap(
      direction: Axis.horizontal,
      alignment: WrapAlignment.start,
      children: subviews.map((e) {
        MenuData subNode = e as MenuData;
        Node node = Node(
            id: subNode.id,
            pId: subNode.pId,
            name: subNode.name,
            isparent: subNode.isParent,
            subName: subNode.subName,
            dartString: subNode.dartPath,
            isExpand: false,
            view: subNode.view);
        return Container(
          margin: EdgeInsets.all(15),
          color: Colours.bg_while,
          width: 320,
          child: Column(
            children: [
              Card(
                color: Colours.bg_while,
                child: new AspectRatio(
                  aspectRatio: 8 / 16, //横纵比 长宽比   node为空显示页面
                  child: HomeNavigtorContentPage(
                    node: null,
                    headerItemBean: widget.headerItemBean,
                    widget: subNode.view,
                  ),
                ),
              ),
              PlatformUtils.isAndroid || PlatformUtils.isIOS
                  ? Container()
                  : Row(
                      children: [
                        Expanded(
                          child: GFButton(
                              onPressed: () {
                                HomeNavigtorTableContentsPage(
                                  node: node,
                                  headerItemBean: widget.headerItemBean,
                                  widget: subNode.view,
                                );

                                Navigator.push(
                                    context,
                                    FadeRoute(HomeNavigtorTableContentsPage(
                                      node: node,
                                      headerItemBean: widget.headerItemBean,
                                      widget: subNode.view,
                                    )));
                              },
                              text: "代码"),
                        ),
                        SizedBox(
                          width: 1,
                        ),
                        // Expanded(child: GFButton(onPressed: () {}, text: "网页")),
                      ],
                    ),
            ],
          ),
        );
      }).toList(),
    ));
  }

  traverseList() {
    nodes.forEach((node) {
      Node nodeAs = node as Node;
      getSubView(nodeAs);
    });
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.topLeft,
      color: Colours.text_title_black,
      child: views.length > 0
          ? SingleChildScrollView(
              child: Column(
              // controller: _scrollController,
              children: views,
            ))
          : Text("加载中"),
    );
  }
}
