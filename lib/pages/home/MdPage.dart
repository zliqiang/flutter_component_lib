import 'package:flutter_web_2021/base/BasePageP.dart';
import 'package:flutter_web_2021/base/base_mvp_page_state.dart';
import 'package:flutter_web_2021/utils/http/DioManager.dart';
import 'package:flutter_web_2021/utils/http/NWMethod.dart';
import 'package:flutter_web_2021/utils/toast/OkToastUtil.dart';
import 'package:flutter_web_2021/widgets/text/custom_text.dart';
import 'package:getwidget/components/button/gf_button.dart';
import 'package:markdown_widget/markdown_toc.dart';
import 'package:markdown_widget/markdown_widget.dart';
import '../../Include.dart';

class MdPage extends BasePage {
  Widget child;
  String link;

  MdPage({
    this.link,
    this.child,
  });

  @override
  State<StatefulWidget> createState() {
    return MdPageState();
  }

  @override
  String get pageName => 'MdPage';
}

class MdP extends BasePageP<MdPageState> {
  String getcode = "";

  void getData(String code) {
    DioManager().request(NWMethod.POST, "cat/setCode", params: {"code": code},
        success: (data) {
      Log().info("-------" + data.toString());
      this.getcode = data;
      view.getData(true);
    }, error: (error) {
      ToastUtil.showToastTop("${error.message}");
    });
  }
}


class MdPageState extends BaseMvpPageState<MdPage, MdP>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => false;
  Map<String, String> allCasePage;

  final TocController controller = TocController();
  String code;

  @override
  void initState() {
    super.initState();
    new Future.delayed(Duration(seconds: 1),(){
      setState(() {});
    });
  }
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    Log().info("################" );
    if(widget.child==null&&code==null){
      code=widget.link;
      if (code != null && code != "") {
        presenter.getData(code.toString());
      }
    }else {
      Future.delayed(Duration.zero, () {
        if (PlatformUtils.isWeb) if (!(presenter.getcode != null &&
            presenter.getcode != "")) {
          Log().info("################" + allCasePage.length.toString());
          code = allCasePage[widget.child.toString().split("\$")[0]];
          if (code != null && code != "") {
            presenter.getData(code.toString());
          }
        }
      });
    }
  }
  @override
  void didUpdateWidget(oldWidget) {
    super.didUpdateWidget(oldWidget);
    new Future.delayed(Duration(seconds: 1),(){
      code="";
      presenter.getcode="";
      setState(() {});
    });
  }


  getData(bool isok) {
    setState(() {});
  }

  List<Widget> getboby(){
    return [
      PlatformUtils.isAndroid||PlatformUtils.isIOS?Container():
      Expanded(
        child: Column(
          children: [
            Offstage(
              offstage: PlatformUtils.isAndroid||PlatformUtils.isIOS?false:true,
              child:  CustomText(
                alignment: Alignment.topLeft,
                padding: EdgeInsets.only(
                    top: 12, bottom: 12, left: 15, right: 15),
                fontSize: Dimens.sp14,
                color: Colours.text_sub_title_gray,
                text: "代码位置：" + code.toString(),
              ),
            ),
            Row(
              children: [
                Offstage(
                  offstage: PlatformUtils.isAndroid||PlatformUtils.isIOS?true:false,
                  child:
                  Expanded(
                    child: CustomText(
                      alignment: Alignment.topLeft,
                      padding: EdgeInsets.only(
                          top: 12, bottom: 12, left: 15, right: 15),
                      fontSize: Dimens.sp14,
                      color: Colours.text_sub_title_gray,
                      text: "代码位置：" + code.toString(),
                    ),
                  ),
                ),
                GFButton(
                    onPressed: () {
                      code="";
                      presenter.getcode="";
                      setState(() {});
                    },
                    text: "代码"),
                SizedBox(
                  width: 1,
                ),
                GFButton(onPressed: () {}, text: "链接"),
                SizedBox(
                  width: 1,
                ),
                GFButton(onPressed: () {}, text: "复制"),
                SizedBox(
                  width: 15,
                ),
              ],
            ),

            Container(
              padding: EdgeInsets.only(left: 15, right: 15),
              height: 800,
              child: MarkdownWidget(
                data: presenter.getcode,
                controller: controller,
                styleConfig:
                StyleConfig(markdownTheme: MarkdownTheme.darkTheme),
              ),
            ),
          ],
        ),
      ),
      Offstage(
        offstage: widget.child==null?true:false,
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.only(left: 15, right: 15),
              height: 800,
              child: new AspectRatio(
                aspectRatio: 8 / 16, //横纵比 长宽比
                child: widget.child, //项目资源文件
              ),
            ),
          ],
        ),
      ),
    ];

  }

  @override
  Widget build(BuildContext context) {

    if(widget.child==null&&code==null){
      code=widget.link;
      if (code != null && code != "") {
        presenter.getData(code.toString());
      }
    }else {
      if (!(presenter.getcode != null && presenter.getcode != "")) {
        Log().info("################11111111111");
        allCasePage = context.select((Counter p) => p.allCasePage);
        Log().info("################" + allCasePage.length.toString());
        Log().info("################" + widget.child.toString());

        String widChi = widget.child.toString();

        if (widChi.contains("\$")) {
          Log().info("#############adasd###");
          this.code = allCasePage[widChi.split("\$")[0]];
        } else {
          Log().info("#############abbbbbbbbb###" + allCasePage.toString());
          Log().info("#############abbbbbbbbb###" + widChi);
          this.code = allCasePage[widChi.toString()];
          Log().info(this.code.toString());
        }

        if (code != null && code != "") {
          presenter.getData(code.toString());
        }
      }
    }
    return   Container(
        // url: "http://123.56.238.167:3000/zhouliqiang/catCatStarClient/raw/branch/master/lib/main.dart",
        color: Colours.text_title_black,
        child: Column(
          children: [
            PlatformUtils.isAndroid||PlatformUtils.isIOS?SingleChildScrollView(
          scrollDirection: Axis.vertical,
          reverse: false,
          padding: EdgeInsets.all(5.0),
          primary: true,
          physics: BouncingScrollPhysics(),
          child: Wrap(
            direction: Axis.horizontal,
            alignment: WrapAlignment.start,
              children: getboby(),
            ),
            ):Row(
              children: getboby(),
            ),
          ],
        ),
      )
    ;
  }

  @override
  MdP createPresenter() {
    return MdP();
  }
}
