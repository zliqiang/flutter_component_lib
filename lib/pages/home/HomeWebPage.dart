import 'package:flutter_web_2021/pages/home/HomeAppBarTabPage.dart';
import 'package:flutter_web_2021/pages/home/HomeWebNodePage3.dart';
import 'package:flutter_web_2021/pages/learning/mindmap/memory1/Number110Page.dart';

import '../../Include.dart';
import 'view/home_view_large.dart';
import 'view/home_view_small.dart';

class HomeWebPage extends BasePage {
  @override
  _HomeWebPageState createState() => _HomeWebPageState();

  @override
  String get pageName => 'HomeWebPage';
}
class _HomeWebPageState extends BasePageState<HomeWebPage> {
  int _currentIndex = 0;
  @override
  Widget build(BuildContext context) {
    AppUtil.instance.setContext = context;
    return Scaffold(
      body: LayoutBuilder(
        builder: (context, constraints) {
          if (constraints.maxWidth < 600) {
            // return HomeViewSmall(_currentIndex, (index) {
            //   setState(() {
            //     _currentIndex = index;
            //   });
            // });
            return Number110Page();
          } else {
            // return HomeViewLarge(_currentIndex, (index) {
            //   setState(() {
            //     _currentIndex = index;
            //   });
            // });
            return HomeAppBarTabPage();
          }
        },
      ),
    );
  }

}