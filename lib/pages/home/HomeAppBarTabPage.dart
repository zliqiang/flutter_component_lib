import 'dart:async';

import 'package:flutter_web_2021/db/TreeDB1.dart';
import 'package:flutter_web_2021/db/TreeDB2.dart';
import 'package:flutter_web_2021/db/TreeDB3.dart';
import 'package:flutter_web_2021/db/TreeDB4.dart';
import 'package:flutter_web_2021/eventbus/event.dart';
import 'package:flutter_web_2021/pages/home/HomeWebNodePage2.dart';
import 'package:flutter_web_2021/pages/home/HomeWebNodePage3.dart';
import 'package:flutter_web_2021/pages/home/HomeWebNodePage1.dart';
import 'package:flutter_web_2021/pages/home/HomeWebNodePage4.dart';
import 'package:flutter_web_2021/pages/home/tab/TabPage2.dart';
import 'package:flutter_web_2021/pages/home/tab/TabPage1.dart';
import 'package:flutter_web_2021/pages/home/tab/TabPage3.dart';
import 'package:flutter_web_2021/pages/home/tab/TabPage4.dart';
import 'package:flutter_web_2021/res/colors.dart';

import '../../Include.dart';

class HeaderItemBean {
  final String labelTitle;

  HeaderItemBean(this.labelTitle);
}

final List<HeaderItemBean> _allPages = <HeaderItemBean>[
  new HeaderItemBean("组件"),
  new HeaderItemBean("页面"),
  new HeaderItemBean("案例"),
  new HeaderItemBean("插件"),
];

class HomeAppBarTabPage extends BasePage {
  @override
  _HomeAppBarTabPageState createState() => _HomeAppBarTabPageState();

  @override
  String get pageName => 'HomeAppBarTabPage';
}

class _HomeAppBarTabPageState extends BasePageState<HomeAppBarTabPage> {
  bool click = true;
  @override
  Widget build(BuildContext context) {
    Widget titleLayout = new TabBar(
      //tabs 的长度超出屏幕宽度后，TabBar，是否可滚动
      //设置为false tab 将平分宽度，为true tab 将会自适应宽度
      isScrollable: true, //  每个label的padding值
      labelPadding: EdgeInsets.all(12.0),
      labelColor: Colours.bg_while,
      unselectedLabelColor: Colours.text_sub_title_gray,
      indicatorWeight: 0.1,
      ///指示器大小计算方式，TabBarIndicatorSize.label跟文字等宽,TabBarIndicatorSize.tab跟每个tab等宽
      indicatorSize: TabBarIndicatorSize.label,
      //设置所有的tab
      tabs: _allPages.map((HeaderItemBean page) => new Tab(text: page.labelTitle)).toList(),
    );

    Widget body = new TabBarView(
        physics: NeverScrollableScrollPhysics(), //禁止滑动
        children: _allPages.map((HeaderItemBean page) {
          switch(page.labelTitle){
            case "组件":
              return Container(
                child: HomeWebNodePage1(page,TreeDB1.getNodes(),TabPage1(page)),
              );
            case "页面":
              return Container(
                child: HomeWebNodePage2(page,TreeDB2.getNodes(),TabPage2(page)),
              );
            case "案例":
              return Container(
                child: HomeWebNodePage3(page,TreeDB3.getNodes(),TabPage3(page)),
              );
            case "插件":
              return Container(
                child: HomeWebNodePage4(page,TreeDB4.getNodes(),TabPage4(page)),
              );
            default :
              return Container(
                child: Text(page.labelTitle),
              );
          }
    }).toList());

    return new DefaultTabController(
        length: _allPages.length,
        child: new Scaffold(
            // appBar: new AppBar(
            //   //标题是否居中显示，默认值根据不同的操作系统，显示方式不一样,true居中 false居左
            //   centerTitle: true,
            //   //Toolbar 中主要内容，通常显示为当前界面的标题文字
            //   title: titleLayout,
            // ),
            body: Container(
          child: Column(
            children: [
              Container(
                  width: Dimens.screenWidth(),
                  height: Dimens.toolbarHeight(),
                  color: Colours.text_title_black,
                  child: Row(
                    children: [
                      Container(
                        width: 250,
                        height: Dimens.toolbarHeight(),
                        child: Row(
                          children: [
                            Container(
                              margin: EdgeInsets.only(left: 0),
                              alignment: Alignment.centerLeft,
                              child: IconButton(
                                  alignment: Alignment.center,
                                  splashRadius: 24,
                                  icon: Icon(
                                    Icons.reorder,
                                    color: Colours.bg_while,
                                    size: 15,
                                  ),
                                  color: Colors.grey,
                                  onPressed: () {
                                      click = !click;
                                      if (!click) {
                                        eventBus.fire(new HomeAppBarTabEvent(click));
                                      } else {
                                        Timer(Duration(milliseconds: 400), () {
                                          eventBus.fire(new HomeAppBarTabEvent(click));
                                        });
                                      }
                                  }),
                            ),
                            Container(
                              margin: Dimens.edgeInsets_lr_dp8,
                              child: Text(
                                "Pigeon",
                                maxLines: 1,
                                style: TextStyle(
                                  fontSize: 24,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: 'HelveticaNeue',
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Expanded(child:Container(
                        alignment: Alignment.center,
                        child: titleLayout,
                      )),
                      Container(
                        width: 250,
                        height: Dimens.toolbarHeight(),
                        child: Row(
                          children: [
                            Expanded(child:SizedBox(width: 32)),
                            Container(child: Icon(Icons.account_circle,color: Colours.bg_while,)),
                            SizedBox(width: 32),
                            Container(
                              child: IconButton(
                                padding: EdgeInsets.all(0),
                                icon: Icon(Icons.exit_to_app,color: Colours.bg_while,),
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                              ),
                            ),
                            SizedBox(width: 32),
                          ],
                        ),
                      ),
                    ],
                  )),
              Expanded(child: body),
            ],
          ),
        )));
  }
}
