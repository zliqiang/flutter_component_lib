import 'dart:async';
import 'package:flutter_web_2021/db/TreeDB2.dart';
import 'package:flutter_web_2021/entity/bean/MenuEntity.dart';
import 'package:flutter_web_2021/eventbus/event.dart';
import 'package:flutter_web_2021/pages/basic/tree/TreePage.dart';
import 'package:flutter_web_2021/pages/home/HomeAppBarTabPage.dart';
import 'package:flutter_web_2021/pages/home/sidebar/MenuListPage.dart';

import '../../Include.dart';
import 'tab/TabPage2.dart';

class HomeWebNodePage4 extends BasePage {
  final HeaderItemBean headerItemBean;
  final List<MenuData> db;
  Widget tab;
  HomeWebNodePage4(this.headerItemBean,this.db,this.tab);

  @override
  _HomePageState createState() => _HomePageState();

  @override
  String get pageName => 'HomePage';
}

class _HomePageState extends BasePageState<HomeWebNodePage4> with AutomaticKeepAliveClientMixin{
  // double w = 50;
  double w = 65;
  bool click = true;

  var actionEventBus;

  @override
  void initState() {
    super.initState();
    if (PlatformUtils.isAndroid || PlatformUtils.isIOS) {
      w = 0;
    }

    actionEventBus = eventBus.on<HomeAppBarTabEvent>().listen((event) {
      this.click = event.isOpen;
      setState(() {
        if (!click) {
          eventBus.fire(new TreeEvent(widget.headerItemBean.labelTitle,click));
        } else {
          Timer(Duration(milliseconds: 400), () {
            eventBus.fire(new TreeEvent(widget.headerItemBean.labelTitle,click));
          });
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    AppUtil.instance.setContext = context;
    return Scaffold(
      body: Container(
        child: Row(
          children: [
            GestureDetector(
              child: AnimatedContainer(
                  width: click ? 250 : w,
                  color: Colors.blue,
                  curve: Curves.ease,
                  duration: Duration(seconds: 1),
                  child:
                  SizedBox(
                    width: 200.0,
                    child:
                    MenuListPage(widget.headerItemBean,widget.db),
                  )
                // child: TreePage(widget.headerItemBean,widget.db),
              ),
            ),
            Expanded(
                child: Container(
                  child: widget.tab,
                )),
          ],
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
