import 'package:flutter_web_2021/Include.dart';
import 'package:flutter_web_2021/utils/string/string_110_util.dart';
import 'package:flutter_web_2021/widgets/card/flip_card.dart';
import 'package:responsive_ui/responsive_ui.dart';

class Number110Page extends BasePage {
  static String dartPath = "lib/pages/learning/mindmap/memory1/Number110Page.dart";
  @override
  _Number110PageState createState() => _Number110PageState();

  @override
  String get pageName => 'Number110Page';
}

class _Number110PageState extends BasePageState<Number110Page> {
  int amount = 110;
  List randomInt;

  bool isNumber = true;
  bool isTitle = true;
  bool isSort = true;

 double viewWidth = Dimens.dp72;

  @mustCallSuper
  @override
  void initState() {
    super.initState();
    randomInt = String110Util.random(110, sortBool: isSort);
  }
  ///正面
  Widget frontView(int index) {
    return Container(
      width: viewWidth,
      color: Colors.white,
      height: viewWidth,
      alignment: Alignment.center,
      child: Column(
        children: [
          SizedBox(
            height: Dimens.dp4,
          ),
          Expanded(
              child: Image.asset(
            'asset/memory/110/${String110Util.getImgName(index)}.png',
            width: viewWidth,
          )),
          SizedBox(
            height: Dimens.dp4,
          ),
          Text(
            "${String110Util.getImgTitle(String110Util.getImgName(index).replaceAll("-", "0").replaceAll("010", "00"))}",
            style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: Dimens.dp4,
          )
        ],
      ),
    );
  }

  ///背面
  Widget backView(int index) {
    return InkWell(
      child: Container(
        width: viewWidth,
        height: viewWidth,
        alignment: Alignment.center,
        child: Text("${String110Util.getImgName(index).replaceAll("-", "0").replaceAll("010", "00")}"),
      ),
    );
  }

  /*一个渐变颜色的正方形集合*/
  List<Widget> randomBoxs() {
    return randomInt.map((e) {
      // print("---------" + e.toString());
      // print("=======${int.parse(e.toString())}=====");
      int index = int.parse(e.toString());
      return Container(
        color: Colors.grey,
        margin: EdgeInsets.all(Dimens.dp1),
        width: viewWidth,
        height: viewWidth,
        child: FlipCard(
            front: isNumber ? backView(index) : frontView(index),
            back: (!isNumber) ? backView(index) : frontView(index),
            direction: FlipDirection.HORIZONTAL),
      );
    }).toList();
  }

  updateRandom() {
    setState(() {
      randomInt.clear();
      randomInt = String110Util.random(amount);
    });
  }

  @override
  Widget build(BuildContext context) {
    AppUtil.instance.setContext = context;
    return Scaffold(
      appBar: AppBar(
        title: Text('数字编码'),
      ),
      body: LayoutBuilder(
        builder: (context, constraints) {
          if (constraints.maxWidth < 600) {
            viewWidth =(Dimens.screenWidth()/4)-2;
          } else {
            viewWidth = Dimens.dp72;
          }

          return   Column(
            children: [
              Expanded(
                child: SingleChildScrollView(
                  child: Wrap(
                    spacing: 0, //主轴上子控件的间距
                    runSpacing: 0, //交叉轴上子控件之间的间距
                    children: randomBoxs(), //要显示的子控件集合
                  ),
                ),
              )
            ],
          );

        },
      ),
    );
  }
}
