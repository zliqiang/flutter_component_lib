import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:fwfh_webview/fwfh_webview.dart';

import '../../../Include.dart';


class WebViewPage extends BasePage {
  static String dartPath = "lib/pages/case_study/webview/WebViewPage.dart";
  @override
  _WebViewPageState createState() => _WebViewPageState();

  @override
  String get pageName => 'WebView';
}

class _WebViewPageState extends BasePageState<WebViewPage> {

  @override
  Widget build(BuildContext context) {
    AppUtil.instance.context=context;
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.pageName),
      ),
      body: Container(
        width: Dimens.screenWidth(),
        height: Dimens.screenHeight(),
        child: HtmlWidget(
          '<iframe src="https://m.baidu.com"></iframe>',
          factoryBuilder: () => MyWidgetFactory(),
        ),
      ),
    );
  }
}

class MyWidgetFactory extends WidgetFactory with WebViewFactory {
  // optional: override getter to configure how WebViews are built
  bool get webViewMediaPlaybackAlwaysAllow => true;
  String get webViewUserAgent => 'My app';
}
