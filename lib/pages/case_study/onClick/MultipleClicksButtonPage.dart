import 'package:flutter/material.dart';
import 'package:flutter_web_2021/Include.dart';
import 'package:flutter_web_2021/utils/toast/OkToastUtil.dart';

class MultipleClicksButtonPage extends BasePage {

  static String dartPath = "";

  @override
  _MultipleClicksButtonPageState createState() => _MultipleClicksButtonPageState();

  @override
  String get pageName => 'MultipleClicksButtonPage[多次点击]';
}

class _MultipleClicksButtonPageState extends BasePageState<MultipleClicksButtonPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        child: MultipleClicksButton(
          text: "多次点击",
          onTap: (){
            ToastUtil.showToastBottom("点击");
            print("-------------------- 点击 -------------------");
          },
        ),
      ),
    );
  }

}










/// 渐变按钮视图
class MultipleClicksButton extends StatelessWidget {
  final GestureTapCallback onTap; //点击事件回调（必须有）
  final Widget title; // 优先使用
  final String text; //中间文字（必须有）
  final double fontSize; // 默认 16.0
  final bool isInfinity; //是否默认撑满父布局
  final double width; //按钮宽度
  final double height; //按钮高度，没有默认撑满父布局
  final EdgeInsetsGeometry margin; //按钮外边距，没有默认无外边距
  final EdgeInsetsGeometry padding; //按钮内边距，没有默认无内边距
  final BorderRadiusGeometry borderRadius; //按钮圆角，默认四个角30

  static int clickSpaceTime = 3000;//延时3s

  MultipleClicksButton({
    Key key,
    this.onTap,
    this.title,
    this.text,
    this.fontSize = 16.0,
    this.margin,
    this.isInfinity = true,
    this.width,
    this.height,
    this.padding,
    this.borderRadius,
  }) : super(key: key);
  bool bCanPress = true;
  @override
  Widget build(BuildContext context) {

    if (onTap == null) {
      return _bodyView(context);
    }

    return Material(
      child: Ink(
        child: InkWell(
          onTap: () {
            ///避免重复响应
            if(onTap != null && bCanPress) {
              bCanPress = false;
              onTap();
              Future.delayed(Duration(milliseconds: clickSpaceTime), (){
                bCanPress = true;
              });
            }
            if(!bCanPress) {
              print("按钮被重复点击");
            }
          },
          child: _bodyView(context),
        ),
      ),
      color: Color(0x00000000),
    );
  }

  Widget _bodyView (BuildContext context) {
    return new Container(
      child: title ??
          Text(
            text ?? "",

          ),
      decoration: BoxDecoration(
          // gradient: LinearGradient(colors: [
          //   EcjiaViewConfig().colorValue.themeColorGradientStart,
          //   EcjiaViewConfig().colorValue.themeColorGradientEnd
          // ]),
          borderRadius: borderRadius??BorderRadius.all(Radius.circular(30))),
      width: width ?? (isInfinity?double.infinity:null),
      height: height ?? double.infinity,
      alignment: Alignment.center,
      margin: margin ?? EdgeInsets.only(top: 0, bottom: 0, left: 0, right: 0),
      padding: padding ?? EdgeInsets.only(top: 0, bottom: 0, left: 0, right: 0),
    );
  }
}
