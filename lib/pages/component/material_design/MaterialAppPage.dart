import '../../../Include.dart';

//https://api.flutter.dev/flutter/material/MaterialApp-class.html
class MaterialAppPage extends BasePage {

  static String dartPath = "lib/pages/component/material_design/MaterialAppPage.dart";

  @override
  _MaterialAppPageState createState() => _MaterialAppPageState();

  @override
  String get pageName => 'MaterialApp必需';
}

class _MaterialAppPageState extends BasePageState<MaterialAppPage> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
          brightness: Brightness.dark,
          primaryColor: Colors.blueGrey
      ),
      // initialRoute: "/",
      // routes: <String, WidgetBuilder>{
      //   '/': (BuildContext context) {
      //     return Scaffold(
      //       appBar: AppBar(
      //         title: const Text('Home Route'),
      //       ),
      //     );
      //   },
      //   '/about': (BuildContext context) {
      //     return Scaffold(
      //       appBar: AppBar(
      //         title: const Text('About Route'),
      //       ),
      //     );
      //   }
      // },
      home: Scaffold(
        appBar: MyAppBar.appBar(context, widget.pageName),
      ),
      debugShowCheckedModeBanner: false,
    );
  }

}