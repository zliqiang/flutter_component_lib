import '../../../../Include.dart';

//https://api.flutter.dev/flutter/material/Chip-class.html
class ChipPage extends BasePage {

  static String dartPath = "lib/pages/component/material_design/info_displays/ChipPage.dart";

  @override
  _ChipPageState createState() => _ChipPageState();

  @override
  String get pageName => 'Chip标签';
}

class _ChipPageState extends BasePageState<ChipPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        child: Chip(
          avatar: CircleAvatar(
            backgroundColor: Colors.grey.shade800,
            child: const Text('AB'),
          ),
          label: const Text('Aaron Burr'),
        ),
      ),
    );
  }

}