import '../../../../Include.dart';

//https://api.flutter.dev/flutter/material/CircularProgressIndicator-class.html
class CircularProgressIndicatorPage extends BasePage {
  static String dartPath = "lib/pages/component/material_design/info_displays/CircularProgressIndicatorPage.dart";

  @override
  _CircularProgressIndicatorPageState createState() => _CircularProgressIndicatorPageState();

  @override
  String get pageName => 'CircularProgressIndicator圆形进度条';
}

class _CircularProgressIndicatorPageState extends BasePageState<CircularProgressIndicatorPage> with TickerProviderStateMixin {
  AnimationController controller;

  @override
  void initState() {
    controller = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 5),
    )..addListener(() {
        setState(() {});
      });
    controller.repeat(reverse: true);
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Text(
              'Linear progress indicator with a fixed color',
              style: Theme.of(context).textTheme.headline6,
            ),
            CircularProgressIndicator(
              value: controller.value,
              semanticsLabel: 'Linear progress indicator',
            ),
          ],
        ),
      ),
    );
  }
}
