import '../../../../Include.dart';

//https://api.flutter.dev/flutter/material/LinearProgressIndicator-class.html
class LinearProgressIndicatorPage extends BasePage {
  static String dartPath = "lib/pages/component/material_design/info_displays/LinearProgressIndicatorPage.dart";

  @override
  _LinearProgressIndicatorPageState createState() => _LinearProgressIndicatorPageState();

  @override
  String get pageName => 'LinearProgressIndicator线性进度条';
}

class _LinearProgressIndicatorPageState extends BasePageState<LinearProgressIndicatorPage> with TickerProviderStateMixin {
  AnimationController controller;

  @override
  void initState() {
    controller = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 10),
    )..addListener(() {
        setState(() {});
      });
    controller.repeat(reverse: true);
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            const Text(
              'Linear progress indicator with a fixed color',
              style: TextStyle(fontSize: 20),
            ),
            LinearProgressIndicator(
              value: controller.value,
              semanticsLabel: 'Linear progress indicator',
            ),
          ],
        ),
      ),
    );
  }
}
