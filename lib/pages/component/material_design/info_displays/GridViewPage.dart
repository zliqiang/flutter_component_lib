import '../../../../Include.dart';

//https://api.flutter.dev/flutter/widgets/GridView-class.html
class GridViewPage extends BasePage {

  static String dartPath = "lib/pages/component/material_design/info_displays/GridViewPage.dart";


  @override
  _GridViewPageState createState() => _GridViewPageState();

  @override
  String get pageName => 'GridView网格列表';
}

class _GridViewPageState extends BasePageState<GridViewPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        child: LayoutDemoBuilder(),
      ),
    );
  }

}

class LayoutDemo extends StatelessWidget {
  List listData=[
    {
      "title": 'Candy Shop',
      "author": 'Mohamed Chahin',
      "imageUrl": 'https://www.itying.com/images/flutter/1.png',
    },
    {
      "title": 'Childhood i',
      "author": 'Google',
      "imageUrl": 'https://www.itying.com/images/flutter/2.png',
    },
    {
      "title": 'Alibaba Shop',
      "author": 'Alibaba',
      "imageUrl": 'https://www.itying.com/images/flutter/3.png',
    },
    {
      "title": 'Candy Shop',
      "author": 'Mohamed Chahin',
      "imageUrl": 'https://www.itying.com/images/flutter/4.png',
    },
    {
      "title": 'Tornado',
      "author": 'Mohamed Chahin',
      "imageUrl": 'https://www.itying.com/images/flutter/5.png',
    },
    {
      "title": 'Undo',
      "author": 'Mohamed Chahin',
      "imageUrl": 'https://www.itying.com/images/flutter/6.png',
    },
    {
      "title": 'white-dragon',
      "author": 'Mohamed Chahin',
      "imageUrl": 'https://www.itying.com/images/flutter/7.png',
    }

  ];

  List<Widget> _getListData() {
    var tempList= listData.map((value){
      return Container(
        child:Column(
          children: <Widget>[
            Image.network(value['imageUrl']),
            SizedBox(height: 12),
            Text(
              value['title'],
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize: 20
              ),
            )
          ],

        ),
        decoration: BoxDecoration(

            border: Border.all(
                color:Color.fromRGBO(233, 233,233, 0.9),
                width: 1
            )
        ),

        // height: 400,  //设置高度没有反应
      );

    });
    // ('xxx','xxx')
    return tempList.toList();
  }


  @override
  Widget build(BuildContext context) {
    return GridView.count(
      crossAxisSpacing:10.0 ,   //水平子 Widget 之间间距
      mainAxisSpacing: 10.0,    //垂直子 Widget 之间间距
      padding: EdgeInsets.all(10),
      crossAxisCount: 2,  //一行的 Widget 数量
      // childAspectRatio:0.7,  //宽度和高度的比例
      children: this._getListData(),
    );
  }
}



class LayoutDemoBuilder extends StatelessWidget {

  List listData=[
    {
      "title": 'Candy Shop',
      "author": 'Mohamed Chahin',
      "imageUrl": 'https://www.itying.com/images/flutter/1.png',
    },
    {
      "title": 'Childhood i',
      "author": 'Google',
      "imageUrl": 'https://www.itying.com/images/flutter/2.png',
    },
    {
      "title": 'Alibaba Shop',
      "author": 'Alibaba',
      "imageUrl": 'https://www.itying.com/images/flutter/3.png',
    },
    {
      "title": 'Candy Shop',
      "author": 'Mohamed Chahin',
      "imageUrl": 'https://www.itying.com/images/flutter/4.png',
    },
    {
      "title": 'Tornado',
      "author": 'Mohamed Chahin',
      "imageUrl": 'https://www.itying.com/images/flutter/5.png',
    },
    {
      "title": 'Undo',
      "author": 'Mohamed Chahin',
      "imageUrl": 'https://www.itying.com/images/flutter/6.png',
    },
    {
      "title": 'white-dragon',
      "author": 'Mohamed Chahin',
      "imageUrl": 'https://www.itying.com/images/flutter/7.png',
    }

  ];
  Widget _getListData (context,index) {
    return Container(
      child:Column(
        children: <Widget>[
          Image.network(listData[index]['imageUrl']),
          SizedBox(height: 15),
          Text(
            listData[index]['title'],
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: 20
            ),
          )
        ],

      ),
      decoration: BoxDecoration(
          border: Border.all(
              color:Color.fromRGBO(233, 233,233, 0.9),
              width: 1
          )
      ),

      // height: 400,  //设置高度没有反应
    );
  }


  @override
  Widget build(BuildContext context) {
    return GridView.builder(

      //注意
      gridDelegate:SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisSpacing:10.0 ,   //水平子 Widget 之间间距
        mainAxisSpacing: 10.0,    //垂直子 Widget 之间间距
        crossAxisCount: 2,  //一行的 Widget 数量
      ),
      itemCount: listData.length,
      itemBuilder:this._getListData,
    );
  }
}
