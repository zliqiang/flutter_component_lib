import '../../../../Include.dart';

//https://api.flutter.dev/flutter/material/Card-class.html
class CardPage extends BasePage {

  static String dartPath = "lib/pages/component/material_design/info_displays/CardPage.dart";

  @override
  _CardPageState createState() => _CardPageState();

  @override
  String get pageName => 'Card卡片';
}

class _CardPageState extends BasePageState<CardPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        padding: EdgeInsets.only(top: 10),
        child:Column(
          children: [
            MyStatelessWidget(),
            MyStatelessWidget2()
          ],
        )
      ),
    );
  }

}


/// This is the stateless widget that the main application instantiates.
class MyStatelessWidget extends StatelessWidget {
  const MyStatelessWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            const ListTile(
              leading: Icon(Icons.album),
              title: Text('The Enchanted Nightingale'),
              subtitle: Text('Music by Julie Gable. Lyrics by Sidney Stein.'),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                TextButton(
                  child: const Text('BUY TICKETS'),
                  onPressed: () {/* ... */},
                ),
                const SizedBox(width: 8),
                TextButton(
                  child: const Text('LISTEN'),
                  onPressed: () {/* ... */},
                ),
                const SizedBox(width: 8),
              ],
            ),
          ],
        ),
      ),
    );
  }
}


/// This is the stateless widget that the main application instantiates.
class MyStatelessWidget2 extends StatelessWidget {
  const MyStatelessWidget2({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        child: InkWell(
          splashColor: Colors.blue.withAlpha(30),
          onTap: () {
            print('Card tapped.');
          },
          child: const SizedBox(
            width: 300,
            height: 100,
            child: Text('A card that can be tapped'),
          ),
        ),
      ),
    );
  }
}