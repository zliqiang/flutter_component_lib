import '../../../Include.dart';

//https://api.flutter.dev/flutter/material/IconButton-class.html
class IconButtonPage extends BasePage {

  static String dartPath = "lib/pages/component/material_design/IconButtonPage.dart";

  @override
  _IconButtonPageState createState() => _IconButtonPageState();

  @override
  String get pageName => 'IconButton图标按钮';
}

class _IconButtonPageState extends BasePageState<IconButtonPage> {
  double _volume = 0.0;


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            IconButton(
              icon: const Icon(Icons.volume_up),
              tooltip: 'Increase volume by 10',
              onPressed: () {
                setState(() {
                  _volume += 10;
                });
              },
            ),
            Text('Volume : $_volume'),

            Material(
              color: Colors.white,
              child: Center(
                child: Ink(
                  decoration: const ShapeDecoration(
                    color: Colors.lightBlue,
                    shape: CircleBorder(),
                  ),
                  child: IconButton(
                    icon: const Icon(Icons.android),
                    color: Colors.white,
                    onPressed: () {},
                  ),
                ),
              ),
            )

          ],
        ),
      ),
    );
  }

}