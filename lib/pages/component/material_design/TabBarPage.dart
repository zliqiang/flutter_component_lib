import '../../../Include.dart';
//TabBar
//https://api.flutter.dev/flutter/material/TabBar-class.html
//TabBarView
//https://api.flutter.dev/flutter/material/TabBarView-class.html
class TabBarPage extends BasePage {

  static String dartPath = "lib/pages/component/material_design/TabBarPage.dart";

  @override
  _TabBarPageState createState() => _TabBarPageState();

  @override
  String get pageName => 'TabBar水平选项卡';
}

class _TabBarPageState extends BasePageState<TabBarPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        child:     DefaultTabController(
          initialIndex: 1,
          length: 3,
          child: Scaffold(
            appBar: AppBar(
              title: const Text('TabBar Widget'),
              bottom: const TabBar(
                tabs: <Widget>[
                  Tab(
                    icon: Icon(Icons.cloud_outlined),
                  ),
                  Tab(
                    icon: Icon(Icons.beach_access_sharp),
                  ),
                  Tab(
                    icon: Icon(Icons.brightness_5_sharp),
                  ),
                ],
              ),
            ),
            body: const TabBarView(
              children: <Widget>[
                Center(
                  child: Text('It\'s cloudy here'),
                ),
                Center(
                  child: Text('It\'s rainy here'),
                ),
                Center(
                  child: Text('It\'s sunny here'),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}