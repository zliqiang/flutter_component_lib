import '../../../../Include.dart';

//https://api.flutter.dev/flutter/material/Slider-class.html
class SliderPage extends BasePage {

  static String dartPath = "lib/pages/component/material_design/input_and_selection/SliderPage.dart";

  @override
  _SliderPageState createState() => _SliderPageState();

  @override
  String get pageName => 'Slider滑块';
}

class _SliderPageState extends BasePageState<SliderPage> {
  double _currentSliderValue = 20;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        child: Slider(
          value: _currentSliderValue,
          min: 0,
          max: 100,
          divisions: 5,
          label: _currentSliderValue.round().toString(),
          onChanged: (double value) {
            setState(() {
              _currentSliderValue = value;
            });
          },
        ),
      ),
    );
  }

}