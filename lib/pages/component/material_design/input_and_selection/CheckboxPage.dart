import '../../../../Include.dart';

//https://api.flutter.dev/flutter/material/Checkbox-class.html
class CheckboxPage extends BasePage {
  static String dartPath = "lib/pages/component/material_design/input_and_selection/CheckboxPage.dart";

  @override
  _CheckboxPageState createState() => _CheckboxPageState();

  @override
  String get pageName => 'Checkbox复选框';
}

class _CheckboxPageState extends BasePageState<CheckboxPage> {
  bool isChecked = false;

  @override
  Widget build(BuildContext context) {
    Color getColor(Set<MaterialState> states) {
      const Set<MaterialState> interactiveStates = <MaterialState>{
        MaterialState.pressed,
        MaterialState.hovered,
        MaterialState.focused,
      };
      if (states.any(interactiveStates.contains)) {
        return Colors.blue;
      }
      return Colors.red;
    }

    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        child: Checkbox(
          checkColor: Colors.white,
          fillColor: MaterialStateProperty.resolveWith(getColor),
          value: isChecked,
          onChanged: (bool value) {
            setState(() {
              isChecked = value;
            });
          },
        ),
      ),
    );
  }
}
