import '../../../../Include.dart';

//https://api.flutter.dev/flutter/material/TextField-class.html
class TextFieldPage extends BasePage {
  static String dartPath = "lib/pages/component/material_design/input_and_selection/TextFieldPage.dart";

  @override
  _TextFieldPageState createState() => _TextFieldPageState();

  @override
  String get pageName => 'TextField文本输入框';
}

class _TextFieldPageState extends BasePageState<TextFieldPage> {
  TextEditingController _controller;

  @override
  void initState() {
    super.initState();
    _controller = TextEditingController();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: ListView(
        padding: EdgeInsets.all(20),
        children: [
          const TextField(
            obscureText: true,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'Password',
            ),
          ),
          TextField(
            controller: _controller,
            onSubmitted: (String value) async {
              await showDialog<void>(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: const Text('Thanks!'),
                    content: Text('You typed "$value", which has length ${value.characters.length}.'),
                    actions: <Widget>[
                      TextButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: const Text('OK'),
                      ),
                    ],
                  );
                },
              );
            },
          ),
        ],
      ),
    );
  }
}
