import '../../../../Include.dart';

//https://api.flutter.dev/flutter/material/Switch-class.html
class SwitchPage extends BasePage {

  static String dartPath = "lib/pages/component/material_design/input_and_selection/SwitchPage.dart";

  @override
  _SwitchPageState createState() => _SwitchPageState();

  @override
  String get pageName => 'Switch开关';
}

class _SwitchPageState extends BasePageState<SwitchPage> {
  bool flag = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Switch(
              value: this.flag,
              activeColor: Colors.red,
              onChanged: (value) {
                setState(() {
                  this.flag = value;
                });
              },
            ),
            Text("此时的状态是${this.flag == true ? "选中" : "未选中"}")
          ],
        ),
      ),
    );
  }

}