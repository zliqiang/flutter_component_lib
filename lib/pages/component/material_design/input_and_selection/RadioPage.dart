import '../../../../Include.dart';

//https://api.flutter.dev/flutter/material/Radio-class.html
class RadioPage extends BasePage {
  static String dartPath = "lib/pages/component/material_design/input_and_selection/RadioPage.dart";

  @override
  _RadioPageState createState() => _RadioPageState();

  @override
  String get pageName => 'Radio单选框';
}

enum SingingCharacter { lafayette, jefferson }

class _RadioPageState extends BasePageState<RadioPage> {
  SingingCharacter _character = SingingCharacter.lafayette;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        child: Column(
          children: <Widget>[
            ListTile(
              title: const Text('Lafayette'),
              leading: Radio<SingingCharacter>(
                value: SingingCharacter.lafayette,
                groupValue: _character,
                onChanged: (SingingCharacter value) {
                  setState(() {
                    _character = value;
                  });
                },
              ),
            ),
            ListTile(
              title: const Text('Thomas Jefferson'),
              leading: Radio<SingingCharacter>(
                value: SingingCharacter.jefferson,
                groupValue: _character,
                onChanged: (SingingCharacter value) {
                  setState(() {
                    _character = value;
                  });
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
