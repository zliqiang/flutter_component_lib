import 'package:date_format/date_format.dart';

import '../../../../Include.dart';

//https://api.flutter.dev/flutter/material/showDatePicker.html
class DateAndTimePickersPage extends BasePage {

  static String dartPath = "lib/pages/component/material_design/input_and_selection/DateAndTimePickersPage.dart";
   DateAndTimePickersPage({this.restorationId}) ;
  final String restorationId;
  @override
  _DateAndTimePickersPageState createState() => _DateAndTimePickersPageState();

  @override
  String get pageName => 'DateAndTimePickers日期时间选择器';
}

class _DateAndTimePickersPageState extends BasePageState<DateAndTimePickersPage> {

  DateTime _nowDate =DateTime.now();

  var _nowTime=TimeOfDay(hour: 12,minute:20 );

  _showDatePicker() async{

    //  showDatePicker(
    //    context:context,
    //    initialDate:_nowDate,
    //    firstDate:DateTime(1980),
    //    lastDate:DateTime(2100),
    //  ).then((result){
    //     print(result);
    //  });

    var result= await showDatePicker(
      context:context,
      initialDate:_nowDate,
      firstDate:DateTime(1980),
      lastDate:DateTime(2100),
      //  locale: Locale('zh'),      //非必须
    );

    //  print(result);

    setState(() {
      if(result!=null){
        this._nowDate= result;
      }
    });

  }

  _showTimePicker() async{

    var result= await showTimePicker(
        context:context,
        initialTime: _nowTime
    );
    setState(() {
      if(result!=null) {
        this._nowTime = result;
      }
    });

  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    //  var now =DateTime.now();
    // print(now);  //2019-06-08 12:08:10.140421

    // print(now.millisecondsSinceEpoch);  //1559967090998


    /// print(DateTime.fromMillisecondsSinceEpoch(1559967090998));  //2019-06-08 12:11:30.998

    // 2019-06-08   2019/06/08   2019年06月08

    // print(formatDate(DateTime.now(), [yyyy, '年', mm, '月', dd]));    //2019年06月08


  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                InkWell(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text("${formatDate(_nowDate, [yyyy, '年', mm, '月', dd])}"),
                      Icon(Icons.arrow_drop_down)
                    ],
                  ),
                  onTap: _showDatePicker,
                ),
                InkWell(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text("${_nowTime.format(context)}"),
                      Icon(Icons.arrow_drop_down)
                    ],
                  ),
                  onTap: _showTimePicker,
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}