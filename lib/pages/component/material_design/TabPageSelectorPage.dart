
import '../../../Include.dart';

//https://api.flutter.dev/flutter/material/TabPageSelector-class.html
class TabPageSelectorPage extends BasePage {

  static String dartPath = "lib/pages/component/material_design/TabPageSelectorPage.dart";

  @override
  _TabPageSelectorPageState createState() => _TabPageSelectorPageState();

  @override
  String get pageName => 'TabPageSelectorPage';
}

class _TabPageSelectorPageState extends BasePageState<TabPageSelectorPage>  with TickerProviderStateMixin {
  TabController _tabController;
  List _spList = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13"];
  @override
  void initState() {
    super.initState();
    _tabController = null;
    _tabController = TabController(initialIndex: 0, length: _spList.length, vsync: this); // 直接传this
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        child: TabPageSelector(
          controller: _tabController,
          indicatorSize: 5,
          color: Colours.splitLine_gray,
            selectedColor:Colours.blue,
        )
        ,
      ),
    );
  }

}