
import '../../../../Include.dart';

//https://api.flutter.dev/flutter/material/DropdownButton-class.html
class DropdownButtonPage extends BasePage {

  static String dartPath = "lib/pages/component/material_design/button/DropdownButtonPage.dart";

  @override
  _DropdownButtonPageState createState() => _DropdownButtonPageState();

  @override
  String get pageName => 'DropdownButton标题栏下拉按钮';
}

class _DropdownButtonPageState extends BasePageState<DropdownButtonPage> {
  String dropdownValue = 'One';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        child: DropdownButton<String>(
          value: dropdownValue,
          icon: const Icon(Icons.arrow_downward),
          iconSize: 24,
          elevation: 16,
          style: const TextStyle(
              color: Colors.deepPurple
          ),
          underline: Container(
            height: 2,
            color: Colors.deepPurpleAccent,
          ),
          onChanged: (String newValue) {
            setState(() {
              dropdownValue = newValue;
            });
          },
          items: <String>['One', 'Two', 'Free', 'Four']
              .map<DropdownMenuItem<String>>((String value) {
            return DropdownMenuItem<String>(
              value: value,
              child: Text(value),
            );
          })
              .toList(),
        ),
      ),
    );
  }


}