import 'package:flutter_web_2021/utils/toast/OkToastUtil.dart';

import '../../../../Include.dart';

class PopupMenuButtonPage extends BasePage {
  static String dartPath = "lib/pages/component/material_design/button/PopupMenuButtonPage.dart";
  @override
  _PopupMenuButtonPageState createState() => _PopupMenuButtonPageState();

  @override
  String get pageName => '标题栏弹出式菜单列表';
}


class _PopupMenuButtonPageState extends BasePageState<PopupMenuButtonPage> {
  //包含分块菜单的项
  Widget listTile2(){

      return     ListTile(
        title: Text("包含分块菜单的项"),
        trailing: PopupMenuButton<String>(
          padding: EdgeInsets.zero,
          onSelected: (value) {
            ToastUtil.showToastBottom(value);
          },
          itemBuilder: (context) => <PopupMenuEntry<String>>[
            PopupMenuItem<String>(
              value: "1-v",
              child: ListTile(
                leading: const Icon(Icons.visibility),
                title: Text(
                  "1",
                ),
              ),
            ),
            PopupMenuItem<String>(
              value: "2-v",
              child: ListTile(
                leading: const Icon(Icons.person_add),
                title: Text(
                  "2",
                ),
              ),
            ),
            PopupMenuItem<String>(
              value: "3-v",
              child: ListTile(
                leading: const Icon(Icons.link),
                title: Text(
                  "3",
                ),
              ),
            ),
            const PopupMenuDivider(),
            PopupMenuItem<String>(
              value: "4-v",
              child: ListTile(
                leading: const Icon(Icons.delete),
                title: Text(
                  "4",
                ),
              ),
            ),
          ],
        ),
      );

  }
  //包含上下文菜单的项
  Widget listTile1(){
    return  ListTile(
      title: Text("包含上下文菜单的项"),
      trailing: PopupMenuButton<String>(
        padding: EdgeInsets.zero,
        onSelected: (value) {
          ToastUtil.showToastBottom(value);
        },
        itemBuilder: (context) => <PopupMenuItem<String>>[
          PopupMenuItem<String>(
            value: "1-v",
            child: Text("1"),
          ),
          PopupMenuItem<String>(
            enabled: false,
            child: Text("2"),
          ),
          PopupMenuItem<String>(
            value: "3-v",
            child: Text(
              "3",
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.pageName),
        ),
        body: ListView(
          children: [
            listTile1(),
            listTile2(),
          ],
        ));
  }
}





