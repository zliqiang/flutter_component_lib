import '../../../../Include.dart';

//https://blog.csdn.net/llayjun/article/details/109747989
class ButtonBarPage extends BasePage {

  static String dartPath = "lib/pages/component/material_design/button/ButtonBarPage.dart";

  @override
  _ButtonBarPageState createState() => _ButtonBarPageState();

  @override
  String get pageName => 'ButtonBar水平排列按钮组-过时';
}

class _ButtonBarPageState extends BasePageState<ButtonBarPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Center(
        // 末端按钮对齐的容器
        child: ButtonBar(
          alignment: MainAxisAlignment.end,
          // child大小
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            RaisedButton(onPressed: null, child: Text('按钮1'),),
            RaisedButton(onPressed: null, child: Text('按钮2'),),
            RaisedButton(onPressed: null, child: Text('按钮3'),),
          ],
        ),
      ),
    );
  }

}