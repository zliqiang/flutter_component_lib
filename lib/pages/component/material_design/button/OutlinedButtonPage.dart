import '../../../../Include.dart';

//https://api.flutter.dev/flutter/material/OutlinedButton-class.html
class OutlinedButtonPage extends BasePage {

  static String dartPath = "lib/pages/component/material_design/button/OutlinedButtonPage.dart";

  @override
  _OutlinedButtonPageState createState() => _OutlinedButtonPageState();

  @override
  String get pageName => 'OutlinedButton边框按钮';
}

class _OutlinedButtonPageState extends BasePageState<OutlinedButtonPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        child: OutlinedButton(
          onPressed: () {
            print('Received click');
          },
          child: const Text('Click Me'),
        ),
      ),
    );
  }
}