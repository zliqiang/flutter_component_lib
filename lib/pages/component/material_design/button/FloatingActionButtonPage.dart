import '../../../../Include.dart';

//https://api.flutter.dev/flutter/material/FloatingActionButton-class.html
class FloatingActionButtonPage extends BasePage {

  static String dartPath = "lib/pages/component/material_design/button/FloatingActionButtonPage.dart";

  @override
  _FloatingActionButtonPageState createState() => _FloatingActionButtonPageState();

  @override
  String get pageName => 'FloatingActionButton浮动按钮';
}

class _FloatingActionButtonPageState extends BasePageState<FloatingActionButtonPage> {

  Widget build(BuildContext context) {



    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: const Center(
        child:  Scaffold(
          body: const Center(
              child: Text('Press the button below!')
          ),
          floatingActionButton: FloatingActionButton(
            child: const Icon(Icons.navigation),
            backgroundColor: Colors.green,
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.startFloat,
      floatingActionButton:
      FloatingActionButton.extended(

        onPressed: () {
          // Add your onPressed code here!
        },
        label: const Text('Approve'),
        icon: const Icon(Icons.thumb_up),
        backgroundColor: Colors.pink,
      ),
    );
  }

}