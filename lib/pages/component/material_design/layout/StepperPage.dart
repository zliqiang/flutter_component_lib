import '../../../../Include.dart';

//https://api.flutter.dev/flutter/material/Stepper-class.html
class StepperPage extends BasePage {

  static String dartPath = "lib/pages/component/material_design/layout/StepperPage.dart";

  @override
  _StepperPageState createState() => _StepperPageState();

  @override
  String get pageName => 'StepperPage';
}

class _StepperPageState extends BasePageState<StepperPage> {
  int _index = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        child: Stepper(
          currentStep: _index,
          onStepCancel: () {
            if (_index > 0) {
              setState(() { _index -= 1; });
            }
          },
          onStepContinue: () {
            if (_index <= 0) {
              setState(() { _index += 1; });
            }
          },
          onStepTapped: (int index) {
            setState(() { _index = index; });
          },
          steps: <Step>[
            Step(
              title: const Text('Step 1 title'),
              content: Container(
                  alignment: Alignment.centerLeft,
                  child: const Text('Content for Step 1')
              ),
            ),
            const Step(
              title: Text('Step 2 title'),
              content: Text('Content for Step 2'),
            ),
          ],
        ),
      ),
    );
  }

}