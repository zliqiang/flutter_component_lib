import '../../../../Include.dart';

//https://api.flutter.dev/flutter/material/Divider-class.html
class DividerPage extends BasePage {
  static String dartPath = "lib/pages/component/material_design/layout/DividerPage.dart";

  @override
  _DividerPageState createState() => _DividerPageState();

  @override
  String get pageName => 'Divider分割线';
}

class _DividerPageState extends BasePageState<DividerPage> {

  Widget getVerticalDivider(){
    return const VerticalDivider(
      width: 20,
      color: Colours.red,
      thickness: 2,
      indent: 5,
      endIndent: 5,
    );
  }
  Widget getDivider(){
    return  const Divider(
      height: 20,
      thickness: 5,
      indent: 20,
      endIndent: 20,
    );
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        child: Center(
          child: ListView(
            children: <Widget>[
              Row(
                children: [
                  Expanded(
                    child: Container(
                      padding: const EdgeInsets.all(20),
                      color: Colors.amber,
                      child: const Center(
                        child: Text('Above'),
                      ),
                    ),
                  ),
                  Container(
                    height: 40,
                    child: getVerticalDivider(),
                  ),
                  Expanded(
                    child: Container(
                      padding: const EdgeInsets.all(20),
                      color: Colors.amber,
                      child: const Center(
                        child: Text('Above'),
                      ),
                    ),
                  ),
                ],
              ),

              Expanded(
                child: Container(
                  padding: const EdgeInsets.all(20),
                  color: Colors.amber,
                  child: const Center(
                    child: Text('Above'),
                  ),
                ),
              ),
              getDivider(),
              // Subheader example from Material spec.
              // https://material.io/components/dividers#types
              Container(
                padding: const EdgeInsets.only(left: 20),
                child: Align(
                  alignment: AlignmentDirectional.centerStart,
                  child: Text(
                    'Subheader',
                    style: Theme.of(context).textTheme.caption,
                    textAlign: TextAlign.start,
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  padding: const EdgeInsets.all(20),
                  color: Theme.of(context).colorScheme.primary,
                  child: const Center(
                    child: Text('Below'),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
