import 'package:flutter_web_2021/Include.dart';

class ExpansionTilePage extends BasePage {
  static String dartPath = "lib/pages/component/material_design/ExpansionTilePage.dart";
  @override
  _ExpansionTilePageState createState() => _ExpansionTilePageState();

  @override
  String get pageName => 'ExpansionTilePage【扩展面板2】';
}

class _ExpansionTilePageState extends BasePageState<ExpansionTilePage> {

  List<bool> dataList = List.generate(20, (index) => false).toList();

  _buildExpansionTilelList() {
    return dataList.map((child) {
      return ExpansionTile(
        initiallyExpanded: false,
        backgroundColor: Colors.red,
        collapsedBackgroundColor: Colors.orange,
        title: Text('老孟'),
        onExpansionChanged: (bool value) {},
        children: [
          ListTile(
            tileColor: Colors.blue,
            title: Text('Camel'),
            subtitle: Text('Comes with humps'),
            onTap: () {
              print('camel');
            },
            enabled: false,
          ),
        ],
      );
    }).toList();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: SingleChildScrollView(
        child: Column(
          children: _buildExpansionTilelList(),
        ),
      ),
    );
  }

}