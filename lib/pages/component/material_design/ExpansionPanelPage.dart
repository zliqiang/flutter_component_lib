import 'package:flutter_web_2021/Include.dart';

class ExpansionPanelPage extends BasePage {
  static String dartPath = "lib/pages/component/material_design/ExpansionPanelPage.dart";
  @override
  _ExpansionPanelListPageState createState() => _ExpansionPanelListPageState();

  @override
  String get pageName => 'ExpansionPanel【扩展面板】';
}

class _ExpansionPanelListPageState extends BasePageState<ExpansionPanelPage> {




  List<bool> dataList = List.generate(20, (index) => false).toList();

  _buildExpansionPanelList() {
    return ExpansionPanelList(
      expansionCallback: (index, isExpanded) {
        setState(() {
          dataList[index] = !isExpanded;
        });
      },
      children: dataList.map((value) {
        return ExpansionPanel(
          isExpanded: value,
          headerBuilder: (context, isExpanded) {
            return ListTile(
              title: Text('老孟'),
            );
          },
          body: Container(
            height: 100,
            color: Colors.greenAccent,
          ),
        );
      }).toList(),
    );
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: SingleChildScrollView(
        child: Container(
          child: _buildExpansionPanelList(),
        ),
      ),
    );
  }
}
