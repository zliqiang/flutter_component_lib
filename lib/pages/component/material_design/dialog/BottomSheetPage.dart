import '../../../../Include.dart';

//https://api.flutter.dev/flutter/material/BottomSheet-class.html
class BottomSheetPage extends BasePage {

  static String dartPath = "lib/pages/component/material_design/dialog/BottomSheetPage.dart";

  @override
  _BottomSheetPageState createState() => _BottomSheetPageState();

  @override
  String get pageName => 'BottomSheet底部滑起';
}

class _BottomSheetPageState extends BasePageState<BottomSheetPage> {

  final _bottomSheetScaffoldKey = GlobalKey<ScaffoldState>();

  _openBottomSheet() {
    _bottomSheetScaffoldKey
        .currentState
        .showBottomSheet((BuildContext context) {
      return BottomAppBar(
        child: Container(
          height: 90.0,
          width: double.infinity,
          padding: EdgeInsets.all(16.0),
          child: Row(
            children: <Widget>[
              Icon(Icons.pause_circle_outline),
              SizedBox(width: 16.0,),
              Text('01:30 / 03:30'),
              Expanded(
                child: Text('从头再来-刘欢', textAlign: TextAlign.right,),
              ),
            ],
          ),
        ),
      );
    });
  }

  Future _openModalBottomSheet() async {
    final option = await showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return Container(
            height: 200.0,
            child: Column(
              children: <Widget>[
                ListTile(
                  title: Text('拍照',textAlign: TextAlign.center),
                  onTap: () {
                    Navigator.pop(context, '拍照');
                  },
                ),
                ListTile(
                  title: Text('从相册选择',textAlign: TextAlign.center),
                  onTap: () {
                    Navigator.pop(context, '从相册选择');
                  },
                ),
                ListTile(
                  title: Text('取消',textAlign: TextAlign.center),
                  onTap: () {
                    Navigator.pop(context, '取消');
                  },
                ),
              ],
            ),
          );
        }
    );

    print(option);
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _bottomSheetScaffoldKey,
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        padding: EdgeInsets.all(16.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  FlatButton(
                    child: Text('Open BottomSheet'),
                    onPressed: _openBottomSheet,
                  ),
                  FlatButton(
                    child: Text('Modal BottomSheet'),
                    onPressed: _openModalBottomSheet,
                  ),
                ]
            ),
          ],
        ),
      ),
    );
  }

}