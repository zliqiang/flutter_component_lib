import '../../../../Include.dart';

//https://api.flutter.dev/flutter/material/AlertDialog-class.html
class AlertDialogPage extends BasePage {

  static String dartPath = "lib/pages/component/material_design/dialog/AlertDialogPage.dart";

  @override
  _AlertDialogPageState createState() => _AlertDialogPageState();

  @override
  String get pageName => 'AlertDialog对话框';
}

class _AlertDialogPageState extends BasePageState<AlertDialogPage> {

  Future<void> _showMyDialog2() async {
    return showDialog<String>(
      context: context,
      builder: (BuildContext context) => AlertDialog(
        title: const Text('AlertDialog Tilte'),
        content: const Text('AlertDialog description'),
        actions: <Widget>[
          TextButton(
            onPressed: () => Navigator.pop(context, 'Cancel'),
            child: const Text('Cancel'),
          ),
          TextButton(
            onPressed: () => Navigator.pop(context, 'OK'),
            child: const Text('OK'),
          ),
        ],
      ),
    );
  }
  Future<void> _showMyDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('AlertDialog Title'),
          content: SingleChildScrollView(
            child: ListBody(
              children: const <Widget>[
                Text('This is a demo alert dialog.'),
                Text('Would you like to approve of this message?'),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: const Text('Approve'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        alignment: Alignment.center,
        child:Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            InkWell(
              child:Text("AlertDialog") ,
              onTap: (){
                _showMyDialog();
              },
            ),
            SizedBox(height: 20,),
            InkWell(
              child:Text("AlertDialog2") ,
              onTap: (){
                _showMyDialog2();
              },
            ),
          ],

        )

      ),
    );
  }

}