import '../../../../Include.dart';

//https://api.flutter.dev/flutter/material/SimpleDialog-class.html
class SimpleDialogPage extends BasePage {

  static String dartPath = "lib/pages/component/material_design/dialog/SimpleDialogPage.dart";

  @override
  _SimpleDialogPageState createState() => _SimpleDialogPageState();

  @override
  String get pageName => 'SimpleDialog简单对话框';
}

class _SimpleDialogPageState extends BasePageState<SimpleDialogPage> {

  Future<void> _showDialog(){
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return SimpleDialog(
          backgroundColor:Colors.deepOrange,
          title: const Text('对话框标题',textAlign:TextAlign.center,style: TextStyle(color: Colors.white),),
          children: [
            SimpleDialogOption(
              onPressed: () {
                print('第一行信息');
                // 使用showDialog后，手动关闭当前dialog，其实原因很简单，因为dialog其实是另一个页面，准确地来说是另一个路由，
                // 因此dialog的关闭也是通过navigator来pop的，所以它的地位跟你当前主页面一样。
                Navigator.pop(context);
              },
              child: const Text('第一行信息'),
            ),
            SimpleDialogOption(
              onPressed: () {
                print('第二行信息');
                Navigator.pop(context);
              },
              child: const Text('第二行信息'),
            ),
          ],
        );
      },
    );
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        child: new Center(
          child:  new RaisedButton(
              onPressed: _showDialog,
              child: new Text("SimpleDialog"),
              color: Colors.blue,
              highlightColor: Colors.lightBlueAccent,
              disabledColor: Colors.lightBlueAccent),

        ),
      ),
    );
  }

}