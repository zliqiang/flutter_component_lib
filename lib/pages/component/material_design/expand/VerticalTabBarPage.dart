
import 'package:flutter_web_2021/widgets/tab/vertical/tab_style.dart';

import '../../../../Include.dart';

class VerticalTabBarPage extends BasePage {
  static String dartPath = "lib/pages/component/material_design/expand/VerticalTabBarPage.dart";
  @override
  _VerticalTabBarPageState createState() => _VerticalTabBarPageState();

  @override
  String get pageName => 'VerticalTabBarPage';
}

class _VerticalTabBarPageState extends BasePageState<VerticalTabBarPage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(''),
      ),
      body: Container(
        child: DefaultTabController(
          length: 15,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[

              Expanded(
                  flex: 5,
                  child: TabBarView(
                    //physics: NeverScrollableScrollPhysics(), //禁止滑动
                    children: List.generate(
                        15,
                            (v) => SingleChildScrollView(
                          child: Column(
                            children: <Widget>[
                              Center(
                                child: new Text(
                                  "asdjfh${v.toString()}",
                                  style: TextStyle(color: Colors.black, fontSize: 22),
                                ),
                              ),
                              Center(
                                child: new Text(
                                  "345643${v.toString()}",
                                  style: TextStyle(color: Colors.black, fontSize: 22),
                                ),
                              ),
                            ],
                          ),
                        )).toList(),
                  )),
              Expanded(
                flex: 1,
                child: VTabBar(
                  tabs: List.generate(
                      15, (t) => Tab(child: Text('tab$t'))).toList(),
                  isScrollable: true,
                  labelColor: Colors.red,
                  unselectedLabelColor: Color(0xff666666),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

}