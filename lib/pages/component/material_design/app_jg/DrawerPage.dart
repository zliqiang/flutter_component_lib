import '../../../../Include.dart';

class DrawerPage extends BasePage {
  static String dartPath = "lib/pages/component/material_design/app_jg/DrawerPage.dart";
  @override
  _DrawerPageState createState() => _DrawerPageState();

  @override
  String get pageName => '侧边栏';
}

class _DrawerPageState extends BasePageState<DrawerPage> {

  @override
  Widget build(BuildContext context) {
    final drawerHeader = UserAccountsDrawerHeader(
      accountName: Text(
        "用户名",
      ),
      accountEmail: Text(
        "zhouliqiangok@163.com",
      ),
      currentAccountPicture: const CircleAvatar(
        child: FlutterLogo(size: 42.0),
      ),
    );
    final drawerItems = ListView(
      children: [
        drawerHeader,
        ListTile(
          title: Text(
            "第一个项目",
          ),
          leading: const Icon(Icons.favorite),
          onTap: () {
            Navigator.pop(context);
          },
        ),
        ListTile(
          title: Text(
            "第二个项目",
          ),
          leading: const Icon(Icons.comment),
          onTap: () {
            Navigator.pop(context);
          },
        ),
      ],
    );
    return Scaffold(
      appBar: AppBar(
        title: Text(
          widget.pageName,
        ),
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(50.0),
          child: Text(
            "从边缘滑动或点按左上角的图标即可查看抽屉式导航栏",
          ),
        ),
      ),
      drawer: Drawer(
        child: drawerItems,
      ),
    );
  }
}



