import '../../../../Include.dart';

class AppbarPage extends BasePage {
  static String dartPath = "lib/pages/component/material_design/app_jg/AppbarPage.dart";
  @override
  _AppbarPageState createState() => _AppbarPageState();

  @override
  String get pageName => 'AppbarPage';
}

class _AppbarPageState extends BasePageState<AppbarPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
          icon: const Icon(Icons.menu),
          onPressed: () {},
        ),
        title: Text(
          "App Bar",
        ),
        actions: [
          IconButton(
            tooltip: "收藏",
            icon: const Icon(
              Icons.favorite,
            ),
            onPressed: () {},
          ),
          IconButton(
            tooltip: "搜索",
            icon: const Icon(
              Icons.search,
            ),
            onPressed: () {},
          ),
          PopupMenuButton<Text>(
            itemBuilder: (context) {
              return [
                PopupMenuItem(
                  child: Text(
                    "1",
                  ),
                ),
                PopupMenuItem(
                  child: Text(
                    "2",
                  ),
                ),
                PopupMenuItem(
                  child: Text(
                    "3",
                  ),
                ),
              ];
            },
          )
        ],
      ),
      body: Center(
        child: Text(
          "首页",
        ),
      ),
    );
  }
}
