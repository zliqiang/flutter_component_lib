import '../../../Include.dart';

//https://api.flutter.dev/flutter/widgets/Opacity-class.html
class OpacityPage extends BasePage {

  static String dartPath = "lib/pages/component/painting_and_effects/OpacityPage.dart";

  @override
  _OpacityPageState createState() => _OpacityPageState();

  @override
  String get pageName => 'Opacity透明';
}

class _OpacityPageState extends BasePageState<OpacityPage> {
 bool _visible=true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: ListView(
        children: [
          Opacity(
            opacity: _visible ? 0.3 : 0.0,
            child: const Text("Now you see me, now you don't!"),
          ),
          SizedBox(height: 80,),
          Opacity(
            opacity: _visible ? 1.0 : 0.0,
            child:  Image.network(
                'https://raw.githubusercontent.com/flutter/assets-for-api-docs/master/packages/diagrams/assets/blend_mode_destination.jpeg',
                color: const Color.fromRGBO(255, 255, 255, 0.5),
                colorBlendMode: BlendMode.modulate
            ),
          ),


        ],
      ),
    );
  }

}