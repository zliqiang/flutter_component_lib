import '../../../Include.dart';

//https://api.flutter.dev/flutter/widgets/ClipOval-class.html
class ClipOvalPage extends BasePage {

  static String dartPath = "lib/pages/component/painting_and_effects/ClipOvalPage.dart";

  @override
  _ClipOvalPageState createState() => _ClipOvalPageState();

  @override
  String get pageName => 'ClipOval椭圆剪辑';
}

class _ClipOvalPageState extends BasePageState<ClipOvalPage> {
  String imageUrl =
      "https://flutter.github.io/assets-for-api-docs/assets/widgets/owl-2.jpg";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        width: 100,
        height: 100,
        ///裁剪组件
        child: ClipOval(
          ///一个图片
          child: new Image.network(
              imageUrl, //图片地址
              ///填充
              fit: BoxFit.fill),
        ),
      ),
    );
  }

}