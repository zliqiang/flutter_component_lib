import '../../../Include.dart';

//https://api.flutter.dev/flutter/widgets/RotatedBox-class.html
class RotatedBoxPage extends BasePage {

  static String dartPath = "lib/pages/component/painting_and_effects/RotatedBoxPage.dart";

  @override
  _RotatedBoxPageState createState() => _RotatedBoxPageState();

  @override
  String get pageName => 'RotatedBox旋转';
}

class _RotatedBoxPageState extends BasePageState<RotatedBoxPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        child: const RotatedBox(
          quarterTurns: 3,
          child: Text('Hello World!'),
        ),
      ),
    );
  }

}