import '../../../Include.dart';
//https://api.flutter.dev/flutter/widgets/ClipRect-class.html
class ClipRectPage extends BasePage {

  static String dartPath = "lib/pages/component/painting_and_effects/ClipRectPage.dart";

  @override
  _ClipRectPageState createState() => _ClipRectPageState();

  @override
  String get pageName => 'ClipRect矩形剪辑';
}

class _ClipRectPageState extends BasePageState<ClipRectPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        child: ClipRect(
          child: Align(
            alignment: Alignment.topCenter,
            heightFactor: 0.5,
            child: Image.network("https://flutter.github.io/assets-for-api-docs/assets/widgets/owl-2.jpg"),
          ),
        ),
      ),
    );
  }

}