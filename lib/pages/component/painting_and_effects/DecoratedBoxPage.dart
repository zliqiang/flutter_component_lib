import '../../../Include.dart';

//https://api.flutter.dev/flutter/widgets/DecoratedBox-class.html
class DecoratedBoxPage extends BasePage {
  static String dartPath = "lib/pages/component/painting_and_effects/DecoratedBoxPage.dart";

  @override
  _DecoratedBoxPageState createState() => _DecoratedBoxPageState();

  @override
  String get pageName => 'DecoratedBox装饰';
}

class _DecoratedBoxPageState extends BasePageState<DecoratedBoxPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: ListView(children: [
        DecoratedBox(
          decoration: BoxDecoration(
            gradient: RadialGradient(
              center: Alignment(-0.5, -0.6),
              radius: 0.15,
              colors: <Color>[
                Color(0xFFEEEEEE),
                Color(0xFF111133),
              ],
              stops: <double>[0.9, 1.0],
            ),
          ),
          child: Container(
            height: 200.0,
          ),
        ),
        Container(
          margin: EdgeInsets.all(20.0),
          child: DecoratedBox(
            // decoration
            decoration: BoxDecoration(
                gradient: LinearGradient(colors: [Colors.redAccent, Colors.blue]),
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
                boxShadow: [
                  // 数组
                  BoxShadow(
                      color: Colors.black45, // 阴影颜色
                      offset: Offset(20.0, 80.0), // 阴影与容器的距离, x轴,y轴
                      blurRadius: 6.0),
                  // // 模糊程度
                ]),
            // the decorated container.
            child: Container(
              height: 200.0,
            ),
          ),
        ),
        SizedBox(height: 80,),

        DecoratedBox(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    colors: [Colors.orange, Colors.blue[700]]), //背景渐变
                borderRadius: BorderRadius.circular(13.0), //3像素圆角
                boxShadow: [
                  //阴影
                  BoxShadow(
                      color: Colors.orange,
                      offset: Offset(6.0, 2.0),
                      blurRadius: 14.0)
                ]),
            child: Padding(
              padding:
              EdgeInsets.symmetric(horizontal: 80.0, vertical: 18.0),
              child: Text(
                "decoration",
                style: TextStyle(color: Colors.white),
              ),
            )
        ),
        DecoratedBox(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    colors: [Colors.red, Colors.orange[700]]), //背景渐变
                borderRadius: BorderRadius.circular(3.0), //3像素圆角
                boxShadow: [
                  //阴影
                  BoxShadow(
                      color: Colors.black54,
                      offset: Offset(2.0, 2.0),
                      blurRadius: 4.0)
                ]),
            position: DecorationPosition.foreground, // background背景装饰，foreground前景装饰
            child: Padding(
              padding:
              EdgeInsets.symmetric(horizontal: 80.0, vertical: 18.0),
              child: Text(
                "Login",
                style: TextStyle(color: Colors.white),
              ),
            )
        ),
        DecoratedBox(
            decoration: BoxDecoration(
                color: Colors.green,
//                    gradient: LinearGradient(
//                        colors: [Colors.red, Colors.blue[700]]), //背景渐变 会覆盖color
                borderRadius: BorderRadius.circular(13.0), //3像素圆角
                border: Border.all(color: Colors.blue, width: 5, style: BorderStyle.solid),
                boxShadow: [
                  //阴影
                  BoxShadow(
                      color: Colors.orange,
                      offset: Offset(6.0, 2.0),
                      blurRadius: 14.0)
                ]),
            child: Padding(
              padding:
              EdgeInsets.symmetric(horizontal: 80.0, vertical: 18.0),
              child: Text(
                "border",
                style: TextStyle(color: Colors.white),
              ),
            )
        ),
        DecoratedBox(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                  colors: [Colors.green, Colors.blue[700]]),
              //背景渐变 会覆盖color
              borderRadius: BorderRadius.circular(13.0),
              //3像素圆角
              border: Border.all(color: Colors.blue, width: 5, style: BorderStyle.solid),
              backgroundBlendMode: BlendMode.srcATop,
              boxShadow: [
                //阴影
                BoxShadow(
                    color: Colors.red,
                    offset: Offset(6.0, 2.0),
                    blurRadius: 14.0),
                BoxShadow(
                    color: Colors.orange,
                    offset: Offset(-6.0, -2.0),
                    blurRadius: 14.0),
              ],
            ),
            child: Padding(
              padding:
              EdgeInsets.symmetric(horizontal: 80.0, vertical: 18.0),
              child: Text(
                "BoxShadow",
                style: TextStyle(color: Colors.white),
              ),
            )
        ),
        DecoratedBox(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                  colors: [Colors.green, Colors.blue[700]]), //背景渐变 会覆盖color
//                  backgroundBlendMode: BlendMode.srcATop,
              boxShadow: [
                //阴影
                BoxShadow(
                    color: Colors.red,
                    offset: Offset(6.0, 2.0),
                    blurRadius: 14.0),
                BoxShadow(
                    color: Colors.orange,
                    offset: Offset(-6.0, -2.0),
                    blurRadius: 14.0),
              ],
              shape: BoxShape.circle, // 圆形
//                    shape:BoxShape.rectangle,// 矩形
            ),
            child: Padding(
              padding:
              EdgeInsets.symmetric(horizontal: 80.0, vertical: 18.0),
              child: Text(
                "shape",
                style: TextStyle(color: Colors.white),
              ),
            )
        ),
      ]),
    );
  }
}
