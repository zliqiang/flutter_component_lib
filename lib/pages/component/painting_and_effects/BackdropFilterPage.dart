import '../../../Include.dart';
import 'dart:ui' as ui;

//https://api.flutter.dev/flutter/widgets/BackdropFilter-class.html
class BackdropFilterPage extends BasePage {

  static String dartPath = "lib/pages/component/painting_and_effects/BackdropFilterPage.dart";

  @override
  _BackdropFilterPageState createState() => _BackdropFilterPageState();

  @override
  String get pageName => 'BackdropFilter高斯模糊';
}

class _BackdropFilterPageState extends BasePageState<BackdropFilterPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        child: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Text('0' * 10000),
            Center(
              child: ClipRect(  // <-- clips to the 200x200 [Container] below
                child: BackdropFilter(
                  filter: ui.ImageFilter.blur(
                    sigmaX: 5.0,
                    sigmaY: 5.0,
                  ),
                  child: Container(
                    alignment: Alignment.center,
                    width: 200.0,
                    height: 200.0,
                    child: const Text('Hello World'),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

}