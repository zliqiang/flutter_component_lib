import '../../../Include.dart';

//https://api.flutter.dev/flutter/widgets/FractionalTranslation-class.html
class FractionalTranslationPage extends BasePage {

  static String dartPath = "lib/pages/component/painting_and_effects/FractionalTranslationPage.dart";

  @override
  _FractionalTranslationPageState createState() => _FractionalTranslationPageState();

  @override
  String get pageName => 'FractionalTranslation平移变换';
}

class _FractionalTranslationPageState extends BasePageState<FractionalTranslationPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        height: 200,
        width: 200,
        color: Colors.blue,
        child: FractionalTranslation(
          //根据Offset平移控件，比如设置Offset的dx为0.25，那么在水平方向上平移控件1/4的宽度。
          translation: Offset(0.25,.2),
          child: Container(
            color: Colors.red,
          ),
        ),
      ),
    );
  }

}