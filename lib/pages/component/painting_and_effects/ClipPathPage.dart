import '../../../Include.dart';

class ClipPathPage extends BasePage {

  static String dartPath = "lib/pages/component/painting_and_effects/ClipPathPage.dart";

  @override
  _ClipPathPageState createState() => _ClipPathPageState();

  @override
  String get pageName => 'ClipPath路径剪辑';
}

class _ClipPathPageState extends BasePageState<ClipPathPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        child: Center(
          child:Column(
            children: <Widget>[
              ClipPath(
                clipper:BottomClipper() ,  //裁切路径
                child: Container(
                    height: 15,
                    width: 15,
                    color:Colors.indigoAccent
                ),
              )
            ],
          ),
        )
      ),
    );
  }

}

class BottomClipper extends CustomClipper<Path> {

  Path getClip(Size size) {
    var path = Path();

    // path.lineTo(0, 0);
    // path.lineTo(0,size.height-30);

    // var firstPoint=Offset(size.width/2,size.height);

    // var endPoint=Offset(size.width,size.height-30);

    // path.quadraticBezierTo(firstPoint.dx, firstPoint.dy, endPoint.dx, endPoint.dy);

    // path.lineTo(size.width, 0);

    path.lineTo(0, 0);
    path.lineTo(0, size.height - 40);

    var firstPoint = Offset(size.width / 4, size.height);
    var secondPoint = Offset(size.width / 2.25, size.height - 30);
    path.quadraticBezierTo(firstPoint.dx, firstPoint.dy, secondPoint.dx, secondPoint.dy);

    var thirdPoint = Offset(size.width / 4 * 3, size.height - 90);
    var fourthPoint = Offset(size.width, size.height - 40);
    path.quadraticBezierTo(thirdPoint.dx, thirdPoint.dy, fourthPoint.dx, fourthPoint.dy);

    path.lineTo(size.width, 0);
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    // TODO: implement shouldReclip
    return false;
  }
}