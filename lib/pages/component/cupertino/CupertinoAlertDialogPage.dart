import 'package:flutter/cupertino.dart';

import '../../../Include.dart';

//https://api.flutter.dev/flutter/cupertino/CupertinoAlertDialog-class.html
class CupertinoAlertDialogPage extends BasePage {

  static String dartPath = "lib/pages/component/cupertino/CupertinoAlertDialogPage.dart";

  @override
  _CupertinoAlertDialogPageState createState() => _CupertinoAlertDialogPageState();

  @override
  String get pageName => 'CupertinoAlertDialog对话框';
}

class _CupertinoAlertDialogPageState extends BasePageState<CupertinoAlertDialogPage> {

  void _showDialog(widgetContext) {
    showCupertinoDialog(
      context: widgetContext,
      builder: (context) {
        return CupertinoAlertDialog(
          title: Text('确认删除'),
          actions: [
            CupertinoDialogAction(
              child: Text('确认'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            CupertinoDialogAction(
              child: Text('取消'),
              isDestructiveAction: true,
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        child: Center(
          child: RaisedButton(
            onPressed: () {
              _showDialog(context);
            },
            child: Text('点击显示弹窗'),
          ),
        ),
      ),
    );
  }

}