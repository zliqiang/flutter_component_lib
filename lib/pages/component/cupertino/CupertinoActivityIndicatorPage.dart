import 'package:flutter/cupertino.dart';

import '../../../Include.dart';

//https://api.flutter.dev/flutter/cupertino/CupertinoActivityIndicator-class.html
class CupertinoActivityIndicatorPage extends BasePage {
  static String dartPath = "lib/pages/component/cupertino/CupertinoActivityIndicatorPage.dart";

  @override
  _CupertinoActivityIndicatorPageState createState() => _CupertinoActivityIndicatorPageState();

  @override
  String get pageName => 'CupertinoActivityIndicator加载指示器';
}

class _CupertinoActivityIndicatorPageState extends BasePageState<CupertinoActivityIndicatorPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        alignment: Alignment.center,
        child: CupertinoActivityIndicator(radius: 15,),
      ),
    );
  }
}
