import 'package:flutter/cupertino.dart';

import '../../../Include.dart';

//https://api.flutter.dev/flutter/cupertino/CupertinoButton-class.html
class CupertinoButtonPage extends BasePage {
  static String dartPath = "lib/pages/component/cupertino/CupertinoButtonPage.dart";

  @override
  _CupertinoButtonState createState() => _CupertinoButtonState();

  @override
  String get pageName => 'CupertinoButton按钮';
}

class _CupertinoButtonState extends BasePageState<CupertinoButtonPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        child: Center(
          child: CupertinoButton(
            child: Text('ios 风格按钮',),
            onPressed: () {},
            color: Colors.blue,
            pressedOpacity: 0.5,
          ),
        ),
      ),
    );
  }
}
