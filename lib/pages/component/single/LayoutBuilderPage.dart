import '../../../Include.dart';

class LayoutBuilderPage extends BasePage {
  static String dartPath = "lib/pages/component/single/LayoutBuilderPage.dart";
  @override
  _LayoutBuilderPageState createState() => _LayoutBuilderPageState();

  @override
  String get pageName => 'LayoutBuilderPage【根据组件的大小确认组件的外观】';
}

class _LayoutBuilderPageState extends BasePageState<LayoutBuilderPage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.pageName),
      ),
      body: Container(
        child: LayoutBuilder(
          builder: (BuildContext context, BoxConstraints constraints) {
            var color = Colors.red;
            if (constraints.maxHeight > 100) {
              color = Colors.blue;
            }
            return Container(
              height: 50,
              width: 50,
              color: color,
            );
          },
        )
        ,
      ),
    );
  }

}