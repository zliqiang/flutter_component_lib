import '../../../Include.dart';

class ListBodyPage extends BasePage {
  static String dartPath = "lib/pages/component/single/ListBodyPage.dart";
  @override
  _ListBodyPageState createState() => _ListBodyPageState();

  @override
  String get pageName => 'ListBodyPage【定轴顺序排列子组件的组件】';
}

class _ListBodyPageState extends BasePageState<ListBodyPage> {


  //http://laomengit.com/flutter/widgets/Column.html
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.pageName),
      ),
      body: Column(

        children: [

          Row(
            children: <Widget>[
              Container(
                height: 50,
                width: 100,
                color: Colors.red,
              ),
              Container(
                height: 50,
                width: 100,
                color: Colors.green,
              ),
              Container(
                height: 50,
                width: 100,
                color: Colors.blue,
              ),
            ],
          ),

          SingleChildScrollView(
            child: ListBody(
              mainAxis: Axis.vertical,
              reverse: false,
              children: <Widget>[
                Container(
                  height: 45,
                  color: Colors.primaries[0],
                ),
                Container(
                  height: 45,
                  color: Colors.primaries[1],
                ),
                Container(
                  height: 45,
                  color: Colors.primaries[2],
                ),
              ],
            ),
          )

        ],
      )

     ,
    );
  }

}