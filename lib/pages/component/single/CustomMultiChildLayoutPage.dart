import '../../../Include.dart';

class CustomMultiChildLayoutPage extends BasePage {
  static String dartPath = "lib/pages/component/single/CustomMultiChildLayoutPage.dart";
  @override
  _CustomMultiChildLayoutPageState createState() => _CustomMultiChildLayoutPageState();

  @override
  String get pageName => 'CustomMultiChildLayoutPage【自定义子组件布局约束】';
}

class _CustomMultiChildLayoutPageState extends BasePageState<CustomMultiChildLayoutPage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.pageName),
      ),
      body: Container(
        width: 200,
        height: 200,
        color: Colors.red,
        child: CustomMultiChildLayout(
          delegate: FollowTheLeader(),
          children: <Widget>[
            LayoutId(
              id: FollowTheLeaderId.leader,
              child: Text('老孟'),
            ),
            LayoutId(
              id: FollowTheLeaderId.follower,
              child: Text('专注分享Flutter'),
            ),
          ],
        ),
      ),
    );
  }

}




enum FollowTheLeaderId { leader, follower }

class FollowTheLeader extends MultiChildLayoutDelegate {
  @override
  void performLayout(Size size) {
    Size leaderSize = Size.zero;
    if (hasChild(FollowTheLeaderId.leader)) {
      leaderSize =
          layoutChild(FollowTheLeaderId.leader, BoxConstraints.loose(size));
      //Offset 设置偏移量
      positionChild(FollowTheLeaderId.leader, Offset.zero);
    }

    if (hasChild(FollowTheLeaderId.follower)) {
      Size followerSize = layoutChild(FollowTheLeaderId.follower, BoxConstraints.loose(size));
      //Offset 设置偏移量
      positionChild(
          FollowTheLeaderId.follower,
          Offset(
              size.width - followerSize.width, size.height - followerSize.height));
    }
  }

  @override
  bool shouldRelayout(MultiChildLayoutDelegate oldDelegate) => false;
}