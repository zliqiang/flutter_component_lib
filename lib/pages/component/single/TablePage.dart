import '../../../Include.dart';

class TablePage extends BasePage {
  static String dartPath = "lib/pages/component/single/TablePage.dart";
  @override
  _TablePageState createState() => _TablePageState();

  @override
  String get pageName => 'TablePage【表格】';
}

class _TablePageState extends BasePageState<TablePage> {


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.pageName),
      ),
      body: Table(

        defaultColumnWidth: FixedColumnWidth(100),
          border: TableBorder(
            top: BorderSide(color: Colors.red),
            left: BorderSide(color: Colors.red),
            right: BorderSide(color: Colors.red),
            bottom: BorderSide(color: Colors.red),
            horizontalInside: BorderSide(color: Colors.red),
            verticalInside: BorderSide(color: Colors.green),
          ),

        children: [
          TableRow(
              decoration: ShapeDecoration(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30)),
                  color: Colors.blue),
              children: [
                TableCell(child: Text('姓名')),
                TableCell(child: Text('性别')),
                TableCell(child: Text('年龄')),
              ]
          ),
          TableRow(
              children: [
                TableCell(child: Text('老孟')),
                TableCell(child: Text('男')),
                TableCell(child: Text('18')),
              ]
          ),
          TableRow(
              children: [
                TableCell(child: Text('小红')),
                TableCell(child: Text('女')),
                TableCell(child: Text('18')),
              ]
          ),
        ],
      )
      ,
    );
  }

}