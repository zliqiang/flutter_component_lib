import '../../../Include.dart';

//https://api.flutter.dev/flutter/widgets/FadeTransition-class.html
class FadeTransitionPage extends BasePage {
  static String dartPath = "lib/pages/component/animated/FadeTransitionPage.dart";

  @override
  _FadeTransitionPageState createState() => _FadeTransitionPageState();

  @override
  String get pageName => 'FadeTransition透明度动画';
}

class _FadeTransitionPageState extends BasePageState<FadeTransitionPage> with SingleTickerProviderStateMixin{
  AnimationController _controller;
  Animation<double> _animation;

  @override
  void initState() {
    super.initState();
     _controller = AnimationController(
      duration: const Duration(seconds: 2),
      vsync: this,
    )..repeat(reverse: true);
    _animation = CurvedAnimation(
      parent: _controller,
      curve: Curves.easeIn,
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        alignment: Alignment.center,
        color: Colors.white,
        child: FadeTransition(
          opacity: _animation,
          child: const Padding(padding: EdgeInsets.all(8), child: FlutterLogo()),
        ),
      ),
    );
  }
}
