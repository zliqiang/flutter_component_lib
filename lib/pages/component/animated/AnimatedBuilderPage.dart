import '../../../Include.dart';
import 'dart:math' as math;

//https://api.flutter.dev/flutter/widgets/AnimatedBuilder-class.html
class AnimatedBuilderPage extends BasePage {
  static String dartPath = "lib/pages/component/animated/AnimatedBuilderPage.dart";

  @override
  _AnimatedBuilderPageState createState() => _AnimatedBuilderPageState();

  @override
  String get pageName => 'AnimatedBuilder单独动画效果';
}

class _AnimatedBuilderPageState extends BasePageState<AnimatedBuilderPage> with TickerProviderStateMixin {
  AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      duration: const Duration(seconds: 1),
      vsync: this,
    )..repeat();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Center(
        child: AnimatedBuilder(
          animation: _controller,
          child: Container(
            width: 200.0,
            height: 200.0,
            color: Colors.green,
            child: const Center(
              child: Text('Whee!'),
            ),
          ),
          builder: (BuildContext context, Widget child) {
            return Transform.rotate(
              angle: _controller.value * 2.0 * math.pi,
              child: child,
            );
          },
        ),
      ),
    );
  }
}
