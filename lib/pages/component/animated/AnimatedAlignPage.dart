import '../../../Include.dart';

class AnimatedAlignPage extends BasePage {
  static String dartPath = "lib/pages/component/animated/AnimatedAlignPage.dart";

  @override
  _AnimatedAlignPageState createState() => _AnimatedAlignPageState();

  @override
  String get pageName => 'AnimatedAlignPage【动画对齐】';
}

class _AnimatedAlignPageState extends BasePageState<AnimatedAlignPage> {
  bool selected = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.pageName),
      ),
      body: Container(
        child: GestureDetector(
          onTap: () {
            setState(() {
              selected = !selected;
            });
          },
          child: Center(
            child: Container(
              width: 250.0,
              height: 250.0,
              color: Colors.red,
              child: AnimatedAlign(
                alignment: selected ? Alignment.topRight : Alignment.bottomLeft,
                duration: const Duration(seconds: 1),
                curve: Curves.fastOutSlowIn,
                child: const FlutterLogo(size: 50.0),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
