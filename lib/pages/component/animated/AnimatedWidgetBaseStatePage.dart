import '../../../Include.dart';

//https://api.flutter.dev/flutter/widgets/AnimatedWidgetBaseState-class.html
class AnimatedWidgetBaseStatePage extends BasePage {

  static String dartPath = "lib/pages/component/animated/AnimatedWidgetBaseStatePage.dart";

  @override
  _AnimatedWidgetBaseStatePageState createState() => _AnimatedWidgetBaseStatePageState();

  @override
  String get pageName => 'AnimatedWidgetBaseState动画基类';
}

class _AnimatedWidgetBaseStatePageState extends BasePageState<AnimatedWidgetBaseStatePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        alignment: Alignment.center,
        child: MyAnimatedWidget(
          curve: Curves.easeIn,
          duration: const Duration(seconds: 1),
          param: 1.0,
        ),
      ),
    );
  }

}


class MyAnimatedWidget extends ImplicitlyAnimatedWidget {
  MyAnimatedWidget({
    Key key,
    this.param, //导致动画的参数
    Curve curve = Curves.linear,
    @required Duration duration,
  }) :super(key: key, curve: curve, duration: duration);
  final double param;

  @override
  _MyAnimatedWidgetState createState() => _MyAnimatedWidgetState();
}

class _MyAnimatedWidgetState extends AnimatedWidgetBaseState<MyAnimatedWidget> {
  Tween<double> _param; //State内部保存的当前状态信息，类型为Tween

  @override
  void forEachTween(TweenVisitor<dynamic> visitor) {
    _param = visitor(_param, widget.param, (value) => Tween<double>(begin: value));
    //widget.param表示来自新Widget的参数，也就是动画的结尾
    //意义即为，根据当前状态与新Widget的参数，重新生成动画
    //可以放置多个参数
    //本文原始链接：zhuanlan.zhihu.com/p/52572411，遇到抄袭欢迎举报
    //默认的Tween都是线性变化的，如果想引入新的逻辑（比如参数互相影响），把默认的visitor无视掉自己实现一个
  }

  @override
  Widget build(BuildContext context) {
    //return a widget built on a parameter
    return Container(
      child: Text("没搞懂"),
    );
  }
}
