import 'package:flutter_web_2021/Include.dart';

//https://api.flutter.dev/flutter/widgets/SlideTransition-class.html
class SlideTransitionPage extends BasePage {

  static String dartPath = "lib/pages/component/animated/SlideTransitionPage.dart:5";

  @override
  _SlideTransitionPageState createState() => _SlideTransitionPageState();

  @override
  String get pageName => 'SlideTransition平移动画';
}

class _SlideTransitionPageState extends BasePageState<SlideTransitionPage>with SingleTickerProviderStateMixin {
  AnimationController _controller;
   Animation<Offset> _offsetAnimation;
  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      duration: const Duration(seconds: 2),
      vsync: this,
    )..repeat(reverse: true);

    _offsetAnimation = Tween<Offset>(
      begin: Offset.zero,
      end: const Offset(1.5, 0.0),
    ).animate(CurvedAnimation(
      parent: _controller,
      curve: Curves.elasticIn,
    ));
  }
  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        alignment: Alignment.center,
        child: SlideTransition(
          position: _offsetAnimation,
          child: const Padding(
            padding: EdgeInsets.all(8.0),
            child: FlutterLogo(size: 150.0),
          ),
        ),
      ),
    );
  }

}