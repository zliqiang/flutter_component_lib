import '../../../Include.dart';

//https://api.flutter.dev/flutter/widgets/RotationTransition-class.html
class RotationTransitionPage extends BasePage {

  static String dartPath = "lib/pages/component/animated/RotationTransitionPage.dart";

  @override
  _RotationTransitionPageState createState() => _RotationTransitionPageState();

  @override
  String get pageName => 'RotationTransition旋转子控件动画';
}

class _RotationTransitionPageState extends BasePageState<RotationTransitionPage>  with TickerProviderStateMixin {
  AnimationController _controller;
  Animation<double> _animation;
  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      duration: const Duration(seconds: 2),
      vsync: this,
    )..repeat(reverse: true);

    _animation = CurvedAnimation(
      parent: _controller,
      curve: Curves.elasticOut,
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        child:  Center(
          child: RotationTransition(
            turns: _animation,
            child: const Padding(
              padding: EdgeInsets.all(8.0),
              child: FlutterLogo(size: 150.0),
            ),
          ),
        ),
      ),
    );
  }

}