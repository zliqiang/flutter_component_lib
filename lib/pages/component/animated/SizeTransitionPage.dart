import '../../../Include.dart';

//https://api.flutter.dev/flutter/widgets/SizeTransition-class.html
class SizeTransitionPage extends BasePage {

  static String dartPath = "lib/pages/component/animated/SizeTransitionPage.dart";

  @override
  _SizeTransitionPageState createState() => _SizeTransitionPageState();

  @override
  String get pageName => 'SizeTransition剪辑对齐动画';
}

class _SizeTransitionPageState extends BasePageState<SizeTransitionPage> with TickerProviderStateMixin{
  AnimationController _controller;
  Animation<double> _animation;
  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      duration: const Duration(seconds: 3),
      vsync: this,
    )..repeat(reverse: true);

    _animation = CurvedAnimation(
      parent: _controller,
      curve: Curves.fastOutSlowIn,
    );
  }
  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        alignment: Alignment.center,
        child: SizeTransition(
          sizeFactor: _animation,
          axis: Axis.horizontal,
          axisAlignment: -1,
          child: const Center(
            child: FlutterLogo(size: 200.0),
          ),
        ),
      ),
    );
  }

}