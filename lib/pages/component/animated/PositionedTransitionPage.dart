import '../../../Include.dart';

//https://api.flutter.dev/flutter/widgets/PositionedTransition-class.html
class PositionedTransitionPage extends BasePage {
  static String dartPath = "lib/pages/component/animated/PositionedTransitionPage.dart";

  @override
  _PositionedTransitionPageState createState() => _PositionedTransitionPageState();

  @override
  String get pageName => 'PositionedTransition移动动画+原路返回动画';
}

class _PositionedTransitionPageState extends BasePageState<PositionedTransitionPage> with TickerProviderStateMixin {
  AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      duration: const Duration(seconds: 2),
      vsync: this,
    )..repeat(reverse: true);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    const double smallLogo = 100;
    const double bigLogo = 200;
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        child: LayoutBuilder(
          builder: (BuildContext context, BoxConstraints constraints) {
            final Size biggest = constraints.biggest;
            return Stack(
              children: <Widget>[
                PositionedTransition(
                  rect: RelativeRectTween(
                    begin: RelativeRect.fromSize(const Rect.fromLTWH(0, 0, smallLogo, smallLogo), biggest),
                    end: RelativeRect.fromSize(Rect.fromLTWH(biggest.width - bigLogo, biggest.height - bigLogo, bigLogo, bigLogo), biggest),
                  ).animate(CurvedAnimation(
                    parent: _controller,
                    curve: Curves.elasticInOut,
                  )),
                  child: const Padding(padding: EdgeInsets.all(8), child: FlutterLogo()),
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
