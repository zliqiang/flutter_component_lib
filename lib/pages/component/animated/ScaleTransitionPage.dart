import '../../../Include.dart';

//https://api.flutter.dev/flutter/widgets/ScaleTransition-class.html
class ScaleTransitionPage extends BasePage {

  static String dartPath = "lib/pages/component/animated/ScaleTransitionPage.dart";

  @override
  _ScaleTransitionPageState createState() => _ScaleTransitionPageState();

  @override
  String get pageName => 'ScaleTransition缩放动画';
}

class _ScaleTransitionPageState extends BasePageState<ScaleTransitionPage> with TickerProviderStateMixin{

  AnimationController _controller;
  Animation<double> _animation;
  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      duration: const Duration(seconds: 1),
      vsync: this,
    )..repeat(reverse: true);

    _animation = CurvedAnimation(
      parent: _controller,
      curve: Curves.fastOutSlowIn,
    );
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        child: Center(
          child: ScaleTransition(
            scale: _animation,
            child: const Padding(
              padding: EdgeInsets.all(8.0),
              child: FlutterLogo(size: 150.0),
            ),
          ),
        ),
      ),
    );
  }

}