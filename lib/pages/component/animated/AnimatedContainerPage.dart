import '../../../Include.dart';

//https://api.flutter.dev/flutter/widgets/AnimatedContainer-class.html
class AnimatedContainerPage extends BasePage {

  static String dartPath = "lib/pages/component/animated/AnimatedContainerPage.dart";

  @override
  _AnimatedContainerPageState createState() => _AnimatedContainerPageState();

  @override
  String get pageName => 'AnimatedContainer时间改变宽高颜色';
}

class _AnimatedContainerPageState extends BasePageState<AnimatedContainerPage> {
  bool selected = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: GestureDetector(
        onTap: () {
          setState(() {
            selected = !selected;
          });
        },
        child: Center(
          child: AnimatedContainer(
            width: selected ? 200.0 : 100.0,
            height: selected ? 100.0 : 200.0,
            color: selected ? Colors.red : Colors.blue,
            alignment: selected ? Alignment.center : AlignmentDirectional.topCenter,
            duration: const Duration(seconds: 2),
            curve: Curves.fastOutSlowIn,
            child: const FlutterLogo(size: 75),
          ),
        ),
      ),
    );
  }

}