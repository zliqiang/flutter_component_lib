import '../../../Include.dart';

//https://api.flutter.dev/flutter/widgets/Hero-class.html
class HeroPage extends BasePage {

  static String dartPath = "lib/pages/component/animated/HeroPage.dart";

  @override
  _HeroPageState createState() => _HeroPageState();

  @override
  String get pageName => 'Hero飞行';
}

class _HeroPageState extends BasePageState<HeroPage> {

  Widget _blueRectangle(Size size) {
    return Container(
      width: size.width,
      height: size.height,
      color: Colors.blue,
    );
  }

  void _gotoDetailsPage(BuildContext context) {
    Navigator.of(context).push(MaterialPageRoute<void>(
      builder: (BuildContext context) => Scaffold(
        appBar: AppBar(
          title: const Text('第二页'),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Hero(
                tag: 'hero-rectangle',
                child: _blueRectangle(const Size(200, 200)),
              ),
            ],
          ),
        ),
      ),
    ));
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            const SizedBox(
              height: 20.0,
            ),
            ListTile(
              leading: Hero(
                tag: 'hero-rectangle',
                child: _blueRectangle(const Size(50, 50)),
              ),
              onTap: () => _gotoDetailsPage(context),
              title:
              const Text('点击图标以查看英雄动画过渡。'),
            ),
          ],
        ),
      ),
    );
  }

}