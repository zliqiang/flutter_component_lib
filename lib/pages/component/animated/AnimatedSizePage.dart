import '../../../Include.dart';

//https://api.flutter.dev/flutter/widgets/AnimatedSize-class.html
class AnimatedSizePage extends BasePage {

  static String dartPath = "lib/pages/component/animated/AnimatedSizePage.dart";

  @override
  _AnimatedSizePageState createState() => _AnimatedSizePageState();

  @override
  String get pageName => 'AnimatedSize大小动画';
}

class _AnimatedSizePageState extends BasePageState<AnimatedSizePage> with SingleTickerProviderStateMixin{
  double _size = 50.0;
  bool _large = false;

  void _updateSize() {
    setState(() {
      _size = _large ? 250.0 : 100.0;
      _large = !_large;
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        alignment: Alignment.center,
        child: GestureDetector(
          onTap: () => _updateSize(),
          child: Container(
            color: Colors.amberAccent,
            child: AnimatedSize(
              curve: Curves.easeIn,
              vsync: this,
              duration: const Duration(seconds: 1),
              child: FlutterLogo(size: _size),
            ),
          ),
        ),
      ),
    );
  }

}