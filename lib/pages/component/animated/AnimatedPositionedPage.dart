import '../../../Include.dart';

//https://api.flutter.dev/flutter/widgets/AnimatedPositioned-class.html
class AnimatedPositionedPage extends BasePage {

  static String dartPath = "lib/pages/component/animated/AnimatedPositionedPage.dart";

  @override
  _AnimatedPositionedPageState createState() => _AnimatedPositionedPageState();

  @override
  String get pageName => 'AnimatedPositioned位置动画';
}

class _AnimatedPositionedPageState extends BasePageState<AnimatedPositionedPage> {

  bool selected = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        alignment: Alignment.center,
        child: SizedBox(
          width: 200,
          height: 350,
          child: Stack(
            children: <Widget>[
              AnimatedPositioned(
                width: selected ? 200.0 : 50.0,
                height: selected ? 50.0 : 200.0,
                top: selected ? 50.0 : 150.0,
                duration: const Duration(seconds: 2),
                curve: Curves.fastOutSlowIn,
                child: GestureDetector(
                  onTap: () {
                    setState(() {
                      selected = !selected;
                    });
                  },
                  child: Container(
                    color: Colors.blue,
                    child: const Center(child: Text('Tap me')),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

}