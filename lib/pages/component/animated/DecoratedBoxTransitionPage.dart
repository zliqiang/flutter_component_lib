import '../../../Include.dart';

//https://api.flutter.dev/flutter/widgets/DecoratedBoxTransition-class.html
class DecoratedBoxTransitionPage extends BasePage {

  static String dartPath = "lib/pages/component/animated/DecoratedBoxTransitionPage.dart";

  @override
  _DecoratedBoxTransitionPageState createState() => _DecoratedBoxTransitionPageState();

  @override
  String get pageName => 'DecoratedBoxTransition运动动画';
}

class _DecoratedBoxTransitionPageState extends BasePageState<DecoratedBoxTransitionPage> with SingleTickerProviderStateMixin{
  final DecorationTween decorationTween = DecorationTween(
    begin: BoxDecoration(
      color: const Color(0xFFFFFFFF),
      border: Border.all(style: BorderStyle.none),
      borderRadius: BorderRadius.circular(60.0),
      shape: BoxShape.rectangle,
      boxShadow: const <BoxShadow>[
        BoxShadow(
          color: Color(0x66666666),
          blurRadius: 10.0,
          spreadRadius: 3.0,
          offset: Offset(0, 6.0),
        )
      ],
    ),
    end: BoxDecoration(
      color: const Color(0xFFFFFFFF),
      border: Border.all(
        style: BorderStyle.none,
      ),
      borderRadius: BorderRadius.zero,
      // No shadow.
    ),
  );
   AnimationController _controller;
  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 3),
    )..repeat(reverse: true);
  }




  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        color: Colors.white,
        child: Center(
          child: DecoratedBoxTransition(
            position: DecorationPosition.background,
            decoration: decorationTween.animate(_controller),
            child: Container(
              width: 200,
              height: 200,
              padding: const EdgeInsets.all(10),
              child: const FlutterLogo(),
            ),
          ),
        ),
      ),
    );
  }

}