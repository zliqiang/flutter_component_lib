import '../../../Include.dart';

//https://api.flutter.dev/flutter/widgets/AnimatedCrossFade-class.html
class AnimatedCrossFadePage extends BasePage {

  static String dartPath = "lib/pages/component/animated/AnimatedCrossFadePage.dart";

  @override
  _AnimatedCrossFadePageState createState() => _AnimatedCrossFadePageState();

  @override
  String get pageName => 'AnimatedCrossFade交叉淡入';
}

class _AnimatedCrossFadePageState extends BasePageState<AnimatedCrossFadePage> {

 bool _first = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: InkWell(
        onTap: (){
          setState(() {
            _first =!_first;
          });
        },
        child:  Container(
          alignment: Alignment.center,
          child: AnimatedCrossFade(
            duration: const Duration(seconds: 1),
            firstChild: const FlutterLogo(style: FlutterLogoStyle.horizontal, size: 100.0),
            secondChild: const FlutterLogo(style: FlutterLogoStyle.stacked, size: 100.0),
            crossFadeState: _first ? CrossFadeState.showFirst : CrossFadeState.showSecond,
          ),
        ),
      )
     ,
    );
  }

}