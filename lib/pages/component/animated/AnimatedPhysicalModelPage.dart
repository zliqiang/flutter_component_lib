import '../../../Include.dart';

//https://api.flutter.dev/flutter/widgets/AnimatedPhysicalModel-class.html
class AnimatedPhysicalModelPage extends BasePage {

  static String dartPath = "lib/pages/component/animated/AnimatedPhysicalModelPage.dart";

  @override
  _AnimatedPhysicalModelPageState createState() => _AnimatedPhysicalModelPageState();

  @override
  String get pageName => 'AnimatedPhysicalModel物理动画';
}

class _AnimatedPhysicalModelPageState extends BasePageState<AnimatedPhysicalModelPage> {

  bool _animated = false;

  _buildAnimatedPhysicalModel() {
    return AnimatedPhysicalModel(
      borderRadius: BorderRadius.circular(_animated ? 20 : 10),
      shape: BoxShape.rectangle,
      color: _animated ? Colors.blue : Colors.red,
      elevation: _animated ? 18 : 8,
      shadowColor: !_animated ? Colors.blue : Colors.red,
      child: Container(
        height: 100,
        width: 100,
      ),
      duration: Duration(seconds: 1),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        alignment: Alignment.center,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            _buildAnimatedPhysicalModel(),
            RaisedButton(
              child: Text('动画'),
              onPressed: () {
                setState(() {
                  _animated = !_animated;
                });
              },
            ),
          ],
        ),
      ),
    );
  }

}