import '../../../Include.dart';

//https://api.flutter.dev/flutter/widgets/AnimatedOpacity-class.html
class AnimatedOpacityPage extends BasePage {
  static String dartPath = "lib/pages/component/animated/AnimatedOpacityPage.dart";

  @override
  _AnimatedOpacityPageState createState() => _AnimatedOpacityPageState();

  @override
  String get pageName => 'AnimatedOpacity透明度动画';
}

class _AnimatedOpacityPageState extends BasePageState<AnimatedOpacityPage> {
  double opacityLevel = 1.0;

  void _changeOpacity() {
    setState(() => opacityLevel = opacityLevel == 0 ? 1.0 : 0.0);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        alignment: Alignment.center,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            AnimatedOpacity(
              opacity: opacityLevel,
              duration: const Duration(seconds: 1),
              child: const FlutterLogo(),
            ),
            ElevatedButton(
              child: const Text('Fade Logo'),
              onPressed: _changeOpacity,
            ),
          ],
        ),
      ),
    );
  }
}
