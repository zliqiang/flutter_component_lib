import '../../../Include.dart';

//https://api.flutter.dev/flutter/widgets/DefaultTextStyle-class.html
class DefaultTextStylePage extends BasePage {

  static String dartPath = "lib/pages/component/text/DefaultTextStylePage.dart";

  @override
  _DefaultTextStylePageState createState() => _DefaultTextStylePageState();

  @override
  String get pageName => 'DefaultTextStyle文字样式';
}

class _DefaultTextStylePageState extends BasePageState<DefaultTextStylePage> {
  @override
  Widget build(BuildContext context) {

    //DefaultTextStyle用法1：
    var text1 = DefaultTextStyle.merge(
      style: TextStyle(
          color: Colors.red,
          fontWeight: FontWeight.w800,
          fontFamily: 'Roboto',
          fontSize: 12.0,
          height: 1.2  //行高，倍数
      ),
      child: Row(
        children: [
          Column(
            children: [
              Text('PREP'),
              Text('25 min')
            ],
          ),
          Column(
            children: [
              Text('COOK'),
              Text('1 hours', style: TextStyle(color: Colors.blue),)
            ],
          ),
          Column(
            children: [
              Text('PEEDS'),
              Text('4-6')
            ],
          )
        ],
      ),
    );

    var text2 = Container(
      width: 150,
      color: Colors.red,
      child: DefaultTextStyle(
        style: TextStyle(fontSize: 18),
        // TextOverflow.clip：直接截取
        // TextOverflow.fade：渐隐
        // TextOverflow.ellipsis：省略号形式
        // TextOverflow.visible：可见
        overflow: TextOverflow.ellipsis,
        child: Text('我爱模板网，我爱模板网，我爱模板网，我爱模板网，我爱模板网，'),
      ),
    );

    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        child: Column(
          children: [
            text1,
            text2
          ],
        ),
      ),
    );
  }

}