import 'package:flutter/gestures.dart';
import 'package:flutter_web_2021/utils/toast/OkToastUtil.dart';

import '../../../Include.dart';

//https://api.flutter.dev/flutter/widgets/RichText-class.html
class RichTextPage extends BasePage {

  static String dartPath = "lib/pages/component/text/RichTextPage.dart";

  @override
  _RichTextPageState createState() => _RichTextPageState();

  @override
  String get pageName => 'RichText富文本';
}

class _RichTextPageState extends BasePageState<RichTextPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        child: ListView(
          children: [
            RichText(
              text: TextSpan(
                  // style: DefaultTextStyle.of(context).style,
                  children: <InlineSpan>[
                    TextSpan(text: '登陆即视为同意'),
                    TextSpan(
                      text: '《xxx服务协议》',
                      style: TextStyle(color: Colors.red),
                      recognizer: TapGestureRecognizer()..onTap = () {
                        // 手势触发
                        ToastUtil.showToastBottomLong("手势触发");
                      },
                    ),
                    WidgetSpan(
                      alignment: PlaceholderAlignment.bottom,
                      child: Image.network(
                        'https://flutter.github.io/assets-for-api-docs/assets/widgets/owl-2.jpg',
                        width: 40,
                        height: 40,
                      ),
                    ),
                  ]),
            ),




          ],
        ),
      ),
    );
  }

}