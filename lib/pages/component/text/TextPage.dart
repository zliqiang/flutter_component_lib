import '../../../Include.dart';

//https://flutter.dev/docs/development/ui/widgets/text
class TextPage extends BasePage {

  static String dartPath = "lib/pages/component/text/TextPge.dart";

  @override
  _TextPgeState createState() => _TextPgeState();

  @override
  String get pageName => 'Text单格式文本';
}

class _TextPgeState extends BasePageState<TextPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        child: ListView(

          children: [
            Text(
              'Hello, How are you?',
              textAlign: TextAlign.center,
              overflow: TextOverflow.ellipsis,
              style: const TextStyle(fontWeight: FontWeight.bold),
            ),
            const Text.rich(
              TextSpan(
                text: 'Hello', // default text style
                children: <TextSpan>[
                  TextSpan(text: ' beautiful ', style: TextStyle(fontStyle: FontStyle.italic)),
                  TextSpan(text: 'world', style: TextStyle(fontWeight: FontWeight.bold)),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

}