import 'dart:async';

import 'package:flutter_web_2021/Include.dart';

class StreamBuilderPage extends BasePage {
  static String dartPath = "lib/pages/component/async_/StreamBuilderPage.dart";
  @override
  _StreamBuilderPageState createState() => _StreamBuilderPageState();

  @override
  String get pageName => 'StreamBuilder【局部刷新】';
}

class _StreamBuilderPageState extends BasePageState<StreamBuilderPage> {
  var index = 0;
  StreamSubscription<String> subscription;
  //创建StreamController
  var streamController;
  // 获取StreamSink用于发射事件
  StreamSink<String> get streamSink  => streamController.sink;
  // 获取Stream用于监听
  Stream<String> get streamData => streamController.stream;

  void onFloatActionButtonPress() {
    //发射一个事件.
    streamSink.add(index.toString());
    index++;
  }

  @override
  void initState() {
    super.initState();

    streamController = StreamController<String>();
    streamSink.add("0");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: MyAppBar.appBar(context, widget.pageName),
        body: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                StreamBuilder<String>(
                    stream: streamData,
                    builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
                      return Text('Result: ${snapshot.data}');
                    }
                )
              ],
            )
        ),
        floatingActionButton: FloatingActionButton(
            onPressed: onFloatActionButtonPress,
            child: Icon(Icons.add))
    );
  }
}