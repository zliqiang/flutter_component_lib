import 'package:flutter_web_2021/Include.dart';

class RepaintBoundaryPage extends BasePage {
  static String dartPath = "lib/pages/component/async_/RepaintBoundaryPage.dart";
  @override
  _RepaintBoundaryPageState createState() => _RepaintBoundaryPageState();

  @override
  String get pageName => 'RepaintBoundary【底层节点优化更新】';
}

class _RepaintBoundaryPageState extends BasePageState<RepaintBoundaryPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        child: RepaintBoundary(
          child: Container(
              margin: EdgeInsets.fromLTRB(10,20,10,10),
              height: 100,
              width: 350,
              color: Colors.yellow,
              child: Center(
                  child: Text("345678")
              )
          ),
        ),
      ),
    );
  }

}