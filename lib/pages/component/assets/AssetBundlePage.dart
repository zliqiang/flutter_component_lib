import 'dart:convert';
import 'dart:typed_data';

import '../../../Include.dart';

//https://api.flutter.dev/flutter/services/AssetBundle-class.html

//name: my_awesome_application
// flutter:
//   assets:
//    - images/hamilton.jpeg
//    - images/lafayette.jpeg
class AssetBundlePage extends BasePage {

  static String dartPath = "lib/pages/component/assets/AssetBundlePage.dart";

  @override
  _AssetBundlePageState createState() => _AssetBundlePageState();

  @override
  String get pageName => 'AssetBundle资源';
}

class _AssetBundlePageState extends BasePageState<AssetBundlePage> {


 void setRootBundle()async{
    //假设您在 assets/images 中有一个图像 clock.png，在 assets/text 中有一个 UTF-8编码的文本文件 distances.json。
    //关键就是资产的路径，所以你可以把整个文件加载成一个 String，然后像这样解码 json:
   String distancesText = await rootBundle.loadString('assets/texts/distances.json');
   Map distances = json.decode(distancesText);

    //loadString 负责为您处理 UTF-8解码，并缓存 String 以便下次更快地访问。
    //Loadstructuredata 将 loadString 再进一步——它加载 String，然后调用提供的回调来解析 String 并返回结果。这次它缓存解码结果——现在保存下次的读取和解码步骤。
   Map distances2 = await rootBundle
       .loadStructuredData('assets/texts/distances.json', (String s) async {
     return json.decode(s);
   });

    //，这对文本文件来说非常有用；二进制文件呢？您可以将整个资产作为字节数组读取。
   ByteData clockData = await rootBundle.load('assets/images/clock.png');
   Uint8List clockBytes = clockData.buffer.asUint8List();


 }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        child: Text("看代码，没搞懂"),
      ),
    );
  }

}