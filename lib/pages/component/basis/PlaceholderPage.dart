
import '../../../Include.dart';

class PlaceholderPage extends BasePage {

  static String dartPath = "lib/pages/component/basis/PlaceholderPage.dart";

  @override
  _PlaceholderPageState createState() => _PlaceholderPageState();

  @override
  String get pageName => 'Placeholder占位符';
}

class _PlaceholderPageState extends BasePageState<PlaceholderPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        child: Placeholder(color: Colours.blue,strokeWidth:1),
      ),
    );
  }

}