
import '../../../Include.dart';

//https://api.flutter.dev/flutter/widgets/Image-class.html
class ImagePage extends BasePage {
  static String dartPath = "lib/pages/component/basis/ImagePage.dart";
  @override
  _ImagePageState createState() => _ImagePageState();

  @override
  String get pageName => 'ImagePage图片';
}

class _ImagePageState extends BasePageState<ImagePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: ListView(
        children: [
          const Image(
            image: NetworkImage('https://flutter.github.io/assets-for-api-docs/assets/widgets/owl.jpg'),
          ),
          Image.network('https://flutter.github.io/assets-for-api-docs/assets/widgets/owl-2.jpg')
        ],
      ),
    );
  }

}