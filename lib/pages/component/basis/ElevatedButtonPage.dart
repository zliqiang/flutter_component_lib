import 'package:flutter_web_2021/Include.dart';

class ElevatedButtonPage extends BasePage {

  static String dartPath = "lib/pages/component/basis/ElevatedButtonPage.dart";

  @override
  _ElevatedButtonPageState createState() => _ElevatedButtonPageState();

  @override
  String get pageName => 'ElevatedButton立面按钮';
}

class _ElevatedButtonPageState extends BasePageState<ElevatedButtonPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        child: RaisedButton(
          color: Colours.blue,
          child: Text("立面按钮--过时"),
        ),
      ),
    );
  }

}