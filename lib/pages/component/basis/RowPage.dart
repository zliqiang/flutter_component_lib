import '../../../Include.dart';

//https://api.flutter.dev/flutter/widgets/Row-class.html
class RowPage extends BasePage {
  static String dartPath = "lib/pages/component/basis/RowPage.dart";
  @override
  _RowPageState createState() => _RowPageState();

  @override
  String get pageName => '【row】行';
}

class _RowPageState extends BasePageState<RowPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: ListView(children: [
        Row(
          children: const <Widget>[
            Expanded(
              child: Text('Deliver features faster', textAlign: TextAlign.center),
            ),
            Expanded(
              child: Text('Craft beautiful UIs', textAlign: TextAlign.center),
            ),
            Expanded(
              child: FittedBox(
                fit: BoxFit.contain, // otherwise the logo will be tiny
                child: FlutterLogo(),
              ),
            ),
          ],
        ),
        Row(
          children: <Widget>[
            const FlutterLogo(),
            const Expanded(
              child: Text(
                  "Flutter's hot reload helps you quickly and easily experiment, build UIs, add features, and fix bug faster. Experience sub-second reload times, without losing state, on emulators, simulators, and hardware for iOS and Android."),
            ),
            const Icon(Icons.sentiment_very_satisfied),
          ],
        )
      ]),
    );
  }
}
