
import '../../../Include.dart';

//https://api.flutter.dev/flutter/widgets/Container-class.html
class ContainerPage extends BasePage {
  static String dartPath = "lib/pages/component/basis/ContainerPage.dart";
  @override
  _ContainerPageState createState() => _ContainerPageState();

  @override
  String get pageName => 'Container容器';
}

class _ContainerPageState extends BasePageState<ContainerPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        child: ListView(
          children: [
            Center(
              child: Container(
                margin: const EdgeInsets.all(10.0),
                color: Colors.amber[600],
                width: 48.0,
                height: 48.0,
              ),
            ),
            Container(
              constraints: BoxConstraints.expand(
                height: Theme.of(context).textTheme.headline4.fontSize * 1.1 + 200.0,
              ),
              padding: const EdgeInsets.all(8.0),
              color: Colors.blue[600],
              alignment: Alignment.center,
              child: Text('Hello World',
                  style: Theme.of(context)
                      .textTheme
                      .headline4
                      .copyWith(color: Colors.white)),
              transform: Matrix4.rotationZ(0.1),
            ),
          ],
        ),
      ),
    );
  }

}