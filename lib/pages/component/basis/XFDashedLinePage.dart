import 'package:flutter_web_2021/Include.dart';
import 'package:flutter_web_2021/widgets/line/XFDashedLine.dart';

class XFDashedLinePage extends BasePage {

  static String dartPath = "lib/pages/component/basis/XFDashedLinePage.dart";

  @override
  _XFDashedLinePageState createState() => _XFDashedLinePageState();

  @override
  String get pageName => '【XFDashedLine】虚线';
}

class _XFDashedLinePageState extends BasePageState<XFDashedLinePage> {
  //目的：实现效果的同时，提供定制，并且可以实现水平和垂直两种虚线效果：
  //
  // axis：确定虚线的方向；
  // dashedWidth：根据虚线的方向确定自己虚线的宽度；
  // dashedHeight：根据虚线的方向确定自己虚线的高度；
  // count：内部会根据设置的个数和宽高确定密度（虚线的空白间隔）；
  // color：虚线的颜色
  //


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
                width: 400,
                child: XFDashedLine(
                  axis: Axis.horizontal,
                  count: 8,
                  dashedWidth: 30,
                  dashedHeight: 2,
                )
            ),
            SizedBox(height: 50,),
            Container(
                height: 400,
                child: XFDashedLine(
                  axis: Axis.vertical,
                  count: 25,
                  dashedWidth: 2,
                  dashedHeight: 8,
                  color: Colors.blue,
                )
            ),
          ],
        ),
      ),
    );
  }

}