import '../../../Include.dart';

//https://api.flutter.dev/flutter/widgets/Icon-class.html
class IconPage extends BasePage {
  static String dartPath = "lib/pages/component/basis/IconPage.dart";

  @override
  _IconPageState createState() => _IconPageState();

  @override
  String get pageName => 'Icon图标';
}

class _IconPageState extends BasePageState<IconPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: const <Widget>[
            Icon(
              Icons.favorite,
              color: Colors.pink,
              size: 24.0,
              semanticLabel: 'Text to announce in accessibility modes',
            ),
            Icon(
              Icons.audiotrack,
              color: Colors.green,
              size: 30.0,
            ),
            Icon(
              Icons.beach_access,
              color: Colors.blue,
              size: 36.0,
            ),
          ],
        ),
      ),
    );
  }
}
