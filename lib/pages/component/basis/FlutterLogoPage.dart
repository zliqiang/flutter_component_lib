import '../../../Include.dart';

//https://api.flutter.dev/flutter/material/FlutterLogo-class.html
class FlutterLogoPage extends BasePage {
  static String dartPath = "lib/pages/component/basis/FlutterLogoPage.dart";

  @override
  _FlutterLogoPageState createState() => _FlutterLogoPageState();

  @override
  String get pageName => 'FlutterLogoPage';
}

class _FlutterLogoPageState extends BasePageState<FlutterLogoPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        alignment: Alignment.center,
        child: FlutterLogo(size: 80.0),
      ),
    );
  }
}
