import '../../../Include.dart';

class CenterPage extends BasePage {
  static String dartPath = "lib/pages/component/multi/CenterPage.dart";
  @override
  _AlignPageState createState() => _AlignPageState();

  @override
  String get pageName => 'Center【中心】';
}

class _AlignPageState extends BasePageState<CenterPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:MyAppBar.tabAppBar(context, widget.pageName, showIcoLeft: false),
      body: Column(
        children: [
          Container(
            color: Colors.red,
            child: Center(
              child: Text(
                'Center',
                style: TextStyle(backgroundColor: Colors.amber),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 10),
            color: Colors.red,
            child: Center(
              widthFactor: 2,
              child: Text(
                'Center',
                style: TextStyle(backgroundColor: Colors.amber),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 10),
            color: Colors.red,
            child: Center(
              heightFactor: 2,
              child: Text(
                'Center',
                style: TextStyle(backgroundColor: Colors.amber),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 10),
            color: Colors.red,
            child: Center(
              widthFactor: 2,
              heightFactor: 2,
              child: Text(
                'Center',
                style: TextStyle(backgroundColor: Colors.amber),
              ),
            ),
          ),
        ],
      ),
    );
  }

}