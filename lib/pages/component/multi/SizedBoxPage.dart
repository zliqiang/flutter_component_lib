import '../../../Include.dart';

class SizedBoxPage extends BasePage {
  static String dartPath = "lib/pages/component/multi/SizedBoxPage.dart";
  @override
  _AlignPageState createState() => _AlignPageState();

  @override
  String get pageName => 'SizedBox【尺寸调节】';
}

class _AlignPageState extends BasePageState<SizedBoxPage> {
  //alignment属性设置子控件的位置，Alignment中已定义了如下几种位置：
  //
  // Alignment.topLeft：顶部左边
  //  Alignment.topCenter：顶部中间
  // Alignment.topRight：顶部右边
  // Alignment.centerLeft：中部左边
  // Alignment.center：中部中间
  // Alignment.centerRight：中部右边
  // Alignment.bottomLeft：底部左边
  // Alignment.bottomCenter：底部中间
  // Alignment.bottomRight：底部右边
  // ————————————————
  // 版权声明：本文为CSDN博主「野猿新一」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
  // 原文链接：https://blog.csdn.net/mqdxiaoxiao/article/details/102814395
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.tabAppBar(context, widget.pageName, showIcoLeft: false),
      body: Container(
        width: 300,
        height: 300,
        color: Colors.green[100],
        child: Row(children: <Widget>[
          SizedBox(
            width: 150.0,
            height: 150.0,
            child: Container(
              margin: EdgeInsets.all(20.0),
              width: 200.0,
              height: 200.0,
              color: Colors.blue,
            ),
          ),
          SizedBox(
            width: 100.0,
            height: 100.0,
            child: Container(
              margin: EdgeInsets.all(20.0),
              width: 200.0,
              height: 200.0,
              color: Colors.yellow,
            ),
          ),
          SizedBox(
            width: 100.0,
            height: 100.0,
            child: Container(
              margin: EdgeInsets.all(20.0),
              width: 200.0,
              height: 200.0,
              color: Colors.red,
            ),
          )
        ]),
      ),
    );
  }
}
