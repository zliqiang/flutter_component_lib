import '../../../Include.dart';

class AspectRatioPage extends BasePage {
  static String dartPath = "lib/pages/component/multi/AspectRatioPage.dart";
  @override
  _AspectRatioState createState() => _AspectRatioState();

  @override
  String get pageName => 'AspectRatio【组件宽高比】';
}

class _AspectRatioState extends BasePageState<AspectRatioPage> {
  //参数详解
  // 属性-说明
  // aspectRatio	宽高比。长宽比表示为宽高比。例如，16:9宽高比的值为16.0/9.0。
  // child-子元素
  //特别说明
  // AspectRatio  宽高比是相对父容器的。宽是父容器的宽，高是根据指定的比例计算出来的。
  // 如果父容器  同时给定了宽和高 则：AspectRatio  宽高比不生效。
  // https://blog.csdn.net/ruoshui_t/article/details/91372165
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.tabAppBar(context, widget.pageName, showIcoLeft: false),
      body: Container(
        width: 300,
        // height: 300,
        color: Colors.blue,
        child: AspectRatio(
          aspectRatio: 3/1,
          child: Container(
            color: Colors.red,
          ),
        ),
      ),
    );
  }
}
