import '../../../Include.dart';

class FractionallySizedBoxPage extends BasePage {
  static String dartPath = "lib/pages/component/multi/FractionallySizedBoxPage.dart";
  @override
  _AlignPageState createState() => _AlignPageState();

  @override
  String get pageName => 'FractionallySizedBox【百分比布局】';
}

class _AlignPageState extends BasePageState<FractionallySizedBoxPage> {
  _myChild() {
    return Container(
      width: 80,
      height: 80,
      color: Colors.red,
    );
  }

  @override
  Widget build(BuildContext context) {



    return Scaffold(
      appBar:MyAppBar.tabAppBar(context, widget.pageName, showIcoLeft: false),
      body: Container(
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.only(top: 10),
              padding: EdgeInsets.all(10),
              color: Colors.blue,
              width: 100,
              height: 100,
              child: FractionallySizedBox(
                child: _myChild(),
                widthFactor: 0.5,
                heightFactor: 0.5,
                alignment: Alignment.center,
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 10),
              padding: EdgeInsets.all(10),
              color: Colors.blue,
              width: 100,
              height: 100,
              child: FractionallySizedBox(
                child: _myChild(),
                alignment: Alignment.center,
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 10),
              padding: EdgeInsets.all(10),
              color: Colors.blue,
              width: 100,
              height: 100,
              child: FractionallySizedBox(
                child: _myChild(),
                widthFactor: 2,
                alignment: Alignment.center,
              ),
            ),
          ],
        ),
      ),
    );
  }

}