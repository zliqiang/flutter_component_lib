import '../../../Include.dart';

class TransformPage extends BasePage {
  static String dartPath = "lib/pages/component/multi/TransformPage.dart";
  @override
  _AspectRatioState createState() => _AspectRatioState();

  @override
  String get pageName => 'AspectRatio【组件宽高比】';
}

class _AspectRatioState extends BasePageState<TransformPage> {
  //https://cloud.tencent.com/developer/article/1727058
  //2 构造函数
  // Transform({
  //     Key key,
  //     @required this.transform,
  //     this.origin,
  //     this.alignment,
  //     this.transformHitTests = true,
  //     Widget child,
  // })
  // 3 常用属性
  // 3.1 origin：指定子组件做平移、旋转、缩放时的中心点
  // origin: Offset(50, 50),
  // 3.2 alignment：对齐方式
  // alignment:Alignment.center,
  // 3.2.1 顶部左边
  //
  // alignment:Alignment.topLeft,
  // 3.2.2 顶部中间
  //
  // alignment:Alignment.topCenter,
  // 3.2.3 顶部右边
  //
  // alignment:Alignment.topRight,
  // 3.2.4 中部左边
  //
  // alignment:Alignment.centerLeft,
  // 3.2.5 中部中间
  //
  // alignment:Alignment.center,
  // 3.2.6 中部右边
  //
  // alignment:Alignment.centerRight,
  // 3.2.7 底部左边
  //
  // alignment:Alignment.bottomLeft,
  // 3.2.8 底部中间
  //
  // alignment:Alignment.bottomCenter,
  // 3.2.9 底部右边
  //
  // alignment:Alignment.bottomRight,
  // 3.3 transformHitTests：点击区域是否也做相应的变换,为true时执行相应的变换,为false不执行
  // transformHitTests:true,
  // 3.4 transform：控制子组件的平移、旋转、缩放、倾斜变换
  // transform: Matrix4.rotationX(radian),
  // 3.4.1 旋转
  //
  // transform: Matrix4.rotationX(radian),
  // transform: Matrix4.rotationY(radian),
  // transform: Matrix4.rotationZ(radian),
  // 3.4.2 平移
  //
  // transform:Matrix4.translation(Vector3(x, y, z)),
  // transform:Matrix4.translation(Vector3.all(val)),
  // transform:Matrix4.translationValues(x, y, z),
  // 3.4.3 缩放
  //
  // transform:Matrix4.diagonal3(Vector3(x, y, z)),
  // transform:Matrix4.diagonal3(Vector3.all(val)),
  // transform:Matrix4.diagonal3Values(x, y, z),
  // 3.4.4 倾斜
  //
  // transform:Matrix4.skewX(alpha),
  // transform:Matrix4.skewY(double beta),
  // transform:Matrix4.skew(alpha, beta),
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.tabAppBar(context, widget.pageName, showIcoLeft: false),
      body: Center(
        child: Container(
          color: Colors.black,
          child: new Transform(
            alignment: Alignment.topRight, //相对于坐标系原点的对齐方式
            transform: new Matrix4.skewY(0.3), //沿Y轴倾斜0.3弧度
            child: new Container(
              padding: const EdgeInsets.all(8.0),
              color: Colors.deepOrange,
              child: const Text('转换的widget'),
            ),
          ),
        ),
      ),
    );
  }
}
