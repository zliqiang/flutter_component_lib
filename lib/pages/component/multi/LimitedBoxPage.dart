import '../../../Include.dart';

class LimitedBoxPage extends BasePage {
  static String dartPath = "lib/pages/component/multi/LimitedBoxPage.dart";
  @override
  _AlignPageState createState() => _AlignPageState();

  @override
  String get pageName => 'LimitedBox【限制布局】';
}

class _AlignPageState extends BasePageState<LimitedBoxPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:MyAppBar.tabAppBar(context, widget.pageName, showIcoLeft: false),
      body: Container(
          child: LimitedBox(
            maxWidth: 80,
            maxHeight: 50,
              child:Column(
                children: [
                  Container(
                    color: Colours.red,
                    child: Text("ashfksahfksah"),
                  ),
                  Container(
                    color: Colours.red,
                    child: Text("ashfksahfksah"),
                  ),
                  Container(
                    color: Colours.red,
                    child: Text("ashfksahfksah"),
                  ),
                  Container(
                    color: Colours.red,
                    child: Text("ashfksahfksah"),
                  ),
                  Container(
                    color: Colours.red,
                    child: Text("ashfksahfksah"),
                  ),
                  Container(
                    color: Colours.red,
                    child: Text("ashfksahfksah"),
                  ),
                  Container(
                    color: Colours.red,
                    child: Text("ashfksahfksah"),
                  ),
                  Container(
                    color: Colours.red,
                    child: Text("ashfksahfksah"),
                  ),
                ],
              )
          )
      ) ,
    );
  }

}