import '../../../Include.dart';

class ConstrainedBoxPage extends BasePage {
  static String dartPath = "lib/pages/component/multi/ConstrainedBoxPage.dart";
  @override
  _AlignPageState createState() => _AlignPageState();

  @override
  String get pageName => 'ConstrainedBox【子窗口约束】';
}

class _AlignPageState extends BasePageState<ConstrainedBoxPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:MyAppBar.tabAppBar(context, widget.pageName, showIcoLeft: false),
      body: Center(
        child: Container(
          width: 300,
          height: 400,
          decoration: BoxDecoration(
              border: Border.all()
          ),
          //利用UnconstrainedBox 消除之前限制
          child: UnconstrainedBox(
            // 对child进行约束，
              child: ConstrainedBox(
                  constraints: BoxConstraints(
                      minHeight: 30,
                      minWidth: 30,
                      maxHeight: 150,
                      maxWidth: 150
                  ),
                  child: Container(
                    width: 110,
                    height: 110,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      gradient: LinearGradient(colors: [Colors.blue, Colors.purple]),

                    ),
                  )
              )
          ),

        ),
      ),
    );
  }

}