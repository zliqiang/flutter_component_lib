import '../../../Include.dart';

//https://api.flutter.dev/flutter/widgets/Baseline-class.html
class BaselinePage extends BasePage {
  static String dartPath = "lib/pages/component/multi/BaselinePage.dart";
  @override
  _BaselinePageState createState() => _BaselinePageState();

  @override
  String get pageName => 'BaselinePage';
}

class _BaselinePageState extends BasePageState<BaselinePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.pageName),
      ),
        body: new Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            new Baseline(
              baseline: 80.0,
              baselineType: TextBaseline.alphabetic,
              child: new Text(
                'AaBbCc',
                style: new TextStyle(
                  fontSize: 18.0,
                  textBaseline: TextBaseline.alphabetic,
                ),
              ),
            ),
            new Baseline(
              baseline: 80.0,
              baselineType: TextBaseline.alphabetic,
              child: new Container(
                width: 40.0,
                height: 40.0,
                color: Colors.green,
              ),
            ),
            new Baseline(
              baseline: 80.0,
              baselineType: TextBaseline.alphabetic,
              child: new Text(
                'DdEeFf',
                style: new TextStyle(
                  fontSize: 26.0,
                  textBaseline: TextBaseline.alphabetic,
                ),
              ),
            )
          ],
        ),
    );
  }

}