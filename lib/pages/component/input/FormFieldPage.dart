import '../../../Include.dart';

//https://api.flutter.dev/flutter/widgets/FormField-class.html
class FormFieldPage extends BasePage {
  static String dartPath = "lib/pages/component/input/FormFieldPage.dart";

  @override
  _FormFieldPageState createState() => _FormFieldPageState();

  @override
  String get pageName => 'FormField表单验证';
}

class _FormFieldPageState extends BasePageState<FormFieldPage> {
  var _account = '';
  var _pwd = '',_name='';
  final _formKey = GlobalKey<FormState>();

  FocusNode _focusNode = FocusNode();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: ListView(children: [
        Form(
          key: _formKey,
          child: Column(
            children: <Widget>[
              TextFormField(
                decoration: InputDecoration(hintText: '输入账号'),
                onSaved: (value) {
                  _name = value;
                },
                validator: (String value) {
                  return value.length >= 6 ? null : '账号最少6个字符';
                },
              ),
              TextFormField(
                decoration: InputDecoration(hintText: '输入密码'),
                obscureText: true,
                onSaved: (value) {
                  _pwd = value;
                },
                validator: (String value) {
                  return value.length >= 6 ? null : '账号最少6个字符';
                },
              ),
              RaisedButton(
                child: Text('登录'),
                onPressed: () {
                  var _state = Form.of(context);
                  if(_state.validate()){
                    _state.save();
                  }
                },
              )
            ],
          ),),


        TextFormField(
          focusNode: _focusNode,
          decoration: InputDecoration(
            hintText: '电话号码',
          ),
        ),
        TextFormField(
          autofocus: true,
          decoration: InputDecoration(
            hintText: '用户名',
          ),
        ),
        TextFormField(
          onSaved: (value){
            print('$value');
          },
          autovalidate: true,
          validator: (String value){
            return value.length>=6?null:'账号最少6个字符';
          },
        )
      ]),
    );
  }
}
