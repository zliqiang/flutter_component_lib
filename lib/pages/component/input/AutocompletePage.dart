import '../../../Include.dart';

//https://api.flutter.dev/flutter/material/Autocomplete-class.html
class AutocompletePage extends BasePage {

  static String dartPath = "lib/pages/component/input/AutocompletePage.dart";

  @override
  _AutocompletePageState createState() => _AutocompletePageState();

  @override
  String get pageName => 'Autocomplete文章列表选择';
}

class _AutocompletePageState extends BasePageState<AutocompletePage> {

  static const List<String> _kOptions = <String>[
    'aardvark',
    'bobcat',
    'chameleon',
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        child: Autocomplete<String>(
          optionsBuilder: (TextEditingValue textEditingValue) {
            if (textEditingValue.text == '') {
              return const Iterable<String>.empty();
            }
            return _kOptions.where((String option) {
              return option.contains(textEditingValue.text.toLowerCase());
            });
          },
          onSelected: (String selection) {
            print('You just selected $selection');
          },
        ),
      ),
    );
  }

}