import 'package:flutter_web_2021/utils/toast/OkToastUtil.dart';

import '../../../Include.dart';

//https://api.flutter.dev/flutter/widgets/RawKeyboardListener-class.html
//http://laomengit.com/flutter/widgets/RawKeyboardListener.html
class RawKeyboardListenerPage extends BasePage {

  static String dartPath = "lib/pages/component/input/RawKeyboardListenerPage.dart";

  @override
  _RawKeyboardListenerPageState createState() => _RawKeyboardListenerPageState();

  @override
  String get pageName => 'RawKeyboardListener键盘按键监听';
}

class _RawKeyboardListenerPageState extends BasePageState<RawKeyboardListenerPage> {


  TextEditingController _controller = new TextEditingController();
  FocusNode _textNode = new FocusNode();

  // // 主动获取焦点
  // FocusScope.of(context).requestFocus(yourFocusNode); //自动获取焦点
  // FocusScope.of(context).autofocus(yourFocusNode);


  handleKey(RawKeyEvent key) {
    Log().info("Event runtimeType is ${key.runtimeType}");
    if(key.runtimeType.toString() == 'RawKeyDownEvent'){
      RawKeyEventDataAndroid data = key.data as RawKeyEventDataAndroid;
      String _keyCode;
      _keyCode = data.keyCode.toString(); //keycode of key event (66 is return)

      ToastUtil.showToastBottom("why does this run twice $_keyCode");
      print("why does this run twice $_keyCode");
    }
  }

  @override
  Widget build(BuildContext context) {
    TextField _textField = new TextField(
      controller: _controller,
    );
    FocusScope.of(context).requestFocus(_textNode);
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
        body: Scaffold(
          body: RawKeyboardListener(
              focusNode: _textNode,
              onKey: handleKey,
              child: _textField
          ),
        ),
    );
  }

}