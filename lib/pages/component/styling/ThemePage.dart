import '../../../Include.dart';

//https://api.flutter.dev/flutter/material/Theme-class.html
class ThemePage extends BasePage {

  static String dartPath = "lib/pages/component/styling/ThemePage.dart";

  @override
  _ThemePageState createState() => _ThemePageState();

  @override
  String get pageName => 'Theme主题';
}

class _ThemePageState extends BasePageState<ThemePage> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      key: Key("12123213"),
      title: widget.pageName,
      theme: ThemeData(
          brightness: Brightness.light,
          primaryColor: Colors.blue[800], //这里我们选蓝色为基准色值。
          accentColor: Colors.lightBlue[100]), //这里我们选浅蓝色为强调色值。
      home: ThemeTest(),
    );
  }

}

class ThemeTest extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Theme.of(context).primaryColor, //内容背景颜色
        margin: EdgeInsets.all(100.0),
        padding: EdgeInsets.all(10.0),
        child: Text(
          "MaterialApp Theme Color", //内容文本
          style: TextStyle(
              fontSize: 20, color: Theme.of(context).accentColor), //内容文本颜色，引用的是accentColor。
          textAlign: TextAlign.center,
        ),
      ),
    );
  }
}