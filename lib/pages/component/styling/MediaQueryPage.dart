
import '../../../Include.dart';

//https://api.flutter.dev/flutter/widgets/MediaQuery-class.html
class MediaQueryPage extends BasePage {

  static String dartPath = "lib/pages/component/styling/MediaQueryPage.dart";

  @override
  _MediaQueryPageState createState() => _MediaQueryPageState();

  @override
  String get pageName => 'MediaQuery当前设备信息';
}

class _MediaQueryPageState extends BasePageState<MediaQueryPage> {

  @override
  void initState() {
    super.initState();
    // var screenSize = MediaQuery.of(context).size;
    // if(screenSize.width>oneColumnLayout){
    //   //平板布局
    // }else{
    //   //手机布局
    // }



  }


  @override
  Widget build(BuildContext context) {
    //字体变大
    var _data = MediaQuery.of(context).copyWith(textScaleFactor: 2.0);

    //屏幕大小
    Size mSize = MediaQuery.of(context).size;
    //密度
    double mRatio = MediaQuery.of(context).devicePixelRatio;
    //设备像素
    double width = mSize.width * mRatio;
    double height = mSize.height * mRatio;

    // 上下边距 （主要用于 刘海  和  内置导航键）
    double topPadding = MediaQuery.of(context).padding.top;
    double bottomPadding = MediaQuery.of(context).padding.bottom;

    double textScaleFactor = MediaQuery.of(context).textScaleFactor;
    Brightness platformBrightness = MediaQuery.of(context).platformBrightness;
    EdgeInsets viewInsets = MediaQuery.of(context).viewInsets;
    EdgeInsets padding = MediaQuery.of(context).padding;
    bool alwaysUse24HourFormat = MediaQuery.of(context).alwaysUse24HourFormat;
    bool accessibleNavigation = MediaQuery.of(context).accessibleNavigation;
    bool invertColors = MediaQuery.of(context).invertColors;
    bool disableAnimations = MediaQuery.of(context).disableAnimations;
    bool boldText = MediaQuery.of(context).boldText;
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body:ListView(
        children: [
          ListTile(
            title: Text('textScaleFactor动态设置比例因子'),
            subtitle:  MediaQuery(
              data: _data,
              child: Text('字体变大'),
            ),
          ),
          ListTile(
            title: Text('width宽度'),
            subtitle: Text(width.toString()),
          ),
          ListTile(
            title: Text('height高度'),
            subtitle: Text(height.toString()),
          ),
          ListTile(
            title: Text('topPadding顶部内边距'),
            subtitle: Text(topPadding.toString()),
          ),
          ListTile(
            title: Text('bottomPadding底部内边距'),
            subtitle: Text(bottomPadding.toString()),
          ),
          ListTile(
            title: Text('textScaleFactor文本比例因子'),
            subtitle: Text(textScaleFactor.toString()),
          ),
          ListTile(
            title: Text('platformBrightness平台亮度'),
            subtitle: Text(platformBrightness.toString()),
          ),

          ListTile(
            title: Text('viewInsets视图样式'),
            subtitle: Text(viewInsets.toString()),
          ),
          ListTile(
            title: Text('padding内边距'),
            subtitle: Text(padding.toString()),
          ),
          ListTile(
            title: Text('alwaysUse24HourFormat始终使用 24 小时制'),
            subtitle: Text(alwaysUse24HourFormat.toString()),
          ),
          ListTile(
            title: Text('accessibleNavigation无障碍导航'),
            subtitle: Text(accessibleNavigation.toString()),
          ),
          ListTile(
            title: Text('invertColors反转颜色'),
            subtitle: Text(invertColors.toString()),
          ),
          ListTile(
            title: Text('disableAnimations禁用动画'),
            subtitle: Text(disableAnimations.toString()),
          ),
          ListTile(
            title: Text('boldText粗体'),
            subtitle: Text(boldText.toString()),
          ),

        ],
      ),
    );
  }
}

