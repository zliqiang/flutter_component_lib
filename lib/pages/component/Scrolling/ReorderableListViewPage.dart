import '../../../Include.dart';

//Flutter 拖拽排序组件 ReorderableListView
//https://api.flutter.dev/flutter/material/ReorderableListView-class.html
class ReorderableListViewPage extends BasePage {

  static String dartPath = "lib/pages/component/Scrolling/ReorderableListViewPage.dart";

  @override
  _ReorderableListViewPageState createState() => _ReorderableListViewPageState();

  @override
  String get pageName => 'ReorderableListView拖拽排序组件';
}

class _ReorderableListViewPageState extends BasePageState<ReorderableListViewPage> {
  final List<int> _items = List<int>.generate(50, (int index) => index);
  @override
  Widget build(BuildContext context) {

    final ColorScheme colorScheme = Theme.of(context).colorScheme;
    final Color oddItemColor = colorScheme.primary.withOpacity(0.05);
    final Color evenItemColor = colorScheme.primary.withOpacity(0.15);
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: ReorderableListView(
        padding: const EdgeInsets.symmetric(horizontal: 40),
        children: <Widget>[
          for (int index = 0; index < _items.length; index++)
            ListTile(
              key: Key('$index'),
              tileColor: _items[index].isOdd ? oddItemColor : evenItemColor,
              title: Text('Item ${_items[index]}'),
            ),
        ],
        onReorder: (int oldIndex, int newIndex) {
          setState(() {
            if (oldIndex < newIndex) {
              newIndex -= 1;
            }
            final int item = _items.removeAt(oldIndex);
            _items.insert(newIndex, item);
          });
        },
      ),
    );
  }

}