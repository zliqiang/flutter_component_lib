import '../../../Include.dart';

//https://api.flutter.dev/flutter/widgets/NestedScrollView-class.html
class NestedScrollViewPage3 extends BasePage {

  static String dartPath = "lib/pages/component/Scrolling/NestedScrollViewPage3.dart";

  @override
  _NestedScrollViewPage3State createState() => _NestedScrollViewPage3State();

  @override
  String get pageName => 'NestedScrollView嵌套其它可滚动组件';
}

class _NestedScrollViewPage3State extends BasePageState<NestedScrollViewPage3> {
  @override
  Widget build(BuildContext context) {

    return Scaffold(
        body: NestedScrollView(
            headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
              return <Widget>[
                SliverOverlapAbsorber(
                  handle: NestedScrollView.sliverOverlapAbsorberHandleFor(context),
                  sliver: SliverAppBar(
                    title:  Text(widget.pageName),
                    floating: true,
                    snap: true,
                    expandedHeight: 200.0,
                    forceElevated: innerBoxIsScrolled,
                  ),
                )
              ];
            },
            body: Builder(
                builder: (BuildContext context) {
                  return CustomScrollView(
                    // The "controller" and "primary" members should be left
                    // unset, so that the NestedScrollView can control this
                    // inner scroll view.
                    // If the "controller" property is set, then this scroll
                    // view will not be associated with the NestedScrollView.
                    slivers: <Widget>[
                      SliverOverlapInjector(handle: NestedScrollView.sliverOverlapAbsorberHandleFor(context)),
                      SliverFixedExtentList(
                        itemExtent: 48.0,
                        delegate: SliverChildBuilderDelegate(
                              (BuildContext context, int index) => ListTile(title: Text('Item $index')),
                          childCount: 30,
                        ),
                      ),
                    ],
                  );
                }
            )
        )
    );

    // return Scaffold(
    //   appBar: MyAppBar.appBar(context, widget.pageName),
    //   body: Container(
    //     child: ,
    //   ),
    // );
  }

}