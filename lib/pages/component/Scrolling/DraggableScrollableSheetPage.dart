import '../../../Include.dart';
//https://www.jianshu.com/p/368760d1a778
class DraggableScrollableSheetPage extends BasePage {

  static String dartPath = "lib/pages/component/Scrolling/DraggableScrollableSheetPage.dart";

  @override
  _DraggableScrollableSheetPageState createState() => _DraggableScrollableSheetPageState();

  @override
  String get pageName => 'DraggableScrollableSheet在屏幕上拖动';
}

class _DraggableScrollableSheetPageState extends BasePageState<DraggableScrollableSheetPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Stack(
        children: [
          Container(
            color: Colors.blue,
          ),
          NotificationListener<DraggableScrollableNotification>(
            onNotification: (notification) {
              print('####################');
              print('minExtent = ${notification.minExtent}');
              print('maxExtent = ${notification.maxExtent}');
              print('initialExtent = ${notification.initialExtent}');
              print('extent = ${notification.extent}');
              print('####################');
              return true;
            },
            child: DraggableScrollableSheet(
              builder: (
                  BuildContext context,
                  ScrollController scrollController,
                  ) {
                return Container(
                  color: Colors.amber,
                  child: ListView.builder(
                    itemBuilder: (context, index) => ListTile(
                      title: Text('item $index'),
                    ),
                    itemCount: 30,
                    controller: scrollController,
                  ),
                );
              },
              initialChildSize: 0.8,
              minChildSize: 0.25,
              maxChildSize: 0.8,
              expand: true,
            ),
          )
        ],
      ),
    );
  }

}