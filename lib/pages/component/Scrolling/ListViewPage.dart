
import 'package:flutter_web_2021/pages/component/multi/AlignPage.dart';
import 'package:flutter_web_2021/pages/component/multi/AspectRatioPage.dart';
import 'package:flutter_web_2021/pages/component/multi/LimitedBoxPage.dart';
import 'package:flutter_web_2021/pages/component/multi/SizedBoxPage.dart';
import 'package:flutter_web_2021/pages/component/multi/TransformPage.dart';
import 'package:flutter_web_2021/pages/component/sliver/CupertinoNavigationBarPage.dart';

import '../../../Include.dart';

class ListViewPage extends BasePage<String> {
  static String dartPath = "lib/pages/component/Scrolling/ListViewPage.dart";
  @override
  _PageOneState createState() => _PageOneState();

  @override
  String get pageName => 'PageOne';
}


class _PageOneState extends BasePageState<ListViewPage> {


  Widget content() {
    return Container(
      child: Center(
        child: Column(
          //自适应密度
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            MyAppBar.tabAppBar(context, widget.pageName, showIcoLeft: false),
            Expanded(
              child: ListView(
                padding: EdgeInsets.all(0),
                children: [
                  RaisedButton(
                    onPressed: () {
                      AppNavigator.push(context,  CupertinoNavigationBarPage());
                    },
                    child: Text('CupertinoNavigationBarPage【导航条】'),
                  ),
                  RaisedButton(
                    onPressed: () {
                      AppNavigator.push(context,  AlignPage());
                    },
                    child: Text('AlignPage【对齐】'),
                  ),
                  RaisedButton(
                    onPressed: () {
                      AppNavigator.push(context,  AspectRatioPage());
                    },
                    child: Text('AspectRatio【组件宽高比】'),
                  ),
                  RaisedButton(
                    onPressed: () {
                      AppNavigator.push(context,  TransformPage());
                    },
                    child: Text('Transform【变化】'),
                  ),
                  RaisedButton(
                    onPressed: () {
                      AppNavigator.push(context,  SizedBoxPage());
                    },
                    child: Text('SizedBox【尺寸调节】'),
                  ),
                  RaisedButton(
                    onPressed: () {
                      AppNavigator.push(context,  LimitedBoxPage());
                    },
                    child: Text('LimitedBox【限制布局】'),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    AppUtil.instance.setContext = context;
    return Scaffold(
      body: content(),
    );
  }


  @override
  void dispose() {
    print('销毁用不到的对象');
    super.dispose();
  }
}
