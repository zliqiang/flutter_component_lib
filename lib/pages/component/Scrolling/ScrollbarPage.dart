import '../../../Include.dart';

//https://api.flutter.dev/flutter/material/Scrollbar-class.html
class ScrollbarPage extends BasePage {

  static String dartPath = "lib/pages/component/Scrolling/ScrollbarPage.dart";

  @override
  _ScrollbarPageState createState() => _ScrollbarPageState();

  @override
  String get pageName => 'Scrollbar滚动条';
}

class _ScrollbarPageState extends BasePageState<ScrollbarPage> {
  final ScrollController _controllerOne = ScrollController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Scrollbar(
        controller: _controllerOne,
        isAlwaysShown: true,
        child: GridView.builder(
          controller:_controllerOne ,
          itemCount: 120,
          gridDelegate:
          const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3),
          itemBuilder: (BuildContext context, int index) {
            return Center(
              child: Text('item $index'),
            );
          },
        ),
      ),
    );
  }

}