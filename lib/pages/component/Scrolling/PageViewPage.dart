import '../../../Include.dart';
//https://api.flutter.dev/flutter/widgets/PageView-class.html
class PageViewPage extends BasePage {

  static String dartPath = "lib/pages/component/Scrolling/PageViewPage.dart";

  @override
  _PageViewPageState createState() => _PageViewPageState();

  @override
  String get pageName => 'PageView水平/垂直滑动';
}

class _PageViewPageState extends BasePageState<PageViewPage> {
  @override
  Widget build(BuildContext context) {
    final PageController controller = PageController(initialPage: 0);
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: PageView(
        /// [PageView.scrollDirection] defaults to [Axis.horizontal].
        /// Use [Axis.vertical] to scroll vertically.
        scrollDirection: Axis.horizontal,
        controller: controller,
        children: const <Widget>[
          Center(
            child: Text('First Page'),
          ),
          Center(
            child: Text('Second Page'),
          ),
          Center(
            child: Text('Third Page'),
          )
        ],
      ),
    );
  }

}