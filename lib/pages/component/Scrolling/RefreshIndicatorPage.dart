import '../../../Include.dart';

class RefreshIndicatorPage extends BasePage {

  static String dartPath = "lib/pages/component/Scrolling/RefreshIndicatorPage.dart";

  @override
  _RefreshIndicatorPageState createState() => _RefreshIndicatorPageState();

  @override
  String get pageName => 'RefreshIndicator下拉刷新指示器';
}

class _RefreshIndicatorPageState extends BasePageState<RefreshIndicatorPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      //下拉刷新组件
      body: RefreshIndicator(
        //圆圈进度颜色
        color: Colors.blue,
        //下拉停止的距离
        displacement: 44.0,
        //背景颜色
        backgroundColor: Colors.grey[200],
        onRefresh: () async {
          //模拟网络请求
          await Future.delayed(Duration(milliseconds: 2000));
          //结束刷新
          return Future.value(true);
        },
        //一个列表
        child: ListView.builder(
          itemBuilder: (BuildContext context, int index) {
            return Container(
              height: 66,
              child: Text("测试数据"),
            );
          },
          //列表数据源数量
          itemCount: 100,
        ),
      ),
    );
  }

}