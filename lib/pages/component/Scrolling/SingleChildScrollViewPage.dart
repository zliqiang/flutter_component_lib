import '../../../Include.dart';

//https://api.flutter.dev/flutter/widgets/SingleChildScrollView-class.html
class SingleChildScrollViewPage extends BasePage {

  static String dartPath = "lib/pages/component/Scrolling/SingleChildScrollViewPage.dart";

  @override
  _SingleChildScrollViewPageState createState() => _SingleChildScrollViewPageState();

  @override
  String get pageName => 'SingleChildScrollView超过父容器时滚动';
}

class _SingleChildScrollViewPageState extends BasePageState<SingleChildScrollViewPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: DefaultTextStyle(
        style: Theme.of(context).textTheme.bodyText2,
        child: LayoutBuilder(
          builder: (BuildContext context, BoxConstraints viewportConstraints) {
            return SingleChildScrollView(
              child: ConstrainedBox(
                constraints: BoxConstraints(
                  minHeight: viewportConstraints.maxHeight,
                ),
                child: IntrinsicHeight(
                  child: Column(
                    children: <Widget>[
                      Container(
                        // A fixed-height child.
                        color: const Color(0xffeeee00), // Yellow
                        height: 120.0,
                        alignment: Alignment.center,
                        child: const Text('Fixed Height Content'),
                      ),
                      Expanded(
                        // A flexible child that will grow to fit the viewport but
                        // still be at least as big as necessary to fit its contents.
                        child: Container(
                          color: const Color(0xffee0000), // Red
                          height: 120.0,
                          alignment: Alignment.center,
                          child: const Text('Flexible Content'),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }

}