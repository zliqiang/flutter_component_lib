import '../../../Include.dart';

//https://api.flutter.dev/flutter/widgets/ScrollConfiguration-class.html
//http://laomengit.com/flutter/widgets/ScrollConfiguration.html
class ScrollConfigurationPage extends BasePage {

  static String dartPath = "lib/pages/component/Scrolling/ScrollConfigurationPage.dart";

  @override
  _ScrollConfigurationPageState createState() => _ScrollConfigurationPageState();

  @override
  String get pageName => 'ScrollConfiguration控制子控件的滚动行为';
}

class _ScrollConfigurationPageState extends BasePageState<ScrollConfigurationPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: ScrollConfiguration(
        behavior: ScrollBehavior(),
        child: ListView.separated(
          itemBuilder: (BuildContext context, int index) {
            return Text('Item$index');
          },
          separatorBuilder: (BuildContext context, int index){
            return Divider();
          },
          itemCount: 50,
        ),
      )
      ,
    );
  }

}