import '../../../Include.dart';

//https://api.flutter.dev/flutter/widgets/NotificationListener-class.html
class NotificationListenerPage extends BasePage {
  static String dartPath = "lib/pages/component/Scrolling/NotificationListenerPage.dart";

  @override
  _NotificationListenerPageState createState() => _NotificationListenerPageState();

  @override
  String get pageName => 'NotificationListener监听列表滑动';
}

class _NotificationListenerPageState extends BasePageState<NotificationListenerPage> {
  List<String> images = List.generate(3, (i) => 'https://picsum.photos/id/$i/800/800');

  ScrollController _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: NotificationListener(
        onNotification: (Notification notification) {
          if (notification is ScrollStartNotification) {
            print('滚动开始');
          }
          if (notification is ScrollUpdateNotification) {
            print('滚动中');
          }
          if (notification is ScrollEndNotification) {
            print('停止滚动');
            if (_scrollController.position.extentAfter == 0) {
              print('滚动到底部');
            }
            if (_scrollController.position.extentBefore == 0) {
              print('滚动到头部');
            }
          }
        },
        child: ListView.builder(
          controller: _scrollController,
          itemCount: images.length,
          itemBuilder: (context, index) {
            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: Image.network(
                images[index],
                gaplessPlayback: true,
              ),
            );
          },
        ),
      ),
    );
  }
}
