import '../../../Include.dart';
//https://api.flutter.dev/flutter/widgets/NestedScrollView-class.html
class NestedScrollViewPage2 extends BasePage {

  static String dartPath = "lib/pages/component/Scrolling/NestedScrollViewPage2.dart";

  @override
  _NestedScrollViewPage2State createState() => _NestedScrollViewPage2State();

  @override
  String get pageName => 'NestedScrollView嵌套其它可滚动组件';
}

class _NestedScrollViewPage2State extends BasePageState<NestedScrollViewPage2> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
        body: NestedScrollView(
          // Setting floatHeaderSlivers to true is required in order to float
          // the outer slivers over the inner scrollable.
            floatHeaderSlivers: true,
            headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
              return <Widget>[
                SliverAppBar(
                  title: const Text('Floating Nested SliverAppBar'),
                  floating: true,
                  expandedHeight: 200.0,
                  forceElevated: innerBoxIsScrolled,
                ),
              ];
            },
            body: ListView.builder(
                padding: const EdgeInsets.all(8),
                itemCount: 30,
                itemBuilder: (BuildContext context, int index) {
                  return SizedBox(
                    height: 50,
                    child: Center(child: Text('Item $index')),
                  );
                }
            )
        )
    );
  }

}