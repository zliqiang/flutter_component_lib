import '../../../Include.dart';

//https://api.flutter.dev/flutter/widgets/IgnorePointer-class.html
class IgnorePointerPage extends BasePage {
  static String dartPath = "lib/pages/component/widget_Interactive/IgnorePointerPage.dart";

  @override
  _IgnorePointerPageState createState() => _IgnorePointerPageState();

  @override
  String get pageName => 'IgnorePointer禁止用户输入';
}

class _IgnorePointerPageState extends BasePageState<IgnorePointerPage> {
  bool ignoring = false;

  void setIgnoring(bool newValue) {
    setState(() {
      ignoring = newValue;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        padding: EdgeInsets.only(top: 50),
        child: Center(
          child: Column(
            children: [
              ElevatedButton(
                onPressed: () {
                  setIgnoring(!ignoring);
                },
                child: Text(
                  ignoring ? 'Set ignoring to false' : 'Set ignoring to true',
                ),
              ),
              Expanded(
                child: IgnorePointer(
                  ignoring: ignoring,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Text('Ignoring: $ignoring'),
                      ElevatedButton(
                        onPressed: () {},
                        child: const Text('Click me!'),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
