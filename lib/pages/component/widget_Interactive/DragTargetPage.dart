import 'package:flutter_web_2021/utils/toast/OkToastUtil.dart';

import '../../../Include.dart';

//https://api.flutter.dev/flutter/widgets/DragTarget-class.html
class DragTargetPage extends BasePage {

  static String dartPath = "lib/pages/component/widget_Interactive/DragTargetPage.dart";

  @override
  _DragTargetPageState createState() => _DragTargetPageState();

  @override
  String get pageName => 'DragTarget拖拽控件';
}

class _DragTargetPageState extends BasePageState<DragTargetPage> {

  _myText(String s) {
    return Text(
      s,
      style: TextStyle(
        fontSize: 14,
        color: Colors.white,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        width: double.infinity,
        height: double.infinity,
        child: Stack(
          alignment: Alignment.center,
          children: [
            Positioned(
              top: 10,
              left: 10,
              child: Draggable(
                data: "aa",
                child: Container(
                  color: Colors.blue,
                  width: 100,
                  height: 100,
                  child: _myText('aa'),
                ),
                feedback: Container(
                  color: Colors.red,
                  width: 100,
                  height: 100,
                  child: _myText('aa'),
                ),
              ),
            ),
            Positioned(
              top: 10,
              right: 10,
              child: Draggable(
                data: "bb",
                child: Container(
                  color: Colors.blue,
                  width: 100,
                  height: 100,
                  child: _myText('bb'),
                ),
                feedback: Container(
                  color: Colors.red,
                  width: 100,
                  height: 100,
                  child: _myText('bb'),
                ),
              ),
            ),
            Positioned(
              child: DragTarget(
                builder: (BuildContext context, List<String> candidateData,
                    List<dynamic> rejectedData) {
                  print('DragTarget builder');
                  Color c;
                  String s;
                  if (candidateData.isNotEmpty && candidateData.first == 'aa') {
                    c = Colors.amber;
                    s = '接收到aa';
                  } else if (rejectedData.isNotEmpty) {
                    c = Colors.grey;
                    s = '未接收到aa，接收为${rejectedData.first}';
                  } else {
                    s = '只接收aa';
                    c = Colors.cyan;
                  }

                  return Container(
                    color: c,
                    width: 100,
                    height: 100,
                    child: _myText(s),
                  );
                },
                onWillAccept: (s) {
                  print('onWillAccept $s');
                  if (s == 'aa') {
                    return true;
                  } else {
                    return false;
                  }
                },
                onAccept: (s) {
                  print('onAccept $s');
                  ToastUtil.showToastBottom('成功接收到指定数据$s');
                },
                onAcceptWithDetails: (DragTargetDetails<String> details) {
                  print('onAcceptWithDetails ${details.data}');
                  print('onAcceptWithDetails ${details.offset}');
                },
                onLeave: (s) {
                  print('onLeave $s');
                },
                onMove: (DragTargetDetails<dynamic> details) {
                  print('onMove ${details.data}');
                  print('onMove ${details.offset}');
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}