import '../../../Include.dart';

//https://api.flutter.dev/flutter/widgets/LongPressDraggable-class.html
class LongPressDraggablePage extends BasePage {
  static String dartPath = "lib/pages/component/widget_Interactive/LongPressDraggablePage.dart";

  @override
  _LongPressDraggablePageState createState() => _LongPressDraggablePageState();

  @override
  String get pageName => 'LongPressDraggable长按时可拖动';
}

class _LongPressDraggablePageState extends BasePageState<LongPressDraggablePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        child: Center(
          child: LongPressDraggable(
            child: Container(
              width: 100,
              height: 100,
              color: Colors.blue,
            ),
            feedback: Container(
              color: Colors.red,
              width: 100,
              height: 100,
            ),
            childWhenDragging: Container(
              color: Colors.amber,
              width: 100,
              height: 100,
            ),
            feedbackOffset: Offset(0, 10),
            dragAnchor: DragAnchor.child,
            onDragStarted: () {
              print("onDragStarted在拖动开始时");
            },
            onDragEnd: (DraggableDetails details) {
              print("onDragEnd在拖拽端 : $details");
            },
            onDraggableCanceled: (Velocity velocity, Offset offset) {
              print('onDraggableCanceled可拖动取消 velocity速度:$velocity,offset抵消:$offset');
            },
            onDragCompleted: () {
              print('onDragCompleted拖动完成');
            },
          ),
        ),
      ),
    );
  }
}
