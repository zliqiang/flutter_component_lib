import '../../../Include.dart';

//https://api.flutter.dev/flutter/widgets/Dismissible-class.html
class DismissiblePage extends BasePage {

  static String dartPath = "lib/pages/component/widget_Interactive/DismissiblePage.dart";

  @override
  _DismissiblePageState createState() => _DismissiblePageState();

  @override
  String get pageName => 'Dismissible拖动时隐藏';
}

class _DismissiblePageState extends BasePageState<DismissiblePage> {

  List<int> items = List<int>.generate(100, (int index) => index);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        child: ListView.builder(
          itemCount: items.length,
          padding: const EdgeInsets.symmetric(vertical: 16),
          itemBuilder: (BuildContext context, int index) {
            return Dismissible(
              child: ListTile(
                title: Text(
                  'Item ${items[index]}',
                ),
              ),
              background: Container(
                color: Colors.green,
              ),
              key: ValueKey<int>(items[index]),
              onDismissed: (DismissDirection direction) {
                setState(() {
                  items.removeAt(index);
                });
              },
            );
          },
        ),
      ),
    );
  }

}