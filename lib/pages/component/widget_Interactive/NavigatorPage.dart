import 'package:flutter_web_2021/Include.dart';
import 'package:flutter_web_2021/res/resources.dart';
import 'package:flutter_web_2021/router/NavigatorRoutes.dart';

class NavigatorPage extends BasePage {
  static String dartPath = "lib/pages/component/widget_Interactive/NavigatorPage.dart";
  @override
  _NavigatorPageState createState() => _NavigatorPageState();

  @override
  String get pageName => 'Navigator【导航器】';
}

class _NavigatorPageState extends BasePageState<NavigatorPage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: MyAppBar.appBar(context, widget.pageName),
        body: Column(
          children: [
            Expanded(
              child: Navigator(
                initialRoute: '/',

                onGenerateRoute: onGenerateRouteNavigator,
              ),
            ),
            InkWell(
              onTap: () {},
              child: Container(
                alignment: Alignment.center,
                height: Dimens.dp52,
                child: Text('页面局部跳转'),
              ),
            )
          ],
        ));
  }
}

class PageD extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 200,
      width: 250,
      color: Colors.grey.withOpacity(.5),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              IconButton(
                icon: Icon(Icons.arrow_back_ios),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              Text('新页面'),
              SizedBox(
                width: 30,
              ),
              Text('举报'),
            ],
          ),
        ],
      ),
    );
  }
}

class PageC extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 200,
      width: 250,
      color: Colors.grey.withOpacity(.5),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              IconButton(
                icon: Icon(Icons.arrow_back_ios),
                onPressed: () {
                  // AppNavigator.push(context, PageD());
                  AppNavigator.pushNameNavigator(context, "/d");
                  print("saf");
                },
              ),
              Text('返回'),
              SizedBox(
                width: 30,
              ),
              Text('ADFJJADFAF'),
            ],
          ),
        ],
      ),
    );
  }
}
