
import '../../../Include.dart';

class AbsorbPointerPage extends BasePage {
  static String dartPath = "lib/pages/component/widget_Interactive/AbsorbPointerPage.dart";
  @override
  _AbsorbPointerPageState createState() => _AbsorbPointerPageState();

  @override
  String get pageName => 'AbsorbPointerPage【吸收指针】';
}

class _AbsorbPointerPageState extends BasePageState<AbsorbPointerPage> {
  //当 absorption 为 true 时，这个小部件通过终止命中测试来阻止它的子树接收指针事件。在布局过程中，它仍然占用空间，并像往常一样绘制它的子元素。它只是防止其子级成为定位事件的目标，因为它从 RenderBox.hitTest 返回 true。
  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: AlignmentDirectional.center,
      children: <Widget>[
        SizedBox(
          width: 200.0,
          height: 100.0,
          child: ElevatedButton(
            onPressed: () {},
            child: null,
          ),
        ),
        SizedBox(
          width: 100.0,
          height: 200.0,
          child: AbsorbPointer(
            absorbing: true,
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                primary: Colors.blue.shade200,
              ),
              onPressed: () {},
              child: null,
            ),
          ),
        ),
      ],
    );
  }

}