import 'package:flutter_web_2021/utils/toast/OkToastUtil.dart';

import '../../../Include.dart';
//https://api.flutter.dev/flutter/widgets/GestureDetector-class.html
class GestureDetectorPage extends BasePage {

  static String dartPath = "lib/pages/component/widget_Interactive/GestureDetectorPage.dart";

  @override
  _GestureDetectorPageState createState() => _GestureDetectorPageState();

  @override
  String get pageName => 'GestureDetector检查手势';
}

class _GestureDetectorPageState extends BasePageState<GestureDetectorPage> {
 bool _lights =false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        alignment: FractionalOffset.center,
        color: Colors.white,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Icon(
                Icons.lightbulb_outline,
                color: _lights ? Colors.yellow.shade600 : Colors.black,
                size: 60,
              ),
            ),
            GestureDetector(
              onTap: () {
                setState(() {
                  print("------onTap");
                  _lights = true;
                  ToastUtil.showToastBottom("点击");
                });
              },
              onDoubleTap: () {
                print("------onDoubleTap");
                setState(() {
                  _lights = false;
                  ToastUtil.showToastBottom("双击");
                });
              },
              onLongPress: () {
                print("-----onLongPress");
                ToastUtil.showToastBottom("长按");
              },
              onVerticalDragStart: (details){
                print("在垂直方向开始位置:"+details.globalPosition.toString());
                ToastUtil.showToastBottom("在垂直方向开始位置:"+details.globalPosition.toString());
              }, onVerticalDragEnd: (details){
              print("在垂直方向结束位置:"+details.primaryVelocity.toString());
              ToastUtil.showToastBottom("在垂直方向结束位置:"+details.primaryVelocity.toString());
            },
              child: Container(
                color: Colors.yellow.shade600,
                padding: const EdgeInsets.all(8),
                child: const Text('TURN LIGHTS ON'),
              ),
            ),
          ],
        ),
      ),
    );
  }

}