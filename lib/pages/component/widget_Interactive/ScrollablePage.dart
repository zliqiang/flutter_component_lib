import '../../../Include.dart';

//https://api.flutter.dev/flutter/widgets/Scrollable-class.html
class ScrollablePage extends BasePage {

  static String dartPath = "lib/pages/component/widget_Interactive/ScrollablePage.dart";

  @override
  _ScrollablePageState createState() => _ScrollablePageState();

  @override
  String get pageName => 'Scrollable滚动组件';
}

class _ScrollablePageState extends BasePageState<ScrollablePage> {

  final _events = [];

  @override
  void initState() {
    super.initState();
    _refresh();
  }


  _refresh(){
    _events.add(GitEvent("1","张三",'https://flutter.github.io/assets-for-api-docs/assets/widgets/owl-2.jpg',"程序员"));
    _events.add(GitEvent("2","张三",'https://flutter.github.io/assets-for-api-docs/assets/widgets/owl-2.jpg',"程序员"));
    _events.add(GitEvent("3","张三",'https://flutter.github.io/assets-for-api-docs/assets/widgets/owl-2.jpg',"程序员"));
    _events.add(GitEvent("4","张三",'https://flutter.github.io/assets-for-api-docs/assets/widgets/owl-2.jpg',"程序员"));
    _events.add(GitEvent("5","张三",'https://flutter.github.io/assets-for-api-docs/assets/widgets/owl-2.jpg',"程序员"));
    _events.add(GitEvent("6","张三",'https://flutter.github.io/assets-for-api-docs/assets/widgets/owl-2.jpg',"程序员"));
    _events.add(GitEvent("7","张三",'https://flutter.github.io/assets-for-api-docs/assets/widgets/owl-2.jpg',"程序员"));
    _events.add(GitEvent("8","张三",'https://flutter.github.io/assets-for-api-docs/assets/widgets/owl-2.jpg',"程序员"));
    _events.add(GitEvent("9","张三",'https://flutter.github.io/assets-for-api-docs/assets/widgets/owl-2.jpg',"程序员"));
    _events.add(GitEvent("10","张三",'https://flutter.github.io/assets-for-api-docs/assets/widgets/owl-2.jpg',"程序员"));
    _events.add(GitEvent("11","张三",'https://flutter.github.io/assets-for-api-docs/assets/widgets/owl-2.jpg',"程序员"));
    _events.add(GitEvent("12","张三",'https://flutter.github.io/assets-for-api-docs/assets/widgets/owl-2.jpg',"程序员"));
    _events.add(GitEvent("13","张三",'https://flutter.github.io/assets-for-api-docs/assets/widgets/owl-2.jpg',"程序员"));
    _events.add(GitEvent("14","张三",'https://flutter.github.io/assets-for-api-docs/assets/widgets/owl-2.jpg',"程序员"));
    _events.add(GitEvent("15","张三",'https://flutter.github.io/assets-for-api-docs/assets/widgets/owl-2.jpg',"程序员"));
    _events.add(GitEvent("16","张三",'https://flutter.github.io/assets-for-api-docs/assets/widgets/owl-2.jpg',"程序员"));
    _events.add(GitEvent("17","张三",'https://flutter.github.io/assets-for-api-docs/assets/widgets/owl-2.jpg',"程序员"));
    setState(() {

    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar.appBar(context, widget.pageName),
      body: Container(
        child: Scrollbar(
          child: RefreshIndicator(
            onRefresh: () async {
              await _refresh();
            },
            child: ListView(
              children: _events.map<Widget>((event) {
                return Dismissible(
                  confirmDismiss: (_) async {
                    return showDialog(
                      context: context,
                      builder: (_) {
                        return AlertDialog(
                          title: Text('Are you sure?'),
                          content: Text('Do you want to delete this item?'),
                          actions: [
                            FlatButton(
                                onPressed: () {
                                  Navigator.of(context).pop(false);
                                },
                                child: Text('Cancel')),
                            FlatButton(
                                onPressed: () {
                                  Navigator.of(context).pop(true);
                                },
                                child: Text('Delete'))
                          ],
                        );
                      },
                    );
                  },
                  onDismissed: (_) {
                    setState(() {
                      _events.removeWhere((e) => e.id == event.id);
                    });
                  },
                  key: ValueKey(event.id),
                  child: ListTile(
                    leading: Image.network(
                        'http://www.tobutomi.top/NewsCat/images/avatar/avatar.jpg'),
                    title: Text('${event.userName}'),
                    subtitle: Text('${event.repoName}'),
                  ),
                );
              }).toList(),
            ),
          ),
        ),
      ),
    );
  }

}


class GitEvent {
  String id;
  String userName;
  String avatarUrl;
  String repoName;


  GitEvent(this.id, this.userName, this.avatarUrl, this.repoName);

  @override
  String toString() {
    // TODO: implement toString
    return 'GitEvent{id:$id,userName:$userName,avatarUrl:$avatarUrl,repoName:$repoName}';
  }
}
