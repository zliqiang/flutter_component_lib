import '../../../Include.dart';

class CustomScrollViewPage extends BasePage {
  static String dartPath = "lib/pages/component/sliver/CustomScrollViewPage.dart";
  @override
  _CustomScrollViewPageState createState() => _CustomScrollViewPageState();

  @override
  String get pageName => 'CustomScrollView【滚动组件】';
}

class _CustomScrollViewPageState extends BasePageState<CustomScrollViewPage> {

  ScrollController  _scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
    //监听滚动位置
    _scrollController.addListener((){
      print('${_scrollController.position}');
    });
    // //滚动到指定位置
    // _scrollController.animateTo(20.0);

  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.pageName),
      ),
      body:CustomScrollView(
        controller: _scrollController,
        slivers: <Widget>[
          SliverAppBar(
            pinned: true,
            expandedHeight: 230.0,
            flexibleSpace: FlexibleSpaceBar(
              title: Text('复仇者联盟'),
              background: Image.network(
                'http://img.haote.com/upload/20180918/2018091815372344164.jpg',
                fit: BoxFit.fitHeight,
              ),
            ),
          ),
          SliverGrid.count(crossAxisCount: 4,children: List.generate(10, (index){
            return Container(
              color: Colors.primaries[index%Colors.primaries.length],
              alignment: Alignment.center,
              child: Text('$index',style: TextStyle(color: Colors.white,fontSize: 20),),
            );
          }).toList(),),
          SliverList(
            delegate: SliverChildBuilderDelegate((content, index) {
              return Container(
                height: 85,
                alignment: Alignment.center,
                color: Colors.primaries[index % Colors.primaries.length],
                child: Text('$index',style: TextStyle(color: Colors.white,fontSize: 20),),
              );
            }, childCount: 25),
          )
        ],
      ),
    );
  }

}