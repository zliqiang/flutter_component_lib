import 'package:flutter/cupertino.dart';

import '../../../Include.dart';

class CupertinoNavigationBarPage extends BasePage {
  static String dartPath = "lib/pages/component/sliver/CupertinoNavigationBarPage.dart";
  @override
  _CupertinoNavigationBarPageState createState() =>
      _CupertinoNavigationBarPageState();

  @override
  String get pageName => 'CupertinoNavigationBarPage【导航条】';
}

class _CupertinoNavigationBarPageState
    extends BasePageState<CupertinoNavigationBarPage> {
  @override
  Widget build(BuildContext context) {
    return CupertinoNavigationBar(
      leading:InkWell(
        onTap: (){
          AppNavigator.pop(context);
        },
        child:Icon(Icons.arrow_back) ,
      ) ,
      middle: Text(widget.pageName),
      backgroundColor: Colors.transparent,
      trailing: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Icon(Icons.arrow_back),
        ],
      ),
      padding: EdgeInsetsDirectional.only(start: 10),
    );
  }
}
