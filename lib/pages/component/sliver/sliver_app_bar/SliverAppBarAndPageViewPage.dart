import 'package:flutter_web_2021/pages/basic/rx_dart_bloc/one_page.dart';
import 'package:flutter_web_2021/pages/basic/rx_dart_bloc/two_page.dart';

import '../../../../Include.dart';
import 'widgets/StickyTabBarDelegate.dart';

class SliverAppBarAndPageViewPage extends BasePage {
  static String dartPath = "lib/pages/component/sliver/sliver_app_bar/SliverAppBarAndPageViewPage.dart";
  @override
  _SliverAppBarAndPageViewPageState createState() =>
      _SliverAppBarAndPageViewPageState();

  @override
  String get pageName => 'SliverAppBarAndTabBar【TabBar搭配】';
}

class _SliverAppBarAndPageViewPageState
    extends BasePageState<SliverAppBarAndPageViewPage> with SingleTickerProviderStateMixin{


  TabController _tabController;

  ScrollController _scrollController = ScrollController();

  List<Widget> pageList = [OnePage(), TwoPage()];


  @override
  void initState() {
    super.initState();

    //监听滚动位置
    _scrollController.addListener(() {
      print('${_scrollController.position}');
    });
    //滚动到指定位置
    //_scrollController.animateTo(20.0);

    print('初始化 数据...');
    _tabController = new TabController(
        vsync: this,//固定写法
        length: 2   //指定tab长度
    );

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverAppBar(
              expandedHeight: 400.0,
              pinned: true,
              flexibleSpace: Padding(
                padding: EdgeInsets.symmetric(vertical: 8),
                child: PageView.builder(
                  itemCount: 10000,
                  itemBuilder: (context, index) {
                    return pageList[index % (pageList.length)];
                  },
                ),
              ),
            ),
            SliverPersistentHeader(
              pinned: true,
              delegate: StickyTabBarDelegate(
                child: TabBar(
                  labelColor: Colors.black,
                  controller: this._tabController,
                  tabs: <Widget>[
                    Tab(text: '资讯'),
                    Tab(text: '技术'),
                  ],
                ),
              ),
            ),
          ];
        },
        body: TabBarView(
          controller: this._tabController,
          children: <Widget>[
            RefreshIndicator(
              onRefresh: () {
                print(('onRefresh'));
              },
              child: ListView(
                children: [
                  Text("ItemView"),
                  Text("ItemView"),
                  Text("ItemView"),
                  Text("ItemView"),
                  Text("ItemView"),
                ],
              ),
            ),
            ListView(
              children: [
                Text("ItemView1"),
                Text("ItemView1"),
                Text("ItemView1"),
                Text("ItemView1"),
                Text("ItemView1"),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
