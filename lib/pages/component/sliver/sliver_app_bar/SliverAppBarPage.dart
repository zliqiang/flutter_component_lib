import '../../../../Include.dart';

class SliverAppBarPage extends BasePage {
  static String dartPath = "lib/pages/component/sliver/sliver_app_bar/SliverAppBarPage.dart";
  @override
  _SliverAppBarPageState createState() => _SliverAppBarPageState();

  @override
  String get pageName => 'SliverAppBar【显示/隐藏标题栏】';
}

class _SliverAppBarPageState extends BasePageState<SliverAppBarPage> {


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          Log().info(innerBoxIsScrolled.toString());
          return <Widget>[SliverAppBar(
            title: Text(widget.pageName),
          )];
        },
        body: ListView.builder(itemBuilder: (BuildContext context,int index){
          return Container(
            height: 80,
            color: Colors.primaries[index % Colors.primaries.length],
            alignment: Alignment.center,
            child: Text(
              '$index',
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
          );
        },itemCount: 20,),
      ),
    );
  }

}