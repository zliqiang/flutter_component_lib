import '../../../../Include.dart';
import 'widgets/StickyTabBarDelegate.dart';

class SliverAppBarAndTabBarPage extends BasePage {
  static String dartPath = "lib/pages/component/sliver/sliver_app_bar/SliverAppBarAndTabBarPage.dart";
  @override
  _SliverAppBarAndTabBarPageState createState() =>
      _SliverAppBarAndTabBarPageState();

  @override
  String get pageName => 'SliverAppBarAndTabBar【TabBar搭配】';
}

class _SliverAppBarAndTabBarPageState
    extends BasePageState<SliverAppBarAndTabBarPage> with SingleTickerProviderStateMixin{


  TabController _tabController;


  ScrollController _scrollController = ScrollController();



  @override
  void initState() {
    super.initState();

    //监听滚动位置
    _scrollController.addListener(() {
      print('${_scrollController.position}');
    });
    //滚动到指定位置
    //_scrollController.animateTo(20.0);

    print('初始化 数据...');
    _tabController = new TabController(
        vsync: this,//固定写法
        length: 2   //指定tab长度
    );

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
          SliverAppBar(
          pinned: true,
          expandedHeight: 200.0,
          flexibleSpace: FlexibleSpaceBar(
          title: Text(widget.pageName),
          background: Image.network(
          'http://img.haote.com/upload/20180918/2018091815372344164.jpg',
          fit: BoxFit.fitHeight,
          ),
          ),
          ),
            SliverPersistentHeader(
              pinned: true,
              delegate: StickyTabBarDelegate(
                child: TabBar(
                  labelColor: Colors.black,
                  controller: this._tabController,
                  tabs: <Widget>[
                    Tab(text: '资讯'),
                    Tab(text: '技术'),
                  ],
                ),
              ),
            ),
          ];
        },
        body: TabBarView(
          controller: this._tabController,
          children: <Widget>[
            RefreshIndicator(
              onRefresh: () {
                print(('onRefresh'));
              },
              child: ListView(
                children: [
                  Text("ItemView"),
                  Text("ItemView"),
                  Text("ItemView"),
                  Text("ItemView"),
                  Text("ItemView"),
                ],
              ),
            ),
            ListView(
              children: [
                Text("ItemView1"),
                Text("ItemView1"),
                Text("ItemView1"),
                Text("ItemView1"),
                Text("ItemView1"),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
