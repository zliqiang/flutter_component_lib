import '../../../../Include.dart';

class SliverChildListDelegatePage extends BasePage {
  static String dartPath = "lib/pages/component/sliver/sliver_list/SliverChildListDelegatePage.dart";
  @override
  _SliverChildListDelegatePageState createState() =>
      _SliverChildListDelegatePageState();

  @override
  String get pageName => 'SliverChildBuilderDelegatePage【list的item】';
}

class _SliverChildListDelegatePageState
    extends BasePageState<SliverChildListDelegatePage> {
  @override
  Widget build(BuildContext context) {
    //因为本路由没有使用Scaffold，为了让子级Widget(如Text)使用
    //Material Design 默认的样式风格,我们使用Material作为本路由的根。
    return Material(
      child:  CustomScrollView(
          slivers: <Widget>[
            //AppBar，包含一个导航栏
            SliverAppBar(
              pinned: true,
              expandedHeight: 250.0,
              flexibleSpace: FlexibleSpaceBar(
                title: const Text('Demo'),
                background: Image.asset(
                  "./memory/110/",
                  fit: BoxFit.cover,
                ),
              ),
            ),

            //List
            new SliverFixedExtentList(
              itemExtent: 50.0,
              delegate: new SliverChildBuilderDelegate(
                  (BuildContext context, int index) {
                //创建列表项
                return new Container(
                  alignment: Alignment.center,
                  color: Colors.lightBlue[100 * (index % 9)],
                  child: new Text('list item $index'),
                );
              }, childCount: 50 //50个列表项
                  ),
            ),
          ],
        ),
    );
  }
}
