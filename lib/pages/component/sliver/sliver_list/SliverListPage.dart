import '../../../../Include.dart';

class SliverListPage extends BasePage {
  static String dartPath = "lib/pages/component/sliver/sliver_list/SliverListPage.dart";
  @override
  _SliverListPageState createState() => _SliverListPageState();

  @override
  String get pageName => 'SliverListPage';
}

class _SliverListPageState extends BasePageState<SliverListPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.pageName),
      ),
      body: CustomScrollView(slivers: <Widget>[
        SliverList(
          delegate: SliverChildBuilderDelegate((content, index) {
            return Container(
              height: 65,
              color: Colors.primaries[index % Colors.primaries.length],
            );
          }, childCount: 122),
        ),
      ]),
    );
  }
}
