import '../../../../Include.dart';

class SliverFixedExtentListPage extends BasePage {
  static String dartPath = "lib/pages/component/sliver/sliver_list/SliverFixedExtentListPage.dart";
  @override
  _SliverFixedExtentListPageState createState() =>
      _SliverFixedExtentListPageState();

  @override
  String get pageName => 'SliverFixedExtentListPage【list的item】';
}

class _SliverFixedExtentListPageState
    extends BasePageState<SliverFixedExtentListPage> {
  @override
  Widget build(BuildContext context) {
    //因为本路由没有使用Scaffold，为了让子级Widget(如Text)使用
    //Material Design 默认的样式风格,我们使用Material作为本路由的根。
    return Material(
      child: Scaffold(
        appBar:
        MyAppBar.tabAppBar(context, widget.pageName, showIcoLeft: false),
    body: CustomScrollView(
          slivers: <Widget>[
            SliverFixedExtentList(
              itemExtent: 100,
              delegate: SliverChildBuilderDelegate((content, index) {
                return Container(
                  color: Colors.primaries[index % Colors.primaries.length],
                );
              }, childCount: 50),
            ),
          ],
        ),
        ),
    );
  }
}
