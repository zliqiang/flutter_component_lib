import '../../../../Include.dart';

class SliverChildBuilderDelegatePage extends BasePage {
  static String dartPath = "lib/pages/component/sliver/sliver_list/SliverChildBuilderDelegatePage.dart";
  @override
  _SliverChildBuilderDelegatePageState createState() => _SliverChildBuilderDelegatePageState();

  @override
  String get pageName => 'SliverChildBuilderDelegatePage【Grid的item】';
}

class _SliverChildBuilderDelegatePageState extends BasePageState<SliverChildBuilderDelegatePage> {
  @override
  Widget build(BuildContext context) {
    //因为本路由没有使用Scaffold，为了让子级Widget(如Text)使用
    //Material Design 默认的样式风格,我们使用Material作为本路由的根。
    return Material(
      child:Scaffold(
        appBar:
        MyAppBar.tabAppBar(context, widget.pageName, showIcoLeft: false),
    body: CustomScrollView(
        slivers: <Widget>[
          SliverPadding(
            padding: const EdgeInsets.all(8.0),
            sliver: new SliverGrid(
              //Grid
              gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2, //Grid按两列显示
                mainAxisSpacing: 10.0,
                crossAxisSpacing: 10.0,
                childAspectRatio: 4.0,
              ),
              delegate: new SliverChildBuilderDelegate(
                    (BuildContext context, int index) {
                  //创建子widget
                  return new Container(
                    alignment: Alignment.center,
                    color: Colors.cyan[100 * (index % 9)],
                    child: new Text('grid item $index'),
                  );
                },
                childCount: 20,
              ),
            ),
          ),
        ],
      ),
      ),
    );
  }

}