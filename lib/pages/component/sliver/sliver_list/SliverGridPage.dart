import '../../../../Include.dart';

class SliverGridPage extends BasePage {
  static String dartPath = "lib/pages/component/sliver/sliver_list/SliverGridPage.dart";
  @override
  _SliverListPageState createState() => _SliverListPageState();

  @override
  String get pageName => 'SliverGridPage';
}

class _SliverListPageState extends BasePageState<SliverGridPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.pageName),
      ),
      body: CustomScrollView(slivers: <Widget>[
        SliverGrid(
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 3, crossAxisSpacing: 5, mainAxisSpacing: 3),
          delegate: SliverChildBuilderDelegate((BuildContext context, int index) {
            return Container(
              color: Colors.primaries[index % Colors.primaries.length],
            );
          }, childCount: 20),
        )
      ]),
    );
  }
}
