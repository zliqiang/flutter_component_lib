import '../../../Include.dart';

class SliverToBoxAdapterPage extends BasePage {
  static String dartPath = "lib/pages/component/sliver/SliverToBoxAdapterPage.dart";
  @override
  _SliverToBoxAdapterPageState createState() => _SliverToBoxAdapterPageState();

  @override
  String get pageName => 'SliverToBoxAdapterPage包含普通的组件】';
}

class _SliverToBoxAdapterPageState extends BasePageState<SliverToBoxAdapterPage> {



  @override
  Widget build(BuildContext context) {

    return Scaffold(
        appBar: AppBar(
          title: Text(widget.pageName),
        ),
        body: CustomScrollView(
          slivers: <Widget>[
            SliverToBoxAdapter(
              child: Container(
                height: 100,
                color: Colors.black,
              ),
            ),
            SliverList(
              delegate: SliverChildBuilderDelegate((content, index) {
                return Container(
                  height: 65,
                  color: Colors.primaries[index % Colors.primaries.length],
                );
              }, childCount: 50),
            )
          ],
        ),
    );
  }

}