/**
 *  Create by fazi
 *  Date: 2019-06-10
 */

import "package:flutter/material.dart";
import 'package:redux/redux.dart';

import 'case_page/CasePageAll.dart';
import 'locale/locale_redux.dart';
import 'theme/theme_redux.dart';
import 'user/user.dart';
import 'user/user_reducer.dart';

/// 定义一个state
class ReduxState {
  //1
  Locale locale;
  //2
  UserRedux user;
  //3
  ThemeData themeData;

  //4
  Map<String,String> allCasePage =Map();

  ReduxState({this.locale,this.user,this.themeData,this.allCasePage});
}

/// 定义action，将action放到theme_redux类里去定义

/// 定义reducer
ReduxState getReduce(ReduxState state, action) {
  return ReduxState(
    //1
    locale: LocaleReducer(state.locale, action),
      //2
      user: UserReducer(state.user, action),
      //3
      themeData: ThemeDataReducer(state.themeData, action),
      //4
      allCasePage: CasePageAll(state.allCasePage, action)
  );
}


//2
/// 定义了一个中间件
final List<Middleware<ReduxState>> middleware = [
  UserInfoMiddleware1(),
  UserInfoMiddleware2(),
];