/**
 *  Create by fazi
 *  Date: 2019-06-29
 */

import "package:flutter/material.dart";
import 'package:redux/redux.dart';

final CasePageAll = combineReducers<Map<String,String>  >([
  TypedReducer<Map<String,String> , CasePageAction>(_refreshTheme)
]);

Map<String,String> _refreshTheme(Map<String,String>   themeData, CasePageAction action) {
  themeData = action.casePage;
  return themeData;
}

class CasePageAction {
  Map<String,String>  casePage;
  CasePageAction(this.casePage);
}
