import 'package:flutter/material.dart';
import 'package:pigeon_base/pigeon_base.dart';

import 'cupertino_date_picker.0.dart';
import 'cupertino_timer_picker.0.dart';

class DatePickerTabPage extends BasePage {
  @override
  _DatePickerTabPageState createState() => _DatePickerTabPageState();

  @override
  String get pageName => 'DatePickerTabPage';
}

class _DatePickerTabPageState extends BasePageState<DatePickerTabPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: DefaultTabController(
        initialIndex: 1,
        length: 2,
        child: Column(
          children: const [
            TabBar(
              tabs: <Widget>[
                Tab(
                  text: 'date_picker',
                  // icon: Icon(Icons.cloud_outlined),
                ),
                Tab(
                  text: 'timer_picker',
                  // icon: Icon(Icons.cloud_outlined),
                ),
              ],
            ),
            Expanded(child: TabBarView(
              children: <Widget>[
                Center(
                  child: DatePickerApp(),
                ),
                Center(
                  child: TimerPickerApp(),
                ),
              ],
            ),),
          ],
        ),
      ),
    );

  }
}
