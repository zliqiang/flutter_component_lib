import 'package:flutter/material.dart';
import 'package:pig_case_web/src/web/tree_view/common.dart';
import 'package:pigeon_base/pigeon_base.dart';
import 'package:pigeon_utils/pigeon_utils.dart';

class InitializePage extends BasePage {
  @override
  _InitializePageState createState() => _InitializePageState();

  @override
  String get pageName => 'InitializePage';
}

class _InitializePageState extends BasePageState<InitializePage> {

  final PigEventBus _eventBus = PigEventBus(); //全局消息

  Widget body = Container();
  @override
  void initState() {
    super.initState();

    ///一定要写在init方法里
    _eventBus.on(PigFrameModeEvent.tree.toString(), (arg) {
      switch(arg["type"]){
        case "onClick":
          Log.info(PigEventName.homeFind.toString()+" = ${arg["data"]}");
          try{
            body = AppUtil.instance.routesNavigatorTree!['/${arg["data"]['id']}']('/${arg["data"]['id']}',arguments:{});
            // TreeNavigator.pushNameReplacement(context, '/${arg["data"]['id']}');
          }catch(e){
            body =Container();
            Log.fm(e);
          }
          setState(() {

          });
          break;
      }
    });
    ///一定要写在init方法里
    _eventBus.on(PigFrameModeEvent.mianRightView.toString(), (arg) {
      switch(arg["type"]){
        case "onClick":
          Log.info(PigEventName.homeFind.toString()+" = ${arg["data"]}");
          try{
            if(arg["data"]['id']==''){
              body =Container();
            }else {
              body = AppUtil.instance.routesNavigatorTree!['/${arg["data"]['id']}'](
                  '/${arg["data"]['id']}', arguments: {});
            }
            // TreeNavigator.pushNameReplacement(context, '/${arg["data"]['id']}');
          }catch(e){
            body =Container();
            Log.fm(e);
          }
          setState(() {

          });
          break;
      }
    });
  }
  @override
  void dispose() {
    ///一定要关闭，防止泄露
    _eventBus.off(PigEventName.homeFind.toString());
    _eventBus.off(PigFrameModeEvent.mianRightView.toString());
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      child: body,
    );
  }
}
